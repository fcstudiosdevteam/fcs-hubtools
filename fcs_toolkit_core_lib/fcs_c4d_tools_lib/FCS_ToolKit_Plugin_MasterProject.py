"""
F.C.S ToolKit Plugin for Maxon Cinema 4D
The FCS Tools Shelf Menu, The FCS ToolBox Plugin, Main Tools, Scripts Tools.
Author / Lead 3D Plugin Developer : Asthon Rolle AKA AP Asthon
Cinema 4D Python plugin
2015 Python Plugin For C4D
Plugin File Made: Aug 14, 2015.
Studio: Field Creators Studios
Last Upated : Feb 30, 2020
Compatible:
- Win / Mac
- R16, R17, R18, R19, R20, R21
Copyright: Field Creators Studios
"""
ActiveDebugMode = True
StudioName = "F.C.S - "
FCSWEBLINKS= { 
                "DIS":"https://discord.gg/CQQn33W",  
                "GUM":"https://gumroad.com/fcstudios",
                "TRE":"https://trello.com/b/ZF6Rakhv/fcs-toolkit-c4d-plugin-add-on",
                "FB":"https://www.facebook.com/FCSTools/",
                "YT":"https://www.youtube.com/channel/UC_5YTyCTAFyNut_QR0YZqsg"
             }
"""
Imports
_________________________________________________
"""
import os
import sys
import subprocess
import webbrowser
import collections
import math
import random
import urllib
import glob
import shutil
import struct
import time
import array
import datetime
import weakref
# Cinema 4D Imports
import c4d
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d import Matrix, Vector
from c4d.gui import GeDialog as WindowDialog        # eg:( class My_Dialog(c4d.gui.GeDialog): ) to ( class My_Dialog(WindowDialog): ) Just keeping code line short.
from c4d.plugins import CommandData, TagData, ObjectData
from c4d.utils import GeRayCollider
from random import randint
from os import urandom
# Regex Import
import re
# XML Imports is for Saving and Loading Project Data for Plugin UI / Xml good but long functions to make.
from xml.dom import minidom
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree
# Json Import to store or load descriptions or any data / .json is easy and alot flexable and short coding.
import json
# Python Plugin System Load Modules Dynamically With importlib.
import importlib



# Plugin Main Res Folder
plugin_res_folder = os.path.join(os.path.dirname(__file__), 'res')  # To find plugin res folder for Icons, UI Icons, Projects and Files etc.. .
PLUGIN_FCS_PACKAGE_CORE_LIB_PATH = os.path.join(os.path.dirname(__file__), 'fcs_toolkit_core_lib')
# Calling and Registering the fcs_module_lib folder to import other modules .py / .pyc files. ( NOTE!!! C4d will make or will complie .py file to .pyc file. )
# Get all modules lib folders in the directory.
fcs_modules_dirs = list()
for root, dirs, files in os.walk(PLUGIN_FCS_PACKAGE_CORE_LIB_PATH, topdown=False):
    for name in dirs:
        fcs_modules_dirs.append(os.path.join(root, name))
for each_module_py_file in fcs_modules_dirs:
    if each_module_py_file not in sys.path:
        sys.path.insert(0, each_module_py_file)
    sys.path.append(each_module_py_file)
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib # Feel free to look at the global common module library its open source.
import fcs_c4d_register_and_enums_ids as fcs_register_ids
import fcs_c4d_quick_scripts_lib as fcs_Qscript
import fcs_c4d_objects_and_tags_lib as fcsNodeData
jsonEdit = g_lib.jsonEdit
fcsDIR = g_lib.PLUGIN_DIR(PLUGIN_FCS_PACKAGE_CORE_LIB_PATH)
fcsID = g_lib.fcsID
FsLib = g_lib.fs_lib
fs_lib = g_lib.fs_lib
QS = fcs_Qscript
fcsLog = g_lib.fcsLog
fcsEngine = g_lib.fcsEngine
fcsNDT = fcsNodeData
# FCS Tools Window Dialogs GUI / Popup Menus GUI Imports Modules .py Files.
import fcs_c4d_logger_win_gui
import fcs_c4d_tools_preferences_win_gui
import fcs_select_n_go_tools_gui as sngTool
import fcs_random_replacer_tool_gui
import fcs_auto_renamer_gui
import fcs_nurbo_tool_gui
import fcs_project_storage_c4d_gui
import fcs_uv_checker_materials_gui
import fcs_collide_points_gui
import fcs_satellite_to_gpoly_gui
import fcs_objects_2_points_gui
import fcs_obj_set_pos_gui
import fcs_target2constraint_gui
import fcs_layouts_popupmenu
import fcs_quick_scripts_popupmenu
import fcs_flightsim_tools_popupmenu
import fcs_attach_to_layer_popupmenu
# ---------------------------------------------------------------
#   Get Plugin Global Strings and Res Dialog GUI File
# ---------------------------------------------------------------
class Get_Global_Plugin_Strings(object):
    """
    Get Plugin Global Strings Text from the c4d_strings.str
    Calling on the IDs from the c4d_symbols.h file, to get text string from c4d_strings.str file
    that as matches IDs.
    eg:
        Inside the c4d_strings.str file.
        dir: res / strings_us / c4d_strings.str

            IDS_TOOL_TITLE   "My Tool";
            IDS_TOOL_TEXT    "My Hello";
            etc...

        ------------------

        Inside the c4d_symbols.h file.
        dir: res / c4d_symbols.h

        enum
        {

            IDS_TOOL_TITLE   1001,
            IDS_TOOL_TEXT    1002,
            etc...

        _DUMMY_ELEMENT_
        };
    """
    def ID(self, id):
        """ Get String ID  """
        str_text = c4d.plugins.GeLoadString(id)
        return str_text
GGPS_IDS = Get_Global_Plugin_Strings()   
class Get_Global_Plugin_ResDialogGUI(object):
    def ResGUI(self, ui_ins, id):
        return ui_ins.LoadDialogResource(id)
resDLG = Get_Global_Plugin_ResDialogGUI()
def open_web_link(weblink):
    return webbrowser.open(weblink, new=2, autoraise=True)
# ---------------------------------------------------------------
rId = fcs_register_ids.RegisterIDs(GGPS_IDS)
# ------------------------------------------------------------- #
class FCS_About_Dialog(WindowDialog):                   # About Dialog.
 ##### \\ UI IDs \\ #####

    fcs_link = "http://fieldcreatorsstudios.com/"
    fcs_text = "www.FieldCreatorsStudios.com"
    InfoTxt = "Lead Plugin Developer: \nAshton J. Rolle aka AP Ashton \n\nExternal Programer: \nCreswell Gould aka CJ"

    fcs_about_gui_WindowDialog_file = 1001
    ABOUT_OVERALL_GRP_ID = 1002
    ABOUT_EMPTY_GRP_1 = 1003
    ABOUT_TXT_INFO_ID = 1004
    ABOUT_PLUG_STATIC_TXT_ID = 1005
    ABOUT_PLUG_VERSION_INFO_ID = 1006
    ABOUT_EMPTY_GRP_2 = 1007

    FCS_WEBSITE_GUI_BANNER_ID = 1008

    def __init__(self):
        super(FCS_About_Dialog, self).__init__()

    # Creating Dialog UI Interface Layout
    def CreateLayout(self):
        self.LoadDialogResource(self.fcs_about_gui_WindowDialog_file) # Loads GUI stuff from the .res by the dialog ID that is in the .res & .str file.
        return True

    # This method will initialize all the values inside the dialog with default values
    def InitValues(self):
        self.SetString(self.ABOUT_TXT_INFO_ID, self.InfoTxt)    #Set the default text
        self.SetString(self.ABOUT_PLUG_VERSION_INFO_ID, fcs_register_ids.PLUGIN_Version)    #Set the default text
        g_lib.Inject_GUI(func_instance=self, injected_group_id=self.ABOUT_EMPTY_GRP_2, gui_element=g_lib.Bottom_FCS_Web_GUI_Banner(ui_instance=self, button_id=self.FCS_WEBSITE_GUI_BANNER_ID))
        self.LayoutChanged(self.ABOUT_OVERALL_GRP_ID)
        return True

# ----------------------------------------------------
#    The Plugin Tools Data Type Classes Section
# ----------------------------------------------------
class FCS_CMD(CommandData):
    # C4D Command Data Class
    """
    FCS Tool UI, Web, Doc Links ``Commands Data Class``
    _________________________________________________
    """    
    # Variables & Properties --------------------- #
    windowUIdlg = None

    def IfWindowUIdlg(self):
        if self.windowUIdlg is None:
            self.windowUIdlg = self.gui_window       
        return self.windowUIdlg

    def __init__(self, Selected_CMD, windowDLG, optData):
        super(FCS_CMD, self).__init__()
        self.CMDTool = Selected_CMD
        self.gui_window = windowDLG
        self.OptionData = optData
    
    def Init(self, op):
        return True
    def Message(self, type, data):
        return True
    
    def Execute(self, doc):

        self.IfWindowUIdlg()
        
        links = FCSWEBLINKS.get(self.CMDTool)
        if links:
            open_web_link(links)

        if self.CMDTool == "A2L":
            m = fcs_attach_to_layer_popupmenu.PopupMenu_ShowLayers()
            #m.PopupMenu_ShowLayers()

        DLGs = ["NSG", "ABT", "PSC", "O2P", "C_P" ]
        for dlg in DLGs:
            if self.CMDTool == dlg:
                self.windowUIdlg.openWindowUI(winDlgType=g_lib.DLG_TYPE1)

        return True

    def RestoreLayout(self, sec_ref):
        self.IfWindowUIdlg()
        return self.windowUIdlg.restoreWindowUI(sec_ref)

    def ExecuteOptionID(self, doc, plugid, subid):
        self.IfWindowUIdlg()
        self.OptionData
        return True





"""
Utilites Tools
Toolkit | Preferences | Toolkit Manager | About
GUI Window Dialog Tools
_________________________________________________
"""
PEF_Data = g_lib.OpenToolWindowDialog_CMD(rId.PEF['id'], fcs_c4d_tools_preferences_win_gui.FCS_ToolKit_Preferences_Dialog(), None, 1)# <---| ABOUT | Window UI UI Tool
ABT_Data = FCS_CMD("ABT", fcs_c4d_logger_win_gui.FCS_Logger_GUI(), None)                        # <---| ABOUT | Window UI UI Tool
"""
GUI Window Dialog Tools
_________________________________________________
"""
N_S_Data = FCS_CMD("NSG", fcs_nurbo_tool_gui.NurboGenerator_Dialog(GGPS_IDS), None)             # <---| Nurbo Generator | Window UI Tool
PSC_Data = FCS_CMD("PSC", fcs_project_storage_c4d_gui.C4D_ProjectStorage_Dialog(), None)        # <---| ProjectStorage C4D Manger | Window UI Tool
O2P_Data = FCS_CMD("O2P", fcs_objects_2_points_gui.GUI_Window(GGPS_IDS), None)                  # <---| Objects 2 Points | Window UI Tool
C_P_Data = FCS_CMD("C_P", fcs_collide_points_gui.CollidePoints_Dialog(GGPS_IDS), None)          # <---| CollidePoints | Window UI Tool
A2L_Data = FCS_CMD("A2L", None, None)                                                           # <---| Attach 2 Layer | Popup Menu UI Tool
SNG_Data = g_lib.OpenToolWindowDialog_CMD(1040384, sngTool.Select_N_Go_Dialog(GGPS_IDS), None, 1)# <---| Select N'Go Tool & GameDev | Window UI Tool
RRP_Data = g_lib.OpenToolWindowDialog_CMD(1041139, fcs_random_replacer_tool_gui.Random_Replacer_Dialog(), None, 1)      # <---| Random Replacer | Window UI Tool
A_R_Data = g_lib.OpenToolWindowDialog_CMD(rId.A_R['id'], fcs_auto_renamer_gui.AutoRenamer_Dialog(GGPS_IDS), None, 1)    # <---| Auto Renamer | Window UI Tool
T2C_Data = g_lib.OpenToolWindowDialog_CMD(rId.T2C['id'], fcs_target2constraint_gui.Target2Constraint_Dialog(GGPS_IDS), None, 1) # <---| Target to Constraint | Window UI Tool
OSP_Data = g_lib.OpenToolWindowDialog_CMD(rId.OSP['id'], fcs_obj_set_pos_gui.SetPos_Dialog(GGPS_IDS), None, 1)          # <---| Object SetPos | Window UI Tool
UCM_Data = g_lib.OpenToolWindowDialog_CMD(1039843, fcs_uv_checker_materials_gui.Pesets_Popup(), None, 2)                # <---| UV Checkers Map Material | Window UI Tool
# Split My Poly Tool | W.I.P # - TODO and REDO
"""
Plugin Registration
_________________________________________________
"""
#  Register Plugin Add-on Tool Tpyes to Cinema 4D 
class C4D_RegisterToolTypes(object):
    """ Register C4D Plugin Data Types and Register Info Plugin Flags Tpyes """
    """
    Register Info Plugin Flags Tpyes
    _________________________________________________
    """
    # Plugin General Flags :
    HidePlugin = c4d.PLUGINFLAG_HIDE
    HideTool = c4d.PLUGINFLAG_HIDEPLUGINMENU
    RefreshPlugin = c4d.PLUGINFLAG_REFRESHALWAYS
    SmallNodePlugin = c4d.PLUGINFLAG_SMALLNODE
    # Command Plugin Flags:
    OptionGear = c4d.PLUGINFLAG_COMMAND_OPTION_DIALOG   # A info flag / Command has additional options. The user can access them through a small gadget.
    # Tag Plugin Flags :
    TagVis = c4d.TAG_VISIBLE                            # The tag can be seen in the object manager.
    TagMul = c4d.TAG_MULTIPLE                           # Multiple copies of the tag allowed on a single object.
    TagHier = c4d.TAG_HIERARCHICAL                      # The tag works hierarchical, so that sub-objects inherit its properties (e.g. the material tag).
    TagExp = c4d.TAG_EXPRESSION                         # The tag is an expression.
    TagTem = c4d.TAG_TEMPORARY                          # Private.
    # Object Plugin Flags:
    oMod = c4d.OBJECT_MODIFIER                          # Modifier object. Deforms the surrounding object. (E.g. bend.)
    oHier = c4d.OBJECT_HIERARCHYMODIFIER                # Hierarchical modifier. Deforms the surrounding objects together with other instances in a hierarchy chain. Only the top-most instance of the plugin in a chain is called. (E.g. bones.)Hierarchical modifier. Deforms the surrounding objects together with other instances in a hierarchy chain. Only the top-most instance of the plugin in a chain is called. (E.g. bones.)
    oGen = c4d.OBJECT_GENERATOR                         # Generator object. Produces a polygonal or spline representation on its own. (E.g. primitive cube.)
    oInput = c4d.OBJECT_INPUT                           # Used in combination with OBJECT_GENERATOR. Specifies that the generator uses builds a polygon or spline, using its subobjects as input. (E.g. Sweep Subdivision Surface, Boolean.)
    oPart = c4d.OBJECT_PARTICLEMODIFIER                 # Particle modifier.
    oSpline = c4d.OBJECT_ISSPLINE                       # The object is a spline.
    oCamera = c4d.OBJECT_CAMERADEPENDENT                # Camera dependent.
    oPointObj = c4d.OBJECT_POINTOBJECT                  # Point Object.
    oPolyObj = c4d.OBJECT_POLYGONOBJECT                 # Polygon object.
    """
    Register C4D Plugin Data Types
    _________________________________________________
    """
    def RegisterIconData(self, id, iconName):
        plugin_Icon_bmp = c4d.bitmaps.BaseBitmap()
        plugin_Icon_bmp.InitWith(fcsDIR.Icon_Path(i=iconName))
        c4d.gui.RegisterIcon(id, 
                             plugin_Icon_bmp, 
                             0, 
                             0, 
                             -1, 
                             -1)
        return True

    def RegisterCommandData(self, id, str_name, infoflags, iconName, helpInfo, dataClass):
        """ A CommandData Tool Plugin Register """
        DESCRIPTIONS = "" #ToolInfo_Description(helpInfo)
        plugin_Icon = c4d.bitmaps.BaseBitmap()
        plugin_Icon.InitWith(fcsDIR.Icon_Path(i=iconName))
        plugins.RegisterCommandPlugin(id=id,                        # Plugin register ID.
                                    str=str_name,                   # This is for the Plugin Name to show in the Plugins lic4d.storage.
                                    info=infoflags,                 # If you want a option button once you have a ExecuteOptionID in Data Class, then put in Flags info=c4d.PLUGINFLAG_COMMAND_OPTION_DIALOG | c4d.PLUGINFLAG_COMMAND_HOTKEY,
                                    icon=plugin_Icon,               # Plugin Icon Image.
                                    help=DESCRIPTIONS,              # The plugin help info is on what the plugin does.
                                    dat=dataClass)                  # The plugin data class.
        return True

    def RegisterTagData(self, id, str_name, TagModeflags, dataClass, desc, iconName):
        """ A TagData Tool Plugin Register """
        plugin_Icon = c4d.bitmaps.BaseBitmap()
        plugin_Icon.InitWith(fcsDIR.Icon_Path(i=iconName))
        plugins.RegisterTagPlugin(id=id,                    # Plugin register ID.
                                str=str_name,               # This is for the Plugin Name to show in the Plugins lic4d.storage.
                                info=TagModeflags,          # If you want a option button once you have a ExecuteOptionID in Data Class, then put in Flags info=c4d.PLUGINFLAG_COMMAND_OPTION_DIALOG | c4d.PLUGINFLAG_COMMAND_HOTKEY,
                                g=dataClass,                # The plugin data class.
                                description=desc,           # The plugin description.
                                icon=plugin_Icon)           # Plugin Icon Image.
        return True

    def RegisterObjectData(self, id, str_name, dataClass, desc_res, iconName, ObjectModeflags):
        """ A ObjectData Tool Plugin Register """
        plugin_Icon = c4d.bitmaps.BaseBitmap()
        plugin_Icon.InitWith(fcsDIR.Icon_Path(i=iconName))
        plugins.RegisterObjectPlugin(id=id,                         # Plugin register ID
                                    str=str_name,                   # This is for the Plugin Name to show in the Plugins lic4d.storage.
                                    g=dataClass,                    # The plugin data class.
                                    description=desc_res,           # The plugin description .res file that is inside res\description folder.
                                    icon=plugin_Icon,               # Plugin Icon Image.
                                    info=ObjectModeflags)           # If you want then put in Plugin General Flags or Object Plugin Flags eg:info=c4d.OBJECT_GENERATOR,
        return True

    def RegisterExportSceneSaverData(self, id, str_name, dataClass, desc_res, SaverModeflags, fileExt):
        """ A SceneSaver Tool Plugin Register """
        plugins.RegisterSceneSaverPlugin(id=id,
                                        str=str_name,
                                        info=SaverModeflags,
                                        g=dataClass,
                                        description=desc_res,
                                        suffix=fileExt)
        return True
register_type = C4D_RegisterToolTypes()
PF = C4D_RegisterToolTypes()
# Plugin ID's, Names, Icon, info_Flags, classData and Info Desc 
PLUG_FCSTools = 1038326
PLUG_ABT = {'ID':rId.ABT['id'], 'Icon':rId.ABT['i'], 'Name':rId.ABT['str'], 'flags':PF.HideTool, 'Data':ABT_Data, 'ToolWith':0, 'Info':'ToolInfo_Pref'}
PLUG_PEF = {'ID':rId.PEF['id'], 'Icon':rId.PEF['i'], 'Name':rId.PEF['str'], 'flags':PF.HideTool, 'Data':PEF_Data, 'ToolWith':0, 'Info':'ToolInfo_Pref'}
PLUG_TPM = {'ID':rId.TPM['id'], 'Icon':rId.TPM['i'], 'Name':rId.TPM['str'], 'flags':PF.HideTool, 'Data':"", 'ToolWith':0, 'Info':'ToolInfo_Forum'}
PLUG_FRM = {'ID':rId.FRM['id'], 'Icon':rId.FRM['i'], 'Name':rId.FRM['str'], 'flags':PF.HideTool, 'Data':FCS_CMD("DIS", None, None), 'ToolWith':0, 'Info':'ToolInfo_Forum'}
PLUG_DIS = {'ID':rId.DIS['id'], 'Icon':rId.DIS['i'], 'Name':rId.DIS['str'], 'flags':PF.HideTool, 'Data':FCS_CMD("DIS", None, None),   'ToolWith':0, 'Info':'ToolInfo_Blank'}
PLUG_TUT = {'ID':rId.TUT['id'], 'Icon':rId.TUT['i'], 'Name':rId.TUT['str'], 'flags':PF.HideTool, 'Data':FCS_CMD("GUM", None, None), 'ToolWith':0, 'Info':'ToolInfo_Manual'}
PLUG_TLO = {'ID':rId.TLO['id'], 'Icon':rId.TLO['i'], 'Name':rId.TLO['str'], 'flags':PF.HideTool, 'Data':FCS_CMD("TRE", None, None), 'ToolWith':0, 'Info':'ToolInfo_Tools'}
PLUG_FB  = {'ID':rId.FB['id'],  'Icon':rId.FB['i'],  'Name':rId.FB['str'],  'flags':PF.HideTool, 'Data':FCS_CMD("FB", None, None), 'ToolWith':0, 'Info':'ToolInfo_Tools'}
PLUG_YT  = {'ID':rId.YT['id'],  'Icon':rId.YT['i'],  'Name':rId.YT['str'],  'flags':PF.HideTool, 'Data':FCS_CMD("YT", None, None), 'ToolWith':0, 'Info':'ToolInfo_Tools'}
PLUG_GRS = {'ID':rId.GRS['id'], 'Icon':rId.GRS['i'], 'Name':rId.GRS['str'], 'flags':PF.HideTool, 'Data':FCS_CMD("GUM", None, None), 'ToolWith':0, 'Info':'ToolInfo_Tools'}
PLUG_AFL = {'ID':rId.AFL['id'], 'Icon':rId.AFL['i'], 'Name':rId.AFL['str'], 'flags':PF.HideTool, 'Data':QS.CMDTool("AFL"), 'ToolWith':0, 'Info':'ToolInfo_AddFCSLayouts'}

#  Register Tools 
PLUG_N_S = {'ID':1038594, 'Icon':"tool_icon_23.png", 'Name':StudioName+"Nurbo (BETA)", 'flags':PF.HideTool, 'Data':N_S_Data, 'ToolWith':1038594, 'Info':'ToolInfo_NurboGenerator'}
PLUG_NTC = {'ID':1053608, 'Icon':"tool_conversion_icon.png", 'Name':GGPS_IDS.ID(fcsID.IDS_TAXILINE_CONVERT_ICON), 'flags':PF.HideTool, 'Data':QS.CMDTool("NTC"), 'ToolWith':1038594, 'Info':'ToolInfo_NurboGenerator'}
PLUG_PSC = {'ID':1050001, 'Icon':"tool_icon_69.png", 'Name':StudioName+"ProjectStorage C4D", 'flags':PF.HideTool, 'Data':PSC_Data, 'ToolWith':1050001, 'Info':'ToolInfo_Blank'}
PLUG_O2P = {'ID':1038730, 'Icon':"tool_icon_32.png", 'Name':StudioName+"Object 2 Points",'flags':PF.HideTool, 'Data':O2P_Data, 'ToolWith':1038730, 'Info':'ToolInfo_TargetToConstraint'}
PLUG_RRP = {'ID':1041139, 'Icon':"tool_icon_67.png", 'Name':StudioName+"Random Re-Placer", 'flags':PF.HideTool, 'Data':RRP_Data, 'ToolWith':1041139, 'Info':'ToolInfo_Blank'}
#PLUG_SMP = {'ID':1050237, 'Icon':"tool_icon_70.png", 'Name':StudioName+"Split My Poly (BETA)", 'flags':PF.HideTool|PF.OptionGear, 'Data':SPLIT_MY_POLY_Data(), 'ToolWith':1050237, 'Info':'ToolInfo_Blank'}
PLUG_A2L = {'ID':1050427, 'Icon':"tool_icon_74.png", 'Name':StudioName+"Attach 2 Layer", 'flags':PF.HideTool, 'Data':A2L_Data, 'ToolWith':1050427, 'Info':'ToolInfo_A2L'}
PLUG_UCM = {'ID':1039843, 'Icon':"tool_icon_55.png", 'Name':StudioName+"UV Checkers Map Lib",'flags':PF.HideTool, 'Data':UCM_Data, 'ToolWith':1039843, 'Info':'ToolInfo_UVMapMat'}
PLUG_C_P = {'ID':1054054, 'Icon':"tool_icon_77.png", 'Name':StudioName+"CollidePoints",'flags':PF.HideTool, 'Data':C_P_Data, 'ToolWith':1054054, 'Info':'ToolInfo_Blank'}
PLUG_T2C = {'ID':rId.T2C['id'], 'Icon':rId.T2C['i'], 'Name':rId.T2C['str'], 'flags':PF.HideTool, 'Data':T2C_Data, 'ToolWith':rId.FQS['id'], 'Info':'ToolInfo_TargetToConstraint'}
PLUG_OSP = {'ID':rId.OSP['id'], 'Icon':rId.OSP['i'], 'Name':rId.OSP['str'], 'flags':PF.HideTool, 'Data':OSP_Data, 'ToolWith':rId.FQS['id'], 'Info':'ToolInfo_SetPos'}
PLUG_A_R = {'ID':rId.A_R['id'], 'Icon':rId.A_R['i'], 'Name':rId.A_R['str'], 'flags':PF.HideTool, 'Data':A_R_Data, 'ToolWith':rId.FQS['id'], 'Info':'ToolInfo_AutoRenamer'}
PLUG_FQS = {'ID':rId.FQS['id'], 'Icon':rId.FQS['i'], 'Name':rId.FQS['str'], 'flags':PF.HideTool, 'Data':QS.CMDTool("FQS"), 'ToolWith':rId.FQS['id'], 'Info':'ToolInfo_Scripts'}


PLUG_SNG = {'ID':1055455, 'Icon':"tool_icon_22.png", 'Name':StudioName+"Select N'Go", 'flags':PF.HideTool, 'Data':SNG_Data, 'ToolWith':1040384, 'Info':'ToolInfo_SelNGO'}
# Register Unity Tools 
PLUG_UCX = {'ID':1053289, 'Icon':"unityUCX_icon.png", 'Name':"Unity Collison", 'flags':PF.HideTool|PF.OptionGear, 'Data':sngTool.Unity_UCX_Data(), 'ToolWith':PLUG_SNG['ID'], 'Info':'ToolInfo_SelNGO'}
PLUG_Ounity_UCXobject = {'PlugID':1053288, 'PlugIcon':"unityUCX_icon.png", 'PlugName':"Unity Collison Object", 'Data':sngTool.O_Unity_Object_Data, 'resData':"fcs_O_UnityUCX", 'flags':PF.oGen|PF.HideTool, 'ToolWith':PLUG_SNG['ID']}

#  Register Quick Commands Scripts Pack  
PLUG_NDS = {'ID':rId.NDS['id'], 'Icon':rId.NDS['i'], 'Name':rId.NDS['str'], 'flags':PF.HideTool, 'Data':QS.CMDTool("NDS"), 'ToolWith':rId.FQS['id'], 'Info':'ToolInfo_NewDocSelected'}
PLUG_APC = {'ID':rId.APC['id'], 'Icon':rId.APC['i'], 'Name':rId.APC['str'], 'flags':PF.HideTool, 'Data':QS.CMDTool("APC"), 'ToolWith':rId.FQS['id'], 'Info':'ToolInfo_APC'}
PLUG_SMR = {'ID':rId.SMR['id'], 'Icon':rId.SMR['i'], 'Name':rId.SMR['str'], 'flags':PF.HideTool, 'Data':QS.CMDTool("SMR"), 'ToolWith':rId.FQS['id'], 'Info':'ToolInfo_MaterialRez'}
PLUG_S_O = {'ID':rId.S_O['id'], 'Icon':rId.S_O['i'], 'Name':rId.S_O['str'], 'flags':PF.HideTool, 'Data':QS.CMDTool("S_O"), 'ToolWith':rId.FQS['id'], 'Info':'ToolInfo_Speckle'}
PLUG_AOM = {'ID':rId.AOM['id'], 'Icon':rId.AOM['i'], 'Name':rId.AOM['str'], 'flags':PF.HideTool, 'Data':QS.CMDTool("AOM"), 'ToolWith':rId.FQS['id'], 'Info':'ToolInfo_Add_AO2Mat'}
PLUG_CIt = {'ID':rId.CIt['id'], 'Icon':rId.CIt['i'], 'Name':rId.CIt['str'], 'flags':PF.HideTool, 'Data':QS.CMDTool("CIt"), 'ToolWith':rId.FQS['id'], 'Info':'ToolInfo_CIt'}
PLUG_D_P = {'ID':rId.D_P['id'], 'Icon':rId.D_P['i'], 'Name':rId.D_P['str'], 'flags':PF.HideTool, 'Data':QS.CMDTool("D_P"), 'ToolWith':rId.FQS['id'], 'Info':'ToolInfo_D_P'}
PLUG_OIt = {'ID':rId.OIt['id'], 'Icon':rId.OIt['i'], 'Name':rId.OIt['str'], 'flags':PF.HideTool, 'Data':QS.CMDTool("OIt"), 'ToolWith':rId.FQS['id'], 'Info':'ToolInfo_OIt'}
PLUG_ARN = {'ID':rId.ARN['id'], 'Icon':rId.ARN['i'], 'Name':rId.ARN['str'], 'flags':PF.HideTool, 'Data':QS.CMDTool("ARN"), 'ToolWith':rId.FQS['id'], 'Info':'ToolInfo_ARN'}
PLUG_A2Z = {'ID':rId.A2Z['id'], 'Icon':rId.A2Z['i'], 'Name':rId.A2Z['str'], 'flags':PF.HideTool, 'Data':QS.CMDTool("A2Z"), 'ToolWith':rId.FQS['id'], 'Info':'ToolInfo_A2Z'}
PLUG_SUS = {'ID':rId.SUS['id'], 'Icon':rId.SUS['i'], 'Name':rId.SUS['str'], 'flags':PF.HideTool, 'Data':QS.CMDTool("SUS"), 'ToolWith':rId.FQS['id'], 'Info':'ToolInfo_SUS'}
PLUG_TGA = {'ID':rId.TGA['id'], 'Icon':rId.TGA['i'], 'Name':rId.TGA['str'], 'flags':PF.HideTool, 'Data':QS.CMDTool("TGA"), 'ToolWith':rId.FQS['id'], 'Info':'ToolInfo_TGA'}
PLUG_SCD = {'ID':rId.SCD['id'], 'Icon':rId.SCD['i'], 'Name':rId.SCD['str'], 'flags':PF.HideTool, 'Data':QS.CMDTool("SCD"), 'ToolWith':rId.FQS['id'], 'Info':'ToolInfo_SCD'}
PLUG_FSS = {'ID':rId.FSS['id'], 'Icon':rId.FSS['i'], 'Name':rId.FSS['str'], 'flags':PF.HideTool, 'Data':QS.CMDTool("FSS"), 'ToolWith':rId.FQS['id'], 'Info':'ToolInfo_FillSplineShape'}
#  Register Tags Tools 
PLUG_NTag1 = {'PlugID':1040522, 'PlugIcon':"tool_icon_64.png", 'PlugName':"Nurbo CusShapeTemp", 'Data':fcsNDT.NurboTags_Data, 'resData':"fcs_T_nurboShape", 'ToolWith':1038594}
PLUG_NTag2 = {'PlugID':1040523, 'PlugIcon':"tool_icon_63.png", 'PlugName':"Nurbo CusPathTemp", 'Data':fcsNDT.NurboTags_Data, 'resData':"fcs_T_nurboSplinePath", 'ToolWith':1038594}
PLUG_NTag3 = {'PlugID':1040524, 'PlugIcon':"tool_icon_65.png", 'PlugName':"NurboSweep", 'Data':fcsNDT.NurboTags_Data, 'resData':"fcs_T_nurboSweep", 'ToolWith':1038594}
PLUG_NTag4 = {'PlugID':1040534, 'PlugIcon':"tool_icon_66.png", 'PlugName':"Controllers", 'Data':fcsNDT.NurboTags_Data, 'resData':"fcs_T_nurboControllers", 'ToolWith':1038594}
PLUG_RPTag = {'PlugID':1050211, 'PlugIcon':"tool_icon_68.png", 'PlugName':"Random RePlacer Select Tag", 'Data':fcsNDT.RandomReplacerTag_Data, 'resData':"fcs_T_rpPreset", 'ToolWith':1041139}
PLUG_RPTempTag = {'PlugID':1050362, 'PlugIcon':"tool_icon_71.png", 'PlugName':"Random RePlacer Template Tag", 'Data':fcsNDT.RR_TempTag_Data, 'resData':"fcs_T_rpTemp", 'ToolWith':1041139}
PLUG_RPPresetTag = {'PlugID':1050361, 'PlugIcon':"tool_icon_73.png", 'PlugName':"Random RePlacer Preset Tag", 'Data':fcsNDT.RR_PresetTag_Data, 'resData':"fcs_T_nurbo", 'ToolWith':1041139}
#  Register Objects Tools 
PLUG_Oplt_root_sys = {'PlugID':1050949, 'PlugIcon':"tool_icon_plts_root.png", 'PlugName':"Power-Line-System", 'Data':fcsNDT.O_PLT_Connector_Data, 'resData':"fcs_Oplts_connector", 'flags':PF.oGen, 'ToolWith':1050948}
PLUG_Oplt_spline_sys = {'PlugID':1050951, 'PlugIcon':"tool_icon_plts_spline_sys.png", 'PlugName':"PLS_Spline", 'Data':fcsNDT.O_PLT_Spline_Data, 'resData':"fcs_Oplts_spline_sys", 'flags':PF.oGen|PF.oSpline|PF.oInput, 'ToolWith':1050948}
PLUG_Oplt_connect_sys = {'PlugID':1050950, 'PlugIcon':"tool_icon_plts_connectors_sys.png", 'PlugName':"PLS_Connectors", 'Data':fcsNDT.O_PLT_Connectors_Data, 'resData':"fcs_Oplts_connectors_sys", 'flags':0, 'ToolWith':1050948}
#PLUG_Oplt_object = {'PlugID':1050952, 'PlugIcon':"tool_icon_plts_object.png", 'PlugName':"PLS_Object", 'Data':O_PLT_Object_Data, 'resData':"fcs_Oplts_object_ins", 'flags':PF.oGen, 'ToolWith':1050948}
PLUG_Oplt_connector = {'PlugID':1050953, 'PlugIcon':"tool_icon_plts_connector.png", 'PlugName':"PLS_Connector", 'Data':fcsNDT.O_PLT_Connector_Data, 'resData':"fcs_Oplts_connector", 'flags':PF.oGen, 'ToolWith':1050948}
PLUG_Oplt_controller = {'PlugID':1050954, 'PlugIcon':"tool_icon_plts_controller.png", 'PlugName':"PLS_Controller", 'Data':fcsNDT.O_PLT_Controller_Data, 'resData':"fcs_Oplts_controller", 'flags':PF.oGen, 'ToolWith':1050948}
PLUG_O_nurbo_taxiline = {'PlugID':1053591, 'PlugIcon':"tool_Nurbo_SweepTaxiLine_icon.png", 'PlugName':"Nurbo TaxiLine Object", 'Data':fcsNDT.O_NurboTaxiline_Object_Data, 'resData':"fcs_O_nurbo_taxiline", 'flags':PF.oGen|PF.HideTool|PF.oInput, 'ToolWith':PLUG_N_S['ID']}

#  Register Our Tools 
def RegisterFCS_Toolset():
    """
     Register Plugins
     Plugin ID's, Names, Icon, info_Flags, classData and Info Desc
     PLUGINID = ``1000001`` <- Get yours at [ www.plugincafe.com ] & Plugin IDs 1000001-1000010 are reserved for development.
     PLUG_Example = {
                     'ID':``PLUGIN_ID``,
                     'Icon':``tool_icon.png``,
                     'Name':"Tools Manager",
                     'flags':0, <-- ``HideTool``|``OptionGear```` or no flags is 0
                     'ToolWith':``AnotherTool_ID``,
                     'Data':``yourToolclass()``,
                     'Info':"This Tool is Cool"
                     }
    """
    # PLUG_TPM / ToolKitManager_Data(CommandData) Put Back when done
    list_of_Toolset_BaseTools = [ PLUG_ABT, PLUG_FRM, PLUG_TUT, PLUG_PEF, PLUG_AFL, PLUG_DIS, PLUG_TLO, PLUG_FB, PLUG_YT, PLUG_GRS ]
    for eachBaseTool in list_of_Toolset_BaseTools:
        register_type.RegisterCommandData(eachBaseTool["ID"], eachBaseTool["Name"], eachBaseTool["flags"], eachBaseTool["Icon"], eachBaseTool["Info"], eachBaseTool["Data"])
    # PLUG_SMP, Put Back when done
    list_of_tools = [ PLUG_UCX, PLUG_FQS, PLUG_A_R, PLUG_N_S, PLUG_T2C, PLUG_SMR, PLUG_O2P, PLUG_NDS, PLUG_S_O,
                    PLUG_UCM, PLUG_AOM, PLUG_APC, PLUG_CIt, PLUG_D_P, PLUG_OIt, PLUG_ARN, PLUG_OSP,
                    PLUG_A2Z, PLUG_SUS, PLUG_TGA, PLUG_SCD, PLUG_RRP, PLUG_PSC, PLUG_A2L, PLUG_NTC, PLUG_C_P ]

    list_of_tags = [ PLUG_NTag1, PLUG_NTag2, PLUG_NTag3, PLUG_NTag4, PLUG_RPTag, PLUG_RPTempTag, PLUG_RPPresetTag ]

    list_of_objects = [ PLUG_Ounity_UCXobject, PLUG_O_nurbo_taxiline ] # PLUG_Oplt_spline_sys #PLUG_Oplt_object, PLUG_Oplt_controller, PLUG_Oplt_connector

    list_of_icons = [ rId.NURBO_TL_CENTER, rId.NURBO_TL_INNER, rId.NURBO_TL_OUTTER, rId.GPP_1_MODE, rId.GPP_2_MODE, rId.GPP_3_MODE, rId.GPP_4_MODE ]
    """
    JFileIDs = g_lib.fcsDIR.FCS_Tools_IDs_CONIG2
    if os.path.exists(JFileIDs):
        for eachTool in list_of_tools:
            CHK_MODE = jsonEdit.GetItemProperty(json_file=JFileIDs, main_property="PLUGIN_ID" + str(eachTool["ToolWith"]) )
            if CHK_MODE == "Enable":
                register_type.RegisterCommandData(eachTool["ID"], eachTool["Name"], eachTool["flags"], eachTool["Icon"], eachTool["Info"], eachTool["Data"])
            else:
                pass
    """
    for eachTool in list_of_tools:
        register_type.RegisterCommandData(eachTool["ID"], eachTool["Name"], eachTool["flags"], eachTool["Icon"], eachTool["Info"], eachTool["Data"])
    for eachTag in list_of_tags:
        register_type.RegisterTagData(eachTag['PlugID'], eachTag['PlugName'], PF.HidePlugin|PF.HideTool|PF.TagVis, eachTag['Data'], eachTag['resData'], eachTag['PlugIcon'])
    for eachObject in list_of_objects:
        register_type.RegisterObjectData(eachObject['PlugID'], eachObject['PlugName'], eachObject['Data'], eachObject['resData'], eachObject['PlugIcon'], eachObject['flags'])
    for eachIcon in list_of_icons:
        register_type.RegisterIconData(id=eachIcon['id'], iconName=eachIcon['i'])

    #  Run Helper Methods After Tools Loaded.
    #PLUGIN_DIR().Create_FCS_Tools_Prefs_Folder()
    #Create_FCS_ID_Tools_XML()
    #fcsEngine.RUN_FCS_STATION_APP(RunFS_fx=True, RunC4D_MCX=False, RunX_File=False, Data="NONE")
    #fcs_register_ids.Print_FCS_ToolKit_Initialized()

    Toolset_State = "-Beta v"
    __version_data__ = "2.40.09"
    DataInfo = "InHouse" + Toolset_State + __version_data__
    PLUGIN_Version = "RC - F.C.S C4D ToolKit | "+ DataInfo # Your Version Our Plugin
    print("===============================================================================")
    print(" "+ PLUGIN_Version +" for Cinema 4D Initialized.\n This is a all Python Plugin.")
    print("===============================================================================")
    return True

# Register and Add FCS C4D Shelf's Menu to Cinema 4D Editor UI 
def pCMD(id):
    return "PLUGIN_CMD_"+str(id)
def EnhanceMainMenu_FCS_Shelf():
    """ Adding FCS Shelf Menu in the Cinema 4D Main Editor / After C4D Start-Up Run Functions. """

    mainMenu = gui.GetMenuResource("M_EDITOR")                                                          # Get main menu resource on where you want the menu in.
    VPopupMenu = gui.GetMenuResource("M_GLOBAL_POPUP")
    pluginsMenu = gui.SearchPluginMenuResource()                                                        # Get 'Plugins' main menu resource

    menu = c4d.BaseContainer()                                                                          # Create a container to hold a new menu information
    menu.InsData(c4d.MENURESOURCE_SUBTITLE, "F.C.S ToolKit")                                            # Set the name of the menu
    menu.InsData(c4d.MENURESOURCE_COMMAND, "PLUGIN_CMD_1038326")                                        # Add command 'Cube' with ID 5159 or Plugin ID to the Menu.
                                                                                                        # eg: "PLUGIN_CMD_1000001"
                                                                                                        # menu.InsData(c4d.MENURESOURCE_COMMAND, "PLUGIN_CMD_1000001")
    menu.InsData(c4d.MENURESOURCE_SEPERATOR, True)                                                      # Add a separator
    # Tools Menu
    submenu_Tools = c4d.BaseContainer()                                                                 # Add the submenu
    submenu_Tools.InsData(c4d.MENURESOURCE_SUBTITLE, "Tools")                                           # This is a submenu title.
    # -----------// Select N'Go + GameDev Tools Menu // ---------- #
    submenu_GameEngineMenu = c4d.BaseContainer()
    submenu_GameEngineMenu.InsData(c4d.MENURESOURCE_SUBTITLE, "Select N'Go + GameDev Tools")
    submenu_GameEngineMenu.InsData(c4d.MENURESOURCE_COMMAND, pCMD(PLUG_SNG['ID']))
    submenu_GameEngineMenu.InsData(c4d.MENURESOURCE_SEPERATOR, True)                                    # Add a separator
    # Unity Menu
    submenu_UnityMenu = c4d.BaseContainer()
    submenu_UnityMenu.InsData(c4d.MENURESOURCE_SUBTITLE, "Unity4D Tools")
    submenu_UnityMenu.InsData(c4d.MENURESOURCE_COMMAND, pCMD(PLUG_UCX['ID']))
    submenu_GameEngineMenu.InsData(c4d.MENURESOURCE_SUBMENU, submenu_UnityMenu)
    submenu_GameEngineMenu.InsData(c4d.MENURESOURCE_SEPERATOR, True)                                    # Add a separator

    submenu_Tools.InsData(c4d.MENURESOURCE_SUBMENU, submenu_GameEngineMenu)
    #---------------------------------------------------------------------#
    submenu_Tools.InsData(c4d.MENURESOURCE_SEPERATOR, True)                                             # Add a separator
    maintools = [ pCMD(PLUG_N_S['ID']), pCMD(1050877), pCMD(PLUG_RRP['ID']), pCMD(PLUG_PSC['ID']), pCMD(PLUG_C_P['ID']), # pCMD(PLUG_SMP['ID'])
                pCMD(PLUG_A2L['ID']), pCMD(PLUG_T2C['ID']), pCMD(PLUG_A_R['ID']), pCMD(PLUG_OSP['ID']) ]
    for tool in maintools:
        submenu_Tools.InsData(c4d.MENURESOURCE_COMMAND, tool)
    menu.InsData(c4d.MENURESOURCE_SUBMENU, submenu_Tools)                                               # Add the Submenu to Main Menu

    # Commands Tools Menu
    sm_CMDTools = c4d.BaseContainer()
    sm_CMDTools.InsData(c4d.MENURESOURCE_SUBTITLE, "Commands Tools")
    sm_CMDTools.InsData(c4d.MENURESOURCE_COMMAND, pCMD(PLUG_FQS['ID']))
    sm_CMDTools.InsData(c4d.MENURESOURCE_SEPERATOR, True)                                               # Add a separator
    qscomands = [ pCMD(PLUG_NDS['ID']), pCMD(PLUG_OIt['ID']), pCMD(PLUG_A2Z['ID']), pCMD(PLUG_FSS['ID']), pCMD(PLUG_ARN['ID']), 
                  pCMD(PLUG_TGA['ID']), pCMD(PLUG_S_O['ID']), pCMD(PLUG_SCD['ID']), pCMD(PLUG_SUS['ID']) ]
    for tool in qscomands:
        sm_CMDTools.InsData(c4d.MENURESOURCE_COMMAND, tool)
    menu.InsData(c4d.MENURESOURCE_SUBMENU, sm_CMDTools)

    # Materials Menu
    submenu_mats = c4d.BaseContainer()
    submenu_mats.InsData(c4d.MENURESOURCE_SUBTITLE, 'Materials/Utilities')
    submenu_mats.InsData(c4d.MENURESOURCE_COMMAND, pCMD(PLUG_UCM['ID']))
    submenu_mats.InsData(c4d.MENURESOURCE_SEPERATOR, True)
    submenu_mats.InsData(c4d.MENURESOURCE_COMMAND, pCMD(PLUG_SMR['ID']))
    submenu_mats.InsData(c4d.MENURESOURCE_COMMAND, pCMD(PLUG_AOM['ID']))
    menu.InsData(c4d.MENURESOURCE_SUBMENU, submenu_mats)

    menu.InsData(c4d.MENURESOURCE_SEPERATOR, True)
    menu.InsData(c4d.MENURESOURCE_COMMAND, "PLUGIN_CMD_1039852")
    menu.InsData(c4d.MENURESOURCE_SEPERATOR, True)
    menu.InsData(c4d.MENURESOURCE_COMMAND, "PLUGIN_CMD_1050523")
    menu.InsData(c4d.MENURESOURCE_COMMAND, "PLUGIN_CMD_1039465")

    # Support & Help Menu
    submenu_HelpMenu = c4d.BaseContainer()
    submenu_HelpMenu.InsData(c4d.MENURESOURCE_SUBTITLE, "Support & Help...")
    submenu_HelpMenu.InsData(c4d.MENURESOURCE_COMMAND, "PLUGIN_CMD_1039242")
    submenu_HelpMenu.InsData(c4d.MENURESOURCE_SEPERATOR, True)
    submenu_HelpMenu.InsData(c4d.MENURESOURCE_COMMAND, "PLUGIN_CMD_1054277")
    submenu_HelpMenu.InsData(c4d.MENURESOURCE_COMMAND, "PLUGIN_CMD_1054278")
    submenu_HelpMenu.InsData(c4d.MENURESOURCE_COMMAND, "PLUGIN_CMD_1050892")
    submenu_HelpMenu.InsData(c4d.MENURESOURCE_COMMAND, "PLUGIN_CMD_1052664")
    submenu_HelpMenu.InsData(c4d.MENURESOURCE_COMMAND, "PLUGIN_CMD_1054279")
    submenu_HelpMenu.InsData(c4d.MENURESOURCE_SEPERATOR, True)
    submenu_HelpMenu.InsData(c4d.MENURESOURCE_COMMAND, "PLUGIN_CMD_1039419")
    submenu_HelpMenu.InsData(c4d.MENURESOURCE_COMMAND, "PLUGIN_CMD_1050473")

    menu.InsData(c4d.MENURESOURCE_SUBMENU, submenu_HelpMenu)

    if pluginsMenu:
        # Insert menu after 'Plugins' menu
        mainMenu.InsDataAfter(c4d.MENURESOURCE_STRING, menu, pluginsMenu)
        VPopupMenu.InsDataAfter(c4d.MENURESOURCE_STRING, menu, pluginsMenu)
    else:
        # Insert menu after the last existing menu ('Plugins' menu was not found)
        mainMenu.InsData(c4d.MENURESOURCE_STRING, menu)
        VPopupMenu.InsData(c4d.MENURESOURCE_STRING, menu)

#  Plugin Message 
def PluginMessage(id, data):
    """ Implementing PluginMessage() and have your code run for example on C4DPL_PROGRAM_STARTED. Also Build Menus. Start a function after c4d startup and open up."""
    JFile = g_lib.fcsDIR.FCS_Tools_IDs_CONIG2

    if id == c4d.C4DPL_BUILDMENU:
        EnhanceMainMenu_FCS_Shelf() # Add FCS Plugin Shelf Menu
        c4d.gui.UpdateMenus()

    if id == c4d.C4DPL_COMMANDLINEARGS:
        # Extend command line parsing, here
		# This is the last plugin message on Cinema 4D's start process.
        if os.path.exists(JFile):
            Get_TM_Data = jsonEdit.GetItemProperty(json_file=JFile, main_property="OpenManager")
            if Get_TM_Data=="MangerStart":
                c4d.CallCommand(1050523)
                jsonEdit.EditItemProperty(json_file=JFile, main_property="OpenManager", new_value="False")

if __name__ == '__main__':
    dir, file = os.path.split(__file__)
    g_lib.FCS_Global_Initalize_Patch(PLUGIN_FCS_PACKAGE_CORE_LIB_PATH, resDLG, ActiveDebugMode)
    #Create_FCS_Tools_Prefs_Folder()
    RegisterFCS_Toolset()