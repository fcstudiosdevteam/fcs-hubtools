#  // Imports for Cinema 4D //
import c4d
import os
import c4d, os, sys, subprocess, webbrowser, collections, math, random, urllib, glob, shutil, time, datetime
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as WindowDialog       # eg:( class My_Dialog(c4d.gui.GeDialog): ) to ( class My_Dialog(WindowDialog): ) Just keeping code line short.
from c4d.gui import GeDialog as dlg 
from c4d.utils import GeRayCollider

import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
import fcs_c4d_common_g_lib as cglib
PLUGIN_ID = 1050877
#for GeLoadString
#values must match with the header file
IDS_PRIMITIVETOOL = 50000


class PowerLineAttDialog(gui.SubDialog):

    surfaceObjects = []
    paintObjects = []
    
    UIDATA_PARAMETERS = {}

    IDS_VER_ID = 1987
    LinkBC = c4d.BaseContainer()
    IDUI_DATA = None
    LinkBox_SourceObj = 1001
    LinkBox_SourceObjTarget = None
    LinkOBJID_STR = 1002
    ObjectLinkStr = 1003
    UI_OVERALL_GRP = 1004
    IDUI_SURFACE_OBJ_LINK = 1005
    IDUI_SURFACE_OBJ_GUID = 1006
    IDUI_CUSTOMGRP_OBJ_LINK = 1007
    IDUI_PANEL_TABS = 1008
    IDUI_TOOLPANEL_TAB = 1009
    IDUI_INSTRUCTIONPANEL_TAB = 1010
    IDUI_GRG_OBJ_GUID = 1011
    IDUI_MARKSCALESIZE = 1012
    IDUI_ROTATION_SPEED = 1013
    ID_STATIC_TXT = 1202
    ID_STATIC_TXT1 = 1203
    ID_STATIC_TXT2 = 1204
    ID_STATIC_TXT3 = 1205
    ID_STATIC_TXT4 = 1206
    ID_STATIC_TXT5 = 1207

    IDUI_MODE_MENU = 1014
    IDUI_MODE_MENU_LIST =   {                             #(12298)IconId(Eg:"&i12298&Model")
                                "Single Paint": {'id':1, 'icon':"&i1054515&"}, 
                                "Flow Paint":   {'id':2, 'icon':"&i1054516&"},
                                "Scatter Paint":{'id':3, 'icon':"&i1054517&"},
                                "Grid Paint":   {'id':4, 'icon':"&i1054518&"}
                            }


    IDUI_OPEN_LIGHTPROJECT_DEMO = 6554

    IDUI_RPO_ON  = { 'id':1345, 'str':"ON", 'state':False }
    IDUI_RPO_OFF = { 'id':1346, 'str':"OFF", 'state':True }
    RPO_RADIOTABS = [ IDUI_RPO_ON, IDUI_RPO_OFF ]

    IDUI_WF_ON  = { 'id':1347, 'str':"ON", 'state':False }
    IDUI_WF_OFF = { 'id':1348, 'str':"OFF", 'state':True }
    WIREFRAME_RADIOTABS = [ IDUI_WF_ON, IDUI_WF_OFF ]

    IDUI_AP_ON  = { 'id':1349, 'str':"ON", 'state':False }
    IDUI_AP_OFF = { 'id':1350, 'str':"OFF", 'state':True }
    ALGINPOLY_RADIOTABS = [ IDUI_AP_ON, IDUI_AP_OFF ]

    IDUI_SO_ON  = { 'id':1351, 'str':"ON", 'state':False }
    IDUI_SO_OFF = { 'id':1352, 'str':"OFF", 'state':True }
    SWITCHING_RADIOTABS = [ IDUI_SO_ON, IDUI_SO_OFF ]


    def __init__(self, arg):
        # Checks if the argument passed is a dictionary
        if not isinstance(arg, dict):
            raise TypeError("arg is not a dict.")
        self.UIDATA_PARAMETERS = arg

        self.data1 = None
        self.TabBar = g_lib.AddCustomQuickTab_GUI(self)
        self.LBL = g_lib.AddLinkBoxInExcludeList_GUI(self)
        self.LB = g_lib.AddCustomLinkBox_GUI(self)

    def CreateLayout(self):
        ui = self

        #self.AddCheckbox(0, c4d.BFH_MASK, 0, 0, "hi")
        #ui.AddButton(0, c4d.BFH_SCALEFIT, 0, 10, " Create Group")
        ui.GroupBegin(ui.UI_OVERALL_GRP, c4d.BFH_MASK, 1, 0, "")

        ui.AddStaticText(0, c4d.BFH_RIGHT, 0, 12, "v0.5", c4d.BORDER_WITH_TITLE_BOLD)

        ui.AddSeparatorH(0, c4d.BFH_SCALEFIT)

        ui.GroupBegin(0, c4d.BFH_MASK, 2, 0, "", c4d.BFV_GRIDGROUP_EQUALCOLS)
        ui.GroupBorderSpace(5,5,5,5)
        ui.AddStaticText(0, c4d.BFH_RIGHT, 0, 12, "P A I N T  M O D E :", c4d.BORDER_WITH_TITLE_BOLD)
        ui.AddComboBox(ui.IDUI_MODE_MENU, c4d.BFH_SCALEFIT, 140, 20, False)
        for eachMode in ui.IDUI_MODE_MENU_LIST:
            itemMode = ui.IDUI_MODE_MENU_LIST.get(str(eachMode))           
            ui.AddChild(ui.IDUI_MODE_MENU, itemMode['id'],  itemMode['icon']+str(eachMode))        
        ui.GroupEnd()

        ui.AddSeparatorH(0, c4d.BFH_SCALEFIT)

        ui.TabGroupBegin(ui.IDUI_PANEL_TABS, c4d.BFH_SCALEFIT, c4d.TAB_TABS)

        def ToolGRP_TAB():

            ui.GroupBegin(ui.IDUI_TOOLPANEL_TAB, c4d.BFH_MASK, 1, 0, "Tool Settings")

            def ObjectsGrp():
                ui.TabBar.Add_BarTitle_GUI(bar_id=0, bar_name=" Clone and Surface Objects:", width=100, height=13, ui_color=g_lib.BG_LightDarker_COL)
                ui.GroupBegin(0, c4d.BFH_SCALEFIT, 3, 0, "")
                ui.GroupBorderSpace(5,5,5,5)
                ui.GroupBegin(0, c4d.BFH_SCALEFIT|c4d.BFV_TOP, 1, 0, "")
                ui.AddStaticText(0, c4d.BFH_MASK|c4d.BFV_TOP, 0, 12, "Paint Objects:", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
                ui.LBL.LinkListWidget(ui.LinkBox_SourceObj, 270, 100, False)
                ui.AddCheckbox(0, c4d.BFH_MASK, 0, 0, "Use Library Selection")
                ui.GroupEnd()
                ui.AddSeparatorV(0, c4d.BFV_SCALEFIT)
                ui.GroupBegin(0, c4d.BFH_SCALEFIT|c4d.BFV_TOP, 1, 0, "")
                ui.AddStaticText(0, c4d.BFH_MASK|c4d.BFV_TOP, 0, 12, "Target Surfaces:", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
                ui.LBL.LinkListWidget(ui.IDUI_SURFACE_OBJ_LINK, 270, 120, False)
                ui.GroupEnd()
                return ui.GroupEnd()
            ObjectsGrp()

            ui.AddSeparatorH(0, c4d.BFH_SCALEFIT)

            ui.TabBar.Add_BarTitle_GUI(bar_id=0, bar_name=" Optional Settings Panel:", width=100, height=15, ui_color=c4d.Vector(0, 0.3359375, 0.45703125))

            def SettingsObjectGrp():
                ui.GroupBegin(0, c4d.BFH_MASK, 1, 0, "")
                ui.GroupBorderSpace(5,5,5,5)

                ui.TabBar.Add_BarTitle_GUI(bar_id=0, bar_name=" Randomize Paint Panel:", width=100, height=13, ui_color=g_lib.BG_LightDarker_COL)
                ui.GroupBorderSpace(5,5,5,5)
                ui.GroupBegin(0, c4d.BFH_MASK, 4, 0, "", c4d.BFV_GRIDGROUP_EQUALCOLS)
                ui.AddStaticText(0, c4d.BFH_MASK, 0, 12, "Enable Switching", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
                self.data2 = self.TabBar.Add_BarTabsRadio_GUI(7670, 100, 10, None, self.SWITCHING_RADIOTABS)                
                ui.AddStaticText(0, c4d.BFH_MASK, 0, 12, "Enable Objects", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
                self.TabBar.Add_BarTabsRadio_GUI(7669, 100, 10, None, self.RPO_RADIOTABS)   
                ui.AddStaticText(0, c4d.BFH_MASK, 0, 12, "Enable Rotation", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
                self.TabBar.Add_BarTabsRadio_GUI(7668, 100, 10, None, self.RPO_RADIOTABS)   
                ui.AddStaticText(0, c4d.BFH_MASK, 0, 12, "Enable Scale", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
                self.TabBar.Add_BarTabsRadio_GUI(7665, 100, 10, None, self.ALGINPOLY_RADIOTABS)                
                ui.GroupEnd()

                ui.AddSeparatorH(0, c4d.BFH_SCALEFIT)

                ui.TabBar.Add_BarTitle_GUI(bar_id=0, bar_name=" Brush Gizo Paint Apperance Panel:", width=100, height=13, ui_color=g_lib.BG_LightDarker_COL)
                ui.GroupBegin(0, c4d.BFH_MASK, 2, 0, "", c4d.BFV_GRIDGROUP_EQUALCOLS)                                 
                ui.AddStaticText(0, c4d.BFH_MASK, 0, 12, "Enable View GhostObject Wireframe", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
                self.data1 = self.TabBar.Add_BarTabsRadio_GUI(7661, 100, 10, None, self.WIREFRAME_RADIOTABS) 
                ui.GroupEnd()

                ui.AddSeparatorH(0, c4d.BFH_SCALEFIT)

                def BrushControlsPanel():
                    ui.TabBar.Add_BarTitle_GUI(bar_id=0, bar_name=" Brush Gizo Paint Controls Panel:", width=100, height=13, ui_color=g_lib.BG_LightDarker_COL)
                    ui.GroupBegin(0, c4d.BFH_MASK, 2, 0, "", c4d.BFV_GRIDGROUP_EQUALCOLS)
                    ui.AddStaticText(0, c4d.BFH_MASK, 0, 12, "Enable Align To Polygon Faces", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
                    self.TabBar.Add_BarTabsRadio_GUI(7666, 100, 10, None, self.ALGINPOLY_RADIOTABS)
                    ui.GroupEnd()
                    ui.GroupBegin(0, c4d.BFH_LEFT, 5, 0, "")
                    ui.AddStaticText(0, c4d.BFH_MASK, 0, 12, "Bush Mark Size:", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
                    ui.AddEditNumberArrows(ui.IDUI_MARKSCALESIZE, c4d.BFH_MASK, 130, 0)
                    ui.AddSeparatorV(0, c4d.BFV_SCALEFIT)
                    ui.AddStaticText(0, c4d.BFH_MASK, 0, 12, "Rotation Speed:", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
                    ui.AddEditNumberArrows(ui.IDUI_ROTATION_SPEED, c4d.BFH_MASK, 130, 0)
                    return ui.GroupEnd()
                BrushControlsPanel()         

                ui.AddSeparatorH(0, c4d.BFH_SCALEFIT)

                def NullObjectGrp():
                    ui.GroupBegin(0, c4d.BFH_MASK, 1, 0, "")
                    ui.TabBar.Add_BarTitle_GUI(bar_id=0, bar_name=" Create & Link Group:", width=100, height=13, ui_color=g_lib.BG_LightDarker_COL)
                    ui.GroupBegin(0, c4d.BFH_SCALEFIT, 4, 0, "")
                    ui.AddStaticText(0, c4d.BFH_RIGHT, 0, 12, "Object:", c4d.BORDER_WITH_TITLE_BOLD)
                    ui.AddCustomGui(ui.IDUI_CUSTOMGRP_OBJ_LINK, c4d.CUSTOMGUI_LINKBOX, "", c4d.BFH_MASK, 240, 12, ui.LinkBC)
                    ui.AddStaticText(0, c4d.BFH_RIGHT, 0, 12, "ID:", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
                    ui.AddEditText(ui.IDUI_GRG_OBJ_GUID, c4d.BFH_MASK, 240, 12)                
                    ui.GroupEnd()
                    return ui.GroupEnd()
                NullObjectGrp()

                return ui.GroupEnd()

            SettingsObjectGrp()                

            return ui.GroupEnd()
        ToolGRP_TAB()


        def LibraryGrp_TAB():
            ui.GroupBegin(657, c4d.BFH_SCALEFIT, 1, 0, "Tool Library")
            ui.GroupBorderSpace(5,5,5,5)
            return ui.GroupEnd()
        LibraryGrp_TAB()

        ui.GroupBegin(987, c4d.BFH_SCALEFIT, 1, 0, "Demos")
        ui.AddButton(ui.IDUI_OPEN_LIGHTPROJECT_DEMO, c4d.BFH_MASK, 0, 15, " Open Taxi Light Demo ")
        ui.GroupEnd() 

        def LibraryGrp_TAB():
            ui.GroupBegin(658, c4d.BFH_SCALEFIT, 1, 0, "Preferences")
            ui.GroupBorderSpace(5,5,5,5)
            return ui.GroupEnd()
        LibraryGrp_TAB()               

        def InstructionsGrp_TAB():
            ui.GroupBegin(ui.IDUI_INSTRUCTIONPANEL_TAB, c4d.BFH_MASK, 1, 0, "Instructions")
            ui.GroupBorderSpace(5,5,5,5)

            ui.TabBar.Add_BarTitle_GUI(bar_id=0, bar_name=" Instructions:", width=100, height=13, ui_color=g_lib.BG_GREEN_COL)
            
            ui.GroupBegin(0, c4d.BFH_SCALEFIT, 3, 0, "", c4d.BFV_GRIDGROUP_EQUALCOLS)
            ui.AddStaticText(ui.ID_STATIC_TXT, c4d.BFH_LEFT, 0, 15, "  Step 1 :", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
            ui.AddStaticText(0, c4d.BFH_RIGHT, 0, 15, "  Create a Null Group", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
            ui.AddStaticText(0, c4d.BFH_LEFT|c4d.BFH_SCALEFIT, 0, 15, "Group object is for your clone objects", c4d.BORDER_WITH_TITLE_BOLD|c4d.BORDER_IN) # Static UI Text
            
            ui.AddStaticText(ui.ID_STATIC_TXT1, c4d.BFH_LEFT, 0, 15, "  Step 2 :", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
            ui.AddStaticText(0, c4d.BFH_RIGHT, 0, 15, "  Link Custom Null Group", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
            ui.AddStaticText(0, c4d.BFH_LEFT|c4d.BFH_SCALEFIT, 0, 15, "Clone objects will go to this group after placement.", c4d.BORDER_WITH_TITLE_BOLD|c4d.BORDER_IN) # Static UI Text
            
            ui.AddStaticText(ui.ID_STATIC_TXT2, c4d.BFH_LEFT, 0, 15, "  Step 3 :", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
            ui.AddStaticText(0, c4d.BFH_RIGHT, 0, 15, "  Link Clone Object of Choice", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
            ui.AddStaticText(0, c4d.BFH_LEFT|c4d.BFH_SCALEFIT, 0, 15, "Clone object is what you will be placing down.", c4d.BORDER_WITH_TITLE_BOLD|c4d.BORDER_IN) # Static UI Text
            
            ui.AddStaticText(ui.ID_STATIC_TXT3, c4d.BFH_LEFT, 0, 15, "  Step 4 :", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
            ui.AddStaticText(0, c4d.BFH_RIGHT, 0, 15, "  Link Surface Object", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
            ui.AddStaticText(0, c4d.BFH_LEFT|c4d.BFH_SCALEFIT, 0, 15, "Object that you will be paint the object on with in the scene that you linked.", c4d.BORDER_WITH_TITLE_BOLD|c4d.BORDER_IN) # Static UI Text

            ui.AddStaticText(ui.ID_STATIC_TXT4, c4d.BFH_LEFT|c4d.BFV_TOP, 0, 15, "  Optional Step 1:", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
            ui.AddStaticText(0, c4d.BFH_RIGHT|c4d.BFV_TOP, 0, 15, "  Hold Shift Key + TAB Key", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
            ui.GroupBegin(0, c4d.BFH_SCALEFIT, 1, 0, "")
            ui.AddStaticText(0, c4d.BFH_LEFT|c4d.BFH_SCALEFIT, 0, 15, "To do a smooth rotating the object to the right.", c4d.BORDER_WITH_TITLE_BOLD|c4d.BORDER_IN) # Static UI Text  
            ui.AddStaticText(0, c4d.BFH_LEFT|c4d.BFH_SCALEFIT, 0, 15, "To through the rotation (Hold Shift Key + Pressed Tab Key).", c4d.BORDER_WITH_TITLE_BOLD|c4d.BORDER_IN) # Static UI Text
            ui.GroupEnd()

            ui.AddStaticText(ui.ID_STATIC_TXT5, c4d.BFH_LEFT|c4d.BFV_TOP, 0, 15, "  Optional Step 2:", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
            ui.AddStaticText(0, c4d.BFH_RIGHT|c4d.BFV_TOP, 0, 15, "  Hold Ctrl Key + TAB Key", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
            ui.GroupBegin(0, c4d.BFH_SCALEFIT, 1, 0, "")
            ui.AddStaticText(0, c4d.BFH_LEFT|c4d.BFH_SCALEFIT, 0, 15, "To do a smooth rotating the object to the left.", c4d.BORDER_WITH_TITLE_BOLD|c4d.BORDER_IN) # Static UI Text
            ui.AddStaticText(0, c4d.BFH_LEFT|c4d.BFH_SCALEFIT, 0, 15, "To through the rotation (Hold Ctrl Key + Pressed Tab Key).", c4d.BORDER_WITH_TITLE_BOLD|c4d.BORDER_IN) # Static UI Text
            ui.GroupEnd()

            return ui.GroupEnd()
        #InstructionsGrp_TAB()

        ui.GroupEnd()
        ui.GroupEnd() 
        return True

    def InitValues(self):
        # Defines the default values
        self.Enable(self.LinkOBJID_STR, False)
        self.Enable(self.IDUI_SURFACE_OBJ_GUID, False)
        self.Enable(self.IDUI_GRG_OBJ_GUID, False)
        self.Enable(7665, False)
        self.Enable(7666, False)
        self.Enable(7667, False)
        self.Enable(7668, False)
        self.Enable(7669, False)                         
        #self.SetDefaultColor(self.UI_OVERALL_GRP, c4d.COLOR_BG, g_lib.BG_DEEP_DARKER)
        self.SetDefaultColor(self.IDS_VER_ID, c4d.COLOR_TEXT, g_lib.DARK_BLUE_TEXT_COL)
        self.SetDefaultColor(self.IDUI_OPEN_LIGHTPROJECT_DEMO, c4d.COLOR_TEXT_BUTTON, g_lib.DARK_BLUE_TEXT_COL)
        self.SetDefaultColor(self.ID_STATIC_TXT, c4d.COLOR_TEXT, g_lib.DARK_RED_TEXT_COL)
        self.SetDefaultColor(self.ID_STATIC_TXT1, c4d.COLOR_TEXT, g_lib.DARK_RED_TEXT_COL)
        self.SetDefaultColor(self.ID_STATIC_TXT2, c4d.COLOR_TEXT, g_lib.DARK_RED_TEXT_COL)
        self.SetDefaultColor(self.ID_STATIC_TXT3, c4d.COLOR_TEXT, g_lib.DARK_RED_TEXT_COL)
        self.SetDefaultColor(self.ID_STATIC_TXT4, c4d.COLOR_TEXT, g_lib.DARK_RED_TEXT_COL)
        self.SetDefaultColor(self.ID_STATIC_TXT5, c4d.COLOR_TEXT, g_lib.DARK_RED_TEXT_COL)
        #self.LBL.SetDataToLinkBoxList(self.LinkBox_SourceObj, self.UIDATA_PARAMETERS['clonelink'])
        self.LBL.SetDataToLinkBoxList(self.IDUI_SURFACE_OBJ_LINK, self.UIDATA_PARAMETERS['surfacelink'])
        self.LB.SetDataFromLinkBox(self.IDUI_CUSTOMGRP_OBJ_LINK, self.UIDATA_PARAMETERS['grouplink'])
        self.SetString(self.IDUI_GRG_OBJ_GUID, self.UIDATA_PARAMETERS['grouplinkGUID'])
        self.SetInt32(self.IDUI_MARKSCALESIZE, self.UIDATA_PARAMETERS['brushRadusAxisMark'])
        self.SetFloat(id=self.IDUI_ROTATION_SPEED, value=self.UIDATA_PARAMETERS['rotationSpeed'], min=0.0, max=100, step=0.01, format=c4d.FORMAT_FLOAT)
        self.SetLong(self.IDUI_MODE_MENU, self.UIDATA_PARAMETERS['paintMode'])
        return True    

    def Command(self, id, msg):

        if id == self.IDUI_MARKSCALESIZE:
            self.UIDATA_PARAMETERS['brushRadusAxisMark']=self.GetFloat(self.IDUI_MARKSCALESIZE)

        if id == self.IDUI_ROTATION_SPEED:
            self.UIDATA_PARAMETERS['rotationSpeed']=self.GetFloat(self.IDUI_ROTATION_SPEED)

        if id == self.LinkBox_SourceObj:
            g_lib.RemoveObjectFromScene("GPPTool_GhostPostionMark")            
            listofObjs, amountOfObjects = self.LBL.Get_ObjectsLinkList(self.LinkBox_SourceObj, self.paintObjects)
            self.UIDATA_PARAMETERS['paintedObjects'] = listofObjs
            #if amountOfObjects < 1: 
                #return gui.MessageDialog("Sorry!\nYou can't add multi objects as yet. This feature is still in-development.")
            #else:
            #    for obj in listofObjs:
            self.UIDATA_PARAMETERS['clonelink'] = listofObjs[0]['OBJ'] 
            self.UIDATA_PARAMETERS['cloneGUID'] = listofObjs[0]['GUID']          
            print(listofObjs)
            #paintObjects = []
            listofObjs = []

        if id == self.IDUI_SURFACE_OBJ_LINK:
            listofObjs, amountOfObjects = self.LBL.Get_ObjectsLinkList(self.IDUI_SURFACE_OBJ_LINK, self.surfaceObjects)
            if amountOfObjects < 1: 
                return gui.MessageDialog("Sorry!\nYou can't add multi objects as yet. This feature is still in-development.")
            else:
                for obj in listofObjs:
                    self.UIDATA_PARAMETERS['surfacelink'] = obj['OBJ'] 
                    self.UIDATA_PARAMETERS['surfaceGUID'] = obj['GUID']          
                print(listofObjs)            
            surfaceObjects = []
            listofObjs = []

        if id == self.IDUI_CUSTOMGRP_OBJ_LINK:
           obj, objName, objID = self.LB.GetDataFromLinkBox(self.IDUI_CUSTOMGRP_OBJ_LINK)
           self.UIDATA_PARAMETERS['grouplink'] = obj
           self.UIDATA_PARAMETERS['grouplinkGUID'] = objID
           self.SetString(self.IDUI_GRG_OBJ_GUID, str(objID))
           print("Linked Object:{0} | ID:{1} | GUI-ID:{2}").format( objName, objID, id )    

        if id == self.IDUI_MODE_MENU:
            for i in self.IDUI_MODE_MENU_LIST:
                getModeID = self.IDUI_MODE_MENU_LIST.get(str(i))
                if self.GetLong(self.IDUI_MODE_MENU) == getModeID["id"]:
                    self.UIDATA_PARAMETERS['paintMode']=getModeID["id"]

        if id == self.IDUI_OPEN_LIGHTPROJECT_DEMO:
            doc = c4d.documents.GetActiveDocument()
            path = g_lib.fcsDIR.plugin_c4d_demo_presets_folder
            fileName_Path = os.path.join(path, "GeoPlacerDemo.c4d")
            file_Load = c4d.documents.MergeDocument(doc, fileName_Path, c4d.SCENEFILTER_OBJECTS | c4d.SCENEFILTER_MATERIALS | c4d.SCENEFILTER_MERGESCENE)
            c4d.documents.BaseDocument(file_Load)
            c4d.EventAdd()
            c4d.CallCommand(1021385) # FCS Geo Paint Placer

        if id == 7661 and self.data1:

            state = self.TabBar.TabRadioIsSelected(widgetUI=self.data1, selectedID=self.IDUI_WF_ON['id'])
            if state == True:
                g_lib.RemoveObjectFromScene("GPPTool_GhostPostionMark")
                self.UIDATA_PARAMETERS['wireframeMode']=True
                print("WF ON")
            else:
                g_lib.RemoveObjectFromScene("GPPTool_GhostPostionMark")
                self.UIDATA_PARAMETERS['wireframeMode']=False
                print("WF OFF")          


        if id == 7670 and self.data2:

            state = self.TabBar.TabRadioIsSelected(widgetUI=self.data2, selectedID=self.IDUI_SO_ON['id'])
            if state == True:
                g_lib.RemoveObjectFromScene("GPPTool_GhostPostionMark")
                self.UIDATA_PARAMETERS['switchingObjs']=True
                print("SO ON")
            else:
                g_lib.RemoveObjectFromScene("GPPTool_GhostPostionMark")
                self.UIDATA_PARAMETERS['switchingObjs']=False
                print("SO OFF")    



        return True

class PowerLine(plugins.ToolData):

    mouseX = None
    mouseY = None    
    AttDialog = None

    defaultVal = None
    storeDefaultVal = None

    currentIndex = -1

    def __init__(self):
        self.UI_DATA = {'clonelink':None, 
                        'surfacelink':None, 
                        'grouplink':None, 
                        'grouplinkGUID':None, 
                        'surfaceGUID':None, 
                        'cloneGUID':None, 
                        'brushRadusAxisMark':20,
                        'rotationSpeed':0.1,
                        'paintMode':1,
                        'wireframeMode':False,
                        'rotobj':None,
                        'posLoc':None,
                        'switchingObjs':False}

    def GetState(self, doc):
        if doc.GetMode()==c4d.Mpaint:
            return 0
        return c4d.CMD_ENABLED    

    def InitDefaultSettings(self, doc, data):
        pass
        # expects None as return value

    def InitTool(self, doc, data, bt):
        return True

    def GhostingObjectPostionMark(self, doc, ghostObj, rot, loc):
        defaultScale = float(int(self.UI_DATA['brushRadusAxisMark']))
        wireframeMode = self.UI_DATA['wireframeMode']

        def NullGeneratorObjectInsert(obj_display, obj_col, obj_size, obj_name, parentObj, obj_loc, obj_rot, obj_PRIM):
            OBJ = c4d.BaseObject(c4d.Onull)
            OBJ.SetName(obj_name)
            OBJ[c4d.ID_BASEOBJECT_USECOLOR]=2
            OBJ[c4d.ID_BASEOBJECT_COLOR]=obj_col
            OBJ[c4d.NULLOBJECT_DISPLAY]=obj_display # Object Null Display Type:
                                                        # None = 14
                                                        # Dot = 0
                                                        # Point = 1
                                                        # Cube = 11
                                                        # Triangle = 5
                                                        # Circle = 2
                                                        # Axis = 10
                                                        # Sphere = 13
                                                        # Rectangle = 3
                            
            OBJ[c4d.NULLOBJECT_ORIENTATION]=obj_PRIM # ORIENTATION:
                                                        # Camera = 0
                                                        # XY = 1
                                                        # ZY = 2
                                                        # XZ = 3
            OBJ[c4d.NULLOBJECT_RADIUS]=obj_size
            OBJ.SetAbsPos(obj_loc)
            OBJ.SetAbsRot(obj_rot)
            if parentObj==None:
                doc.InsertObject(OBJ)
            else:
                doc.InsertObject(OBJ, parentObj)
            # N-Bit Methods
            #Bit:_________________________________________________
            # c4d.NBIT_OHIDE   Hide object/tag in Object Manager
            #                  or material in Material Manager.
            #Bit mode:____________________________________________
            # c4d.NBITCONTROL_SET              Set bit.
            # c4d.NBITCONTROL_CLEAR            Clear bit.
            # c4d.NBITCONTROL_TOGGLE           Toggle bit.
            # c4d.NBITCONTROL_PRIVATE_NODIRTY  Private.
            OBJ.ChangeNBit(c4d.NBIT_OHIDE,c4d.NBITCONTROL_SET)         
            return OBJ     

        def ShapeReSized(obj):
            if defaultScale == 20.0:
                self.defaultVal = 5.0
            elif obj[c4d.NULLOBJECT_RADIUS] == self.storeDefaultVal:
                pass
            else:
                 self.defaultVal = defaultScale/obj[c4d.NULLOBJECT_RADIUS]
                 self.storeDefaultVal = self.defaultVal

            return self.defaultVal     



        get_merge_obj = doc.SearchObject("GPPTool_GhostPostionMark")
        if get_merge_obj:
            get_merge_obj.SetAbsPos(loc)            
            radS = doc.SearchObject("GPPTool_RadusCircle")
            if radS:
                radS[c4d.NULLOBJECT_RADIUS]=defaultScale

            axisS = doc.SearchObject("GPPTool_Axis")
            if axisS:
                axisS[c4d.NULLOBJECT_RADIUS]=defaultScale

            yaxisS = doc.SearchObject("GPPTool_Y+")
            if yaxisS:
                yaxisS.SetAbsPos(c4d.Vector(0, defaultScale, 0))
                yaxisS[c4d.NULLOBJECT_RADIUS]=ShapeReSized(yaxisS)

            ZPaxisS = doc.SearchObject("GPPTool_Z+")
            if ZPaxisS:
                ZPaxisS.SetAbsPos(c4d.Vector(0, 0, defaultScale))
                ZPaxisS[c4d.NULLOBJECT_RADIUS]=ShapeReSized(ZPaxisS)

            ZNaxisS = doc.SearchObject("GPPTool_Z-")
            if ZNaxisS:
                ZNaxisS.SetAbsPos(c4d.Vector(0, 0, -defaultScale))
                ZNaxisS[c4d.NULLOBJECT_RADIUS]=ShapeReSized(ZNaxisS)

            XPaxisS = doc.SearchObject("GPPTool_X+")
            if XPaxisS:
                XPaxisS.SetAbsPos(c4d.Vector(defaultScale, 0, 0))
                XPaxisS[c4d.NULLOBJECT_RADIUS]=ShapeReSized(XPaxisS)

            XNaxisS = doc.SearchObject("GPPTool_X-")
            if XNaxisS:
                XNaxisS.SetAbsPos(c4d.Vector(-defaultScale, 0, 0))
                XNaxisS[c4d.NULLOBJECT_RADIUS]=ShapeReSized(XNaxisS)

            if rot:
                newVec = c4d.Vector(rot, 0, 0)
                get_merge_obj.SetAbsRot(newVec)

            pass
        else:
            # Create the Mark Position Setup and Instance Ghosting Object
            mainparent = NullGeneratorObjectInsert(obj_display=14, 
                                                   obj_col=c4d.Vector(0, 1, 0.933), 
                                                   obj_size=8.0, 
                                                   obj_name="GPPTool_GhostPostionMark", 
                                                   parentObj=None, 
                                                   obj_loc=loc, obj_rot=c4d.Vector(0, 0, 0),
                                                   obj_PRIM=3)

            NullGeneratorObjectInsert(obj_display=2, 
                                      obj_col=c4d.Vector(0, 1, 0.933), 
                                      obj_size=defaultScale, 
                                      obj_name="GPPTool_RadusCircle", 
                                      parentObj=mainparent, 
                                      obj_loc=c4d.Vector(0, 0, 0), obj_rot=c4d.Vector(0, 0, 0),
                                      obj_PRIM=3)

            NullGeneratorObjectInsert(obj_display=10, 
                                      obj_col=c4d.Vector(0, 1, 0.933), 
                                      obj_size=defaultScale,
                                      obj_name="GPPTool_Axis", 
                                      parentObj=mainparent, 
                                      obj_loc=c4d.Vector(0, 0, 0), obj_rot=c4d.Vector(0, 0, 0),
                                      obj_PRIM=1)

            NullGeneratorObjectInsert(obj_display=2, 
                                      obj_col=g_lib.BG_GREEN_COL, 
                                      obj_size=5.0, 
                                      obj_name="GPPTool_Y+", parentObj=mainparent, 
                                      obj_loc=c4d.Vector(0, defaultScale, 0), obj_rot=c4d.Vector(0, 0, 0), 
                                      obj_PRIM=0)

            NullGeneratorObjectInsert(obj_display=2, 
                                      obj_col=g_lib.BG_PUPLE_COL, 
                                      obj_size=5.0, 
                                      obj_name="GPPTool_Z+", parentObj=mainparent, 
                                      obj_loc=c4d.Vector(0, 0, defaultScale), obj_rot=c4d.Vector(0, 0, 0), 
                                      obj_PRIM=3)

            NullGeneratorObjectInsert(obj_display=2, 
                                      obj_col=g_lib.BG_PUPLE_COL, 
                                      obj_size=5.0, obj_name="GPPTool_Z-", 
                                      parentObj=mainparent, 
                                      obj_loc=c4d.Vector(0, 0, -defaultScale), obj_rot=c4d.Vector(0, 0, 0),
                                      obj_PRIM=3)

            NullGeneratorObjectInsert(obj_display=2, 
                                      obj_col=g_lib.BG_RED_COL, 
                                      obj_size=5.0, 
                                      obj_name="GPPTool_X+", 
                                      parentObj=mainparent, 
                                      obj_loc=c4d.Vector(defaultScale, 0, 0), obj_rot=c4d.Vector(0, 0, 0), 
                                      obj_PRIM=3)

            NullGeneratorObjectInsert(obj_display=2, 
                                      obj_col=g_lib.BG_RED_COL, 
                                      obj_size=5.0, 
                                      obj_name="GPPTool_X-", 
                                      parentObj=mainparent, 
                                      obj_loc=c4d.Vector(-defaultScale, 0, 0), obj_rot=c4d.Vector(0, 0, 0), 
                                      obj_PRIM=3)

            if ghostObj:
                ghost_obj = c4d.BaseObject(c4d.Oinstance)
                ghost_obj[c4d.INSTANCEOBJECT_LINK] = ghostObj
                ghost_obj[c4d.ID_BASEOBJECT_USECOLOR]=2
                ghost_obj[c4d.ID_BASEOBJECT_COLOR]=c4d.Vector(0, 1, 0.933)
                ghost_obj[c4d.ID_BASEOBJECT_XRAY]=1
                doc.InsertObject(ghost_obj, mainparent)
                ghost_obj.ChangeNBit(c4d.NBIT_OHIDE,c4d.NBITCONTROL_SET)
                tagID = c4d.Tdisplay # Display Tag ID
                ItemType_Tag = c4d.BaseTag(tagID)
                ItemType_Tag[c4d.DISPLAYTAG_AFFECT_DISPLAYMODE] = True
                ItemType_Tag[c4d.DISPLAYTAG_SDISPLAYMODE] = 2
                ItemType_Tag[c4d.DISPLAYTAG_WDISPLAYMODE] = 1
                if wireframeMode==True:
                    ItemType_Tag[c4d.ONION_USE] = True
                ghost_obj.InsertTag(ItemType_Tag)

        c4d.EventAdd()
        return True

    def KeyboardInput(self, doc, data, bd, win, msg):

        get_merge_obj = doc.SearchObject("GPPTool_GhostPostionMark")
        getloc = get_merge_obj.GetAbsRot()

        rot_speed = self.UI_DATA['rotationSpeed']

        key = msg.GetLong(c4d.BFM_INPUT_CHANNEL)
        cstr = msg.GetString(c4d.BFM_INPUT_ASC)

        # Rotate Postive
        if msg[c4d.BFM_INPUT_QUALIFIER] & c4d.QSHIFT:
            if key == c4d.KEY_TAB:
                # Do what you want.
                print("Smooth Rot")
                DATA = self.UI_DATA['rotobj'] = getloc.x + rot_speed
                print(DATA)            
                #return True to signal that the key is processed
                return True

        # Rotate Negtive
        if msg[c4d.BFM_INPUT_QUALIFIER] & c4d.QCTRL:
            if key == c4d.KEY_TAB:
                # Do what you want.
                print("Smooth Rot")
                DATA = self.UI_DATA['rotobj'] = getloc.x - rot_speed
                print(DATA)
                #return True to signal that the key is processed
                return True

        return False

    def MouseInput(self, doc, data, bd, win, msg):

        if bd != doc.GetActiveBaseDraw():
            return True

        #mx = msg[c4d.BFM_INPUT_X]
        #my = msg[c4d.BFM_INPUT_Y]
        #mz = msg[c4d.BFM_INPUT_Z]
        #vect.y = 0

        def SinglePaintMode():
            doc.StartUndo()

            o = self.UI_DATA['clonelink'].GetClone(c4d.COPYFLAGS_NO_HIERARCHY|c4d.COPYFLAGS_NO_ANIMATION|c4d.COPYFLAGS_NO_BITS)
            
            print(o)
            
            g_lib.SetObjectName(name=o.GetName(), obj=o, amount=5000)
            get_loc = doc.SearchObject("GPPTool_GhostPostionMark")
            getPos = get_loc.GetAbsPos()
            getRot = get_loc.GetAbsRot()
            o.SetAbsPos(getPos)
            o.SetAbsRot(getRot)
            doc.AddUndo(c4d.UNDOTYPE_CHANGE, o)
            doc.InsertObject(o)
            o.Message(c4d.MSG_UPDATE)
            doc.AddUndo(c4d.UNDOTYPE_NEW, o)

            custom_grp = self.UI_DATA['grouplink']
            if custom_grp:
                custom_grpID = custom_grp.GetGUID() 
                if custom_grpID == self.UI_DATA['grouplinkGUID']:
                    o.InsertUnder(custom_grp)
                    doc.AddUndo(c4d.UNDOTYPE_CHANGE, o)

            doc.AddUndo(c4d.UNDOTYPE_NEW, o)

            doc.EndUndo()
            c4d.EventAdd()
            c4d.DrawViews(c4d.DA_ONLY_ACTIVE_VIEW | c4d.DA_NO_THREAD | c4d.DA_NO_ANIMATION)

            return True

        shift = c4d.QSHIFT
        ctrl = c4d.QCTRL

        if self.UI_DATA['switchingObjs'] == True & msg[c4d.BFM_INPUT_QUALIFIER] == shift or msg[c4d.BFM_INPUT_QUALIFIER] == ctrl:
            
            paintObjects = self.AttDialog.paintObjects
            if self.AttDialog is None:
                print("Att returned null force exiting mmethod!")
                pass

            if msg[c4d.BFM_INPUT_QUALIFIER] & shift:
                if msg[c4d.BFM_INPUT_CHANNEL] == c4d.BFM_INPUT_MOUSELEFT:
                    g_lib.RemoveObjectFromScene("GPPTool_GhostPostionMark")
                    self.currentIndex = self.currentIndex + 1
                    index = len(paintObjects) - 1
                    if self.currentIndex > index:
                        self.currentIndex = index
                        pass 
                    print(paintObjects[self.currentIndex])
                    self.UI_DATA['clonelink'] = paintObjects[self.currentIndex]['OBJ']
                    self.GhostingObjectPostionMark(doc, ghostObj=self.UI_DATA['clonelink'], rot=self.UI_DATA['rotobj'], loc=self.UI_DATA['posLoc'])            

            if msg[c4d.BFM_INPUT_QUALIFIER] & ctrl:
                if msg[c4d.BFM_INPUT_CHANNEL] == c4d.BFM_INPUT_MOUSELEFT:
                    g_lib.RemoveObjectFromScene("GPPTool_GhostPostionMark")
                    self.currentIndex = self.currentIndex - 1
                    index = 0
                    if self.currentIndex < index:
                        self.currentIndex = index
                        pass
                    print(paintObjects[self.currentIndex])
                    self.UI_DATA['clonelink'] = paintObjects[self.currentIndex]['OBJ']
                    self.GhostingObjectPostionMark(doc, ghostObj=self.UI_DATA['clonelink'], rot=self.UI_DATA['rotobj'], loc=self.UI_DATA['posLoc'])   
        else:
            if msg[c4d.BFM_INPUT_CHANNEL] == c4d.BFM_INPUT_MOUSELEFT:   #//the LMB is Pressed

                if self.UI_DATA['paintMode']==1:
                    SinglePaintMode()

                if self.UI_DATA['paintMode']==2:
                    c4d.gui.MessageDialog("Flow Paint Mode is not available and a work in progress.\nOnly Single paint mode is available to use.")

                if self.UI_DATA['paintMode']==3:
                    c4d.gui.MessageDialog("Scatter Paint Mode is not available and a work in progress.\nOnly Single paint mode is available to use.")

                if self.UI_DATA['paintMode']==4:
                    c4d.gui.MessageDialog("Grid Paint Mode is not available and a work in progress.\nOnly Single paint mode is available to use.")

            else:
                pass
        return True

    def GetCursorInfo(self, doc, data, bd, x, y, bc):
        self.mouseX = x
        self.mouseY = y
        if bc.GetId()==c4d.BFM_CURSORINFO_REMOVE:
            #bc.SetString(c4d.RESULT_BUBBLEHELP, plugins.GeLoadString(IDS_PRIMITIVETOOL))
            #bc.SetLong(c4d.RESULT_CURSOR, 1054515)
            bc.SetString(c4d.RESULT_BUBBLEHELP, "TOOL READY")
            return True

        if bd != doc.GetActiveBaseDraw():
            return True
        """ 
        Create a new GeRayCollider object 
        """
        ray = GeRayCollider()
        """ 
        Assign the object to a variable 
        """
        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False
        target_obj = self.UI_DATA['surfacelink'] #doc.SearchObject("Plane") #doc.GetActiveObject()
        
        if target_obj:

            hostObjMtx = target_obj.GetMg()
            points = target_obj.GetAllPoints()
            polyObj = target_obj.GetPolygonCount()
            bs = target_obj.GetPolygonS()

            ray.Init(target_obj, True)
            """
            Cal
            """
            vectTail = bd.SW(c4d.Vector(self.mouseX, self.mouseY, 0))
            vectHead = bd.SW(c4d.Vector(self.mouseX, self.mouseY, 300000.0))
            startCalVec = hostObjMtx * vectTail
            directionCalVec = (vectHead - vectTail) ^ hostObjMtx         
            """
            The direction the ray points. 
            In this case we shoot a ray out in each direction.
            """
            start_point = startCalVec     
            direction_pos = directionCalVec
            distance = 300000.0
            
            CollisionState = ray.Intersect( start_point, direction_pos, distance )

            rayResults = ray.GetNearestIntersection()

            if rayResults:
                #print("Ray Line Did Hit A Surface! " + "| Hit-State: " + str(CollisionState))
                ray_hit_pnt_pos = rayResults["hitpos"]
                faceID = rayResults["face_id"]
                faceNormal = rayResults["f_normal"].GetNormalized()
                """
                selectedPoly = None
                for index, selected in enumerate(bs.GetAll(polyObj)):
                    if not selected:
                        continue
                    else:
                        selectedPoly = target_obj.GetPolygon(faceID)            
                pntA = points[selectedPoly.a]
                pntB = points[selectedPoly.b]
                pntC = points[selectedPoly.c]
                pntD = points[selectedPoly.d]            
                polyMtx = None
                polyMtx = pos*hostObjMtx
                if pntC == pntD:
                    polyMtx.v1 = float((pntB - pntA) + 1 % faceNormal)
                else:
                    polyMtx.v1 = float((pntD - pntA) + 1 % faceNormal)

                polyMtx.v2 = faceNormal
                polyMtx.v3 = (polyMtx.v1 % faceNormal)
                """                
                pos = hostObjMtx*ray_hit_pnt_pos
                self.UI_DATA['posLoc']=pos
                self.GhostingObjectPostionMark(doc, ghostObj=self.UI_DATA['clonelink'], rot=self.UI_DATA['rotobj'], loc=self.UI_DATA['posLoc'])
            else:
                #print("Ray Line Did Not Hit Surface!")                
                ray_hit_pnt_pos = None
                g_lib.RemoveObjectFromScene("GPPTool_GhostPostionMark")
                pass
        else:
            g_lib.RemoveObjectFromScene("GPPTool_GhostPostionMark")
            pass

        c4d.DrawViews(c4d.DA_ONLY_ACTIVE_VIEW | c4d.DA_NO_THREAD | c4d.DA_NO_ANIMATION)

        return True

    def AllocSubDialog(self, bc):
        #PowerLineAttDialog(self.UI_DATA, self.ROTOBJ)
        self.AttDialog = PowerLineAttDialog(getattr(self, "UI_DATA", self.UI_DATA))
        """
        Called by Cinema 4D To allocate the Tool Dialog Option.
        :param bc: Currently not used.
        :type bc: c4d.BaseContainer
        :return: The allocated sub dialog.
        """        
        return self.AttDialog


if __name__ == "__main__":
    bmp = bitmaps.BaseBitmap()
    dir, file = os.path.split(__file__)
    fn = os.path.join(dir, "res", "tool_icon_81.tif")
    bmp.InitWith(fn)
    plugins.RegisterToolPlugin(id=PLUGIN_ID, 
                                str="GeoPaint4D Tool",
                                info=c4d.PLUGINFLAG_HIDEPLUGINMENU, 
                                icon=bmp, 
                                help="GeoPaint4D by (FCS)-FieldCreators Stuios</b>This tool allows you to place down objects on to\nanother object surface within the project scene.",
                                dat=PowerLine())