"""
-----------------------------------------------------------------
Global Methods Functions Operations and Enums Types 
and Plugin Directory Folder & Folders
-----------------------------------------------------------------

Global Custom GUI's Utils Functions for Dialogs UI Elements, Tools Functions and etc...

These are c4d custom gui elements. They are ui elements for your dialogs.
Instead of repeating and building the same UI element in each dialog class 
as a function and then add to the c4d CreateLayout function, and having 
long lines of code, and getting lost with in code.
Since these custom gui elements are on they own and out these dialog classes.
They are basicaly instances when they get added to the UI with their attinbes 
that hold's the data that you are passing to it.
eg(ui_instance , func_instance are self)
"""
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
from random import randint
from os import urandom
import weakref
# Cinema 4D Imports
import c4d
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as WindowDialog       # eg:( class My_Dialog(c4d.gui.GeDialog): ) to ( class My_Dialog(WindowDialog): ) Just keeping code line short.
from c4d.gui import GeDialog as dlg 
from c4d.utils import GeRayCollider
from c4d.plugins import CommandData, TagData, ObjectData
# Regex Import
import re
# XML Imports is for Saving and Loading Project Data for Plugin UI / Xml good but long functions to make.
from xml.dom import minidom
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree
# Json Import to store or load descriptions or any data / .json is easy and alot flexable and short coding.
import json
# Python Plugin System Load Modules Dynamically With importlib.
import importlib
"""
NOTE!!!:
For Development Purposes to access the your packages or modules files while coding. 
NOTE!!!: Well C4D and because file is not .py and its a .pyp will not read the module 
this way like how python would read it. The only way is import module, that you see above.
So disable this before Testing or Publishing Plugin or you will get an error like this.
Error:
    Traceback (most recent call last):
      File "'xxxxxx.pyp'", line 72, in <module>
    ImportError: No module named res._fcs_lib.fcs_module_lib  

from res.fcs_core_lib.fcs_package_module_lib.fcs_common_lib import fcs_common_global_lib as g_lib
"""
# FCS Imports Modules .py Files.
import fcs_flight_sim_sdk_lib as fs_main_lib
import fcs_json_sys_utils as jsonEditSystem
import fcs_logger_sys_utils as fcslogSystem
import fcs_c4d_register_and_enums_ids
from fcs_c4d_register_and_enums_ids import Global_Enums_IDs as ids

fcsID = ids
jsonEdit = jsonEditSystem.FCS_JsonSystem_Editor()
fs_lib = fs_main_lib
DebugMode = None
fcsDIR = None
fcsResDLG = None
fcsLog = None
import fcs_station_engine_app_lib as fcsEngine
fcsEngine = fcsEngine

#  Date System
DateT = str(datetime.date.today())

# ---------------------------------------------------------------
#               FCS Plugin Directory Folder & Folders
# ---------------------------------------------------------------
class PLUGIN_DIR(object):
    """ 
    Getting Different Directory Folders / Get Folder Dir's / Get Paths of files / .exe / UI Icons Paths and Tool Icons.
    Also some Helper Methods Functions. 
    """
    # -----------------------------------------
    #       Global Cinema 4D Directory Folders 
    #       Appdata Plugin Directory Folders
    # -----------------------------------------
    c4d_user_Appdata_dir = c4d.storage.GeGetStartupWritePath()                   # To find Cinema 4D Appdata Dir's Folder.
    c4d_app_folder = c4d.storage.GeGetC4DPath(c4d.C4D_PATH_APPLICATION)          # The complete path to the host application. eg( (Mac) e.g: '/Applications/MAXON/Cinema 4D R12/Cinema 4D.app' )
    c4d_prefs_folder = c4d.storage.GeGetC4DPath(c4d.C4D_PATH_PREFS)              # To find Cinema 4D Appdata Dir's of the Preferences Folder.
    
    plugin_prefs_folder = os.path.join(c4d_prefs_folder, 'fcs_plugin_prefs')
    plugin_Saves_folder = os.path.join(plugin_prefs_folder, 'user_xml_saves')
    FCS_PREF_CONIG = os.path.join(plugin_prefs_folder, 'FCS_PREF_CONFIG.xml')
    FCS_Tools_IDs_CONIG = os.path.join(plugin_prefs_folder, 'FCS_Tools_IDs_CONFIG.xml')
    FCS_Tools_IDs_CONIG2 = os.path.join(plugin_prefs_folder, 'FCS_Tools_IDs_CONFIG.json')
    FCS_LOGGING_CONIG = os.path.join(plugin_prefs_folder, 'FCS_Tools_Logging_Output.txt')  
    FCS_OManager = os.path.join(plugin_prefs_folder, 'OpenManager.xml')
    FCS_SPLITMY_POLY_CONIG = os.path.join(plugin_prefs_folder, 'FCS_SplitMyPoly_CONFIG.xml')
    FCS_SPLITMY_POLY_CONIG2 = os.path.join(plugin_prefs_folder, 'FCS_SplitMyPoly_CONFIG.json')
    FCS_NURBO_CONIG = os.path.join(plugin_prefs_folder, 'FCS_Nurbo_CONFIG.json')
    FCS_RRP_CONFIG_SETTINGS = os.path.join(plugin_prefs_folder, 'FCS_RRP_CONFIG.xml')
    fcsTempFile = os.path.join(c4d_prefs_folder, 'FCS_Temp.c4d')
    PSC4D_CONIG = os.path.join(plugin_prefs_folder, 'PSC4D_CONFIG.json')

    # -----------------------------------------
    #             Tools Data Saves
    # -----------------------------------------
    ini_file_RecentSelNGO = os.path.join(os.path.split(__file__)[0], plugin_prefs_folder, 'ini_AddRecentFolder.txt')
    ExportSelTogeter = os.path.join(os.path.split(__file__)[0], plugin_Saves_folder, 'ExportSelTogeter.txt')
    presetLayout = os.path.join(plugin_Saves_folder, 'AutoRenamer_Preset.xml')
    NurboBackup = os.path.join(plugin_Saves_folder, 'AutoUIBackup_Nurbo.xml')
    TogglePlay = os.path.join(os.path.split(__file__)[0], plugin_Saves_folder, 'PlayClios.path.txt')
    SameButton = os.path.join(os.path.split(__file__)[0], plugin_Saves_folder, 'SameButton.txt')
    NurboSaving = os.path.join(os.path.split(__file__)[0], plugin_Saves_folder, 'NurboSaving.txt')
    SelectNGoTakesSaving = os.path.join(os.path.split(__file__)[0], plugin_Saves_folder, 'SelectNGoTakesSaving.txt')
    DoneExportedGood = os.path.join(os.path.split(__file__)[0], plugin_Saves_folder, 'DoneExportedGood.txt')
    MessageErrorLog = os.path.join(os.path.split(__file__)[0], plugin_Saves_folder, 'MessageErrorLog.txt')
    SavingLog = os.path.join(os.path.split(__file__)[0], plugin_Saves_folder, 'SaveLog.txt')

    # -----------------------------------------
    #      Main FCS Plugin Directory Folders
    # -----------------------------------------
    def __init__(self, plugin_res):

        super(PLUGIN_DIR, self).__init__()
        
        self.res = plugin_res   

        self.plugin_database_folder = os.path.join(plugin_res)

        self.plugin_modules_folder = os.path.join(plugin_res, 'fcs_toolkit_core_lib')

        self.plugin_pipline_tool_folder = os.path.join(self.plugin_database_folder, "fcs_common_lib")

        self.plugin_presets_folder = os.path.join(self.plugin_database_folder, 'fcs_resources_lib')

        self.plugin_c4d_presets_folder = os.path.join(self.plugin_presets_folder, 'fcs_c4d_presets')

        self.plugin_ui_icons_folder = os.path.join(self.plugin_presets_folder, 'fcs_icons')

        self.plugin_c4d_texture_presets_folder = os.path.join(self.plugin_presets_folder, 'fcs_texture_images')

        self.plugin_c4d_sus_presets_folder = os.path.join(self.plugin_c4d_presets_folder, 'c4d_scale_units_presets')

        self.plugin_c4d_demo_presets_folder = os.path.join(self.plugin_c4d_presets_folder, 'c4d_demos_presets')        

        self.plugin_Station_folder = os.path.join(self.plugin_pipline_tool_folder, 'fcs_station_pipeline_engine')

        self.plugin_UVMaps_folder = os.path.join(self.plugin_presets_folder, 'uv_tex')

        self.plugin_FCS_folder = os.path.join(self.plugin_presets_folder, 'fcs_presets')

        self.plugin_CP_folder = os.path.join(self.plugin_presets_folder, 'user_presets')

        self.plugin_SimFX_folder = os.path.join(self.plugin_Station_folder, "FxList")

        self.fcs_station_xml_file = os.path.join(self.plugin_Station_folder, 'FCS_Station.xml')

        self.plugin_FCSStation_System_folder = os.path.join(self.plugin_Station_folder, 'System')

        self.plugin_FCSStation_Settings_folder = os.path.join(self.plugin_FCSStation_System_folder, 'Settings')

        self.plugin_FCSStation_Log_folder = os.path.join(self.plugin_FCSStation_System_folder, 'Log')

        self.plugin_FCSStation_Config_file = os.path.join(self.plugin_FCSStation_Settings_folder, 'config.xml')

        self.plugin_FCSStation_Log_file = os.path.join(self.plugin_FCSStation_Log_folder, 'FCSStation_logs.xml')

        self.PSC4Djsonfile = os.path.join(self.plugin_prefs_folder, 'PSC4D_CONFIG.json')

        # -----------------------------------------
        #               FS SDK Files 
        # -----------------------------------------
        self.AddFXtxt = os.path.join(self.plugin_Station_folder, 'C4DFXs_Xfile.txt')
        self.xmlXfile = os.path.join(self.plugin_Station_folder, 'C4DFXs_Xfile.xml')
        self.CovertPath = os.path.join(self.plugin_Station_folder, "ConvertXfile")
        self.FSX_file = os.path.join(self.plugin_SimFX_folder, 'FSX_Effects.txt')
        self.FSXSE_file = os.path.join(self.plugin_SimFX_folder, 'FSXSE_Effects.txt')
        self.P3D3_file = os.path.join(self.plugin_SimFX_folder, 'P3Dv3_Effects.txt')
        self.P3D4_file = os.path.join(self.plugin_SimFX_folder, 'P3Dv4_Effects.txt')

    # -----------------------------------------
    # Get Icon Image from Plugin Icons Folder
    # -----------------------------------------
    GUI_Icons = {
                'c4dfile':'fcs_c4dfile_thum.png',
                'PlayIcon':'tool_icon_playbtn.png', 
                'StopIcon':'tool_icon_stopbtn.png', 
                'RecentIcon':'tool_icon_Refolder.png', 
                'SettingsIcon':'UI_Settings_BTN.png',
                'Settings':"UI_Settings_BTN.png", 
                'Folder':"tool_icon_folder.png",
                'HelpIcon':"tool_icon_info.png",
                'InfoIcon':'tool_icon_info.png', 
                'C4D2UE4Logo':'UI_C4D2UE4.png',
                'C4D2UE4':"tool_icon_57.png",
                'C4D2UE4T2':"tool_icon_61.png", 
                'HL_G':'UI_HL_Good.png', 
                'HL_N':'UI_HL_Notting.png', 
                'HL_B':'UI_HL_Bad.png', 
                'PresetIcon':'fcs_preset_thum.png',
                'AddPIcon':'fcs_create_preset.png', 
                "Del":"icon_Del.png", 
                "Web":"tool_icon_openweb.png", 
                "PUPAbout":"tool_icon_popupAbout.png", 
                "FIcon":"icon_Folder.png", 
                "DelTrash":'tool_icon_DelFile.png', 
                "Openbox":'tool_icon_OpenBox.png', 
                "MergeIt":'tool_icon_Merging.png', 
                "AddBox":'tool_icon_AddBox.png', 
                "RefFolder":'tool_icon_Refresh.png',
                "Add_Fx_Icon":'tool_icon_add_fx.png',
                "Fx_Icon":'tool_icon_fx_Logo.png',
                "rp_add_icon":'tool_icon_rp_add.png',
                "rp_grp_icon":'tool_icon_rp_add_grp.png',
                "rp_pre_icon":'tool_icon_rp_preset_grp.png',
                "rp_clr_icon":'tool_icon_rp_clear.png',
                "rp_set_icon":'tool_icon_rp_settings.png',
                "rp_tag_icon":'tool_icon_rp_tag.png',
                "rp_tagE_icon":'tool_icon_rp_tag_enable.png',
                "rp_pre_grp_tag_icon":'tool_icon_rp_preset_item.png',
                "rp_temp_tag_icon":'tool_icon_rp_temp_item.png',
                "rp_del_icon":'tool_icon_rp_delete_btn.png',
                "tpm_icon":'tool_icon_plugM.png',
                "tpm_Disable_icon":'tool_icon_plugM_Disable.png',
                "tpm_Enable_icon":'tool_icon_plugM_Enable.png',
                "searchIcon":'search_icon_btn.png',
                "nurbologo_banner":'tool_nurbo_logo_banner.png',
                "nurbopreset_icon":'tool_Nurbo_PresetLib_icon.png',
                "nurbosweepset_icon":'tool_Nurbo_SweepSettings_icon.png', 
                "nurboangleset_icon":'tool_Nurbo_AngleSettings_icon.png',
                "nurbocut_icon":'tool_Nurbo_CutOut_icon.png',
                "checker_6":'preset_checker_6.png',
                "checker_5":'preset_checker_5.png',
                "checker_4":'preset_checker_4.png',
                "checker_3":'preset_checker_3.png',
                "checker_2":'preset_checker_2.png',
                "checker_1":'preset_checker_1.png',
                "constraint_icon":'tool_icon_31.png',
                "GPP_Icon":'tool_icon_81.tif',
                "GPP_Icon_MODE_S1":'tool_icon_82.png',
                "GPP_Icon_MODE_S2":'tool_icon_83.png'
                }
    
    def Global_Icon_Path(self, i):
        """ Global Icon Path """
        image_icon = os.path.join(i)
        return image_icon

    # -----------------------------------------
    #           Get & Load Tool Icon 
    # -----------------------------------------
    def Icon_Path(self, i):
        """ Get and Load Tool-Icon from Plugin Icon Path.  """
        image_icon = os.path.join(self.plugin_ui_icons_folder, i)
        return image_icon      

    # -----------------------------------------
    # Get Tools Save Files from xml file 
    # -----------------------------------------
    def SNG_SaveFiles(self):
        """ Select N'Go Save Files """
        folder = ""
        # Check if folder with CONIG files exists. 
        if os.path.exists(self.FCS_PREF_CONIG):

            XmlFileTree = etree.parse(self.FCS_PREF_CONIG) 

            XmlRoot = XmlFileTree.getroot()

            for get_root_data in XmlRoot.findall('Select_N_Go_Data'):

                for data in get_root_data.findall('Select_N_Go_ProjectSaves'):

                    LoadData = data.get("UserCustom_Path")

                    # If LoadData have a emty string, then load from the default FCS database dir. 
                    if LoadData == "":
                        folder = self.plugin_Saves_folder
                    else:
                        folder = LoadData
        else:
            folder = self.plugin_Saves_folder
        return folder

    def I(self, i):
        """ 
        Get Icon Image from Plugin Icons Folder
        Add your icon image to GUI_Icons{..} (dict) on above.
        eg. GUI_Icons = {
                        'myicon1':"xxx_icon.png",
                        . . . .
                        } 
        Then later in code:
        output = I(i="myicon1")
        """
        GetImagePath = os.path.join(self.plugin_ui_icons_folder, self.GUI_Icons[i])
        return GetImagePath
"""[summary]
def strID(TextStringID):
    #
    GLOBAL STRINGS TEXT in the c4d_strings.str
    Calling on the IDs from the c4d_symbols.h file, to get Text String from c4d_strings.str file.
    and Convert C4D String ID to a String for Python with .Json file.
    #
    stringData = c4d.plugins.GeLoadString(TextStringID)

    jsonEdit.EditItemProperty(PLUGIN_DIR.STRING_CONVERTER, "CovertString", stringData)

    convertData = jsonEdit.GetItemProperty(PLUGIN_DIR.STRING_CONVERTER, "CovertString")

    newString = convertData.replace('\\n', '\n')

    return newString
"""
def FCS_Global_Initalize_Patch(res, resDLG, ActiveDebugMode):
    """
    Passing Plugin Res Dir to this module .py file
    """
    global fcsDIR
    global fcsResDLG
    global DebugMode
    global fcsLog
    fcsDIR = PLUGIN_DIR(res)
    fcsResDLG = resDLG
    DebugMode = ActiveDebugMode
    fcsLog = fcslogSystem.FCS_LOGGER(MainLogFile=fcsDIR.FCS_LOGGING_CONIG)
    return True


# ---------------------------------------------------------------------
#               Enums Types and GUI Enums
# ---------------------------------------------------------------------

#  // UI BG COLORS Tpyes  ///
"""
To get the colors, there is a tool call ( Cinema 4D Vector Color Picker)
created by Crewell Gould from FieldCreatorsStudios.
"""
BG_C4D_DEFAULT_COL = c4d.COLOR_BG
BG_C4D_Trans_COL = c4d.COLOR_TRANS
BG_BLACK_COL = c4d.Vector(0, 0, 0)
BG_RED_COL = c4d.Vector(0.71875, 0, 0)
BG_LightDarker_COL = c4d.Vector(0.125, 0.125, 0.125)
BG_GREEN_COL = c4d.Vector(0.00390625, 0.5859375, 0.125)
BG_DARK = c4d.Vector(0.1015625, 0.09765625, 0.10546875)
BG_DEEP_DARKER = c4d.Vector(0.0625, 0.05859375, 0.0625)
BG_DARKER_COL = c4d.Vector(0.1484375, 0.1484375, 0.1484375)
BG_LitDarkBlue_Col = c4d.Vector(0.08203125, 0.1015625, 0.12109375)
BG_FCS_ORANGE_COL = c4d.Vector(0.99609375, 0.26953125, 0)
DARK_BLUE_TEXT_COL = c4d.Vector(0, 0.78125, 0.99609375)
DARK_RED_TEXT_COL = c4d.Vector(0.99609375, 0, 0)
BG_PUPLE_COL = c4d.Vector(0, 0.015625, 0.99609375)

#  // The UI Window Dialog Tpyes  ///
DLG_TYPE1 = c4d.DLG_TYPE_ASYNC                          # 	Asynchronous dialog.
DLG_TYPE2 = c4d.DLG_TYPE_ASYNC_POPUPEDIT                #   Asynchronous dialog: popup dialog style (no menu bar, no window frame).
DLG_TYPE3 = c4d.DLG_TYPE_ASYNC_POPUP_RESIZEABLE         #   Asynchronous dialog: resizable popup dialog style (no menu bar).
DLG_TYPE4 = c4d.DLG_TYPE_MODAL                          #   Modal dialog.
DLG_TYPE5 = c4d.DLG_TYPE_MODAL_RESIZEABLE               #   Resizable modal dialog.
DLG_TYPE6 = c4d.DLG_TYPE_ASYNC_FULLSCREEN_WORK          #   Asynchronous dialog: fullscreen over desktop area.
DLG_TYPE7 = c4d.DLG_TYPE_ASYNC_FULLSCREEN_MONITOR       #   Asynchronous dialog: fullscreen over the whole monitor area.

#  // Plugin Flags Tpyes  //
class PluginFlags:
    """ Register Info Plugin Flags Tpyes """
    # Plugin General Flags :
    HidePlugin = c4d.PLUGINFLAG_HIDE
    HideTool = c4d.PLUGINFLAG_HIDEPLUGINMENU
    RefreshPlugin = c4d.PLUGINFLAG_REFRESHALWAYS
    SmallNodePlugin = c4d.PLUGINFLAG_SMALLNODE

    # Command Plugin Flags:
    OptionGear = c4d.PLUGINFLAG_COMMAND_OPTION_DIALOG   # A info flag / Command has additional options. The user can access them through a small gadget.

    # Tag Plugin Flags :
    TagVis = c4d.TAG_VISIBLE                            # The tag can be seen in the object manager.
    TagMul = c4d.TAG_MULTIPLE                           # Multiple copies of the tag allowed on a single object.
    TagHier = c4d.TAG_HIERARCHICAL                      # The tag works hierarchical, so that sub-objects inherit its properties (e.g. the material tag).
    TagExp = c4d.TAG_EXPRESSION                         # The tag is an expression.
    TagTem = c4d.TAG_TEMPORARY                          # Private.

    # Object Plugin Flags:
    oMod = c4d.OBJECT_MODIFIER                          # Modifier object. Deforms the surrounding object. (E.g. bend.)
    oHier = c4d.OBJECT_HIERARCHYMODIFIER                # Hierarchical modifier. Deforms the surrounding objects together with other instances in a hierarchy chain. Only the top-most instance of the plugin in a chain is called. (E.g. bones.)Hierarchical modifier. Deforms the surrounding objects together with other instances in a hierarchy chain. Only the top-most instance of the plugin in a chain is called. (E.g. bones.)
    oGen = c4d.OBJECT_GENERATOR                         # Generator object. Produces a polygonal or spline representation on its own. (E.g. primitive cube.)
    oInput = c4d.OBJECT_INPUT                           # Used in combination with OBJECT_GENERATOR. Specifies that the generator uses builds a polygon or spline, using its subobjects as input. (E.g. Sweep Subdivision Surface, Boolean.)
    oPart = c4d.OBJECT_PARTICLEMODIFIER                 # Particle modifier.
    oSpline = c4d.OBJECT_ISSPLINE                       # The object is a spline.
    oCamera = c4d.OBJECT_CAMERADEPENDENT                # Camera dependent.
    oPointObj = c4d.OBJECT_POINTOBJECT                  # Point Object.
    oPolyObj = c4d.OBJECT_POLYGONOBJECT                 # Polygon object.

#  // Image Filters Types  //
class ImageFilters:
    """ All Image Filters Different Types eg(.png, .tiff, .bmp, etc..)"""
    PNG = c4d.FILTER_PNG

#  // Cinema 4D SCENESAVER Filters Tpyes //
def ObjFilter(get_ver):
    """ // Check if which Obj install  // """
    OBJ1 = {'export_id':1030178, 'import_id':1030177}  # Obj for R17/R19 
    OBJ2 = {'export_id':1001040, 'import_id':1001039} # Obj for R14/R16 

    if get_ver == "export":
        obj = OBJ1['export_id'] or OBJ2['export_id']
    if get_ver == "import":
        obj = OBJ1['import_id'] or OBJ2['import_id']
        
    plug = c4d.plugins.FindPlugin(obj, c4d.PLUGINTYPE_SCENESAVER)     
    if plug is None:
        pass
    return obj
class c4dSceneFilter(object):
    """ The Main C4D Scene Filters, Export & Import Formats ID's eg.(fbx, 3ds, dae, etc) """

    FBX_Filter = {'id':100, 'export_id':1026370, 'import_id':1026369, 'str':"FBX (*.fbx)", 'ext':".fbx", 'extstr':".fbx"}
    DAE14_Filter = {'id':101, 'export_id':1022316, 'import_id':1022315, 'str':"COLLADA 1.4 (*.dae)", 'ext':".dae", 'extstr':".dae 1.4"}
    ABC_Filter = {'id':102, 'export_id':1028082, 'import_id':1028081, 'str':"Alembic (*.abc)", 'ext':".abc", 'extstr':".abc"}
    C4D3Ds_Filter = {'id':103, 'export_id':1001038, 'import_id':1001037, 'str':"3D Studio (*.3ds)", 'ext':".3ds", 'extstr':".3ds"}
    OBJ_Filter = {'id':104, 'export_id':ObjFilter(get_ver="export"), 'import_id':ObjFilter(get_ver="import"), 'str':"Wavefront (*.obj)", 'ext':".obj", 'extstr':".obj"}
    C4D_Filter = {'id':105, 'export_id':1001026, 'import_id':1001025, 'str':"CINEMA 4D Project(*.c4d)", 'ext':".c4d", 'extstr':".c4d"}
    X3D_Filter = {'id':106, 'export_id':1001028, 'str':"CINEMA 4D XML (*.xml)", 'ext':".xml", 'extstr':".xml"}
    C4DX_Filter = {'id':107, 'export_id':1001047, 'str':"Direct 3D (*.x)", 'ext':".x", 'extstr':".x"}
    DAE15_Filter = {'id':108, 'export_id':1025755, 'import_id':1025754, 'str':"COLLADA 1.5 (*.dae)", 'ext':".dae", 'extstr':".dae 1.5"}
    AI_Filter = {'id':109, 'export_id':1012074, 'import_id':1001045, 'str':"Illustrator (*.ai)", 'ext':".ai", 'extstr':".ai"}
    STL_Filter = {'id':110, 'export_id':1001021, 'import_id':1001020, 'str':"STL (*.stl)", 'ext':".stl", 'extstr':".stl"}
    WRL_Filter = {'id':111, 'export_id':1001034, 'import_id':1001033, 'str':"VRML 2 (*.wrl)", 'ext':".wrl", 'extstr':".wrl"}
    DXF_Filter = {'id':112, 'export_id':1001028, 'import_id':1001027, 'str':"DXF (*.dxf)", 'ext':".dxf", 'extstr':".dxf"}
    IPACS_TGI_Filter = {'id':113, 'export_id':1001959, 'str':"IPACS Scenery TGI Export", 'ext':".tgi", 'extstr':".tgi"}
    IPACS_TGC_Filter = {'id':114, 'export_id':1001957, 'str':"IPACS Scenery TSC Export", 'ext':".tsc", 'extstr':".tsc"}
    IPACS_TGC1cm_Filter = {'id':115, 'export_id':1001957, 'str':"IPACS TGI Export 1cm", 'ext':".tgi", 'extstr':".tgi 1cm"}
    IPACS_TGC1mm_Filter = {'id':116, 'export_id':1002961, 'str':"IPACS TGI Export 1mm", 'ext':".tgi", 'extstr':".tgi 1mm"}
    Corona_Filter = {'id':117, 'export_id':1036396, 'str':"Corona Proxy (*.cgeo)", 'ext':".cgeo", 'extstr':".cgeo"}
    Redshift_Filter = {'id':118, 'export_id':1038650, 'str':"Redshift Proxy (*.rs)", 'ext':".rs", 'extstr':".rs"}

    ExportFilters = [ FBX_Filter, DAE14_Filter, ABC_Filter, C4D3Ds_Filter, OBJ_Filter, C4D_Filter, X3D_Filter, C4DX_Filter, DAE15_Filter, AI_Filter, STL_Filter, 
                        WRL_Filter, DXF_Filter, IPACS_TGI_Filter, IPACS_TGC_Filter, IPACS_TGC1cm_Filter, IPACS_TGC1mm_Filter, Corona_Filter, Redshift_Filter ]
    ExportFiltersC1 = [ FBX_Filter, DAE14_Filter, ABC_Filter, C4D3Ds_Filter, OBJ_Filter]
    ExportFiltersC2 = [ C4D_Filter, X3D_Filter, C4DX_Filter, DAE15_Filter, AI_Filter, STL_Filter, WRL_Filter, DXF_Filter] 
    ExportFiltersC3 = [ IPACS_TGC_Filter, IPACS_TGC1cm_Filter, IPACS_TGC1mm_Filter, Corona_Filter, Redshift_Filter]

    ImportFilters = [ FBX_Filter, DAE14_Filter, ABC_Filter, C4D3Ds_Filter, OBJ_Filter, C4D_Filter, DAE15_Filter, AI_Filter, STL_Filter, WRL_Filter, DXF_Filter]


# ---------------------------------------------------------------------
#       Creating GUI Instance Functions UI Elements Operations 
#                          Hepler Methods. 
# ---------------------------------------------------------------------
# Create a UI Window Dialog GUI Base Class
class BaseWindowDialogUI(c4d.gui.GeDialog):
    """ The Base Window GUI Dialog Class """

    """
    Window Dialog Main Properties
    _________________________________________________
    """
    windowMainTileName = "Base Window"
    windowDialogWidthSize = None
    windowDialogHeightSize = None
    windowDialogPluginID = 0


    def BuildUI(self):
        """
        Creating Window UI Layout Sturcture.
        """
        ui = self
        pass

    def UIsettings(self):
        """ To set UI elements values for when the window is initialized. """
        ui = self
        pass

    def UIfunctions(self, id):
        """ Excuting Commands for UI Elements Functions. """
        ui = self
        pass

    def openWindowUI(self, winDlgType):
        """ To Open the UI window. """
        sWidth = self.windowDialogWidthSize 
        sHeight = self.windowDialogHeightSize         
        return self.Open(dlgtype=winDlgType, pluginid=self.windowDialogPluginID, xpos=-3, ypos=-3, defaultw=sWidth, defaulth=sHeight, subid=0)
    
    def restoreWindowUI(self, sec_ref):
        """ To Restore the UI window. """
        return self.Restore(pluginid=self.windowDialogPluginID, secret=sec_ref)

    def closeWindowUI(self):
        """ To CLOSE the UI window. """
        return self.Close()

    """
    Main C4D GeDialog GUI Class Overrides.
    _________________________________________________
    """
    def __init__(self):
        """
        The __init__ is an Constuctor and help get or set data
        into the class and passes data on from the another class.
        """
        super(BaseWindowDialogUI, self).__init__()


    def CreateLayout(self):
        """
        Window GUI elements layout that display to the User.
        """
        self.BuildUI()
        return True

    def InitValues(self):
        """
        Called when the dialog is initialized by the GUI / GUI's startup values basically.
        """
        fcsLog.DEBUG("F.C.S"+ self.windowMainTileName +" is Open Now!", False, DebugMode)
        self.SetTitle(self.windowMainTileName)
        self.UIsettings()
        return True

    def Command(self, id, msg):
        """
        Excuting Commands for UI Elements Functions.
        """
        if id:
            fcsLog.INFO(str(id), False, DebugMode)        
        self.UIfunctions(id)
        return True

    def CoreMessage(self, id, msg):
        """
        Override this function if you want to react to Cinema 4D core messages.
        The original message is stored in msg
        """
        #if id == c4d.EVMSG_CHANGE:
        #    pass
        #self.UI_COREMESSAGES
        return True

    def DestroyWindow(self):
        """
        DestroyWindow Override this method - this function is called when the dialog is
        about to be closed temporarily, for example for layout switching.
        """
        #self.UI_CLOSEFUNCTION
        print( self.windowMainTileName + " Window Dialog Close.")
        pass

# Create a Bitmap Button Custom GUI.
def Add_Image_Button_GUI(ui_instance, btn_settings):
    """
    Create a Bitmap Button Custom GUI.

    `Code Example:`
    
    button1_set = {
                    'id':`ID`,
                    'btn_look':`c4d.BORDER_NONE`,
                    'tip':`"Hi"`, 
                    'icon':`Icon_Path`,
                    'clickable':`True`, 
                    'background_col:`c4d.COLOR_TEXTFOCUS`,
                    'toggle_btn':`True`
                    }

    g_lib.Add_Image_Button_GUI(ui_instance=``self``, btn_settings=``button1_set``)

    """
    ButtonID = btn_settings['id']
    BTN_Image = btn_settings['icon']

    # Create GUI Button.
    CustomGui_UI = c4d.BaseContainer()                                # Create a new container to store the button image.

    CustomGui_UI.SetFilename(ButtonID, BTN_Image)                     # Add this location info to the conatiner

    for setting, value in btn_settings.items():                       # Adding settings from the dic that item setting is there in or added to it.

        if setting == 'btn_look':
            BorderLook =  btn_settings['btn_look']
            CustomGui_UI.SetLong(c4d.BITMAPBUTTON_BORDER, BorderLook) # Sets the border flag to look like a button look. eg.( c4d.BORDER_NONE or c4d.BORDER_THIN_OUT )

        elif setting == 'toggle_btn':
            # ToggleBTN = btn_settings['toggle_btn']
            CustomGui_UI.SetBool(c4d.BITMAPBUTTON_TOGGLE, True)       # Toggle button, like a checkbox.

        elif setting == 'clickable':
            # ClickBTN = btn_settings['clickable']
            CustomGui_UI.SetBool(c4d.BITMAPBUTTON_BUTTON, True)       # Clickable button.

        elif setting == 'background_col':
            AddBackgroundColor = btn_settings['background_col']
            CustomGui_UI.SetInt32(c4d.BITMAPBUTTON_BACKCOLOR, AddBackgroundColor) # int - Background color.

        elif setting == 'tip':
            TipInfo = btn_settings['tip']
            if TipInfo == "":
                return
            else:
                CustomGui_UI.SetString(c4d.BITMAPBUTTON_TOOLTIP, TipInfo) # Add tooltios.path. eg.[self.SetString(c4d.BITMAPBUTTON_TOOLTIP, "<b>Bold Text</b><br>New line")]

    ui_instance.SetBTN = ui_instance.AddCustomGui(ButtonID, c4d.CUSTOMGUI_BITMAPBUTTON, "", c4d.BFH_MASK, 0, 0, CustomGui_UI)
    ui_instance.SetBTN.SetImage(BTN_Image, True)
    return True

# Create a Bitmap Button Custom GUI with ID up front.
def Add_Image_Button(ui_instance, button_id, btn_settings):
    """
    Create a Bitmap Button Custom GUI with ID for UI up front.

    `Code Example:`
    
    button1_set = {
                    'btn_look':`c4d.BORDER_NONE`,
                    'tip':`"Hi"`, 
                    'icon':`Icon_Path`,
                    'clickable':`True`, 
                    'background_col:`c4d.COLOR_TEXTFOCUS`,
                    'toggle_btn':`True`
                    }

    g_lib.Add_Image_Button_GUI(ui_instance=``self``, btn_settings=``button1_set``)

    """
    BTN_Image = btn_settings['icon']

    # Create GUI Button.
    CustomGui_UI = c4d.BaseContainer()                                # Create a new container to store the button image.

    CustomGui_UI.SetFilename(button_id, BTN_Image)                     # Add this location info to the conatiner

    for setting, value in btn_settings.items():                       # Adding settings from the dic that item setting is there in or added to it.

        if setting == 'btn_look':
            BorderLook =  btn_settings['btn_look']
            CustomGui_UI.SetLong(c4d.BITMAPBUTTON_BORDER, BorderLook) # Sets the border flag to look like a button look. eg.( c4d.BORDER_NONE or c4d.BORDER_THIN_OUT )

        elif setting == 'toggle_btn':
            # ToggleBTN = btn_settings['toggle_btn']
            CustomGui_UI.SetBool(c4d.BITMAPBUTTON_TOGGLE, True)       # Toggle button, like a checkbox.

        elif setting == 'clickable':
            # ClickBTN = btn_settings['clickable']
            CustomGui_UI.SetBool(c4d.BITMAPBUTTON_BUTTON, True)       # Clickable button.

        elif setting == 'background_col':
            AddBackgroundColor = btn_settings['background_col']
            CustomGui_UI.SetInt32(c4d.BITMAPBUTTON_BACKCOLOR, AddBackgroundColor) # int - Background color.

        elif setting == 'tip':
            TipInfo = btn_settings['tip']
            if TipInfo == "":
                return
            else:
                CustomGui_UI.SetString(c4d.BITMAPBUTTON_TOOLTIP, TipInfo) # Add tooltios.path. eg.[self.SetString(c4d.BITMAPBUTTON_TOOLTIP, "<b>Bold Text</b><br>New line")]

    ui_instance.SetBTN = ui_instance.AddCustomGui(button_id, c4d.CUSTOMGUI_BITMAPBUTTON, "", c4d.BFH_MASK, 0, 0, CustomGui_UI)
    ui_instance.SetBTN.SetImage(BTN_Image, True)
    return True

# Create a Edit Text Field Filename Path Custom GUI.
class AddCustomFileNamePath_GUI(object):
    """ 
    Add a Custom FileName Path GUI Layout 
    
    
    Code Example:
    
    self.CUSTOM_FILENAME = g_lib.AddCustomFileName_GUI(self, res)
    
    
    self.CUSTOM_FILENAME.GUI(string_id=id, 
                             btn_id=id, 
                             size_w=190, 
                             size_l=10, 
                             custom_btn=None)
                             
    self.CUSTOM_FILENAME.SetStringData(string_id=id, 
                                       ld_title="JsonImages",
                                       ld_type=c4d.FILESELECTTYPE_IMAGES,
                                       ld_flags=c4d.FILESELECT_LOAD, 
                                       suffix=".json"
                                       mgs="Invaild File!\n File needs to be Json.")
                                                               
    self.CUSTOM_FILENAME.GetStringData(string_id=id)
                                    
    """
    
    def __init__(self, ui_instance):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """
        self.instance = ui_instance
  
    def GUI(self, string_id, btn_id, size_w, size_l, custom_icon_btn):
        """ GUI Layout """
        self.instance.GroupBegin(0, c4d.BFH_SCALEFIT, 2, 0, "")
        self.instance.AddEditText(string_id, c4d.BFH_SCALEFIT, size_w, size_l)
        if custom_icon_btn:
            Add_Image_Button(ui_instance=self.instance, button_id=btn_id, btn_settings=custom_icon_btn)
        else:
            self.instance.AddButton(btn_id, c4d.BFH_MASK, 0, size_l, "...")
        self.instance.GroupEnd()
        return True
    
    def ClearFileName(self, string_id):
        self.instance.SetString(string_id, "")
        return True

    def VaildFileName(self, string_id, userpath, suffix, mgs):

        data = None

        if suffix == "":

            data = userpath

        else:

            vaild_file = os.path.splitext(userpath)

            if vaild_file[1] != suffix:
                fcsLog.WARNING(mgs, True, DebugMode)
                self.ClearFileName(string_id)
                data = ""

            elif vaild_file[1] == suffix:
                data = userpath

        return data

    def VaildFileLocation(self, file_path):
        if os.path.exists(file_path):
            return True
        return False

    def GetStringData(self, string_id):
        """ Get from custom filename Input string data. """
        data = self.instance.GetString(string_id)
        return data

    def SetStringData(self, string_id, ld_title, ld_type, ld_flags, suffix, mgs):

        userpath = c4d.storage.LoadDialog(type=ld_type, 
                                          title=ld_title, 
                                          flags=ld_flags, 
                                          force_suffix=suffix )

        self.instance.SetString( string_id, self.VaildFileName(string_id, userpath, suffix, mgs) )

        return True                 

# Create ProgressBar GUI
class AddCustomProgressBar_GUI(object):
    """
    # Progress Bar GUI 

    # Add a Custom Progress Bar GUI Layout 

    # Code Example:
    
    self.CUSTOM_PROGRESSBAR = AddCustomProgressBar_GUI(ui_instance=self)
    
    # Note: If you dont want a string amount UI display then set (str_amount_id=None).
    self.CUSTOM_PROGRESSBAR.GUI(str_amount_id=id, 
                                progress_id=id, 
                                size_w=190, 
                                size_l=10 )

    # Note: If you dont want a custom color then set (col=None).                            
    self.CUSTOM_PROGRESSBAR.Run_ProgressBar(progressbar_ui_id=id, 
                                           currentItemNum=1, 
                                           amountOfItems=(Amount of Items eg:10), 
                                           col=g_lib.DARK_BLUE_TEXT_COL)  # DARK_BLUE_TEXT_COL # DARK_RED_TEXT_COL 
                                                                            color is a c4d.Vector(0,0,0). 
                                                                            eg: DARK_RED_TEXT_COL=c4d.Vector(0.99609375, 0, 0)

    self.CUSTOM_PROGRESSBAR.Stop_ProgressBar(progressbar_ui_id=id)
    """

    def __init__(self, ui_instance):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """
        self.instance = ui_instance    

    def GUI(self, str_amount_id, progress_id, size_w, size_l):
        """ Progress Bar GUI Layout """
        self.instance.GroupBegin(0, c4d.BFH_SCALEFIT, 3, 0, "")
        self.instance.GroupBorderNoTitle(c4d.BORDER_THIN_IN)
        # ProgressBar
        self.instance.AddCustomGui(progress_id, c4d.CUSTOMGUI_PROGRESSBAR, "", c4d.BFH_SCALEFIT, size_w, size_l)
        if str_amount_id:
            # Static UI Text
            self.instance.AddSeparatorV(0, c4d.BFV_SCALEFIT)
            self.instance.AddStaticText(str_amount_id, c4d.BFH_MASK, 50, size_l, " 0%", c4d.BORDER_WITH_TITLE_BOLD)
        self.instance.GroupEnd()
        return True

    def Run_ProgressBar(self, str_amount_id, progressbar_ui_id, currentItemNum, amountOfItems, col):
        """ Progress Bar Running  """
        percent = float(currentItemNum)/amountOfItems*100
        # Set Data to PROGRESSBAR
        progressMsg = c4d.BaseContainer(c4d.BFM_SETSTATUSBAR)
        progressMsg[c4d.BFM_STATUSBAR_PROGRESSON] = True
        progressMsg[c4d.BFM_STATUSBAR_PROGRESS] = percent/100.0 
        # this if you want a custom color
        if col:
            self.instance.SetDefaultColor(progressbar_ui_id, c4d.COLOR_PROGRESSBAR, col)    
        self.instance.SendMessage(progressbar_ui_id, progressMsg)
        # Return Percent String Data
        PercentData = str(int(percent))+"%"
        return self.instance.SetString(str_amount_id, " "+PercentData)
    
    def Stop_ProgressBar(self, progressbar_ui_id):
        """ Progress Bar Stop Running  """
        progressMsg = c4d.BaseContainer(c4d.BFM_SETSTATUSBAR)
        progressMsg.SetBool(c4d.BFM_STATUSBAR_PROGRESSON, False)
        self.instance.SendMessage(progressbar_ui_id, progressMsg)
        return True

# Create a QuickTab Custom GUI.
class AddCustomQuickTab_GUI(object):
    """
    This Custom GUI call a QuickTab is how to add and create a :
        - ``QuickTab Default Bar Title GUI``
        - ``QuickTab Fold Arrow Handle Bar Title GUI``
        - ``QuickTab Radio Tabs Bar Titles GUI ``
    """

    CUSTOMGUI_FLAG = c4d.CUSTOMGUI_QUICKTAB
    uiRadioTab = None

    def __init__(self, ui_instance):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """
        self.instance = ui_instance    

    def Add_BarTitle_GUI(self, bar_id, bar_name, width, height, ui_color):
        """ 
        Create a QuickTab Default Bar Title Custom GUI. 
        """
        self.instance.CustomGui_UI = c4d.BaseContainer()
        self.instance.CustomGui_UI.FlushAll()
        self.instance.CustomGui_UI.SetBool(c4d.QUICKTAB_BAR, True)
        self.instance.CustomGui_UI.SetString(c4d.QUICKTAB_BARTITLE, bar_name)
        if ui_color:
            self.instance.CustomGui_UI[c4d.QUICKTAB_BGCOLOR] = ui_color
        self.instance.AddCustomGui(bar_id, self.CUSTOMGUI_FLAG, "", c4d.BFH_SCALEFIT, width, height, self.instance.CustomGui_UI)
        return True   

    def Add_BarTitleArrowHandle_GUI(self, bar_id, bar_name, width, height, ui_color):
        """ 
        Create a QuickTab Fold Arrow Handle Bar Title Custom GUI. 
        """
        self.instance.CustomGui_UI = c4d.BaseContainer()
        self.instance.CustomGui_UI.FlushAll()
        self.instance.CustomGui_UI.SetBool(c4d.QUICKTAB_BAR, True)
        self.instance.CustomGui_UI.SetString(c4d.QUICKTAB_BARTITLE, bar_name)
        if ui_color:
            self.instance.CustomGui_UI[c4d.QUICKTAB_BARLAYERCOLOR] = ui_color
        # GUI Toggle System for QuickTab Bar Fold Icon Button.  
        # To Help with Toggle Mode of Open and Closing the Bar Grouos.path. / Its just a dummy to help with Toggling.
        # Handle as subgroup. Like bar mode, but with fold arrow icon. Implies QUICKTAB_BAR. Call QuickTabCustomGui.IsSelected()            
        self.instance.CustomGui_UI.SetBool(c4d.QUICKTAB_BARSUBGROUP, True)
         # Set the Fold Handle to Open or Close.
        self.instance.ui = self.instance.AddCustomGui(bar_id, self.CUSTOMGUI_FLAG, "", c4d.BFH_SCALEFIT, width, height, self.instance.CustomGui_UI)
        self.instance.ui.Select(0, True) # Set the Fold Handle to Open or Close.
        return True

    def Add_BarTabsRadio_GUI(self, bar_id, width, height, ui_color, list_tabs):
        """
        Create a QuickTab Radio Tabs Bar Titles Custom GUI.
        """
        self.instance.GroupBegin(0, c4d.BFH_SCALEFIT, 2, 0, "")
        self.instance.GroupSpace(0, 0)
        CustomGui_UI = c4d.BaseContainer()
        CustomGui_UI.FlushAll()
        CustomGui_UI.SetBool(c4d.QUICKTAB_BAR, False)
        CustomGui_UI.SetBool(c4d.QUICKTAB_SHOWSINGLE, True)
        CustomGui_UI.SetBool(c4d.QUICKTAB_NOMULTISELECT, True)
        self.instance.uiRadioTab = self.instance.AddCustomGui(bar_id, self.CUSTOMGUI_FLAG, "", c4d.BFH_SCALEFIT, width, height, CustomGui_UI)
        for tab in list_tabs:
            self.instance.uiRadioTab.AppendString(tab['id'], tab['str'], tab['state'])
        if ui_color:
            # c4d.COLOR_QUICKTAB_BG_ACTIVE
            # c4d.COLOR_QUICKTAB_TEXT_ACTIVE 
            # c4d.COLOR_QUICKTAB_TEXT_INACTIVE
            # c4d.COLOR_QUICKTAB_BG_INACTIVE
            self.instance.uiRadioTab.SetDefaultColor(bar_id, c4d.COLOR_QUICKTAB_BG_ACTIVE, ui_color)       
        self.instance.GroupEnd()
        return self.instance.uiRadioTab

    def TabRadioIsSelected(self, widgetUI, selectedID):
        state = None
        if widgetUI.IsSelected(selectedID):
            state = True
        else:
            state = False
        return state

# Create a Linkbox List InExcludeCustomGui Custom GUI.
class AddLinkBoxInExcludeList_GUI(object):
    """
    This GUI name is really call a c4d.gui.InExcludeCustomGui.
    / InExclude custom GUI (CUSTOMGUI_INEXCLUDE_LIST).
    Link: https://developers.maxon.net/docs/Cinema4DPythonSDK/html/modules/c4d/CustomDataType/InExcludeData/index.html
    """
    #inexclude = c4d.InExcludeData() # Create an InExcludeData class instance
    
    def __init__(self, ui_WidgetInstance):
        self.uiIns = ui_WidgetInstance

    def LinkListWidget(self, widgetID, widgetW, widgetH, enable_state_flags):
        """
        This GUI Widget
        """
        #First create a container that will hold the items we will allow to be dropped into the INEXCLUDE_LIST gizmo
        acceptedObjs = c4d.BaseContainer()
        acceptedObjs.InsData(c4d.Obase, "") # -> # Accept all objects / you change as desired
                                                # Take a look at c4d Objects Types in SDK.
                                                
        # Create another base container for the INEXCLUDE_LIST gizmo's settings and add the above container to it
        bc_IEsettings = c4d.BaseContainer()
        bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_SEND_SELCHANGE_MSG, True)
        bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_INIT_STATE, 1)
        """
        Its buttons with states for each object in the list container gui of the CUSTOMGUI_INEXCLUDE_LIST.
        feel free to enable this in the ui by seting the  enable_state_flags to True.
        """
        if enable_state_flags == True:
            bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_NUM_FLAGS, 2)
            # button 1
            bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_IMAGE_01_ON, 1039241) # -> Id Icon or Plugin Id
            bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_IMAGE_01_OFF, 1039241)
            # button 2
            bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_IMAGE_02_ON, 1039801)
            bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_IMAGE_02_OFF, 1036394)

        bc_IEsettings.SetData(c4d.DESC_ACCEPT, acceptedObjs)
        w_size = widgetW
        h_size = widgetH
        return self.uiIns.AddCustomGui(widgetID, c4d.CUSTOMGUI_INEXCLUDE_LIST, "", c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, w_size, h_size, bc_IEsettings)

    def Get_ObjectsLinkList(self, widgetID, ListOfData):
        """ Get Objects From LinkList View GUI Widget. """
        doc = c4d.documents.GetActiveDocument()
        amountOfObjects = None
        LinkList =  self.uiIns.FindCustomGui(widgetID, c4d.CUSTOMGUI_INEXCLUDE_LIST)
        Get_ObjectListData = LinkList.GetData()
        objs = Get_ObjectListData.GetObjectCount()
        if objs:
            #currentNum = 1
            amountOfObjects = objs
            for i in xrange(objs):
                # Get Data From Objects
                objData = Get_ObjectListData.ObjectFromIndex(doc, i)
                objName_data = Get_ObjectListData.ObjectFromIndex(doc, i).GetName()
                objID_data = Get_ObjectListData.ObjectFromIndex(doc, i).GetGUID()
                Data = { "OBJ":objData, "OBJNAME":objName_data, "GUID":objID_data }
                ListOfData.append(Data)
        return ListOfData, amountOfObjects

    def SetDataToLinkBoxList(self, widgetID, Data):
        """ Set Objects to LinkList View GUI Widget."""
        inexclude = c4d.InExcludeData() # Create an InExcludeData class instance
        doc = c4d.documents.GetActiveDocument()
        LinkList = self.uiIns.FindCustomGui(widgetID, c4d.CUSTOMGUI_INEXCLUDE_LIST)
        if Data:            
            inexclude.InsertObject(Data, 1)
            LinkList.SetData(inexclude)
            print("ReAdded")
        else:
            pass
        return True

# Create a Linkbox (c4d.CUSTOMGUI_LINKBOX) GUI.
class AddCustomLinkBox_GUI(object):

    def __init__(self, ui_WidgetInstance):
        self.uiIns = ui_WidgetInstance

        

    def GetDataFromLinkBox(self, widgetID):
        DATA = self.uiIns.FindCustomGui(widgetID, c4d.CUSTOMGUI_LINKBOX).GetLink()
        objDATA = DATA
        objName = DATA.GetName()
        objGUID = DATA.GetGUID()
        return objDATA, objName, objGUID
        
    def SetDataFromLinkBox(self, widgetID, Data):
        LinkWidget = self.uiIns.FindCustomGui(widgetID, c4d.CUSTOMGUI_LINKBOX)
        LinkWidget.SetLink(Data)
        return True




# Create a QuickTab Custom GUI.
def Add_QuickTab_Bar_GUI(ui_instance, bar_id, bar_name, fold, ui_state_id, width, height, color_mode, ui_color):
    """ 
    Create a Quick Bar Title Custom GUI. 

    g_lib.Add_QuickTab_Bar_GUI(ui_instance=self, bar_id=0, bar_name="Add Recent Folder Export Paths", fold=False, ui_state_id=0, width=150, height=15, color_mode=False, ui_color=0)

    """

    FoldToggle = fold

    ui_instance.CustomGui_UI = c4d.BaseContainer()

    ui_instance.GroupBegin(0, c4d.BFH_SCALEFIT, 0, 1, "")
    ui_instance.GroupSpace(0, 0)
    ui_instance.CustomGui_UI.FlushAll()
    ui_instance.CustomGui_UI.SetBool(c4d.QUICKTAB_BAR, True)
    ui_instance.CustomGui_UI.SetString(c4d.QUICKTAB_BARTITLE, bar_name)

    if color_mode == True:
        ui_instance.CustomGui_UI[c4d.QUICKTAB_BGCOLOR] = ui_color

    if FoldToggle == True:
        # GUI Toggle System for QuickTab Bar Fold Icon Button.  
        ui_instance.CustomGui_UI.SetBool(c4d.QUICKTAB_BARSUBGROUP, True)
        # To Help with Toggle Mode of Open and Closing the Bar Grouos.path. / Its just a dummy to help with Toggling.
        # I have to StaticText set 1 , so u can not see it.
        ui_instance.AddStaticText(ui_state_id, c4d.BFH_MASK, 1, 1, "c")
        ui_instance.HideElement(ui_state_id, True) # Hide UI element.

    ui_instance._add_ui_bar = ui_instance.AddCustomGui(bar_id, c4d.CUSTOMGUI_QUICKTAB, bar_name, c4d.BFH_SCALEFIT, width, height, ui_instance.CustomGui_UI)
    ui_instance._add_ui_bar.Select(0, True) # Set the Fold Handle to Open or Close.
    ui_instance.GroupEnd()
    return True
def Add_TitleBar_wIconButton(ui_instance, bar_id, bar_col, title_name, addfold, fold_state_id, btn_id, btn_icon, tooltip_info, btn_colBG):
    """
    Create a QuickTab Title Bar with Custom Icon GUI.

    `Code Example:`

    g_lib.Add_TitleBar_wIconButton(
                            ui_instance=``self``,
                            bar_id=``ID``,
                            bar_col=``c4d.Vector(0, 0, 0)``,
                            title_name=``"MyTool"``,
                            addfold=``True/False``,
                            fold_state_id=``ID``,
                            btn_id=``ID``,
                            btn_icon=``ICON_PATH``,
                            tooltip_info=``"HelloWorld"``,
                            btn_colBG=``c4d.Vector(0, 0, 0)``
                            )
    """

    btn_set = {'id':id, 
               'btn_look':c4d.BORDER_NONE, 
               'tip':tooltip_info, 
               'icon':btn_icon, 
               'clickable':True, 
               'background_col':btn_colBG, 
               'toggle_btn':True}
    Add_Image_Button_GUI(ui_instance=ui_instance, btn_settings=btn_set)
    
    ui_instance.GroupBegin(0, c4d.BFH_SCALEFIT, 2, 0, "")
    ui_instance.GroupSpace(0, 0)
    Add_QuickTab_Bar_GUI(ui_instance=ui_instance, bar_id=bar_id, bar_name=title_name, fold=addfold, ui_state_id=fold_state_id, width=150, height=15, color_mode=True, ui_color=bar_col)
    Add_Image_Button_GUI(ui_instance=ui_instance, btn_settings=btn_set)
    ui_instance.GroupEnd()
    return True          

# Create ProgressBar GUI
def Add_ProgressBar_GUI(ui_instance, progress_id, str_amount_id):
    # PROGRESSBAR
    ui_instance.GroupBegin(0, c4d.BFH_SCALEFIT, 0, 1)   
    ui_instance.GroupBorderNoTitle(c4d.BORDER_THIN_IN)
    #self.GroupBorderSpace(1, 1, 1, 1) 
    ui_instance.AddCustomGui(progress_id, c4d.CUSTOMGUI_PROGRESSBAR, "", c4d.BFH_SCALEFIT, 100, 10)
    ui_instance.AddSeparatorV(0, c4d.BFV_SCALEFIT)
    ui_instance.AddStaticText(str_amount_id, c4d.BFH_MASK, 50, 10, "", c4d.BORDER_WITH_TITLE_BOLD)    
    ui_instance.GroupEnd() # Group End           
    return True



# Create a TreeView Custom GUI and the class data function structure which is the TreeViewFunction_Data class.
def Add_TreeView_GUI(ui_instance, ui_id, group_id, tv_colums, get_folder, extension):
    """
    Create a TreeView Custom GUI and the class data function structure which is the TreeViewFunction_Data class.
    """
    ui_instance.LayoutFlushGroup(group_id) # Refresh Group UI

    column_id = {'id':101, 'col_name':"Files"}

    ##############################################################
    # Add the TreeView GUI to UI
    ##############################################################
    TreeViewCustomGui = c4d.BaseContainer()
    TreeViewCustomGui.SetLong(column_id['id'], c4d.LV_TREE)           # This helps the .SetLayout function.
    TreeViewCustomGui.SetLong(c4d.TREEVIEW_BORDER, c4d.BORDER_THIN_IN)
    TreeViewCustomGui.SetBool(c4d.TREEVIEW_HAS_HEADER, True)
    TreeViewCustomGui.SetBool(c4d.TREEVIEW_HIDE_LINES, False)
    TreeViewCustomGui.SetBool(c4d.TREEVIEW_MOVE_COLUMN, True)
    TreeViewCustomGui.SetBool(c4d.TREEVIEW_RESIZE_HEADER, True)
    TreeViewCustomGui.SetBool(c4d.TREEVIEW_FIXED_LAYOUT, True)  # Don't allow Columns to be re-ordered
    TreeViewCustomGui.SetBool(c4d.TREEVIEW_ALTERNATE_BG, True)  # Alternate Light/Dark Gray BG
    TreeViewCustomGui.SetBool(c4d.TREEVIEW_CURSORKEYS, True)    # Process Up/Down Arrow Keys
    ui_instance.AddCustomGui(ui_id, c4d.CUSTOMGUI_TREEVIEW, "", c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 300, 100, TreeViewCustomGui)

    ################################################################
    # Setup the TreeView Layout and Set initial values.
    ################################################################
    # Find CustomGui TreeView
    SetTreeView = ui_instance.FindCustomGui(ui_id, c4d.CUSTOMGUI_TREEVIEW)
    if not SetTreeView:
       return False
    # Setup TreeView and it's global data TreeView functions class, which is call TreeViewFunction_Data class. 
    TreeView_FunctionDataClass = TreeViewFunction_Data(ext=extension, get_files=get_folder)
    SetTreeView.SetRoot(None, TreeView_FunctionDataClass, None)
    SetTreeView.SetLayout(tv_colums, TreeViewCustomGui)
    SetTreeView.SetHeaderText(column_id['id'], column_id['col_name'])   
    SetTreeView.Refresh()

    ui_instance.LayoutChanged(group_id) # Update Group UI
    return True
class ListFileItem():
    """A simple class for storing an item with a name, selection, & open status."""
    def __init__(self, name=""):
        self.name = name
        self.selected = False
        self.opened = True
class TreeViewFunction_Data(c4d.gui.TreeViewFunctions):
    """Data structure for a TreeView of the Files."""
    # The __init__ is an Constuctor and help get and passes data on from the another class.
    def __init__(self, ext, get_files):
        self._XML_files_items = []

        """
        Get the data from the gui tree view funtion that was pass down into the class.
        The gui function is called Add_TreeView_GUI. 
        """
        self.end_extension = ext
        self.end_ext = self.end_extension
        self.split_ext = " " + self.end_extension
        self.folder_with_files = get_files

        # Now load data and the object that are files from the folder.
        self.LoadObjects()        

    def LoadObjects(self):
        for file in os.listdir(self.folder_with_files):
            if file.endswith(self.end_ext):                
                # Get all files and split or cut off the extension from it.
                for files in file.splitlines():
                    fileName = files.split(self.split_ext)
                    xmlflie = ListFileItem(fileName[0])
                    # Add file to menu list by append.
                    self._XML_files_items.append(xmlflie) # [0] to remove extra characters.
        return True        

    def GetFirst(self, root, userdata):
        """Returns the first Material in the document."""
        if self._XML_files_items:
            return self._XML_files_items[0]

    def GetDown(self, root, userdata, XML_files):
        return None
  
    def GetNext(self, root, userdata, XML_files):
        """Get the next files in the lic4d.storage."""
        if XML_files in self._XML_files_items:
            obj_index = self._XML_files_items.index(XML_files)
            next_index = obj_index + 1
            if next_index < len(self._XML_files_items):
                return self._XML_files_items[next_index]

    def GetPred(self, root, userdata, XML_files):
        if  XML_files in self._XML_files_items:
            obj_index = self._XML_files_items.index(XML_files)
            prev_index = obj_index - 1
            if prev_index > 0:
                return self._XML_files_items[prev_index]

    def GetName(self, root, userdata, XML_files):
        return XML_files.name
    
    def IsOpened(self, root, userdata, XML_files):
        return XML_files.opened    
        
    def IsSelected(self, root, userdata, XML_files):
        """Returns:(bool): True if *obj* is selected, False if not."""
        return XML_files.selected

    def Select(self, root, userdata, XML_files, mode):
        """Called when the user selects an element."""
        if mode == c4d.SELECTION_NEW:
            for item in self._XML_files_items:
                item.selected = False
                if item == XML_files:
                    item.selected = True
        elif mode == c4d.SELECTION_ADD:
            XML_files.selected = True
        elif mode == c4d.SELECTION_SUB:
            XML_files.selected = False

    def DoubleClick(self, root, userdata, XML_files, col, mouseinfo):
        """Called when the user double-clicks on an entry in the TreeView."""
        #Remove_And_Share_Popup_Menu(f_Name=XML_files.name, F_ext=self.SplitEXT)
        return True

# Create a full custom image to add to UI, like a banner, logo, and etc.. and register it. Then add to UI dialog.
class Image_Function_Data(c4d.gui.GeUserArea):
    
    def __init__(self, image, width, height, info_text):
        self.bmp = c4d.bitmaps.BaseBitmap()
        self.image_path = image
        self.W = width
        self.H = height
        self.text = info_text

    def GetMinSize(self):
        self.width = self.W        # eg: 245  /  WIDTH OF YOUR IMAGE FILE
        self.height = self.H       # eg: 60  /  HEIGHT OF YOUR IMAGE FILE
        return (self.width, self.height)

    def DrawMsg(self, x1, y1, x2, y2, msg):
        result, ismovie = self.bmp.InitWith(self.image_path)
        x1 = 0
        y1 = 0
        x2 = self.bmp.GetBw()
        y2 = self.bmp.GetBh()
        if result == c4d.IMAGERESULT_OK:
            self.DrawBitmap(self.bmp, 0, 0, self.bmp.GetBw(), self.bmp.GetBh(), x1, y1, x2, y2, c4d.BMP_NORMALSCALED | c4d.BMP_ALLOWALPHA)
            self.DrawSetTextCol(c4d.COLOR_TEXT,c4d.COLOR_TRANS)
            self.DrawSetFont(c4d.FONT_BOLD)            
            self.DrawText(self.text, 130, 45)

    def Redraw(self):
        result, ismovie = self.bmos.path.InitWith(self.image_path)
        x1 = 0
        y1 = 0
        x2 = self.bmp.path.GetBw()
        y2 = self.bmp.path.GetBh()
        if result == c4d.IMAGERESULT_OK:
            self.DrawBitmap(self.bmp, 0, 0, self.os.path.GetBw(), self.bmp.GetBh(), x1, y1, x2, y2, c4d.BMP_NORMALSCALED | c4d.BMP_ALLOWALPHA)
            self.DrawSetTextCol(c4d.COLOR_TEXT,c4d.COLOR_TRANS)
            self.DrawSetFont(c4d.FONT_BOLD)            
            self.DrawText(self.text, 130, 45)
            
    def Timer(self, msg):
        self.Redraw()

def Add_Image_GUI(ui_instance, image, width, height, info_text):
    """
    Create a full custom image to add to UI, like a banner, logo, and etc.. and register it. Then add to UI dialog.
    """
    # Get the GeUserArea class and pass the data to it.
    ui_instance.Image_Class = Image_Function_Data(image, width, height, info_text)
    # This is were your plugin Image Banner in group go.
    ui_instance.AddUserArea(1, c4d.BFH_CENTER)
    ui_instance.AttachUserArea(ui_instance.Image_Class, 1)
    ui_instance.Image_Class.LayoutChanged()
    return True
def Inject_Image_GUI(func_instance, injected_group_id, image, width, height, info_text):
    """ Inject your UI layout or GUI element to a empty group. """
    func_instance.LayoutFlushGroup(injected_group_id) # Refresh Group UI
    Add_Image_GUI(ui_instance=func_instance, image=image, width=width, height=height, info_text=info_text)
    func_instance.LayoutChanged(injected_group_id) # Update Group UI
    return True
class Set_Area_Image_Function_Data(c4d.gui.GeUserArea):
    
    def __init__(self, image, width, height, info_text):
        self.bmp = c4d.bitmaps.BaseBitmap()
        self.image_path = image
        self.W = width
        self.H = height
        self.text = info_text

    def GetMinSize(self):
        self.width = self.W        # eg: 245  /  WIDTH OF YOUR IMAGE FILE
        self.height = self.H       # eg: 60  /  HEIGHT OF YOUR IMAGE FILE
        return (self.width, self.height)

    def DrawMsg(self, x1, y1, x2, y2, msg):
        result, ismovie = self.bmp.InitWith(self.image_path)
        x1 = 0
        y1 = 0
        x2 = self.bmp.GetBw()
        y2 = self.bmp.GetBh()
        if result == c4d.IMAGERESULT_OK:
            self.DrawBitmap(self.bmp, 0, 0, self.W, self.H, x1, y1, x2, y2, c4d.BMP_NORMALSCALED | c4d.BMP_ALLOWALPHA)
            self.DrawSetTextCol(c4d.COLOR_TEXT,c4d.COLOR_TRANS)
            self.DrawSetFont(c4d.FONT_BOLD)            
            self.DrawText(self.text, 130, 45)



    def Redraw(self):
        result, ismovie = self.bmp.InitWith(self.image_path)
        x1 = 0
        y1 = 0
        x2 = self.bmp.GetBw()
        y2 = self.bmp.GetBh()
        if result == c4d.IMAGERESULT_OK:
            self.DrawBitmap(self.bmp, 0, 0, self.W, self.H, x1, y1, x2, y2, c4d.BMP_NORMALSCALED | c4d.BMP_ALLOWALPHA)
            self.DrawSetTextCol(c4d.COLOR_TEXT,c4d.COLOR_TRANS)
            self.DrawSetFont(c4d.FONT_BOLD)            
            self.DrawText(self.text, 130, 45)

    def Timer(self, msg):
        self.Redraw()
def Add_SizeFit_Image_GUI(ui_instance, id, image, width, height, info_text, auto_store=True):
    """
    Create a full custom image to add to UI, like a banner, logo, and etc.. and register it. Then add to UI dialog.
    """
    # Get the GeUserArea class and pass the data to it.
    ui_instance.Image_Class = Set_Area_Image_Function_Data(image, width, height, info_text)


    if auto_store:
        if not hasattr(ui_instance, '_icon_buttons'):
            ui_instance._icon_buttons = []
        ui_instance._icon_buttons.append(ui_instance.Image_Class)

    # This is were your plugin Image Banner in group go.
    ui_instance.AddUserArea(id, c4d.BFH_CENTER)
    ui_instance.AttachUserArea(ui_instance.Image_Class, id)
    ui_instance.Image_Class.LayoutChanged()
    return True

# Create a full customize GUI Button with text str and image icon.
class IconButton(c4d.gui.GeUserArea):
    
    VERSION = (1, 4)
 
    M_NOICON = 0
    M_ICONLEFT = 1
    M_ICONRIGHT = 2
    M_FULL = 3
    M_ICONUP = 4
 
    C_TEXT = c4d.COLOR_TEXT
    C_BG = c4d.COLOR_BGEDIT
    C_HIGHLIGHT = c4d.COLOR_BGFOCUS
    C_BGPRESS = c4d.COLOR_BG
 
    S_ICON = 32
    S_PADH = 1
    S_PADV = 1
 
    def __init__(self, paramid, text, icon, mode=M_ICONLEFT):
        super(IconButton, self).__init__()
        self.paramid = paramid
        self.text = text
        self.icon = icon
        self.mode = mode
        self.pressed = False
        #self.col_BG = bg_col

        self.last_t = -1
        self.mouse_in = False
        self.interval = 0.2
 
    def _CalcLayout(self):
        text_x = self.S_PADH
        text_w = self.DrawGetTextWidth(str(self.text))
        text_h = self.DrawGetFontHeight()
        icon_x = self.S_PADH
        
        
        
        width = text_w + self.S_PADH * 2
        height = max([text_h, self.S_ICON]) + self.S_PADV * 2
 
        draw_icon = True
        if self.mode == self.M_ICONLEFT:
            icon_x = self.S_PADH
            text_x = self.S_PADH + self.S_ICON + self.S_PADH
            width += self.S_ICON + self.S_PADH
            
        elif self.mode == self.M_ICONRIGHT:
            icon_x = self.GetWidth() - (self.S_PADH + self.S_ICON)
            text_x = self.S_PADH
            width += self.S_ICON + self.S_PADH
            
        elif self.mode == self.M_ICONUP:
            self.S_PADH = self.GetWidth()/2
            text_x = self.S_PADH - (text_w/2)
            icon_x = (text_x + (text_w/2)) - (self.S_ICON/2)
            width += self.S_ICON + self.S_PADH
            height += self.GetHeight() +  self.S_ICON
        else:
            draw_icon = False
 
        return locals()
 
    def _DrawIcon(self, icon, x1, y1, x2, y2):
        # Determine if the icon is a simple color.
        if not icon:
            pass
        if isinstance(icon, (int, c4d.Vector)):
            self.DrawSetPen(icon)
            self.DrawRectangle(x1, y1, x2, y2)
        # or if it is a bitmap icon.
        elif isinstance(icon, c4d.bitmaps.BaseBitmap):
            self.DrawBitmap(icon, x1, y1, (x2 - x1), (y2 - y1),
                            0, 0, icon.GetBw(), icon.GetBh(), c4d.BMP_ALLOWALPHA)
        else:
            return False
 
        return True
 
    def _GetHighlight(self):
        delta = time.time() - self.last_t
        return delta / self.interval
 
    def _GetColor(self, v):
        if isinstance(v, c4d.Vector):
            return v
        elif isinstance(v, int):
            d = self.GetColorRGB(v)
            return c4d.Vector(d['r'], d['g'], d['b']) ^ c4d.Vector(1.0 / 255)
        else:
            raise TypeError('Unexpected value of type %s' % v.__class__.__name__)
 
    def _InterpolateColors(self, x, a, b):
        if x < 0: x = 0.0
        elif x > 1.0: x = 1.0
 
        a = self._GetColor(a)
        b = self._GetColor(b)
        return a * x + b * (1 - x)

    ###################################################
    #  GeUserArea Overrides
    ###################################################
    def DrawMsg(self, x1, y1, x2, y2, msg):
        self.DrawSetFont(c4d.FONT_BOLD)
        self.OffScreenOn() # Double buffering
 
        # Draw the background color.
        bgcolor = c4d.COLOR_BG
        if self.pressed:
            bgcolor = self.C_BGPRESS
        elif self.mode == self.M_FULL and self.icon:
            bgcolor = self.icon
        else:
            h = self._GetHighlight()
            ca, cb = self.C_HIGHLIGHT, c4d.COLOR_BG
            if not self.mouse_in:
                ca, cb = cb, ca
 
            # Interpolate between these two colors.
            bgcolor = self._InterpolateColors(h, ca, cb)
 
        w, h = self.GetWidth(), self.GetHeight()
        self._DrawIcon(bgcolor, 0, 0, w, h)
        
        # Determine the drawing position and size of the
        # colored icon and the text position.
        layout = self._CalcLayout()
 
       
        if layout['draw_icon']:
            x = layout['icon_x']
            y = min([h / 2 - self.S_ICON / 2, self.S_PADV])
 
            # Determine if the icon_DrawIcon
            self._DrawIcon(self.icon, x, y, x + self.S_ICON, y + self.S_ICON)
 
        if 'draw_text':
            self.DrawSetTextCol(self.C_TEXT, c4d.COLOR_TRANS)
            x = layout['text_x']
            y = max([h / 2 - layout['text_h'] / 2, self.S_PADV])

            if self.mode == self.M_ICONUP:
                #=========================================================================================================================
                y = y + 10
            self.DrawText(str(self.text), x, y)
        vec = c4d.Vector(0.99609375, 0, 0)
        self.DrawSetPen(vec)
        self.DrawSetFont(c4d.FONT_BOLD)
        #self.DrawSetTextCol(fg, bg)
        #self.DrawBorder(c4d.BORDER_BLACK|c4d.BORDER_ROUND, x1,y1,x2-1,y2-1)

    def GetMinSize(self):
        layout = self._CalcLayout()
        return layout['width'], layout['height']
 
    def InputEvent(self, msg):
        device = msg.GetLong(c4d.BFM_INPUT_DEVICE)
        channel = msg.GetLong(c4d.BFM_INPUT_CHANNEL)
 
        catched = False
        if device == c4d.BFM_INPUT_MOUSE and channel == c4d.BFM_INPUT_MOUSELEFT:
            self.pressed = True
            catched = True
 
            # Poll the event.
            tlast = time.time()
            while self.GetInputState(device, channel, msg):
                if not msg.GetLong(c4d.BFM_INPUT_VALUE): break
 
                x, y = msg.GetLong(c4d.BFM_INPUT_X), msg.GetLong(c4d.BFM_INPUT_Y)
                map_ = self.Global2Local()
                x += map_['x']
                y += map_['y']
 
                if x < 0 or y < 0 or x >= self.GetWidth() or y >= self.GetHeight():
                    self.pressed = False
                else:
                    self.pressed = True
 
                # Do not redraw all the time, this would be useless.
                tdelta = time.time() - tlast
                if tdelta > (1.0 / 30): # 30 FPS
                    tlast = time.time()
                    self.Redraw()
 
            if self.pressed:
                # Invoke the dialogs Command() method.
                actionmsg = c4d.BaseContainer(msg)
                actionmsg.SetId(c4d.BFM_ACTION)
                actionmsg.SetLong(c4d.BFM_ACTION_ID, self.paramid)
                self.SendParentMessage(actionmsg)
 
            self.pressed = False
            self.Redraw()
 
        return catched
 
    def Message(self, msg, result):
        if msg.GetId() == c4d.BFM_GETCURSORINFO:
            if not self.mouse_in:
                self.mouse_in = True
                self.last_t = time.time()
                self.SetTimer(30)
                self.Redraw()
        return super(IconButton, self).Message(msg, result)
 
    def Timer(self, msg):
        self.GetInputState(c4d.BFM_INPUT_MOUSE, c4d.BFM_INPUT_MOUSELEFT, msg)
        g2l = self.Global2Local()
        x = msg[c4d.BFM_INPUT_X] + g2l['x']
        y = msg[c4d.BFM_INPUT_Y] + g2l['y']
 
        # Check if the mouse is still inside the user area or not.
        if x < 0 or y < 0 or x >= self.GetWidth() or y >= self.GetHeight():
            if self.mouse_in:
                self.mouse_in = False
                self.last_t = time.time()
 
        h = self._GetHighlight()
        if h < 1.0:
            self.Redraw()
        elif not self.mouse_in:
            self.Redraw()
            self.SetTimer(0)
def Add_Icon_Button_GUI(ui_instance, paramid, text, icon, mode=IconButton.M_ICONLEFT, auto_store=True):
    """
    Creates an Icon Button on the passed dialog.
    """
    ua = IconButton(paramid, text, icon, mode)
 
    if auto_store:
        if not hasattr(ui_instance, '_icon_buttons'):
            ui_instance._icon_buttons = []
        ui_instance._icon_buttons.append(ua)
 
    ui_instance.AddUserArea(paramid, c4d.BFH_CENTER)
    ui_instance.AttachUserArea(ua, paramid)
    return ua            

# Create Our Status Bar GUI
def Add_StatusBar_GUI(ui_instance, ui_id, state_col, str_id, message):
    """A module-level docstring

    Notice the comment above the docstring specifying the encoding.
    Docstrings do appear in the bytecode, so you can access this through
    the ``__doc__`` attribute. This is also what you'll see if you call
    help() on a module or any other Python object.
    """    
    ui_instance.GroupBegin(ui_id, c4d.BFH_SCALEFIT, 1, 0, "")
    ui_instance.SetDefaultColor(ui_id, c4d.COLOR_BG, state_col)
    ui_instance.AddStaticText(str_id, c4d.BFH_SCALEFIT, 0, 15, " Status : " + message, c4d.BORDER_WITH_TITLE_BOLD)
    ui_instance.GroupEnd()
    return True
def Status_GUI_Updater(ui_instance, ui_id, str_id, state_col, message):
    ui_instance.SetDefaultColor(ui_id, c4d.COLOR_BG, state_col)
    ui_instance.SetString(str_id," Status : " + message)
    return True

# Create our GUI Bottom Company Banner with Button
def Bottom_FCS_Web_GUI_Banner(ui_instance, button_id):
    ui_instance.GroupBegin(10, c4d.BFH_SCALEFIT, 1, 0)
    ui_instance.AddSeparatorH(0, flags=c4d.BFH_SCALEFIT)
    ui_instance.SetDefaultColor(10, c4d.COLOR_BG, BG_DEEP_DARKER)
    Add_Icon_Button_GUI(ui_instance, button_id, "www.FieldCreatorsStudios.com", None, IconButton.M_NOICON)
    ui_instance.GroupEnd() 
    return True

# To Inject UI layout to a empty group.
def Inject_GUI(func_instance, injected_group_id, gui_element):
    """ Inject your UI layout or GUI element to a empty group. """
    func_instance.LayoutFlushGroup(injected_group_id) # Refresh Group UI
    gui_element
    func_instance.LayoutChanged(injected_group_id) # Update Group UI
    return True
def Inject_GUI_Icon(func_instance, injected_group_id, data):
    """ Inject your UI layout or GUI element to a empty group. """
    func_instance.LayoutFlushGroup(injected_group_id) # Refresh Group UI
    Add_Image_Button_GUI(ui_instance=func_instance, btn_settings=data)
    func_instance.LayoutChanged(injected_group_id) # Update Group UI
    return True



# ---------------------------------------------------------------------
#        Instance Functions Operations Hepler Method Functions 
# ---------------------------------------------------------------------

def RemoveObjectFromScene(objName):

    doc = c4d.documents.GetActiveDocument()
    
    if not isinstance(objName, str):
        if objName:
            objName.Remove()
    else:
        doc = c4d.documents.GetActiveDocument()
        get_merge_obj = doc.SearchObject(objName)
        if get_merge_obj:
            get_merge_obj.Remove()
    return True

# Makes each object editable
def ObjectToCurrentState(obj):
    """
    Makes each object editable.
    The object stays in the document
    """
    commandid = c4d.MCOMMAND_MAKEEDITABLE
    objdoc = obj.GetDocument()
    flags = c4d.MODELINGCOMMANDFLAGS_CREATEUNDO
    mode = c4d.MODELINGCOMMANDMODE_ALL
    
    converted = utils.SendModelingCommand(command=commandid, list=[obj], doc=objdoc, flags=flags, mode=mode)
    return converted

def MakePlanePolygonObject(doc, polyObjName):
    """
    Make default c4d plane object and convert polygon object.
    """
    planeObj = c4d.BaseObject(c4d.Oplane)
    planeObj.SetName(polyObjName)
    planeObj[c4d.PRIM_PLANE_SUBW]=1       
    planeObj[c4d.PRIM_PLANE_SUBH]=1
    doc.InsertObject(planeObj)       
    ObjectToCurrentState(planeObj)
    doc.SetActiveObject(planeObj, c4d.SELECTION_NEW)
    obj = doc.GetActiveObject()
    return obj

# Getting Icon from Icon Folder.
def Get_Res_Bitmap_Image(path_loc_filename):
    bmp = c4d.bitmaps.BaseBitmap()
    result = bmp.InitWith(path_loc_filename)
    if not result or result[0] != c4d.IMAGERESULT_OK:
        return None
    return bmp

# Set the Plugin Icon Toggle State System.
def SetIconToggleState(pluginid, bmp, overwrite_alpha=True):
    icon = c4d.gui.GetIcon(pluginid)
    if not icon and bmp:
        return c4d.gui.RegisterIcon(pluginid, bmp)
    elif icon and not bmp:
        return c4d.gui.UnregisterIcon(pluginid, bmp)
    elif not icon:
        return

    ref = icon['bmp']
    w, h = icon['w'], icon['h']

    temp = c4d.bitmaps.BaseBitmap()
    if temp.Init(w, h, bmp.GetBt()) != c4d.IMAGERESULT_OK:
        return False

    bmp.ScaleIt(temp, 256, True, True)

    a1 = a2 = None
    if overwrite_alpha:
        a1 = ref.GetInternalChannel()
        a2 = temp.GetInternalChannel()
    for x in xrange(w):
        rx = x + icon['x']
        for y in xrange(h):
            ry = y + icon['y']
            ref[rx, ry] = temp[x, y]
            if a1:
                if a2:
                    alpha = temp.GetAlphaPixel(a2, x, y)
                else:
                    alpha = 255
                ref.SetAlphaPixel(a1, rx, ry, alpha)
    return True 

def LoadC4DFile(folder, c4dfile):
    """ This Function Loads c4d file. (Example: .l4d, .c4d files). """
    openfile = os.path.join(folder, c4dfile) # To open up folder Path 
    c4d.documents.LoadFile(openfile)    
    return True

# Open Preset Data folder for Tools 
def OpenDataFolder(FolderName):
    c4d.storage.ShowInFinder(FolderName, False)
    #self.Fold_Bar(openbar="o", close="None")
    return True  

# Open FCS Web Link Function 
def OpenFCSWebLink(page):
    url = "http://www.fieldcreatorsstudios.com/"+ page
    webbrowser.open(url, new=2, autoraise=True)  
    return True    

# Open Web Link Function 
def OpenWebLink(page):
    url = page
    webbrowser.open(url, new=2, autoraise=True)  
    return True   

# These Operations Functions helps the tools and scripts tools.
def Run_Tool_ID(Tool_ID):
    plug = plugins.FindPlugin(Tool_ID, c4d.PLUGINTYPE_COMMAND)     
    if plug is None:
        c4d.gui.MessageDialog("Error 404:\nSorry! You don't have this tool installed or\nThis is feature coming soon.")
        return   
    else:
        c4d.CallCommand(Tool_ID) #Call on Tool
        return True    
def Load_Character2Doc_Scene(P_MergeFile, path):
    doc = c4d.documents.GetActiveDocument()
    fileName_Path = os.path.join(path, P_MergeFile)
    file_Load = c4d.documents.MergeDocument(doc, fileName_Path, c4d.SCENEFILTER_OBJECTS | c4d.SCENEFILTER_MATERIALS | c4d.SCENEFILTER_MERGESCENE)
    c4d.documents.BaseDocument(file_Load)
    c4d.EventAdd()      
    return True      
def CheckTool_and_Add_Tool_Menu(menu, menu_ID, plug_i, plug_data):

    plug_icon = plug_i
    plug_id = plug_data["ID"]
    plug_Name = plug_data["Name"]

    plug = plugins.FindPlugin(plug_id, c4d.PLUGINTYPE_COMMAND)     
    if plug is None:
        return
    else:
        menu.SetString(menu_ID, plug_icon + plug_Name)
    return True
def RemoveEmptyNulls(obj):
    doc = c4d.documents.GetActiveDocument()
    doc.StartUndo() 
    if not obj:
        return
    RemoveEmptyNulls(obj.GetDown())
    RemoveEmptyNulls(obj.GetNext())
    if not obj.GetDown():
        if obj.GetType()== c4d.Onull:
            if not obj.GetFirstTag():
                obj.Remove()
                return
    doc.EndUndo()                 
    return True 

# Loading a build in C4D preset file. / 
def Load_C4D_Preset_File(C4D_File):
    "Load BUILD-IN C4D Set Preset File."
    c4d.documents.LoadFile(C4D_File)
    c4d.EventAdd()
    return True

# To add and create random colors to UI elements and objects.
def Add_Vector_Random_Colors():
    """
    To add and create random colors to UI elements and objects.
    """
    r = randint(0,255) / 256.0
    g = randint(0,255) / 256.0
    b = randint(0,255) / 256.0
    return c4d.Vector(r, g, b)

# Send Instructions to the Instructions Dialog of a c4d [.MessageDialog()].
def SendInstructions(Message):
    """
    Send Instructions to the Instructions Dialog of a c4d [.MessageDialog()].
    """
    # Set Read Message Error Log Text Doc.
    # Then Open Instructions_Dialog
    gui.MessageDialog(Message)
    return True 

# Toggle System function for hiding GUI element ON and OFF.    
def Toggle_On_and_Off_GUI(func_instance, ui_group, toggle_id, update_main_grp): 
    # This Method function works like this, to pass data or info to this function:
    # eg: [ QuickTab_FoldIcon_BTN_Toggle(func_instance=self, ui_group=[ID INFO], toggle_id=[ID INFO], update_main_grp=[ID INFO]) ]

    self_ui = func_instance

    self_ui.GET_TOGGLE_ID = toggle_id
    self_ui.GET_GROUP_ID = ui_group
    self_ui.GET_Overall_GRP_ID = update_main_grp

    if self_ui.GetString(self_ui.GET_TOGGLE_ID) == "Hide_UI":
        self_ui.HideElement(self_ui.GET_GROUP_ID, False) # Show
        self_ui.SetString(self_ui.GET_TOGGLE_ID, "Show_UI")
        #print "Open"

    else:
        self_ui.HideElement(self_ui.GET_GROUP_ID, True) # Hide
        self_ui.SetString(self_ui.GET_TOGGLE_ID, "Hide_UI")
        #print "Close"

    self_ui.LayoutChanged(self_ui.GET_Overall_GRP_ID) # Update UI
    return True        

def Toggle_Image_GUI_BTN(func_instance, toggle_mode, btn_id, ui_icon):
    self_ui = func_instance
    self_ui.GET_BUTTON_ID = btn_id
    # Change Button to default play icon.
    Button = self_ui.FindCustomGui(self_ui.GET_BUTTON_ID, c4d.CUSTOMGUI_BITMAPBUTTON)
    Button.SetToggleState(toggle_mode)   
    Button.SetImage(ui_icon, True)
    self_ui.LayoutChanged(10042)     
    return True

# Toggle System function for GUI Icon/image Button with Enable or Disable.
def Toggle_Image_BTN_GUI(func_instance, btn_id, toggle_id, default_icon, enable_icon, default_str_state):
    """
    Toggle System function for GUI Icon/image Button with Enable or Disable GUI Image.
    This Method function works like this, to pass data or info to this function:
    eg: [ self.GUI_BTN_Toggle(func_instance=self, btn_id=[ID INFO], toggle_id=[ID INFO], default_icon=[INFO], enable_icon=[INFO], default_str_state=[INFO])
    """
    self_ui = func_instance

    self_ui.GET_BUTTON_ID = btn_id
    self_ui.GET_TOGGLE_ID = toggle_id

    if self_ui.GetString(self_ui.GET_TOGGLE_ID) == default_str_state:
        # Change Button to enable icon.
        Button = self_ui.FindCustomGui(self_ui.GET_BUTTON_ID, c4d.CUSTOMGUI_BITMAPBUTTON)
        Button.SetToggleState(True)   
        Button.SetImage(enable_icon, True)
        self_ui.LayoutChanged(10042)                
    else:
        # Change Button to default icon.
        Button = self_ui.FindCustomGui(self_ui.GET_BUTTON_ID, c4d.CUSTOMGUI_BITMAPBUTTON)
        Button.SetToggleState(False)   
        Button.SetImage(default_icon, True)
        self_ui.LayoutChanged(10042) 
    return True

# Gettngs Layers and Layers Names From Cinema 4D Layer TreeView Manger.
MatchedLayer = None
def GetAll_Childen_Of_Layers(AllLayers, layername):
        #print "Find Layer: " + layername
        global MatchedLayer
        for layer in AllLayers:
            name = layer.GetName()
            #print "Current Layer: " + name
            
            if name == layername:
                print("Returning Layer: " + name)
                MatchedLayer = layer
                print(MatchedLayer)
            
            #print "Current Layer " + layername + " doesnt match checking for chiildren"
            
            # Recursive Checking System
            if(layer.GetChildren > 0):
                #print "Current Layer " + layer.GetName() + "has children processing....."
                GetAll_Childen_Of_Layers(layer.GetChildren(), layername)
        return True
def GetLayer(layername):
    doc = c4d.documents.GetActiveDocument()
    root = doc.GetLayerObjectRoot() #Gets the layer manager
    LayersList = root.GetChildren()
    GetAll_Childen_Of_Layers(LayersList, layername)  
    print("In GetLayer matched value is: " + str(MatchedLayer))
    return True

"""
def SetObjName(MainObj, GetEditName):
    SweepName = doc.SearchObject(GetEditName+".300000000")
    if SweepName == None:
        SweepName = GetEditName
    degit = +1
    MainObj.SetName(GetEditName + "." + str(degit))
    for N in list(reversed(range(100))):
        obj = doc.SearchObject(GetEditName + "." + str(N)) 
        if obj == None:
            MainObj.SetName(GetEditName + "." + str(N))
    return MainObj  
"""

# Set Object Name
def SetObjectName(name, obj, amount):
    """ 
    Set Object Name
    This will allow you to set name object with a digit 
    number at the end of the string.
    eg:
        Cube_M.1
        Cube_M.2
    eg: 
        SetObjectName(name="Cube_M",  # String
                      obj=Object,     # Select Object
                      amount=100      # Range Amount
                      )
        In Code:
        SetObjectName(name="Cube_M", obj=Object, amount=100)                  
    """
    doc = c4d.documents.GetActiveDocument() 
    baseName = doc.SearchObject(name +".100000000000")
    if baseName == None:
        baseName = name + ".0"
    degit = +1
    obj.SetName(name + '.' + str(degit))
    for N in list(reversed(range(amount))):
        findobj = doc.SearchObject(name + '.' + str(N)) 
        if findobj == None:            
            obj.SetName(name+ '.' + str(N)) #+ '.' + str(N)
    return obj

# Remove Children from Parent Object
def removeChildren(obj_children):
    """
    Remove Child Object
    This will allow you to remove children from Parent Object
    """
    if obj_children:
        for child in obj_children:
            child.Remove()
    return True

# Create custom Null group object and add to object manger.
def Generate_Null_Group(str_name):
    doc = c4d.documents.GetActiveDocument()
    doc.StartUndo()           
    Insert_Null = c4d.BaseObject(c4d.Onull)
    doc.AddUndo(c4d.UNDOTYPE_CHANGE, Insert_Null)
    Insert_Null[c4d.ID_BASEOBJECT_USECOLOR]=1
    Insert_Null[c4d.ID_BASEOBJECT_COLOR] = Add_Vector_Random_Colors()
    #Insert_Null[c4d.NULLOBJECT_ICONCOL]=True
    Insert_Null.SetName(str_name)
    doc.InsertObject(Insert_Null)
    doc.AddUndo(c4d.UNDOTYPE_NEW, Insert_Null)
    doc.EndUndo()        
    c4d.EventAdd()
    return True

# Add Settings Item.
def add_item_(ui, ui_id, str_info, config_file):
    add_item = ui
    CHECKBOX = '&c&'
    DISABLE_UI = '&d&' # Disable this item    

    jsonEdit = FCS_JsonSystem_Editor()

    xml_data = jsonEdit.GetItemProperties(config_file, "SweepSettings", str_info)

    if xml_data == True:
        add_item.InsData(ui_id, str_info + CHECKBOX)

    #elif xml_mode == "Disable":
    #    add_item.InsData(ui_id, str_info + DISABLE_UI)
        
    else:
        add_item.InsData(ui_id, str_info)
        
    return True
# Create Settings Checklist Menu.     
def PopupSettings_Checklist(config_file, list):

    # Main Menu
    menu = c4d.BaseContainer()

    for item_list in list:
        add_item_(ui=menu, ui_id=item_list['IDM'], str_info=item_list['StrName'], config_file=config_file)
        menu.InsData(0, '') # Append separator

    # Sub Menu 
    #submenu1 = c4d.BaseContainer()     
    #menu.InsData(ui_id, str_info + CHK)
    
    #menu.SetContainer(IDM_MENU3, submenu1)

    #self.add_item_(ui=menu, ui_id=IDM_MENU5, str_info='Cap_Start', store_state="CBC", store_mode="CBC_Mode")
    #menu.InsData(0, '')     # Append separator
    #self.add_item_(ui=menu, ui_id=IDM_MENU5, str_info='Cap_End', store_state="CBC", store_mode="CBC_Mode")
    #menu.InsData(0, '')     # Append separator
    #self.add_item_(ui=menu, ui_id=IDM_MENU5, str_info='CreateSingleObject', store_state="CBC", store_mode="CBC_Mode")

    # Finally show popup dialog
    result = gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)
    print(result)
    
    #if result == IDM_MENU1:
    #    self.Checked_Toggle(store_state="Random")
        
    #if result == IDM_MENU2:
    #    self.Checked_Toggle(store_state="AF")

    return True

# Get and Check or Remove Material.
def GetAndCheckMaterial(doc, MatName, remove_mat):
    """ 
    Get and check or remove material you are searching for. 
    """
    # Search material and Get it.
    Mat = doc.SearchMaterial(MatName)
    # Check if material is there and delete it.
    if remove_mat == True: 
        if Mat:
            Mat.Remove()
    return Mat 

# Add a Bitmap Material    
def ApplyBitmapMaterial(doc, obj, mat_name, bitmap_image, add_col, col, check_mat, mat_prez_res, add_tex_tag):
    """ Adding a Bitmap Material and a Material Texture Tag to object """

    # // Create and Set Material //
    MatName = mat_name
    Mat = check_mat

    if add_tex_tag == True:
        Material_TexTag = obj.MakeTag(c4d.Ttexture)
        Material_TexTag[c4d.TEXTURETAG_PROJECTION]=6
        Material_TexTag[c4d.TEXTURETAG_TILE]=True

    if not Mat:
        shdBitmap = c4d.BaseShader(c4d.Xbitmap)
        if shdBitmap is None:
            print("Error: Shader allocation failed.")
            return
        shdBitmap[c4d.BITMAPSHADER_FILENAME] = bitmap_image
        Material_Tex = c4d.BaseMaterial(5703)               # Generate Material
        Material_Tex[c4d.ID_BASELIST_NAME] = MatName        # Generate Material Name
        if add_col == True:
            Material_Tex[c4d.MATERIAL_USE_COLOR] = True
            Material_Tex[c4d.MATERIAL_COLOR_COLOR] = col
        Material_Tex[c4d.MATERIAL_PREVIEWSIZE]=mat_prez_res  # Set Material Resulation  Size
        Material_Tex[c4d.MATERIAL_USE_REFLECTION]=False
        Material_Tex[c4d.MATERIAL_COLOR_SHADER] = shdBitmap # Set Material Shader Tex
        Material_Tex.InsertShader(shdBitmap)                # Insert Material Shader 
        doc.InsertMaterial(Material_Tex)                    # Insert Material  
        if add_tex_tag == True:
            Material_TexTag[c4d.TEXTURETAG_MATERIAL]=Material_Tex
    else:
        if add_tex_tag == True:
            Material_TexTag[c4d.TEXTURETAG_MATERIAL]=Mat

    c4d.EventAdd()        
    return True  


###########################################
# Settings UI Popup Menu with a Checklist
###########################################

# Create .XML config.. settings file.
def CreatConfigFile(self):
    if not os.path.exists(PLUGIN_DIR().FCS_RRP_CONFIG_SETTINGS):
        # //  Create Xml Structure. //
        MakeXml_root = minidom.Document()
        AddToXML = MakeXml_root.createElement('FCS_RP_CONIG')
        MakeXml_root.appendChild(AddToXML)
        
        rpData = MakeXml_root.createElement('Settings_Data')
        # Toggle Data
        rpData.setAttribute('Random', "True")
        rpData.setAttribute('AF', "False")
        rpData.setAttribute('RR', "False")
        rpData.setAttribute('GS', "False")
        rpData.setAttribute('CBC', "False")

        rpData.setAttribute('R_Mode', "Enable")
        rpData.setAttribute('AF_Mode', "Enable")
        rpData.setAttribute('RR_Mode', "Enable")
        rpData.setAttribute('GS_Mode', "Enable")
        rpData.setAttribute('CBC_Mode', "Enable")

        AddToXML.appendChild(rpData)
        
        # Save XML CONIG
        SaveXml = MakeXml_root.toprettyxml(indent="\t")
        with open(PLUGIN_DIR().FCS_RRP_CONFIG_SETTINGS, 'w') as f:
            f.write(SaveXml)
            f.close()
    return True  
# Toggle the .xml file by editing it function.
def Toggle(self, State, store_state):
    # Parse the xml file and get the data (Creates an object of the xml file to minipulate)
    tree = etree.parse(PLUGIN_DIR().FCS_RRP_CONFIG_SETTINGS)
    # Now Edit the xml attribute by providing the tree elementName, attribute value and the new value in a string
    EditXmlAttribute(tree,"Settings_Data", store_state, str(State))
    # Now lets get the root of the xml
    root = tree.getroot()
    # Fix the xml structure from the root level
    indent(root)
    # and finally save the xml to the file
    with open(PLUGIN_DIR().FCS_RRP_CONFIG_SETTINGS, 'w') as f:
        f.write(etree.tostring(root))
    returnc
# Check item when toggle.
def Checked_Toggle(store_state):
    XML_DATA_FILES = PLUGIN_DIR().FCS_RRP_CONFIG_SETTINGS
    XmlFileTree = etree.parse(XML_DATA_FILES) 
    XmlRoot = XmlFileTree.getroot()
    for rp_data in XmlRoot.findall('Settings_Data'):
        
        xml_data = rp_data.get(store_state)
        if xml_data == "True":
            Toggle(State=False, store_state=store_state)

        else:
            Toggle(State=True, store_state=store_state)
    return True
def Checked_ModeToggle(store_mode):
    XML_DATA_FILES = PLUGIN_DIR().FCS_RRP_CONFIG_SETTINGS
    XmlFileTree = etree.parse(XML_DATA_FILES) 
    XmlRoot = XmlFileTree.getroot()
    for rp_data in XmlRoot.findall('Settings_Data'):

        xml_mode = rp_data.get(store_mode)
        if xml_mode == "Enable":
            Toggle(State="Disable", store_state=store_mode)
        else:
            Toggle(State="Enable", store_state=store_mode)
    return True


# ---------------------------------------------------------------------
#                  XML System 
# ---------------------------------------------------------------------
def GetXmlData(xml_conig, rootData, data):
    """
    g_lib.GetXmlData(xml_conig="xml.file", rootData="RootName", data="PathRoot" )
    """
    # To look in both files 
    XML_DATA_FILES = xml_conig
    XmlFileTree = etree.parse(XML_DATA_FILES) 
    XmlRoot = XmlFileTree.getroot()
    # C4D To MCX XML Data
    # Set Data
    for get_C4D2MCX_data in XmlRoot.findall(rootData):

        GetMCXData = get_C4D2MCX_data.get(data)

    return GetMCXData

# ---------------------------------------------------------------------
#                Create a Thumbnail Image
# ---------------------------------------------------------------------
def WriteBitmap(bmp, format_filter=c4d.FILTER_PNG, settings=c4d.BaseContainer()):
    mfs = storage.MemoryFileStruct()
    mfs.SetMemoryWriteMode()
    hf = storage.HyperFile()
    if hf.Open(0, mfs, c4d.FILEOPEN_WRITE, c4d.FILEDIALOG_NONE):
        if not hf.WriteImage(bmp, format_filter, settings):  
            return None
        hf.Close()
    return mfs
def RenderSystemCustomSettings(imagePath, imageWidth, imageHeight, imageFormat):
    """ Create a Thumbnail Image. """
    doc = c4d.documents.GetActiveDocument()
    if doc == None:
        return False

    c4d.StatusSetText("Adding Object to Storage please wait......")


    c4d.StatusSetSpin()
     
    #c4d.CallCommand(431000060, 431000060)                 # Viewport Solo Hierarchy
    
    # Get the User Render Data.
    GET_User_rd = doc.GetActiveRenderData().GetData()
    GET_RENDERENGINE = GET_User_rd[c4d.RDATA_RENDERENGINE]
    GET_xres = int(GET_User_rd[c4d.RDATA_XRES])
    GET_yres = int(GET_User_rd[c4d.RDATA_YRES])
    GET_FilePath = GET_User_rd[c4d.RDATA_PATH]
    GET_FileFormat = GET_User_rd[c4d.RDATA_FORMAT]
    
    #c4d.CallCommand(12151, 12151)                        # Center Object to Camera

    # Set the Render Data.   
    set_renderData = doc.GetFirstRenderData()             # c4d.documents.RenderData()

    # ------------------------------#
    # Set Render c4d.RDATA_RENDERENGINE 
    """
    RDATA_RENDERENGINE  ( LONG ) - ( Render Engine (external renderers pass their plugin ID). )

        RDATA_RENDERENGINE_STANDARD

        Full Render.

        RDATA_RENDERENGINE_PREVIEWSOFTWARE     /  ID : 1

        Software Preview.

        RDATA_RENDERENGINE_PREVIEWHARDWARE     /  ID : 300001061

        Hardware Preview.
    """

    set_renderData[c4d.RDATA_RENDERENGINE]=c4d.RDATA_RENDERENGINE_PREVIEWHARDWARE
    set_renderData[c4d.RDATA_XRES] = float(imageWidth)           # Set Dimensions 
    set_renderData[c4d.RDATA_YRES] = float(imageHeight)          # Set Dimensions
    set_renderData[c4d.RDATA_FORMAT] = imageFormat
    set_renderData[c4d.RDATA_PATH] = imagePath
    doc.SetActiveRenderData(set_renderData)

    # Get Render Data / Get Image Dimensions in the Render Data.
    rd_temp = doc.GetActiveRenderData().GetData()
    xres = int(rd_temp[c4d.RDATA_XRES])
    yres = int(rd_temp[c4d.RDATA_YRES])

    #rd_temp.SetFilename(c4d.RDATA_PATH, FileName)
    """
    Initialize the bitmap with the result size
    The resolution/Image Dimensions must match with 
    the output size of the render settings.
    """
    bmp = bitmaps.BaseBitmap()
    bmp.Init(x=xres, y=yres, depth=24)
    #bmp.Save(FileName, c4d.FILTER_PNG)
    res = documents.RenderDocument(doc, rd_temp, bmp, c4d.RENDERFLAGS_EXTERNAL) 
    if res == c4d.RENDERRESULT_OK:
        print("done")
        #bitmaps.ShowBitmap(bmp)

    #c4d.CallCommand(12148, 12148)                         # Frame Geometry

    # Set User Render Data Back.
    rd_two = doc.GetFirstRenderData()
    rd_two[c4d.RDATA_RENDERENGINE] = GET_RENDERENGINE  
    rd_two[c4d.RDATA_XRES] = float(GET_xres)
    rd_two[c4d.RDATA_YRES] = float(GET_yres)
    rd_two[c4d.RDATA_PATH] = GET_FilePath
    rd_two[c4d.RDATA_FORMAT]= GET_FileFormat
    doc.SetActiveRenderData(rd_two)
        
    #c4d.CallCommand(431000058, 431000058)                 # Viewport Solo Off
    c4d.StatusSetSpin()

    c4d.EventAdd()        
    return True
def C4D_CreateThumbnail_Image(imagePath, imageFormat):
    """ Create a Thumbnail Image. """
    doc = c4d.documents.GetActiveDocument()
    if doc == None:
        return False

    c4d.documents.SaveDocument(doc, fcsDIR.fcsTempFile, c4d.SAVEDOCUMENTFLAGS_DONTADDTORECENTLIST, 1001026)
    c4d.EventAdd()
       
    doc = c4d.documents.GetActiveDocument() # Get Doc again.
    doc[c4d.DOCUMENT_PREVIEW_COMMAND]=0
    c4d.EventAdd(c4d.EVENT_FORCEREDRAW)
    dst = bitmaps.BaseBitmap()
    dst = doc.GetDocPreviewBitmap()
    dst.Save(imagePath, imageFormat)
    c4d.EventAdd()
    return True
def C4D_DefaultLockThumbnail_Image(imageTempPath, imagePath, path, imageName):
    """ Create a Thumbnail Image. """
    doc = c4d.documents.GetActiveDocument()
    if doc == None:
        return False

    Temp_Image = path + "/" + imageName

    shutil.copy2(imageTempPath, path)

    os.rename(Temp_Image, imagePath)

    c4d.EventAdd()
    return True

# ---------------------------------------------------------------------
#                  Cinema 4D Material
# ---------------------------------------------------------------------
class C4dMat(object):
    """ C4D Material Channels """

    def m_color(self, userMat):
        if userMat[c4d.MATERIAL_USE_COLOR] == False:
            return
        else:
            return {'str':"Color", 'shader':userMat[c4d.MATERIAL_COLOR_SHADER]}
        return

    def m_alpha(self, userMat):
        if userMat[c4d.MATERIAL_USE_ALPHA] == False:
            return
        else:
            return {'str':"Alpha", 'shader':userMat[c4d.MATERIAL_ALPHA_SHADER]}
        return 
    
    def m_diffusion(self, userMat):
        if userMat[c4d.MATERIAL_USE_DIFFUSION] == False:
            return
        else:            
            return {'str':"Diffusion", 'shader':userMat[c4d.MATERIAL_DIFFUSION_SHADER]}
        return 

    def m_lum(self, userMat):
        if userMat[c4d.MATERIAL_USE_LUMINANCE] == False: 
            return
        else:            
            return {'str':"Luminance", 'shader':userMat[c4d.MATERIAL_LUMINANCE_SHADER]}
        return

    def m_norm(self, userMat):
        if userMat[c4d.MATERIAL_USE_NORMAL] == False:
            return
        else:
            return {'str':"Normal", 'shader':userMat[c4d.MATERIAL_NORMAL_SHADER]}
        return

    def m_trans(self, userMat):
        if userMat[c4d.MATERIAL_USE_TRANSPARENCY] == False:
            return
        else:
            return {'str':"Transparency", 'shader':userMat[c4d.MATERIAL_TRANSPARENCY_SHADER]}
        return

    def m_enviro(self, userMat):
        if userMat[c4d.MATERIAL_USE_ENVIRONMENT] == False:
            return
        else:
            return {'str':"Environment", 'shader':userMat[c4d.MATERIAL_ENVIRONMENT_SHADER]}
        return

    def m_bump(self, userMat):
        if userMat[c4d.MATERIAL_USE_BUMP] == False:
            return
        else:            
            return {'str':"Bump", 'shader':userMat[c4d.MATERIAL_BUMP_SHADER]}
        return

    def m_spec(self, userMat):
        if userMat[c4d.MATERIAL_USE_REFLECTION] == False:
            return
        else:            
            # To Get Reflection or Spec map texture from the Reflectance Channel of the material.
            m_spec = userMat.GetReflectionLayerCount()
            for each_layer in xrange(0, m_spec):  
                layer = userMat.GetReflectionLayerIndex(each_layer)
                get_layer_col = userMat[layer.GetDataID() + c4d.REFLECTION_LAYER_COLOR_TEXTURE]
                return {'str':"Reflectance", 'shader':get_layer_col}   
        return

# ---------------------------------------------------------------------
#                  Cinema 4D Splines Shapes
# ---------------------------------------------------------------------
class C4DSplineShapesObjects(object):

    """ Different Cinema 4D default Splines Object and Its Parameters """

    def __init__(self, global_strings):
        self.IDS = global_strings
        self.c4d_Arc = {"ui_id":7022, "obj_name":global_strings.ID(fcsID.GLOBAL_ID_ARC), "otype":c4d.Osplinearc, "c4d_id_otype":5182}
        self.c4d_Circle = {"ui_id":7023, "obj_name":global_strings.ID(fcsID.GLOBAL_ID_CIRCLE), "otype":c4d.Osplinecircle, "c4d_id_otype":5181}
        self.c4d_Helix = {"ui_id":7024, "obj_name":global_strings.ID(fcsID.GLOBAL_ID_HELIX), "otype":c4d.Osplinehelix, "c4d_id_otype":5185}
        self.c4d_Nside = {"ui_id":1022, "obj_name":global_strings.ID(fcsID.GLOBAL_ID_NSIDE), "otype":c4d.Osplinenside, "c4d_id_otype":5179}
        self.c4d_Rectangle = {"ui_id":1023, "obj_name":global_strings.ID(fcsID.GLOBAL_ID_RECTANGLE), "obj_type":c4d.Osplinerectangle, "c4d_id_otype":5186}
        self.c4d_Star = {"ui_id":1024, "obj_name":global_strings.ID(fcsID.GLOBAL_ID_STAR), "otype":c4d.Osplinestar, "c4d_id_otype":5187}
        self.c4d_Text = {"ui_id":1025, "obj_name":global_strings.ID(fcsID.GLOBAL_ID_TEXT), "otype":c4d.Osplinetext, "c4d_id_otype":5178}
        self.c4d_4side = {"ui_id":1026, "obj_name":global_strings.ID(fcsID.GLOBAL_ID_4SIDE), "otype":c4d.Ospline4side, "c4d_id_otype":5180}
        self.c4d_Cissoid = {"ui_id":1027, "obj_name":global_strings.ID(fcsID.GLOBAL_ID_CISSOID), "otype":c4d.Osplinecissoid, "c4d_id_otype":5183}
        self.c4d_Cogwheel = {"ui_id":1028, "obj_name":global_strings.ID(fcsID.GLOBAL_ID_COGWHEEL), "otype":c4d.Osplinecogwheel, "c4d_id_otype":5188}
        self.c4d_Vectorizer = {"ui_id":1028, "obj_name":global_strings.ID(fcsID.GLOBAL_ID_VECTORIZER), "otype":c4d.Osplinecontour, "c4d_id_otype":5189}
        self.c4d_Cycloid = {"ui_id":1029, "obj_name":global_strings.ID(fcsID.GLOBAL_ID_CYCLOID), "otype":c4d.Osplinecycloid, "c4d_id_otype":5184}
        self.c4d_Formula = {"ui_id":1030, "obj_name":global_strings.ID(fcsID.GLOBAL_ID_FORMULA), "otype":c4d.Osplineformula, "c4d_id_otype":5177}
        self.c4d_Flower = {"ui_id":1031, "obj_name":global_strings.ID(fcsID.GLOBAL_ID_FLOWER), "otype":c4d.Osplineflower, "c4d_id_otype":5176}
        self.c4d_Profile = {"ui_id":1032, "obj_name":global_strings.ID(fcsID.GLOBAL_ID_PROFILE), "otype":c4d.Osplineprofile, "c4d_id_otype":5175}   

        self.c4d_gen_splines = [
                                    self.c4d_Arc, 
                                    self.c4d_Circle, 
                                    self.c4d_Helix, 
                                    self.c4d_Nside, 
                                    self.c4d_Rectangle, 
                                    self.c4d_Star, 
                                    self.c4d_Text, 
                                    self.c4d_4side, 
                                    self.c4d_Cissoid, 
                                    self.c4d_Cogwheel, 
                                    self.c4d_Vectorizer, 
                                    self.c4d_Cycloid, 
                                    self.c4d_Formula, 
                                    self.c4d_Flower, 
                                    self.c4d_Profile 
                                ]

# ---------------------------------------------------------------------
#          XY2LatLong Helper Methods Functions
# ---------------------------------------------------------------------

class VectorLatLon():
    def __init__(self, Lon, Lat, Alt=0.0):
        self.Lon = Lon
        self.Lat = Lat
        self.Alt = Alt

# Enables a UIElement
def EnableElement(self, element):
    """ Enables a UI Widget Element """
    self.Enable(element, True)
    return True

# Disables a UIElement
def DisableElement(self, element):
    """ Disables a UI Widget Element """
    self.Enable(element, False)
    return True

#Turns the regex value into a bool
def RegexValueConverter(regex):
    regexBool = False

    if(regex == None):
        regexBool = False
    else:
        regexBool = True;
    return regexBool

def Rad2DegOverLoad(rad):
    return rad * 180.0 / math.pi

def Deg2RadOverLoad(deg):
    return float(deg) * math.pi / 180.0

def Rad2Deg(rad):   
    return VectorLatLon(Lon=Rad2DegOverLoad(rad.Lon), Lat=Rad2DegOverLoad(rad.Lat), Alt=rad.Alt)

def Deg2Rad(deg):
    return VectorLatLon(Lon=Deg2RadOverLoad(deg.Lon),Lat=Deg2RadOverLoad(deg.Lat),Alt=deg.Alt)

def FlatEarthXYZtoLongWG84(refPoint, pos):

    rad =  VectorLatLon(Lon=0.0,Lat=0.0,Alt=pos.Alt)

    refPoint = Deg2Rad(refPoint)

    rad.Lon = float(refPoint.Lon) + float(pos.Lon) / (6378134.34440771 * math.cos(float(refPoint.Lat)))
    rad.Lat = float(refPoint.Lat) + float(pos.Lat) / 6367311.80827746        

    return Rad2Deg(rad=rad)
    
#class StationOperators:


# ---------------------------------------------------------------------
#        Utils C4D Operations Hepler Method Functions 
# ---------------------------------------------------------------------

def newSolo_docTemp(doc, docData):
    
    # Get Models form the Object Manager.
    objs = doc.GetSelection()
    
    newdoc = c4d.documents.GetActiveDocument()
    
    # Solo the selected object.
    docTemp = c4d.documents.IsolateObjects(newdoc, objs)
    if docTemp == None:
        return False
    
    # Set Document Data.
    docTemp.SetDocumentData(c4d.DOCUMENTSETTINGS_DOCUMENT, docData) 
    
    get_docTemp_Data = c4d.documents.GetActiveDocument()   
    
    return get_docTemp_Data

def CollidePointToObject(pnt_dir, start_loc, target_obj):
    """ 
    Create a new GeRayCollider object 
    """
    ray = GeRayCollider()
    """ 
    Assign the object to a variable 
    """
    ray.Init(target_obj, True)
    # The object the ray will start from 
    start_point = start_loc
    """
    The direction the ray points. 
    In this case we shoot a ray out in each direction.
    """
    # Postive
    if pnt_dir == "+X":
        direction_pos = c4d.Vector(1, 0, 0)
    elif pnt_dir == "+Y":
        direction_pos = c4d.Vector(0, 1, 0)
    elif pnt_dir == "+Z":    
       direction_pos = c4d.Vector(0, 0, 1)
    # Negtive
    elif pnt_dir == "-X":
        direction_pos = c4d.Vector(-1, 0, 0)
    elif pnt_dir == "-Y":
        direction_pos = c4d.Vector(0, -1, 0)
    elif pnt_dir == "-Z":
        direction_pos = c4d.Vector(0, 0, -1)
    else:
        return

    direction = direction_pos
    distance = 10000000000000.0
    
    CollisionState = ray.Intersect( start_point, direction, distance )
    
    ray_pos = ray.GetNearestIntersection()
    
    ray_hit_pnt_pos = None
    
    if CollisionState:
        print("Ray Line Did Hit A Surface! " + "| Hit-State: " + str(CollisionState))
        ray_hit_pnt_pos = ray_pos["hitpos"]
    else:
        print("Ray Line Did Not Hit Surface!")  
        ray_hit_pnt_pos = None 
        pass
    return ray_hit_pnt_pos
def Convert_Object_Axis_To_Zero(obj):
    """ Converting Object Axis To Zero Script Tool """
    doc = c4d.documents.GetActiveDocument()
    mg = obj.GetMg()
    pcount = obj.GetPointCount()
    for i in xrange(pcount):
        point = obj.GetPoint(i)
        obj.SetPoint(i, point * mg)
    obj.SetAbsPos(c4d.Vector(0))
    obj.SetAbsRot(c4d.Vector(0))
    obj.Message(c4d.MSG_UPDATE)
    return True
def GetObjectWorldLocation(obj):
    # Get Global Postion of Object.
    OBJ_Pos = obj.GetMg() # Globe position from the matrix
    globalPosData = OBJ_Pos.off  # Get the position from the matrix
    #print str(globalPosData)
    # Split up the GlobalPosData Vector that is Store
    removeOpenBracket = str(globalPosData).split('(')
    removeCloseBracket = removeOpenBracket[1].split(')')
    values = removeCloseBracket[0].split(',')
    X_Data = float(values[0])
    Y_Data = float(values[1])
    Z_Data = float(values[2])    
    return c4d.Vector(X_Data, Y_Data, Z_Data)

def LocalToGlobal(obj, local_pos):
    """
    Returns a point in local coordinate in global space.
    """
    obj_mg = obj.GetMg()
    return obj_mg * local_pos
    
def GetPointGlobal(point_object, point_index):
    """
    Return the position of a point in Global Space
    """
    ppos = point_object.GetPoint(point_index) # Get the point in local coords
    return LocalToGlobal(point_object, ppos)  # Return the point in global space

def DrawGuideLineRayProjection(doc, start_pos, end_pos, col):
    spline = c4d.SplineObject(2, c4d.SPLINETYPE_LINEAR)
    spline.SetName("RayGuideLine")
    spline[c4d.ID_BASEOBJECT_USECOLOR]=2
    if col == "RED":
        spline[c4d.ID_BASEOBJECT_COLOR]=DARK_RED_TEXT_COL     
    elif col == "BLUE":
        spline[c4d.ID_BASEOBJECT_COLOR]=DARK_BLUE_TEXT_COL
    else:
        spline[c4d.ID_BASEOBJECT_COLOR]=BG_GREEN_COL        
    spline.SetPoint(0, start_pos)
    spline.SetPoint(1, end_pos)        
    doc.InsertObject(spline)
    spline.Message(c4d.MSG_UPDATE)
    return True

def Delete_Poly_After(self, obj):
    """ Delete Polygon After Split """
    doc = c4d.documents.GetActiveDocument()
    settings = c4d.BaseContainer()
    res2 = c4d.utils.SendModelingCommand(c4d.MCOMMAND_DELETE,
                list = [obj],
                mode = c4d.MODELINGCOMMANDMODE_POLYGONSELECTION,
                bc = settings,
                doc = doc)
    #res2[0].InsertAfter(obj)
    #doc.SetActiveObject(res2[0])
    c4d.EventAdd()
    return True

def split_polygon(self, obj):
    """ Spilt Polygon from Object. """

    doc = c4d.documents.GetActiveDocument()
    if doc == None:
        return False

    doc.StartUndo()

    MainObjName = obj.GetName()                     # Get Name of the main object.

    # // Split Function Operation //
    settings = c4d.BaseContainer()
    res = c4d.utils.SendModelingCommand(c4d.MCOMMAND_SPLIT,
                list = [obj],
                mode = c4d.MODELINGCOMMANDMODE_POLYGONSELECTION,
                bc = settings,
                doc = doc)

    doc.AddUndo(c4d.UNDOTYPE_CHANGE, res[0])

    res[0].InsertBefore(obj)                        # Insert Object to the Object Manager.

    doc.AddUndo(c4d.UNDOTYPE_NEW, res[0])

    doc.SetActiveObject(res[0])                     # Set object as Actice to get.

    split_poly = doc.GetActiveObject()              # Get Split Object.

    doc.AddUndo(c4d.UNDOTYPE_HIERARCHY_PSR, split_poly)

    c4d.CallCommand(1019940) # Reset PSR

    strName = MainObjName + "_split"                # The object name.
    SetObjectName(strName, split_poly, 100)   # Set Name for Spilt Object.
    get_children = split_poly.GetChildren()         # Get Children of the Parent.
    removeChildren(get_children)              # Remove Child.

    doc.AddUndo(c4d.UNDOTYPE_NEW, split_poly)

    doc.EndUndo()

    c4d.EventAdd()
    return split_poly

def WholeObject_SplitUp(self, obj, APPLY_Color):
    """ Its basically Polygon Groups """

    doc = c4d.documents.GetActiveDocument()
    if doc == None:
        return False

    c4d.StatusSetText("Calulating Object please wait......")
    MainObjName2 = obj.GetName()
    Name2 = MainObjName2 + "_split"                  # The object name.

    c4d.CallCommand(13323, 13323) # Select All

    settings = c4d.BaseContainer()
    settings.SetData(c4d.MDATA_DISCONNECT_PRESERVEGROUPS, False)
    res = c4d.utils.SendModelingCommand(c4d.MCOMMAND_DISCONNECT,
                list = [obj],
                mode = c4d.MODELINGCOMMANDMODE_POLYGONSELECTION,
                bc = settings,
                doc = doc)
    c4d.CallCommand(17891, 17891)                      # Polygon Groups to Objects

    doc.SetActiveObject(obj)                           # Set object as Actice to get.

    splitObj = doc.GetActiveObject()

    get_children = splitObj.GetChildren()
    amont = len(get_children)
    print(amont)
    #currentNum = 1
    for splitChild in get_children:
        c4d.StatusSetSpin()

        #percent = float(currentNum)/amont*100
        #c4d.StatusSetBar(percent)

        if APPLY_Color == "True":
            splitChild[c4d.ID_BASEOBJECT_USECOLOR]=1
            splitChild[c4d.ID_BASEOBJECT_COLOR]=g_lib.Add_Vector_Random_Colors()
        g_lib.SetObjectName(name=Name2, obj=splitChild, amount=amont)  # Set Name for Spilt Object.

    return obj

def OptimizePolyon(self):
    c4d.CallCommand(12139) # Points
    c4d.CallCommand(13323, 13323) # Select All
    c4d.CallCommand(14039, 14039) # Optimize
    c4d.CallCommand(12298) # Model
    return True

def RemoveTags(self, obj, KEEP_SomeTags):

    doc = c4d.documents.GetActiveDocument()
    if doc == None:
        return False

    object_Tags = obj.GetTags()
    for each_c4d_tag  in object_Tags:

        if not each_c4d_tag.CheckType(c4d.Tpoint) and not each_c4d_tag.CheckType(c4d.Tpolygon):

            if KEEP_SomeTags == "True":
                if not each_c4d_tag.CheckType(c4d.Tphong) and not each_c4d_tag.CheckType(c4d.Tuvw):
                    each_c4d_tag.Remove()
            else:
                each_c4d_tag.Remove()
    return True







class Preset_Saver_Dialog(WindowDialog):                 # Global Preset Save Dialog
    """Global Preset Save Dialog for all tools that need to have tool data as preset. """ 
 ##### UI IDs #####
    EB_Data1 = 1001
    BTN_OK = 1002
    BTN_Close = 1003
 ##### UI Window Dialog #####
    def CreateLayout(self):
        self.SetTitle("New Save")
        self.GroupBegin(0, c4d.BFH_CENTER, 1, 0, "")
        self.GroupBorderSpace(10, 10, 10, 10) 
        self.GroupBegin(0, c4d.BFH_CENTER, 0, 1, "")
        self.AddStaticText(0, c4d.BFH_CENTER, 0, 0, "Project Name :")   
        self.AddEditText(self.EB_Data1, c4d.BFH_CENTER, 250, 0)         
        self.GroupEnd()
        self.GroupBegin(0, c4d.BFH_CENTER, 0, 1, "")
        self.AddButton(self.BTN_OK, c4d.BFH_CENTER, 0, 0, "OK")
        self.AddButton(self.BTN_Close, c4d.BFH_CENTER, 0, 0, "Close")
        self.GroupEnd()
        self.GroupEnd()          
        return True
 ##### Main UI Operations Functions #####
    def MakePresetXML_fileDoc(self):
        doc = c4d.documents.GetActiveDocument()
        fName = self.GetString(self.EB_Data1)
    #    if os.path.exists(ModularSaving):
    #        ToolDataSavePath = PLUGIN_DIR().plugin_Saves_folder
    #        TempFileName = os.path.join(ToolDataSavePath,"Temp_Tool_Data404.xml")
    #        NewFileName = os.path.join(ToolDataSavePath, fName+" MB-Preset.xml")            
    #        if os.path.exists(NewFileName):
    #            os.remove(NewFileName)            
    #        os.rename(TempFileName, NewFileName)
    #        os.remove(ModularSaving)

        if os.path.exists(PLUGIN_DIR().NurboSaving):
            ToolDataSavePath = PLUGIN_DIR().plugin_Saves_folder
            TempFileName = os.path.join(ToolDataSavePath,"Temp_Tool_Data404.xml")
            NewFileName = os.path.join(ToolDataSavePath, fName+" NS-Preset.xml")
            if os.path.exists(NewFileName):
                os.remove(NewFileName)
            os.rename(TempFileName, NewFileName)
            os.remove(PLUGIN_DIR().NurboSaving)

    #    if os.path.exists(SwitchPlacerSaving):
    #        ToolDataSavePath = PLUGIN_DIR().plugin_Saves_folder
    #        TempFileName = os.path.join(ToolDataSavePath,"Temp_Tool_Data404.xml")
    #        NewFileName = os.path.join(ToolDataSavePath, fName+" SP-Preset.xml")
    #        # Set Read Message Error Log Text Doc.
    #        with open(SavingLog,'w') as txt:
    #            txt.write(fName)
    #        if os.path.exists(NewFileName):
    #            os.remove(NewFileName)
    #        os.rename(TempFileName, NewFileName)
    #        os.remove(SwitchPlacerSaving)


    #    if os.path.exists(SwitchPlacerUISet):
    #        ToolDataSavePath = PLUGIN_DIR().plugin_Saves_folder
    #        TempFileName = os.path.join(ToolDataSavePath,"Temp_Tool_Data404.xml")
    #        NewFileName = os.path.join(ToolDataSavePath, fName+" SP-UI-Preset.xml")
    #        if os.path.exists(NewFileName):
    #            os.remove(NewFileName)
    #        os.rename(TempFileName, NewFileName)

            # moving file over
    #        Moving_NewSource = NewFileName
    #        New_destination_Dir = PLUGIN_DIR().plugin_Saves_folder
    #        shutil.copy2(Moving_NewSource, New_destination_Dir)

            # Rename File to Default Xml name for SwitchPlacer.
    #        CopyFileName = os.path.join(PLUGIN_DIR().plugin_Saves_folder, fName+" UI-Preset.xml")
    #        NewFileName2 = os.path.join(PLUGIN_DIR().plugin_Saves_folder, "AutoStartupUI_SwitchPlacer.xml")
    #        if os.path.exists(NewFileName2):
    #            os.remove(NewFileName2)            
    #        os.rename(CopyFileName, NewFileName2)
    #        os.remove(SwitchPlacerUISet)
    #        c4d.CallCommand(1039060, 1039060) # SwitchPlacer


    #    if os.path.exists(PLUGIN_DIR().SelectNGoTakesSaving):
    #        ToolDataSavePath = PLUGIN_DIR().plugin_Saves_folder
    #        TempFileName = os.path.join(ToolDataSavePath,"Temp_Tool_Data404.xml")
    #        NewFileName = os.path.join(ToolDataSavePath, fName+" SNG-Preset.xml")
    #        if os.path.exists(NewFileName):
    #            os.remove(NewFileName)
    #        os.rename(TempFileName, NewFileName)
    #        os.remove(PLUGIN_DIR().SelectNGoTakesSaving)

    #    if os.path.exists(LevelPolySaving):
    #        ToolDataSavePath = PLUGIN_DIR().plugin_Saves_folderplugin_Saves_folder
    #        TempFileName = os.path.join(ToolDataSavePath,"Temp_Tool_Data404.xml")
    #        NewFileName = os.path.join(ToolDataSavePath, fName+" LPW-Preset.xml")
    #        if os.path.exists(NewFileName):
    #            os.remove(NewFileName)
    #        os.rename(TempFileName, NewFileName)
    #        os.remove(LevelPolySaving)

        c4d.EventAdd() 
        return True
    def DestroyWindow(self):

    #    ModularBTemp = os.path.join(PLUGIN_DIR().plugin_Saves_folder,"Temp_Tool_Data404.xml")
    #    if os.path.exists(ModularBTemp):
    #        os.remove(ModularBTemp)
    #        os.remove(ModularSaving)

        NurboTemp = os.path.join(PLUGIN_DIR().plugin_Saves_folder,"Temp_Tool_Data404.xml")
        if os.path.exists(NurboTemp):
            os.remove(NurboTemp)
            os.remove(PLUGIN_DIR().NurboSaving)

    #    SwitchPlacerTemp = os.path.join(PLUGIN_DIR().plugin_Saves_folder,"Temp_Tool_Data404.xml")
    #    if os.path.exists(SwitchPlacerTemp):
    #        os.remove(SavingLog)
    #        os.remove(SwitchPlacerTemp)
    #        os.remove(SwitchPlacerUISet)

    #    SwitchPlacerTemp = os.path.join(PLUGIN_DIR().plugin_Saves_folder,"Temp_Tool_Data404.xml")
    #    if os.path.exists(SwitchPlacerTemp):
    #        os.remove(SavingLog)
    #        os.remove(SwitchPlacerTemp)
    #        os.remove(SwitchPlacerSaving)

    #    LevelPolyTemp = os.path.join(PLUGIN_DIR().plugin_Saves_folder,"Temp_Tool_Data404.xml")
    #    if os.path.exists(LevelPolyTemp):
    #        os.remove(LevelPolyTemp)
    #        os.remove(LevelPolySaving)

    #    SelectNGoTakesTemp = os.path.join(PLUGIN_DIR().plugin_Saves_folder,"Temp_Tool_Data404.xml")
    #    if os.path.exists(SelectNGoTakesTemp):
    #        os.remove(SelectNGoTakesTemp)
    #        os.remove(PLUGIN_DIR().SelectNGoTakesSaving)
 ##### Excuting Operations #####
    def Command(self, id, msg=None):
        # Okay Button
        if (id == self.BTN_OK):

            empty = ""

            if self.GetString(self.EB_Data1)==empty:

                c4d.gui.MessageDialog("Error:\nThere is no name in the edit box field.\nPlease enter a name and press okay.")
                return True

            self.MakePresetXML_fileDoc()

            print("Done")

            self.Close()

        # Close Button
        if (id == self.BTN_Close):
            self.Close()

        return True       


class Instructions_Dialog(dlg): # Instructions Dialog for all tools. 
    """
    Instructions_Dialog
    To pass main data to it you need to make a (dict) for it.
    Example:
    Info = "My Instructions"
    Icon = I(i="tpm_Enable_icon")
    Data = { 'DLG_Title':"Test", 'INFO_STR':"...", 'INFO_ICON':Icon}
    output = Instructions_Dialog(Data)
    """
  ##### UI IDs #####
    EB_Data = 1001
    BTN_OK = 1002
    BTN_Close = 1003

    def __init__(self, Data):
        self.text = Data['INFO_STR']
        self.title = Data['DLG_Title']
        self.Icon = Data['INFO_ICON']
  ##### UI Window Dialog #####

    # UI Layout 
    def CreateLayout(self):
        self.SetTitle(self.title + " Instructions")
        self.GroupBegin(809, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 0, 1, "")
        self.Custom_UI_BTN(BTN_id=0, BTN_Icon_Path=self.Icon)
        btn_set = {'id':0, 'btn_look':c4d.BORDER_NONE, 'icon':I(i=self.Icon)}
        Add_Image_Button_GUI(ui_instance=self, btn_settings=btn_set)
        self.GroupBegin(810, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 1, 0, "")
        self.AddMultiLineEditText(self.EB_Data, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 230, 200, c4d.DR_MULTILINE_READONLY|c4d.DR_MULTILINE_MONOSPACED)
        self.SetString(self.EB_Data, self.text)
        self.AddButton(self.BTN_Close, c4d.BFH_RIGHT, 100, 10, "Close")
        self.GroupEnd()        
        self.GroupEnd()
        return True
  ##### Excuting Operations #####
    def Command(self, id, msg=None):
        if (id == self.BTN_Close):
            self.Close()
        #self.LayoutChanged(10042)
        return True

class Global_Dialog_GUI(dlg):
    """ Main C4D GUI Dialog Operations Functions which are Methods to Override for GeDialog class. """

    # ---------------------------------------------------------------------
    #       Main Override Methods Functions of C4D GUI GeDialog class
    # ---------------------------------------------------------------------
    def __init__(self):
        super(Global_Dialog_GUI, self).__init__()
        #self.DLG_Layout_Data = ui_data["ui_layout"]
        #self.DLG_IntValue_Data = ui_data["ui_int"]
        #self.DLG_Commands_Data = ui_data["ui_func"]
        #self.DLG_DestroyWindow_Data = ui_data["ui_dw"]

    def CreateLayout(self):
        #test1(self, data)

        #self.DLG_Layout_Data
        return True 

    def InitValues(self):
        #self.DLG_IntValue_Data
        return True

    def Command(self, id, msg):
        """ Excuting Commands for Functions """
        return True

    def DestroyWindow(self):
        """ Auto Save User UI Settings. """
        pass
        #self.DLG_DestroyWindow_Data


# hoh
def BarToggle(self, id, BTN, UI_Group, ToggleID):

    if (id == BTN):

        if self.GetString(ToggleID) == "c":
            self.HideElement(UI_Group, False) # Show
            self.SetString(ToggleID, "Open")
            print("Open")
            self.LayoutChanged(self.GroupUI_ID) # Update

        else:
            self.HideElement(UI_Group, True) # Hide
            self.SetString(ToggleID, "c")
            print("Close")
            self.LayoutChanged(self.GroupUI_ID) # Update        
    return True



# To Be remove this command class when all wwin dlg tools Fixed.
class OpenToolWindowDialog_CMD(CommandData):            # Open GUI Dialogs Command Data Class.
    dialog = None
    DLG_TYPE1 = c4d.DLG_TYPE_ASYNC  
    def __init__(self, id, dlg, data, dlgType):
        super(OpenToolWindowDialog_CMD, self).__init__()
        self.tool_id = id
        self.gui_window = dlg
        self.OptionData = data
        self.dlgType = dlgType 
    def Init(self, op):
        return True
    def Message(self, type, data):
        return True
    def Execute(self, doc):
        if self.dlgType == 1:
            if self.dialog is None:
                self.dialog = self.gui_window
            return self.dialog.Open(dlgtype=self.DLG_TYPE1, pluginid=self.tool_id, defaultw=50, defaulth=50)
        return True
    def RestoreLayout(self, sec_ref):
        if self.dialog is None:
            self.dialog = self.gui_window
        return self.dialog.Restore(pluginid=self.tool_id, secret=sec_ref)
    def ExecuteOptionID(self, doc, plugid, subid):
        self.OptionData
        return True





"""
# Mouse Scroll Wheel Function 
dev = msg[c4d.BFM_INPUT_DEVICE]
if dev == c4d.BFM_INPUT_MOUSE:
    chn = msg[c4d.BFM_INPUT_CHANNEL]
    if chn == c4d.BFM_INPUT_MOUSEWHEEL:
        val = msg[c4d.BFM_INPUT_VALUE]
        if val > 0:
            print "dir up"
            rotY = (1/100)
            self.rot_obj['rot'] = 1*rotY                    
        else:
            print "dir down"
"""




"""
# // Create and Set Material //
MatName = "Oplts_object_ins"
userMat = doc.SearchMaterial(MatName)
tex = UCX_box_Outline.MakeTag(c4d.Ttexture)
if not userMat:
    PREVIEWSIZE=1
    shdBitmap = c4d.BaseShader(c4d.Xbitmap)
    if shdBitmap is None:
        print "Error: Shader allocation failed"
        return
    shdBitmap[c4d.BITMAPSHADER_FILENAME] = PLUGIN_DIR().Icon_Path(i="UnityUCX512_editor_icon.png")
    Material_Tex = c4d.BaseMaterial(5703)               # Generate Material
    Material_Tex[c4d.ID_BASELIST_NAME] = MatName        # Generate Material Name
    Material_Tex[c4d.MATERIAL_USE_COLOR] = True #c4d.Vector(0.08203125, 0.1015625, 0.12109375)
    Material_Tex[c4d.MATERIAL_COLOR_COLOR] = c4d.Vector(0.08203125, 0.1015625, 0.12109375)
    Material_Tex[c4d.MATERIAL_PREVIEWSIZE]=PREVIEWSIZE  # Set Material Size
    Material_Tex[c4d.MATERIAL_USE_REFLECTION]=False
    #Material_Tex[c4d.MATERIAL_COLOR_SHADER] = #shdBitmap # Set Material Shader Tex
    #Material_Tex.InsertShader(shdBitmap)                # Insert Material Shader 
    doc.InsertMaterial(Material_Tex)                    # Insert Material  
    tex[c4d.TEXTURETAG_MATERIAL]=Material_Tex
else:
    tex[c4d.TEXTURETAG_MATERIAL]=userMat

tex[c4d.TEXTURETAG_PROJECTION]=6
tex[c4d.TEXTURETAG_TILE]=True
"""

# Node Editor with Xpresso
# [c4d.ID_BASELIST_NAME]
"""
doc = c4d.documents.GetActiveDocument()
getnodeobj = doc.SetActiveObject(doc.SearchObject("node")) 
print(getnodeobj)
getnodegraphtag = doc.GetActiveObject().GetTag(c4d.Texpresso)
if getnodegraphtag:
    print(getnodegraphtag)       
    nodegraphviewport = getnodegraphtag.GetNodeMaster()
    print(nodegraphviewport)
    # Add to UI  
    self.AddStaticText(0, c4d.BFH_CENTER, 0, 15, "Node Editor", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text
    self.AddUserArea(898, c4d.BFH_CENTER, 250, 200)
    self.AttachUserArea(nodegraphviewport, 898)   
"""

