"""
FCS C4D LOGGER Window GUI Dialog
"""
#  // Imports for Cinema 4D //
import c4d
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as WindowDialog        # eg:( class My_Dialog(c4d.gui.GeDialog): ) to ( class My_Dialog(WindowDialog): ) Just keeping code line short.
from c4d.plugins import CommandData, TagData, ObjectData
from random import randint
# Python Plugin System Load Modules Dynamically With importlib.
import importlib
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
#from res._fcs_lib.fcs_module_lib import fcs_common_global_lib as g_lib
fcsID = g_lib.fcsID
jsonEdit = g_lib.jsonEdit

# Create ProgressBar GUI
class AddCustomProgressBar_GUI(object):
    """
    # Progress Bar GUI 

    # Add a Custom Progress Bar GUI Layout 

    # Code Example:
    
    self.CUSTOM_PROGRESSBAR = AddCustomProgressBar_GUI(ui_instance=self)
    
    # Note: If you dont want a string amount UI display then set (str_amount_id=None).
    self.CUSTOM_PROGRESSBAR.GUI(str_amount_id=id, 
                                progress_id=id, 
                                size_w=190, 
                                size_l=10 )

    # Note: If you dont want a custom color then set (col=None).                            
    self.CUSTOM_PROGRESSBAR.Run_ProgressBar(progressbar_ui_id=id, 
                                           currentItemNum=1, 
                                           amountOfItems=(Amount of Items eg:10), 
                                           col=g_lib.DARK_BLUE_TEXT_COL)  # DARK_BLUE_TEXT_COL # DARK_RED_TEXT_COL 
                                                                            color is a c4d.Vector(0,0,0). 
                                                                            eg: DARK_RED_TEXT_COL=c4d.Vector(0.99609375, 0, 0)

    self.CUSTOM_PROGRESSBAR.Stop_ProgressBar(progressbar_ui_id=id)
    """

    def __init__(self, ui_instance):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """
        self.instance = ui_instance    

    def GUI(self, str_amount_id, progress_id, size_w, size_l):
        """ Progress Bar GUI Layout """
        self.instance.GroupBegin(0, c4d.BFH_SCALEFIT, 2, 0, "")
        self.instance.GroupBorderNoTitle(c4d.BORDER_THIN_IN)
        # ProgressBar
        self.instance.AddCustomGui(progress_id, c4d.CUSTOMGUI_PROGRESSBAR, "", c4d.BFH_SCALEFIT, size_w, size_l)
        if str_amount_id:
            # Static UI Text
            self.instance.AddSeparatorV(0, c4d.BFV_SCALEFIT)
            self.instance.AddStaticText(str_amount_id, c4d.BFH_MASK, 50, size_l, "", c4d.BORDER_WITH_TITLE_BOLD)
        self.instance.GroupEnd()
        return True

    def Run_ProgressBar(self, progressbar_ui_id, currentItemNum, amountOfItems, col):
        """ Progress Bar Running  """
        percent = float(currentItemNum)/amountOfItems*100
        # Set Data to PROGRESSBAR
        progressMsg = c4d.BaseContainer(c4d.BFM_SETSTATUSBAR)
        progressMsg[c4d.BFM_STATUSBAR_PROGRESSON] = True
        progressMsg[c4d.BFM_STATUSBAR_PROGRESS] = percent/100.0 
        # this if you want a custom color
        if col:
            self.instance.SetDefaultColor(progressbar_ui_id, c4d.COLOR_PROGRESSBAR, col)    
        self.instance.SendMessage(progressbar_ui_id, progressMsg)
        # Return Percent String Data
        PercentData = str(int(percent))+"%"
        return PercentData
    
    def Stop_ProgressBar(self, progressbar_ui_id):
        """ Progress Bar Stop Running  """
        progressMsg = c4d.BaseContainer(c4d.BFM_SETSTATUSBAR)
        progressMsg.SetBool(c4d.BFM_STATUSBAR_PROGRESSON, False)
        self.instance.SendMessage(progressbar_ui_id, progressMsg)
        return True

# Create a QuickTab Custom GUI.
class AddCustomQuickTab_GUI(object):

    CUSTOMGUI_FLAG = c4d.CUSTOMGUI_QUICKTAB

    def __init__(self, ui_instance):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """
        self.instance = ui_instance    

    def Add_BarTitle_GUI(self, bar_id, bar_name, width, height, ui_color):
        """ 
        Create a QuickTab Default Bar Title Custom GUI. 
        """
        self.instance.CustomGui_UI = c4d.BaseContainer()
        self.instance.CustomGui_UI.FlushAll()
        self.instance.CustomGui_UI.SetBool(c4d.QUICKTAB_BAR, True)
        self.instance.CustomGui_UI.SetString(c4d.QUICKTAB_BARTITLE, bar_name)
        if ui_color:
            self.instance.CustomGui_UI[c4d.QUICKTAB_BGCOLOR] = ui_color
        self.instance.AddCustomGui(bar_id, self.CUSTOMGUI_FLAG, "", c4d.BFH_SCALEFIT, width, height, self.instance.CustomGui_UI)
        return True   

    def Add_BarTitleArrowHandle_GUI(self, bar_id, bar_name, width, height, ui_color):
        """ 
        Create a QuickTab Fold Arrow Handle Bar Title Custom GUI. 
        """
        self.instance.CustomGui_UI = c4d.BaseContainer()
        self.instance.CustomGui_UI.FlushAll()
        self.instance.CustomGui_UI.SetBool(c4d.QUICKTAB_BAR, True)
        self.instance.CustomGui_UI.SetString(c4d.QUICKTAB_BARTITLE, bar_name)
        if ui_color:
            self.instance.CustomGui_UI[c4d.QUICKTAB_BARLAYERCOLOR] = ui_color
        # GUI Toggle System for QuickTab Bar Fold Icon Button.  
        # To Help with Toggle Mode of Open and Closing the Bar Grouos.path. / Its just a dummy to help with Toggling.
        # Handle as subgroup. Like bar mode, but with fold arrow icon. Implies QUICKTAB_BAR. Call QuickTabCustomGui.IsSelected()            
        self.instance.CustomGui_UI.SetBool(c4d.QUICKTAB_BARSUBGROUP, True)
         # Set the Fold Handle to Open or Close.
        self.instance.ui = self.instance.AddCustomGui(bar_id, self.CUSTOMGUI_FLAG, "", c4d.BFH_SCALEFIT, width, height, self.instance.CustomGui_UI)
        self.instance.ui.Select(0, True) # Set the Fold Handle to Open or Close.
        return True

    def Add_BarTabsRadio_GUI(self, bar_id, width, height, ui_color, list_tabs):
        """
        Create a QuickTab Fold Arrow Handle Bar Title Custom GUI.
        """
        self.instance.CustomGui_UI = c4d.BaseContainer()
        self.instance.GroupBegin(0, c4d.BFH_SCALEFIT, 2, 0, "")
        self.instance.GroupSpace(0, 0)
        self.instance.CustomGui_UI.FlushAll()
        self.instance.CustomGui_UI.SetBool(c4d.QUICKTAB_BAR, False)
        self.instance.CustomGui_UI.SetBool(c4d.QUICKTAB_SHOWSINGLE, True)
        self.instance.ui = self.instance.AddCustomGui(bar_id, self.CUSTOMGUI_FLAG, "", c4d.BFH_SCALEFIT, width, height, self.instance.CustomGui_UI)
        for tab in list_tabs:
            self.instance.ui.AppendString(tab['id'], tab['str'], tab['state'])
        if ui_color:
            # c4d.COLOR_QUICKTAB_BG_ACTIVE
            # c4d.COLOR_QUICKTAB_TEXT_ACTIVE 
            # c4d.COLOR_QUICKTAB_TEXT_INACTIVE
            # c4d.COLOR_QUICKTAB_BG_INACTIVE
            self.instance.SetDefaultColor(bar_id, c4d.COLOR_QUICKTAB_BG_ACTIVE, ui_color)       
        self.instance.GroupEnd()
        return True    


class FCS_Logger_GUI(g_lib.BaseWindowDialogUI):
    """ FCS Logger GUI Window. """
    
    """
    GUI Elements ID's.
    _________________________________________________
    """ 
    IDS_VER_ID = 1000
    IDUI_OVERALL_GRP = 1001
    IDUI_TAB = 1002
    IDUI_TAB2 = 1003
    IDUI_ON = { 'id':1004, 'str':"ON", 'state':False }
    IDUI_OFF = { 'id':1005, 'str':"OFF", 'state':True }
    TOGGLE_TABS = [ IDUI_ON, IDUI_OFF ]
    IDUI_EXPORTLOG = 1006
    IDUI_MULTI_LINE_STRINGBOX = 1007
    IDUI_FILTER_LIST_COMBOBOX = 1008
    IDUI_ = 1009

    """
    Variables & Properties
    _________________________________________________
    """
    windowMainTileName = "FCS Station Manager"
    windowDialogWidthSize = 200
    windowDialogHeightSize = 200
    windowDialogPluginID = 1050473   


    """
    Tool Functions Operation Hepler Methods.
    _________________________________________________
    """


    """
    Main GeDialog GUI Class Overrides.
    _________________________________________________
    """
    def __init__(self):
        """
        The __init__ is an Constuctor and help get or set data 
        into the class and passes data on from the another class.
        """
        super(FCS_Logger_GUI, self).__init__()
        self.CUSTOM_FILENAME = g_lib.AddCustomFileNamePath_GUI(self)
        self.CUSTOM_PROGRESSBAR = AddCustomProgressBar_GUI(self)
        self.CUSTOM_QTabs = AddCustomQuickTab_GUI(self)

    def BuildUI(self):
        # Top Menu addinng Tool Version
        self.GroupBeginInMenuLine()
        self.AddStaticText(self.IDS_VER_ID, 0)
        self.SetString(self.IDS_VER_ID, " v1.0  ")
        self.GroupEnd()

        self.GroupBegin(self.IDUI_OVERALL_GRP, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 1, 0, "")

        self.GroupBorderSpace(3,3,3,3)

        #self.AddStaticText(0, c4d.BFH_CENTER, 0, 15, "Satellite To GroundPoly", c4d.BORDER_WITH_TITLE_BOLD)
        
        self.AddSeparatorH(0, c4d.BFH_SCALEFIT) # Separator
        
        self.GroupBegin(0, c4d.BFH_RIGHT, 2, 0, "")
        self.AddStaticText(0, c4d.BFH_RIGHT, 0, 15, " Filter:", c4d.BORDER_WITH_TITLE_BOLD)
        self.AddComboBox(self.IDUI_FILTER_LIST_COMBOBOX,  c4d.BFH_RIGHT, 150, 10, specialalign=False)
        self.AddChild(self.IDUI_FILTER_LIST_COMBOBOX, 1, g_lib.fcsEngine.LogType.NoneLog)
        self.AddChild(self.IDUI_FILTER_LIST_COMBOBOX, 2, g_lib.fcsEngine.LogType.InfoLog)
        self.AddChild(self.IDUI_FILTER_LIST_COMBOBOX, 3, g_lib.fcsEngine.LogType.ModelExportLog)
        self.AddChild(self.IDUI_FILTER_LIST_COMBOBOX, 4, g_lib.fcsEngine.LogType.ErrorLog)
        self.AddChild(self.IDUI_FILTER_LIST_COMBOBOX, 5, g_lib.fcsEngine.LogType.CriticalLog)
        self.AddChild(self.IDUI_FILTER_LIST_COMBOBOX, 6, g_lib.fcsEngine.LogType.DebugLog)       
        self.GroupEnd()

        self.AddMultiLineEditText(self.IDUI_MULTI_LINE_STRINGBOX, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 590, 300, c4d.DR_MULTILINE_MONOSPACED)

        self.GroupBegin(0, c4d.BFH_RIGHT|c4d.BFV_BOTTOM, 1, 0, "")    
        self.GroupBegin(0, c4d.BFH_SCALEFIT|c4d.BFH_RIGHT, 6, 0, "")
        self.AddStaticText(0, c4d.BFH_RIGHT, 0, 15, "Export X file to MCX", c4d.BORDER_WITH_TITLE_BOLD)
        self.CUSTOM_QTabs.Add_BarTabsRadio_GUI(self.IDUI_TAB2, 100, 10, g_lib.DARK_BLUE_TEXT_COL, self.TOGGLE_TABS)       
        self.AddStaticText(0, c4d.BFH_RIGHT, 0, 15, "Automatic XML Deletion", c4d.BORDER_WITH_TITLE_BOLD)
        self.CUSTOM_QTabs.Add_BarTabsRadio_GUI(self.IDUI_TAB2, 100, 10, None, self.TOGGLE_TABS)   
        self.AddButton(self.IDUI_EXPORTLOG, c4d.BFH_RIGHT, 100, 15, name="Export Log")  
        self.AddButton(898, c4d.BFH_RIGHT, 100, 15, name="Clear Log")
        self.GroupEnd()
        self.GroupEnd()

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT) # Separator

        self.GroupEnd() 
        return super(FCS_Logger_GUI, self).BuildUI()

    def UIsettings(self):
        """
        Called when the dialog is initialized by the GUI / GUI's startup values basically.
        """
        g_lib.fcsLog.DEBUG("FCS ToolKit About Window is Open Now! From FCS ToolKit", False, g_lib.DebugMode)
        self.SetDefaultColor(self.IDS_VER_ID, c4d.COLOR_TEXT, g_lib.DARK_BLUE_TEXT_COL)
        self.SetDefaultColor(898, c4d.COLOR_TEXT_BUTTON, g_lib.DARK_BLUE_TEXT_COL)
        self.SetDefaultColor(self.IDUI_EXPORTLOG, c4d.COLOR_TEXT_BUTTON, g_lib.DARK_BLUE_TEXT_COL)
        self.SetDefaultColor(self.IDUI_OVERALL_GRP, c4d.COLOR_BG, g_lib.BG_DARK)
        self.SetDefaultColor(self.IDUI_EXPORTLOG, c4d.COLOR_TEXT_BUTTON, g_lib.DARK_BLUE_TEXT_COL)
        self.SetLong(self.IDUI_FILTER_LIST_COMBOBOX,1)
        """
        self.SetDefaultColor(self.UI_SATELLITE_IMAGE_TXT_ID, c4d.COLOR_TEXT, g_lib.DARK_BLUE_TEXT_COL)
        self.SetDefaultColor(self.UI_SATELLITE_DATA_TXT_ID, c4d.COLOR_TEXT, g_lib.DARK_BLUE_TEXT_COL) 
        """
        return super(FCS_Logger_GUI, self).UIsettings()
