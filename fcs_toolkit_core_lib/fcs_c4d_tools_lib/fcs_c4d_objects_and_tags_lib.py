"""
Our FCS Tags and Objects Tools
"""
#  // Imports for Cinema 4D //
import c4d
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
import weakref
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as WindowDialog        # eg:( class My_Dialog(c4d.gui.GeDialog): ) to ( class My_Dialog(WindowDialog): ) Just keeping code line short.
from c4d.plugins import CommandData, TagData, ObjectData
from random import randint
# XML Imports is for Saving and Loading Project Data for Plugin UI / Xml good but long functions to make.
from xml.dom import minidom
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree
# Python Plugin System Load Modules Dynamically With importlib.
import importlib
# Date System
DateT = str(datetime.date.today())
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
#from res._fcs_lib.fcs_module_lib import fcs_common_global_lib as g_lib
fcsID = g_lib.fcsID
jsonEdit = g_lib.jsonEdit


# ------------------------------------------------------------------
#                          Tags Tools
# ------------------------------------------------------------------

class NurboTags_Data(TagData):
    """ Tag Tool """
    def Init(self, tag):       
        return True

    def Execute(self, tag, doc, op, bt, priority, flags):
        return True

class RandomReplacerTag_Data(TagData):
    """ Tag Tool """
    def Init(self, tag):
        #print "Random Replacer Tag just got added."
        return True
    def Execute(self, tag, doc, op, bt, priority, flags):
        return True
class RR_TempTag_Data(TagData):
    """ Tag Tool """
    def Init(self, tag):
        #print "Random Replacer Tag just got added."
        return True
    def Execute(self, tag, doc, op, bt, priority, flags):
        return True        
class RR_PresetTag_Data(TagData):
    """ Tag Tool """
    def Init(self, tag):
        #print "Random Replacer Tag just got added."
        return True

    def Execute(self, tag, doc, op, bt, priority, flags):
        return True        


















# ------------------------------------------------------------------
#                          Objects Tools
# ------------------------------------------------------------------


class O_NurboTaxiline_Object_Data(ObjectData):

    THE_BUTTON_ID = 1024
    

    def __init__(self):
        self.SetOptimizeCache(True) 

    def Init(self, op):

        Nurbojsonfile = g_lib.fcsDIR.FCS_NURBO_CONIG

        self.InitAttr(op, float, c4d.IDS_NURBO_TAXILINE_SIZE)
        self.InitAttr(op, float, c4d.IDS_NURBO_TAXILINE_SPLINE_ANGLE)

        op[c4d.ID_BASEOBJECT_USECOLOR]=2
        op[c4d.ID_BASEOBJECT_COLOR]=g_lib.Add_Vector_Random_Colors()

        op[c4d.IDS_NURBO_TAXILINE_SIZE] = jsonEdit.GetItemProperty(Nurbojsonfile, "Nurbo_TaxilineObject_Size")

        op[c4d.IDS_NURBO_TAXILINE_ANCHOR_TYPE] = 0

        if jsonEdit.GetItemProperty(Nurbojsonfile, "Nurbo_TaxilineObject_Type") == "Inner":
            op[c4d.IDS_NURBO_TAXILINE_ANCHOR_TYPE] = 1
        if jsonEdit.GetItemProperty(Nurbojsonfile, "Nurbo_TaxilineObject_Type") == "Outter":
            op[c4d.IDS_NURBO_TAXILINE_ANCHOR_TYPE] = 2

        op[c4d.IDS_NURBO_TAXILINE_SPLINE_ANGLE] = jsonEdit.GetItemProperty(Nurbojsonfile, "Nurbo_TaxilineObject_AngleCurvesNum")
        op[c4d.IDS_NURBO_CAP_CONNECT] = True
        op[c4d.IDS_NURBO_SWEEPOBJECT_PARALLEL] = False
        op[c4d.IDS_NURBO_SWEEPOBJECT_BANKING] = True
        op[c4d.IDS_NURBO_SWEEPOBJECT_RAILDIRECTION] = True
        op[c4d.IDS_NURBO_SWEEPOBJECT_RAILSCALE] = True
        op[c4d.IDS_NURBO_SWEEPOBJECT_CONSTANT] = True
        op[c4d.IDS_NURBO_SWEEPOBJECT_KEEPSEGMENTS] = False
        op[c4d.IDS_NURBO_SWEEPOBJECT_BIRAIL] = True
        op[c4d.IDS_NURBO_SWEEPOBJECT_FLIPNORMALS] = False
        op[c4d.IDS_NURBO_SWEEPOBJECT_GROWUV] = False

        """
        op[c4d.SWEEPOBJECT_ISOPARM]=5
        op[c4d.SWEEPOBJECT_SCALE]=1
        op[c4d.SWEEPOBJECT_GROWTH]=100
        op[c4d.SWEEPOBJECT_PARALLEL]=False
        op[c4d.SWEEPOBJECT_BANKING]=True
        op[c4d.SWEEPOBJECT_RAILDIRECTION]=True
        op[c4d.SWEEPOBJECT_RAILSCALE]=True
        op[c4d.SWEEPOBJECT_CONSTANT]=True
        op[c4d.SWEEPOBJECT_KEEPSEGMENTS]=False
        op[c4d.SWEEPOBJECT_BIRAIL]=True
        op[c4d.SWEEPOBJECT_FLIPNORMALS]=False
        op[c4d.SWEEPOBJECT_GROWUV]=False
        """
        op.Message(c4d.MSG_UPDATE)
        c4d.EventAdd()
        return True

    def GenerateSplineObject(self, op):
        """ Create Spline Object. """    
        spline = c4d.SplineObject(2, c4d.SPLINETYPE_LINEAR)
        spline[c4d.ID_BASEOBJECT_USECOLOR]=2
        spline[c4d.ID_BASEOBJECT_COLOR]=op[c4d.ID_BASEOBJECT_COLOR]
        spline[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_X]=0
        spline[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Y]=0
        spline[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Z]=0
        spline[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_X]=0
        spline[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_Y]=0
        spline[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_Z]=0

        # Set Point on Spline Object 

        if op[c4d.IDS_NURBO_TAXILINE_ANCHOR_TYPE]==0:
            #print("Center")  
            dataSize = op[c4d.IDS_NURBO_TAXILINE_SIZE]/2
            spline.SetPoint(0, c4d.Vector(-dataSize, 0, 0))
            spline.SetPoint(1, c4d.Vector(dataSize, 0, 0))

        if op[c4d.IDS_NURBO_TAXILINE_ANCHOR_TYPE]==1:
            #print("Inner")
            dataSize = op[c4d.IDS_NURBO_TAXILINE_SIZE]
            spline.SetPoint(0, c4d.Vector(-dataSize, 0, 0))
            spline.SetPoint(1, c4d.Vector(0, 0, 0))

        if op[c4d.IDS_NURBO_TAXILINE_ANCHOR_TYPE]==2:
            #print("Outer")
            dataSize = op[c4d.IDS_NURBO_TAXILINE_SIZE]
            spline.SetPoint(0, c4d.Vector(0, 0, 0))
            spline.SetPoint(1, c4d.Vector(dataSize, 0, 0))


             
        spline.Message(c4d.MSG_UPDATE)
        
        return spline

    def GetVirtualObjects(self, op, hh):

        Nurbojsonfile = g_lib.fcsDIR.FCS_NURBO_CONIG

        dirty = op.CheckCache(hh) or op.IsDirty(c4d.DIRTY_DATA)
        if dirty is False: return op.GetCache(hh)

        doc = c4d.documents.GetActiveDocument()

        user_path = op.GetDown()#.GetClone(c4d.COPYFLAGS_0)
        if user_path is None:
            op[c4d.IDS_NURBO_TAXILINE_ANCHOR_TYPE]=0
            pass
            return     

        sweep_obj = c4d.BaseObject(c4d.Osweep)
        sweep_obj[c4d.ID_BASEOBJECT_USECOLOR]=2
        sweep_obj[c4d.ID_BASEOBJECT_COLOR]=op[c4d.ID_BASEOBJECT_COLOR]    
        sweep_obj[c4d.CAP_START] = 0
        sweep_obj[c4d.CAP_END] = 0
        sweep_obj[c4d.CAP_TYPE] = 0
        sweep_obj[c4d.CAP_CONNECT] = op[c4d.IDS_NURBO_CAP_CONNECT]
        sweep_obj[c4d.SWEEPOBJECT_PARALLEL]=op[c4d.IDS_NURBO_SWEEPOBJECT_PARALLEL]
        sweep_obj[c4d.SWEEPOBJECT_BANKING]=op[c4d.IDS_NURBO_SWEEPOBJECT_BANKING]
        sweep_obj[c4d.SWEEPOBJECT_RAILDIRECTION]=op[c4d.IDS_NURBO_SWEEPOBJECT_RAILDIRECTION]
        sweep_obj[c4d.SWEEPOBJECT_RAILSCALE]=op[c4d.IDS_NURBO_SWEEPOBJECT_RAILSCALE]
        sweep_obj[c4d.SWEEPOBJECT_CONSTANT]=op[c4d.IDS_NURBO_SWEEPOBJECT_CONSTANT]
        sweep_obj[c4d.SWEEPOBJECT_KEEPSEGMENTS]=op[c4d.IDS_NURBO_SWEEPOBJECT_KEEPSEGMENTS]
        sweep_obj[c4d.SWEEPOBJECT_BIRAIL]=op[c4d.IDS_NURBO_SWEEPOBJECT_BIRAIL]
        sweep_obj[c4d.SWEEPOBJECT_FLIPNORMALS]=op[c4d.IDS_NURBO_SWEEPOBJECT_FLIPNORMALS]
        sweep_obj[c4d.SWEEPOBJECT_GROWUV]=op[c4d.IDS_NURBO_SWEEPOBJECT_GROWUV]

        # Add Phong Tag to Sweep Oject if Checked.
        tag = sweep_obj.MakeTag(c4d.Tphong)
        doc.AddUndo(c4d.UNDOTYPE_CHANGE, tag)
        tag[c4d.PHONGTAG_PHONG_ANGLELIMIT]=True
        tag[c4d.PHONGTAG_PHONG_USEEDGES]=True
        doc.AddUndo(c4d.UNDOTYPE_NEW, tag)


        """
        sweep_obj[c4d.SWEEPOBJECT_ISOPARM]=op[c4d.SWEEPOBJECT_ISOPARM]
        sweep_obj[c4d.SWEEPOBJECT_SCALE]=op[c4d.SWEEPOBJECT_SCALE]
        sweep_obj[c4d.SWEEPOBJECT_GROWTH]=op[c4d.SWEEPOBJECT_GROWTH]
        sweep_obj[c4d.SWEEPOBJECT_PARALLEL]=op[c4d.SWEEPOBJECT_PARALLEL]
        sweep_obj[c4d.SWEEPOBJECT_BANKING]=op[c4d.SWEEPOBJECT_BANKING]
        sweep_obj[c4d.SWEEPOBJECT_RAILDIRECTION]=op[c4d.SWEEPOBJECT_RAILDIRECTION]
        sweep_obj[c4d.SWEEPOBJECT_RAILSCALE]=op[c4d.SWEEPOBJECT_RAILSCALE]
        sweep_obj[c4d.SWEEPOBJECT_CONSTANT]=op[c4d.SWEEPOBJECT_CONSTANT]
        sweep_obj[c4d.SWEEPOBJECT_KEEPSEGMENTS]=op[c4d.SWEEPOBJECT_KEEPSEGMENTS]
        sweep_obj[c4d.SWEEPOBJECT_BIRAIL]=op[c4d.SWEEPOBJECT_BIRAIL]
        sweep_obj[c4d.SWEEPOBJECT_FLIPNORMALS]=op[c4d.SWEEPOBJECT_FLIPNORMALS]
        sweep_obj[c4d.SWEEPOBJECT_GROWUV]=op[c4d.SWEEPOBJECT_GROWUV]
        """



        # Add User Custom Spline
        op[c4d.ID_OBJECT_INS_LINK]=user_path
        custom_obj = op[c4d.ID_OBJECT_INS_LINK]
        if custom_obj == None:
            pass
        else:
            custom_obj_inst = c4d.BaseObject(c4d.Oinstance)
            custom_obj_inst[c4d.INSTANCEOBJECT_LINK] = custom_obj
            custom_obj_inst[c4d.ID_BASEOBJECT_USECOLOR] = 1
            custom_obj_inst.InsertUnder(sweep_obj)

        # Create Spline Object
        spline = c4d.SplineObject(2, c4d.SPLINETYPE_LINEAR)
        spline[c4d.ID_BASEOBJECT_USECOLOR]=2
        spline[c4d.ID_BASEOBJECT_COLOR]=op[c4d.ID_BASEOBJECT_COLOR]
        spline[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_X]=0
        spline[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Y]=0
        spline[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Z]=0
        spline[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_X]=0
        spline[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_Y]=0
        spline[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_Z]=0
        #     Set Point on Spline Object 
        if op[c4d.IDS_NURBO_TAXILINE_ANCHOR_TYPE]==0:
            print("Center")  
            dataSize = op[c4d.IDS_NURBO_TAXILINE_SIZE]/2
            spline.SetPoint(0, c4d.Vector(-dataSize, 0, 0))
            spline.SetPoint(1, c4d.Vector(dataSize, 0, 0))

        if op[c4d.IDS_NURBO_TAXILINE_ANCHOR_TYPE]==1:
            print("Inner")
            dataSize = op[c4d.IDS_NURBO_TAXILINE_SIZE]
            spline.SetPoint(0, c4d.Vector(-dataSize, 0, 0))
            spline.SetPoint(1, c4d.Vector(0, 0, 0))

        if op[c4d.IDS_NURBO_TAXILINE_ANCHOR_TYPE]==2:
            print("Outer")
            dataSize = op[c4d.IDS_NURBO_TAXILINE_SIZE]
            spline.SetPoint(0, c4d.Vector(0, 0, 0))
            spline.SetPoint(1, c4d.Vector(dataSize, 0, 0))

        spline.InsertUnder(sweep_obj) 
        spline.Message(c4d.MSG_UPDATE)

        user_path[c4d.SPLINEOBJECT_ANGLE]=op[c4d.IDS_NURBO_TAXILINE_SPLINE_ANGLE]
        print(user_path[c4d.SPLINEOBJECT_ANGLE])

        #doc.SetActiveObject(user_path)
        #obj = doc.GetActiveObject()
        jsonEdit.EditItemProperty(Nurbojsonfile, "Nurbo_TaxilineObject_Size", op[c4d.IDS_NURBO_TAXILINE_SIZE])
        jsonEdit.EditItemProperty(Nurbojsonfile, "Nurbo_TaxilineObject_Type", op[c4d.IDS_NURBO_TAXILINE_ANCHOR_TYPE])
        jsonEdit.EditItemProperty(Nurbojsonfile, "Nurbo_TaxilineObject_AngleCurvesNum", user_path[c4d.SPLINEOBJECT_ANGLE])

        op[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_X] = op[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_X]
        op[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Y] = op[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Y]
        op[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Z] = op[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Z]
        op[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_X] = op[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_X]
        op[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_Y] = op[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_Y]
        op[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_Z] = op[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_Z]

        return sweep_obj
        
    def Message(self, node, type, data):
        Nurbojsonfile = g_lib.fcsDIR.FCS_NURBO_CONIG
        if type==c4d.MSG_DESCRIPTION_COMMAND:
            if data['id'][0].id==self.THE_BUTTON_ID:

                Nurbo_Config = {}

                Nurbo_Config["OptionMode"]="SweepSplines"
                Nurbo_Config["CustomNameChk"]=False
                Nurbo_Config["CustomName"]=""
                Nurbo_Config["GroupItChk"]=False
                Nurbo_Config["GroupName"]=""
                Nurbo_Config["PresetTemp"]="Arc"
                Nurbo_Config["AngleCurvesSettings"]="Default"
                Nurbo_Config["ObjectSplineModeType"]="Default"
                Nurbo_Config["TempSplineName"]="NoTempSpineObjData"
                Nurbo_Config["SweepSettings"]={"Parallel Movement":False, 
                                            "Banking":True, 
                                            "Use Rail Direction":True, 
                                            "Use Rail Scale":True, 
                                            "Stick UVs":False, 
                                            "Constraint Cross Section":True, 
                                            "Keep Segements":False, 
                                            "2-Rail":True,
                                            "Flip Normals":False, 
                                            "Cap_Start":"None", 
                                            "Cap_End":"None", 
                                            "CreateSingleObject":True,}
                Nurbo_Config["Nurbo_TaxilineObject_Type"]="Center"
                Nurbo_Config["Nurbo_TaxilineObject_Size"]=50
                Nurbo_Config["Nurbo_TaxilineObject_AngleCurvesNum"]=0.08726646259971647

                jsonEdit.SaveFile(Nurbojsonfile, Nurbo_Config)

                #jsonEdit.EditItemProperty(self.Nurbojsonfile, "Nurbo_TaxilineObject_Size", self.GetInt32(1026))
                print("Pushed button with the ID and Config is Save.")
        return True            

    def Execute(op, doc, bt, priority, flags):
        op.Message(c4d.MSG_UPDATE)
        return c4d.EXECUTIONRESULT_OK


# ------------------------------
#  Power-Line Gen Tool | W.I.P
# ------------------------------
class O_PLT_Object_Data(ObjectData):
  
    def LookatCamera(self, p):

        doc = c4d.documents.GetActiveDocument()

        bd = doc.GetRenderBaseDraw()
        if bd is None: 
            return c4d.EXECUTIONRESULT_OK
        cp = bd.GetSceneCamera(doc)
        if cp is None: 
            cp = bd.GetEditorCamera()
        if cp is None: 
            return c4d.EXECUTIONRESULT_OK
        local = cp.GetMg().off * (~(p.GetUpMg() * p.GetFrozenMln())) - p.GetRelPos()
        hpb = utils.VectorToHPB(local)
        hpb.y = p.GetRelRot().y
        hpb.z = p.GetRelRot().z
        hpb.x = p.GetRelRot().x
        p.SetRelRot(hpb) 
        p.Message(c4d.MSG_UPDATE)
        return c4d.EXECUTIONRESULT_OK

    def GetVirtualObjects(self, op, hh):

        doc = c4d.documents.GetActiveDocument()

        # Get Power-Line Root Systems UI Settings.
        get_rootSys_obj = op[c4d.ID_SYSTEM_PARENT]
        RootSys = doc.SearchObject(get_rootSys_obj)
        size_set = RootSys[c4d.ID_USERDATA,25]
        icon_On_Off = RootSys[c4d.ID_USERDATA,29]

        obj_null = c4d.BaseObject(c4d.Onull)
        p = c4d.BaseObject(c4d.Oplane)
        p[c4d.PRIM_PLANE_SUBW] = 1
        p[c4d.PRIM_PLANE_SUBH] = 1
        p[c4d.PRIM_PLANE_WIDTH] = size_set
        p[c4d.PRIM_PLANE_HEIGHT] = size_set
        p[c4d.PRIM_AXIS] = 5
        if icon_On_Off == True:
            p[c4d.ID_BASEOBJECT_VISIBILITY_EDITOR] = 2
        else:
            p[c4d.ID_BASEOBJECT_VISIBILITY_EDITOR] = 1        
        p.InsertUnder(obj_null)

        # // Create and Set Material //
        MatName = "Oplts_object_ins"
        userMat = doc.SearchMaterial(MatName)
        tex = p.MakeTag(c4d.Ttexture)
        if not userMat:
            PREVIEWSIZE=1
            shdBitmap = c4d.BaseShader(c4d.Xbitmap)
            if shdBitmap is None:
                print("Error: Shader allocation failed")
                return
            shdBitmap[c4d.BITMAPSHADER_FILENAME] = PLUGIN_DIR().Icon_Path(i="tool_icon_plts_object_128.png")
            Material_Tex = c4d.BaseMaterial(5703)               # Generate Material
            Material_Tex[c4d.ID_BASELIST_NAME] = MatName        # Generate Material Name                  
            Material_Tex[c4d.MATERIAL_PREVIEWSIZE]=PREVIEWSIZE  # Set Material Size
            Material_Tex[c4d.MATERIAL_USE_REFLECTION]=False
            Material_Tex[c4d.MATERIAL_COLOR_SHADER] = shdBitmap # Set Material Shader Tex
            Material_Tex.InsertShader(shdBitmap)                # Insert Material Shader 
            doc.InsertMaterial(Material_Tex)                    # Insert Material  
            tex[c4d.TEXTURETAG_MATERIAL]=Material_Tex
        else:
            tex[c4d.TEXTURETAG_MATERIAL]=userMat

        tex[c4d.TEXTURETAG_PROJECTION]=6
        tex[c4d.TEXTURETAG_TILE]=True

        self.LookatCamera(p)

        custom_obj = op[c4d.ID_OBJECT_INS_LINK]
        if custom_obj == None:
            pass
        else:
            custom_obj_inst = c4d.BaseObject(c4d.Oinstance)
            custom_obj_inst[c4d.INSTANCEOBJECT_LINK] = custom_obj
            custom_obj_inst[c4d.ID_BASEOBJECT_USECOLOR] = 1
            custom_obj_inst.InsertUnder(obj_null)

        return obj_null

    def Execute(op, doc, bt, priority, flags):
        op.SetName("Controller")
        return c4d.EXECUTIONRESULT_OK
class O_PLT_Connector_Data(ObjectData):

    def LookatCamera(self, p):

        doc = c4d.documents.GetActiveDocument()

        bd = doc.GetRenderBaseDraw()
        if bd is None: 
            return c4d.EXECUTIONRESULT_OK
        cp = bd.GetSceneCamera(doc)
        if cp is None: 
            cp = bd.GetEditorCamera()
        if cp is None: 
            return c4d.EXECUTIONRESULT_OK
        local = cp.GetMg().off * (~(p.GetUpMg() * p.GetFrozenMln())) - p.GetRelPos()
        hpb = utils.VectorToHPB(local)
        hpb.y = p.GetRelRot().y
        hpb.z = p.GetRelRot().z
        p.SetRelRot(hpb) 
        p.Message(c4d.MSG_UPDATE)
        return c4d.EXECUTIONRESULT_OK
    def AddObjectIcon(self, parent_root, obj_name, icon, icon_size, enable):
        doc = c4d.documents.GetActiveDocument()
        # // Make Object //
        p = c4d.BaseObject(c4d.Oplane)
        p.SetName(obj_name+"_icon")
        p[c4d.PRIM_PLANE_SUBW] = 1
        p[c4d.PRIM_PLANE_SUBH] = 1
        p[c4d.PRIM_PLANE_WIDTH] = icon_size
        p[c4d.PRIM_PLANE_HEIGHT] = icon_size
        p[c4d.PRIM_AXIS] = 5
        if enable == True:
            p[c4d.ID_BASEOBJECT_VISIBILITY_EDITOR] = 2
        else:
            p[c4d.ID_BASEOBJECT_VISIBILITY_EDITOR] = 1
        p.InsertUnder(parent_root)

        # // Create and Set Material //
        MatName = "Oplts_"+obj_name
        userMat = doc.SearchMaterial(MatName)
        tex = p.MakeTag(c4d.Ttexture)
        if not userMat:
            PREVIEWSIZE=1
            shdBitmap = c4d.BaseShader(c4d.Xbitmap)
            if shdBitmap is None:
                print("Error: Shader allocation failed")
                return
            shdBitmap[c4d.BITMAPSHADER_FILENAME] = PLUGIN_DIR().Icon_Path(i=icon)
            Material_Tex = c4d.BaseMaterial(5703)               # Generate Material
            Material_Tex[c4d.ID_BASELIST_NAME] = MatName        # Generate Material Name                  
            Material_Tex[c4d.MATERIAL_PREVIEWSIZE]=PREVIEWSIZE  # Set Material Size
            Material_Tex[c4d.MATERIAL_USE_REFLECTION]=False
            Material_Tex[c4d.MATERIAL_COLOR_SHADER] = shdBitmap # Set Material Shader Tex
            Material_Tex.InsertShader(shdBitmap)                # Insert Material Shader 
            doc.InsertMaterial(Material_Tex)                    # Insert Material  
            tex[c4d.TEXTURETAG_MATERIAL]=Material_Tex
        else:
            tex[c4d.TEXTURETAG_MATERIAL]=userMat

        tex[c4d.TEXTURETAG_PROJECTION]=6
        tex[c4d.TEXTURETAG_TILE]=True

        self.LookatCamera(p)

        return c4d.EXECUTIONRESULT_OK

    def GetVirtualObjects(self, op, hh):
        doc = c4d.documents.GetActiveDocument()
        # Get Power-Line Root Systems UI Settings.
        get_rootSys_obj = op[c4d.ID_SYSTEM_PARENT]
        RootSys = doc.SearchObject(get_rootSys_obj)
        size_set = RootSys[c4d.ID_USERDATA,24]
        icon_On_Off = RootSys[c4d.ID_USERDATA,27]

        obj_null = c4d.BaseObject(c4d.Onull)
        self.AddObjectIcon(parent_root=obj_null, 
                        obj_name="connector", 
                        icon="tool_icon_plts_connector_128.png", 
                        icon_size=size_set, 
                        enable=icon_On_Off)
        return obj_null

    def Execute(op, doc, bt, priority, flags):
        return c4d.EXECUTIONRESULT_OK
class O_PLT_Controller_Data(ObjectData):

    def Cal_CenterPoint(self, PntA, PntB):
        """ Calulating between two objects, to get center point of them. """
        # X
        PntA_X = PntA.split('--')[0]
        PntB_X = PntB.split('--')[0]
        # Y
        PntA_Y = PntA.split('--')[1]
        PntB_Y = PntB.split('--')[1]
        # Z
        PntA_Z = PntA.split('--')[2]
        PntB_Z = PntB.split('--')[2]
        # Calulating X, Y, Z of A and B.
        X = ( float(PntA_X) + float(PntB_X) ) / 2
        Y = ( float(PntA_Y) + float(PntB_Y) ) / 2
        Z = ( float(PntA_Z) + float(PntB_Z) ) / 2
        # New Point Location
        newLoc = c4d.Vector(X, Y, Z) 
        return newLoc

    def GetObjGlobalCorr(self, OBJ):
        """ Get Object Global Location. """
        getLoc = OBJ.GetMg()
        globalPosData = getLoc.off  # Get the position from the matrix
        # Split up the GlobalPosData Vector that is Store
        removeOpenBracket = str(globalPosData).split('(')
        removeCloseBracket = removeOpenBracket[1].split(')')
        values = removeCloseBracket[0].split(',')
        X_Data = float(values[0])
        Y_Data = float(values[1])
        Z_Data = float(values[2])
        ObjectLocationValue =   str(X_Data) + "--" + str(Y_Data) + "--" + str(Z_Data)
        return ObjectLocationValue

    def A_and_B_Controller(self, op):
        A = op[c4d.CONNECTOR_LK1A]
        B = op[c4d.CONNECTOR_LK2B]

        A_N_B = op.GetChildren()
        for o in A_N_B:
            if A and B == None:
                o.SetAbsPos(c4d.Vector(0, 0, 0))
                o[c4d.ID_BASEOBJECT_VISIBILITY_EDITOR] = 1
            else:
                o[c4d.ID_BASEOBJECT_REL_POSITION] = (A[c4d.ID_BASEOBJECT_REL_POSITION] + B[c4d.ID_BASEOBJECT_REL_POSITION])/2.0
                o[c4d.ID_BASEOBJECT_VISIBILITY_EDITOR] = 2
        return True

    def LookatCamera(self, p):

        doc = c4d.documents.GetActiveDocument()

        bd = doc.GetRenderBaseDraw()
        if bd is None: 
            return c4d.EXECUTIONRESULT_OK
        cp = bd.GetSceneCamera(doc)
        if cp is None: 
            cp = bd.GetEditorCamera()
        if cp is None: 
            return c4d.EXECUTIONRESULT_OK
        local = cp.GetMg().off * (~(p.GetUpMg() * p.GetFrozenMln())) - p.GetRelPos()
        hpb = utils.VectorToHPB(local)
        hpb.y = p.GetRelRot().y
        hpb.z = p.GetRelRot().z
        p.SetRelRot(hpb) 
        p.Message(c4d.MSG_UPDATE)
        return c4d.EXECUTIONRESULT_OK

    def GetVirtualObjects(self, op, hh):

        doc = c4d.documents.GetActiveDocument()

        # Get Power-Line Root Systems UI Settings.
        get_rootSys_obj = op[c4d.ID_SYSTEM_PARENT]
        RootSys = doc.SearchObject(get_rootSys_obj)
        size_set = RootSys[c4d.ID_USERDATA,26]
        icon_On_Off = RootSys[c4d.ID_USERDATA,30]

        obj_null = c4d.BaseObject(c4d.Onull)
        p = c4d.BaseObject(c4d.Oplane)
        p[c4d.PRIM_PLANE_SUBW] = 1
        p[c4d.PRIM_PLANE_SUBH] = 1
        p[c4d.PRIM_PLANE_WIDTH] = size_set
        p[c4d.PRIM_PLANE_HEIGHT] = size_set
        p[c4d.PRIM_AXIS] = 5
        if icon_On_Off == True:
            p[c4d.ID_BASEOBJECT_VISIBILITY_EDITOR] = 2
        else:
            p[c4d.ID_BASEOBJECT_VISIBILITY_EDITOR] = 1        
        p.InsertUnder(obj_null)

        # // Create and Set Material //
        MatName = "Oplts_controller"
        userMat = doc.SearchMaterial(MatName)
        tex = p.MakeTag(c4d.Ttexture)
        if not userMat:
            PREVIEWSIZE=1
            shdBitmap = c4d.BaseShader(c4d.Xbitmap)
            if shdBitmap is None:
                print("Error: Shader allocation failed")
                return
            shdBitmap[c4d.BITMAPSHADER_FILENAME] = PLUGIN_DIR().Icon_Path(i="tool_icon_plts_controller_128.png")
            Material_Tex = c4d.BaseMaterial(5703)               # Generate Material
            Material_Tex[c4d.ID_BASELIST_NAME] = MatName        # Generate Material Name                  
            Material_Tex[c4d.MATERIAL_PREVIEWSIZE]=PREVIEWSIZE  # Set Material Size
            Material_Tex[c4d.MATERIAL_USE_REFLECTION]=False
            Material_Tex[c4d.MATERIAL_COLOR_SHADER] = shdBitmap # Set Material Shader Tex
            Material_Tex.InsertShader(shdBitmap)                # Insert Material Shader 
            doc.InsertMaterial(Material_Tex)                    # Insert Material  
            tex[c4d.TEXTURETAG_MATERIAL]=Material_Tex
        else:
            tex[c4d.TEXTURETAG_MATERIAL]=userMat

        tex[c4d.TEXTURETAG_PROJECTION]=6
        tex[c4d.TEXTURETAG_TILE]=True

        self.LookatCamera(p)

        return obj_null


    def Execute(op, doc, bt, priority, flags):
        op.SetName("Controller")
        return c4d.EXECUTIONRESULT_OK
class O_PLT_Spline_Data(ObjectData):

    def __init__(self):
        self.SetOptimizeCache(True)

    ALIGN_LEFT = (1 << 0)
    ALIGN_RIGHT = (1 << 1)
    ALIGN_CENTERH = (1 << 2)
    ALIGN_TOP = (1 << 3)
    ALIGN_BOTTOM = (1 << 4)
    ALIGN_CENTERV = (1 << 5)

    def get_draw_screen_uvcoords(self, bmp, x, y, w, h):
        bmpw, bmph = map(float, bmp.GetSize())
        corners = [x / bmpw, y / bmph, (x + w) / bmpw, (y + h) / bmph]
        return [
            c4d.Vector(corners[0], corners[1], 0.0),
            c4d.Vector(corners[2], corners[1], 0.0),
            c4d.Vector(corners[2], corners[3], 0.0),
            c4d.Vector(corners[0], corners[3], 0.0)]

    def get_draw_screen_padr(self, pos, w, h, align):
        if align & self.ALIGN_LEFT:
            xoff = 0
        elif align & self.ALIGN_RIGHT:
            xoff = w
        elif align & self.ALIGN_CENTERH or True:
            xoff = w / 2.0
        if align & self.ALIGN_TOP:
            yoff = 0
        elif align & self.ALIGN_BOTTOM:
            yoff = h
        elif align & self.ALIGN_CENTERV or True:
            yoff = h / 2.0

        x, y = (pos.x - xoff, pos.y - yoff)
        return [
            c4d.Vector(x,     y, 0.0),
            c4d.Vector(x + w, y, 0.0),
            c4d.Vector(x + w, y + h, 0.0),
            c4d.Vector(x,     y + h, 0.0)]

    def Draw(self, op, drawpass, bd, bh):

        plugin_Icon = c4d.bitmaps.BaseBitmap()
        plugin_Icon.InitWith(PLUGIN_DIR().Icon_Path(i="tool_icon_plts_connector_128.png"))

        if drawpass != c4d.DRAWPASS_OBJECT: 
            return c4d.DRAWRESULT_SKIP
        if not plugin_Icon: 
            return c4d.DRAWRESULT_SKIP

        wpsize = 48

        bottomIcon_Flag  = self.ALIGN_CENTERH|self.ALIGN_BOTTOM
        centerIcon_Flag = self.ALIGN_CENTERH|self.ALIGN_CENTERV

        # Draw the object icon on the screen.
        pos = bd.WS(op.GetMg().off)
        bmp = plugin_Icon
        padr = self.get_draw_screen_padr(pos, wpsize, wpsize, centerIcon_Flag)
        uvadr = self.get_draw_screen_uvcoords(bmp, 0, 0, bmp.GetBw(), bmp.GetBh())
        cadr = [c4d.Vector(1.0)] * 4
        vnadr = [c4d.Vector(0.0, 0.0, 1.0)] * 4
        mode = c4d.DRAW_ALPHA_NONE # c4d.DRAW_ALPHA_NORMAL
        flags = c4d.DRAW_TEXTUREFLAGS_INTERPOLATION_LINEAR #c4d.DRAW_TEXTUREFLAGS_0
        
        """
        The coordinates must be in the space defined by SetMatrix_Screen(), SetMatrix_Camera() or SetMatrix_Matrix().
        SetMatrix_Camera() -> Sets the transformation matrix to the camera system.
        SetMatrix_Screen(offset=-1) -> Sets the transformation matrix to screen coordinates, i.e. from (0, 0) to (width, height).
        Note: This function only affects the new draw functions below it, i.e. DrawHandle(), DrawTexture(), DrawCircle(), DrawBox(), and DrawSphere().
        """
        bd.SetMatrix_Screen(4)

        bd.DrawTexture(bmp, padr, cadr, vnadr, uvadr, 4, mode, flags)

        return c4d.DRAWRESULT_OK

    def GetVirtualObjects(self, op, hh):
        doc = c4d.documents.GetActiveDocument()
        obj = c4d.BaseObject(c4d.Oconnector)
        obj[c4d.ID_BASEOBJECT_USECOLOR]=2
        obj[c4d.ID_BASEOBJECT_COLOR]=c4d.Vector(0.8, 0.453, 0)
        return obj

    def Execute(op, doc, bt, priority, flags):
        return c4d.EXECUTIONRESULT_OK
class O_PLT_Connectors_Data(plugins.ObjectData):

    def GetVirtualObjects(self, op, hh):
        doc = c4d.documents.GetActiveDocument()
        obj = c4d.BaseObject(c4d.Onull)
        obj[c4d.ID_BASEOBJECT_USECOLOR]=2
        obj[c4d.ID_BASEOBJECT_COLOR]=c4d.Vector(0.8, 0.453, 0)
        return obj

    def Execute(op, doc, bt, priority, flags):
        op.SetName("PLS_Connectors")
        return c4d.EXECUTIONRESULT_OK
