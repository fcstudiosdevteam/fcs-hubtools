# ----------------------------------------------------
#               Imports for Cinema 4D
# ----------------------------------------------------
import c4d, os, sys, subprocess, webbrowser, collections, math, random, urllib, glob, shutil, time, datetime, weakref
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as WindowDialog        # eg:( class My_Dialog(c4d.gui.GeDialog): ) to ( class My_Dialog(WindowDialog): ) Just keeping code line short.
from c4d.plugins import CommandData, TagData, ObjectData
from random import randint
from os import urandom
# Regex Import
import re
# XML Imports is for Saving and Loading Project Data for Plugin UI / Xml good but long functions to make.
from xml.dom import minidom
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree
# Json Import to store or load descriptions or any data / .json is easy and alot flexable and short coding.
import json
# Python Plugin System Load Modules Dynamically With importlib.
import importlib

# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
import fcs_quick_scripts_popupmenu
import fcs_layouts_popupmenu
#from fcs_common_global_lib import Global_Enums_IDs as fcsID
#jsonEdit = g_lib.FCS_JsonSystem_Editor()
c4d_prefs_folder = c4d.storage.GeGetC4DPath(c4d.C4D_PATH_PREFS) # To find Cinema 4D Appdata Dir's of the Preferences Folder.
FCS_LOGGING_CONIG = os.path.join(c4d_prefs_folder, 'fcs_plugin_prefs', 'FCS_Tools_Logging_Output.txt')

#fcsLog = g_lib.FCS_LOGGER(MainLogFile=FCS_LOGGING_CONIG)

#fcsLog.INFO("FCS Quick Scripts is Added and Loaded", False)


###############################################################################################################################
# An example for how you call or access the function method that is in a class from anther (.py) file, like this file.
# In the other main (.py) file your code will look like this to access this file you are in now, example below.  
# Example:
# fcs_module_folder = os.path.join(os.path.dirname(__file__), 'res', '_plugin_database', 'fcs_plugin_modules')
# if fcs_module_folder not in sys.path:
#    sys.path.insert(0, fcs_module_folder)
# sys.path.append(fcs_module_folder)
# import fcs_scripts_pack
# from fcs_scripts_pack import Global_QuickScripts_Data as QS
# .... 
# output_Data = QS().Auto_Poly_Checker()
# ....
# or to get a class():
# ....
# output_Data = fcs_scripts_pack.Auto_Poly_Checker_Data()
# .....
#################################################################################################################################

# To add and create random colors to UI elements and objects.
def Add_Vector_Random_Colors():
    r = randint(0,255) / 256.0
    g = randint(0,255) / 256.0
    b = randint(0,255) / 256.0
    return c4d.Vector(r, g, b)

""" All The Global Scripts Main Data """
class Global_QuickScripts_Data(object):
    """ All The Global Scripts Main Data """

    def __init__(self):
        super(Global_QuickScripts_Data, self).__init__()

    """ Auto Poly Checker Script Tool """
    def Auto_Poly_Checker(self):
        doc = c4d.documents.GetActiveDocument()
        SelObjs = doc.GetActiveObjects(1)
        self.TaxilineCoreFunc()
        for EObj in SelObjs:
            doc.SetActiveObject(EObj, c4d.SELECTION_ADD)
        sel_objs = doc.GetSelection()
        g_lib.fcsLog.INFO("AutoCheck is now Auto Checking", False, g_lib.DebugMode)
        c4d.StatusSetText("AutoCheck is now Auto Checking")
        processWin = FCS_ProcessPopupWindow(" Auto Poly Checker Checking... | Running")
        processWin.openWindowUI(g_lib.DLG_TYPE1) 
        c4d.CallCommand(12298) # Model
        c4d.CallCommand(100004768, 100004768) # Select Children
        c4d.CallCommand(100004768, 100004768) # Select Children
        c4d.CallCommand(12236) # Make Editable
        c4d.CallCommand(100004768, 100004768) # Select Children
        c4d.CallCommand(1031882, 1031882) # Instance Vault C4D Make Editable
        # Find Splines and Remove it.
        currentNum = 1
        for EachObj in sel_objs:
            processWin.processBAR.Run_ProgressBar(1001, 1010, currentNum, len(SelObjs), g_lib.DARK_BLUE_TEXT_COL)
            """
            if EachObj.CheckType(c4d.Ospline):
                # Find Splines and Remove it.
                doc.SetActiveObject(EachObj)
                #c4d.CallCommand(1019951, 1019951) # Delete Without Children  
                g_lib.fcsLog.INFO("Spline Remove", False, g_lib.DebugMode)
            else:
                pass
            """
            c4d.CallCommand(100004768, 100004768) # Select Children
            c4d.CallCommand(12236) # Make Editable            
            currentNum += 1
        processWin.processBAR.Stop_ProgressBar(1010)
        c4d.StatusSetText("AutoCheck is Compelete")
        g_lib.fcsLog.INFO("AutoCheck is Compelete", False, g_lib.DebugMode)
        processWin.closeWindowUI()
        g_lib.fcsLog.INFO("AutoCheck was done Successfully.", True, g_lib.DebugMode)
        c4d.CallCommand(12298) # Model
        c4d.CallCommand(100004768, 100004768) # Select Children   
        g_lib.fcsLog.INFO("Make Editable Auto Check Done", False, g_lib.DebugMode)
        c4d.StatusClear()
        c4d.EventAdd()
        return True

    """ NewDoc + Selected Script Tool """
    def NewDocSelected(self):
        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False
        # Get Active Document Data.     
        docActiveUnit = doc.GetDocumentData(c4d.DOCUMENTSETTINGS_DOCUMENT)
        # Get Objects form the Object Manager 
        c4d.CallCommand(100004820, 100004820) # Copy
        #---------------| New Doc |-----------------#
        c4d.CallCommand(12094, 12094) # New
        Newdoc = c4d.documents.GetActiveDocument()
        Newdoc.SetDocumentData(c4d.DOCUMENTSETTINGS_DOCUMENT, docActiveUnit)
        c4d.CallCommand(100004821, 100004821) # Paste
        c4d.CallCommand(12373) # Project Settings...
        return True 

    """ Set Material Rez Script Tool """
    def SetMaterialRez(self):
        IDM1 = c4d.FIRST_POPUP_ID
        IDM2 = c4d.FIRST_POPUP_ID+2
        IDM3 = c4d.FIRST_POPUP_ID+3
        IDM4 = c4d.FIRST_POPUP_ID+4
        IDM5 = c4d.FIRST_POPUP_ID+5
        IDM6 = c4d.FIRST_POPUP_ID+6
        IDM7 = c4d.FIRST_POPUP_ID+7
        IDM8 = c4d.FIRST_POPUP_ID+8
        IDM9 = c4d.FIRST_POPUP_ID+9
                
        menu = c4d.BaseContainer()
        menu.SetString(IDM7, 'Set Texture Size')
        menu.SetString(0, '')
        menu.SetString(IDM9, '&i13015&No Scaling')
        menu.SetString(IDM8, '&i13015&256 x 256')
        menu.SetString(IDM1, '&i13015&512 x 512')
        menu.SetString(IDM2, '&i13015&1024 x 1024')
        menu.SetString(IDM3, '&i13015&2048 x 2048')
        menu.SetString(IDM4, '&i13015&4096 x 4096')
        menu.SetString(IDM5, '&i13015&8192 x 8192')
        menu.SetString(IDM6, '&i13015&16381 x 16381')    

        result = gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)
        Material_NoScaling = 1
        Material_256 = 8
        Material_512 = 9
        Material_1024 = 10
        Material_2048 = 11
        Material_4096 = 12 
        Material_8192 = 13
        Material_16381 = 14

        if result==IDM1: 
            doc = c4d.documents.GetActiveDocument()
            Mats = doc.GetActiveMaterials()
            for eachMat in Mats:
                eachMat[c4d.MATERIAL_PREVIEWSIZE]=Material_512
        if result==IDM2: 
            doc = c4d.documents.GetActiveDocument()
            Mats = doc.GetActiveMaterials()
            for eachMat in Mats:
                eachMat[c4d.MATERIAL_PREVIEWSIZE]=Material_1024
        if result==IDM3: 
            doc = c4d.documents.GetActiveDocument()
            Mats = doc.GetActiveMaterials()
            for eachMat in Mats:
                eachMat[c4d.MATERIAL_PREVIEWSIZE]=Material_2048
        if result==IDM4: 
            doc = c4d.documents.GetActiveDocument()
            Mats = doc.GetActiveMaterials()
            for eachMat in Mats:
                eachMat[c4d.MATERIAL_PREVIEWSIZE]=Material_4096
        if result==IDM5: 
            doc = c4d.documents.GetActiveDocument()
            Mats = doc.GetActiveMaterials()
            for eachMat in Mats:
                eachMat[c4d.MATERIAL_PREVIEWSIZE]=Material_8192           
        if result==IDM6: 
            doc = c4d.documents.GetActiveDocument()
            Mats = doc.GetActiveMaterials()
            for eachMat in Mats:
                eachMat[c4d.MATERIAL_PREVIEWSIZE]=Material_16381
        if result==IDM8: 
            doc = c4d.documents.GetActiveDocument()
            Mats = doc.GetActiveMaterials()
            for eachMat in Mats:
                eachMat[c4d.MATERIAL_PREVIEWSIZE]=Material_256
        if result==IDM9: 
            doc = c4d.documents.GetActiveDocument()
            Mats = doc.GetActiveMaterials()
            for eachMat in Mats:
                eachMat[c4d.MATERIAL_PREVIEWSIZE]=Material_NoScaling                                
        c4d.EventAdd()  
        return True
    
    """ Select All + Connect + Delete Script Tool """
    def SelectAllConnectDelete(self):
        c4d.CallCommand(100004768, 100004768) # Select Children
        c4d.CallCommand(12236) # Make Editable
        c4d.CallCommand(16768, 16768) # Connect Objects + Delete
        return True

    """ Add AO-AmbientOcclusion To Material Script Tool """
    def Add_AO2Mat(self):
        #--| AmbientOcclusion |--#
        doc = c4d.documents.GetActiveDocument()
        Mats = doc.GetActiveMaterials()
        for MatAO in Mats:
            AO = c4d.BaseShader(c4d.Xambientocclusion)
            AO[c4d.AMBIENTOCCLUSIONSHADER_MINLENGTH]=0.0
            AO[c4d.AMBIENTOCCLUSIONSHADER_MAXLENGTH]=500.0
            AO[c4d.AMBIENTOCCLUSIONSHADER_DISPERSION]=1.0
            AO[c4d.AMBIENTOCCLUSIONSHADER_ACCURACY]=1.0
            AO[c4d.AMBIENTOCCLUSIONSHADER_MINSAMPLES]=128
            AO[c4d.AMBIENTOCCLUSIONSHADER_MAXSAMPLES]=128
            AO[c4d.AMBIENTOCCLUSIONSHADER_CONTRAST]=0.01
            AO[c4d.AMBIENTOCCLUSIONSHADER_ENVIRONMENT]=0
            AO[c4d.AMBIENTOCCLUSIONSHADER_TRANSPARENCY]=1
            AO[c4d.AMBIENTOCCLUSIONSHADER_SELFTEST]=0
            #--| SET MATERIAL SETTINGS |--#
            MatAO[c4d.MATERIAL_USE_DIFFUSION]=True
            MatAO[c4d.MATERIAL_DIFFUSION_SHADER] = AO
            MatAO.InsertShader(AO)
        c4d.EventAdd()      
        return True

    """ Compress It Script Tool """    
    def CompressIt(self):
        desc_pos_x = c4d.DescID(c4d.DescLevel(c4d.ID_BASEOBJECT_POSITION,c4d.DTYPE_VECTOR,0), c4d.DescLevel(c4d.VECTOR_X,c4d.DTYPE_REAL,0))
        desc_pos_y = c4d.DescID(c4d.DescLevel(c4d.ID_BASEOBJECT_POSITION,c4d.DTYPE_VECTOR,0), c4d.DescLevel(c4d.VECTOR_Y,c4d.DTYPE_REAL,0))
        desc_pos_z = c4d.DescID(c4d.DescLevel(c4d.ID_BASEOBJECT_POSITION,c4d.DTYPE_VECTOR,0), c4d.DescLevel(c4d.VECTOR_Z,c4d.DTYPE_REAL,0))
        desc_rot_x = c4d.DescID(c4d.DescLevel(c4d.ID_BASEOBJECT_ROTATION,c4d.DTYPE_VECTOR,0), c4d.DescLevel(c4d.VECTOR_X,c4d.DTYPE_REAL,0))
        desc_rot_y = c4d.DescID(c4d.DescLevel(c4d.ID_BASEOBJECT_ROTATION,c4d.DTYPE_VECTOR,0), c4d.DescLevel(c4d.VECTOR_Y,c4d.DTYPE_REAL,0))
        desc_rot_z = c4d.DescID(c4d.DescLevel(c4d.ID_BASEOBJECT_ROTATION,c4d.DTYPE_VECTOR,0), c4d.DescLevel(c4d.VECTOR_Z,c4d.DTYPE_REAL,0))
        desc_scale_x = c4d.DescID(c4d.DescLevel(c4d.ID_BASEOBJECT_SCALE,c4d.DTYPE_VECTOR,0), c4d.DescLevel(c4d.VECTOR_X,c4d.DTYPE_REAL,0))
        desc_scale_y = c4d.DescID(c4d.DescLevel(c4d.ID_BASEOBJECT_SCALE,c4d.DTYPE_VECTOR,0), c4d.DescLevel(c4d.VECTOR_Y,c4d.DTYPE_REAL,0))
        desc_scale_z = c4d.DescID(c4d.DescLevel(c4d.ID_BASEOBJECT_SCALE,c4d.DTYPE_VECTOR,0), c4d.DescLevel(c4d.VECTOR_Z,c4d.DTYPE_REAL,0))        

        doc = c4d.documents.GetActiveDocument()
        doc.StartUndo()    
        gp_sel = doc.GetSelection()
        for x in gp_sel:
            doc.AddUndo(c4d.UNDOTYPE_CHANGE, x)        
            xPos=x.GetMg()
            xPosL=x.GetMl()
            null=c4d.BaseObject(c4d.Oconnector)
            null[c4d.CONNECTOBJECT_WELD]=False
            name=x.GetName() # Test name=str("c4d")+x.GetName()
            doc.AddUndo(c4d.UNDOTYPE_NEW, null)
            null.SetName(name)
            doc.InsertObject(null, None, x)
            null.SetMg(xPos)
            doc.SetSelection(null, c4d.SELECTION_ADD)
            doc.SetSelection(x, c4d.SELECTION_SUB)
            x.InsertUnder(null)
            x.SetMg(xPos)
            trackPosX=x.FindCTrack(desc_pos_x)
            trackPosY=x.FindCTrack(desc_pos_y)
            trackPosZ=x.FindCTrack(desc_pos_z)
            trackRotX=x.FindCTrack(desc_rot_x)
            trackRotY=x.FindCTrack(desc_rot_y)
            trackRotZ=x.FindCTrack(desc_rot_z)
            trackScaleX=x.FindCTrack(desc_scale_x)
            trackScaleY=x.FindCTrack(desc_scale_y)
            trackScaleZ=x.FindCTrack(desc_scale_z)
            for track in x.GetCTracks():
                null.InsertTrackSorted(trackPosX)
                null.InsertTrackSorted(trackPosY)
                null.InsertTrackSorted(trackPosZ)
                null.InsertTrackSorted(trackRotX)
                null.InsertTrackSorted(trackRotY)
                null.InsertTrackSorted(trackRotZ)
                null.InsertTrackSorted(trackScaleX)
                null.InsertTrackSorted(trackScaleY)
                null.InsertTrackSorted(trackScaleZ)
            doc.AddUndo(c4d.UNDOTYPE_NEW, x)
            x.SetAbsPos(c4d.Vector(0,0,0))
            x.SetAbsRot(c4d.Vector(0,0,0))
            x.SetAbsScale(c4d.Vector(1,1,1))    
        c4d.CallCommand(12236) # Make Editable
        c4d.CallCommand(100004768, 100004768) # Select Children
        c4d.CallCommand(100004773, 100004773) # Expand Object Group
        c4d.CallCommand(100004768, 100004768) # Select Children       
        doc = c4d.documents.GetActiveDocument() 
        obj = doc.GetFirstObject()        
        g_lib.RemoveEmptyNulls(obj)       
        doc.EndUndo()
        g_lib.fcsLog.INFO("Compress It is Compelete.", True, g_lib.DebugMode)
        c4d.EventAdd()      
        return True # Compress It Script Tool 

    """ Delete Poly Script Tool """  
    def Delete_Poly(self):
        c4d.CallCommand(12109, 12109) # Delete
        c4d.CallCommand(12139) # Points
        c4d.CallCommand(13323, 13323) # Select All
        c4d.CallCommand(14039, 14039) # Optimize
        c4d.CallCommand(12187) # Polygons    
        return True

    """ Optimize It Script Tool """
    def Optimize_It(self):
        c4d.CallCommand(12298, 12298) # Model
        c4d.CallCommand(12139) # Points
        c4d.CallCommand(13323, 13323) # Select All
        c4d.CallCommand(14039, 14039) # Optimize
        c4d.CallCommand(13324, 13324) # Deselect All
        c4d.CallCommand(12298, 12298) # Model 
        return True

    """ AR Normals Script Tool """
    def ARNormals(self):
        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False
        c4d.StatusSetBar(1)   
        #c4d.StatusSetText("Auto Normaling")    
        c4d.CallCommand(100004768, 100004768) # Select Children
        c4d.CallCommand(100004768, 100004768) # Select Children
        c4d.CallCommand(100004768, 100004768) # Select Children
        c4d.CallCommand(100004768, 100004768) # Select Children
        c4d.StatusSetBar(40)
        c4d.StatusSetText("Auto Normaling")
        c4d.CallCommand(100004768, 100004768) # Select Children
        c4d.CallCommand(100004768, 100004768) # Select Children
        c4d.CallCommand(100004768, 100004768) # Select Children
        c4d.CallCommand(100004768, 100004768) # Select Children
        c4d.StatusSetBar(50)
        c4d.StatusSetText("Auto Normaling")
        c4d.CallCommand(100004768, 100004768) # Select Children
        c4d.CallCommand(100004768, 100004768) # Select Children
        c4d.CallCommand(100004768, 100004768) # Select Children
        c4d.CallCommand(100004768, 100004768) # Select Children        
        c4d.CallCommand(12187) # Polygons
        c4d.StatusSetBar(70)   
        c4d.StatusSetText("Auto Normaling")     
        c4d.CallCommand(13323, 13323) # Select All
        c4d.StatusSetBar(90)   
        c4d.StatusSetText("Auto Normaling")     
        c4d.CallCommand(14041, 14041) # Reverse Normals
        c4d.CallCommand(12298) # Model
        c4d.StatusSetBar(100)   
        c4d.StatusSetText("Auto Normal Done")
        c4d.StatusClear()
        c4d.EventAdd()
        c4d.StatusSetText("Auto Normal was done Successfully.")
        g_lib.fcsLog.INFO("Auto Normal is compeletely successfully.", True, g_lib.DebugMode)  
        c4d.EventAdd()
        return True 

    """ Axis To Zero Script Tool """
    def Axis_2_Zero(self):
        doc = c4d.documents.GetActiveDocument()  
        
        objsel = doc.GetSelection()

        if not objsel:
            g_lib.fcsLog.INFO("Please Select an Object", True, g_lib.DebugMode)
            return
        
        for obj in objsel:
            
            if not obj or not obj.CheckType(c4d.Opoint):
                return
                
            mg     = obj.GetMg()
            pcount = obj.GetPointCount()
            
            # tell c4d to encapsulate the following steps into a single
            # undo step
            doc.StartUndo()
            
            for i in xrange(pcount):
                point = obj.GetPoint(i)
                doc.AddUndo(c4d.UNDOTYPE_CHANGE, obj)
                obj.SetPoint(i, point * mg)
            
            doc.AddUndo(c4d.UNDOTYPE_CHANGE, obj)
            obj.SetAbsPos(c4d.Vector(0))
            obj.SetAbsRot(c4d.Vector(0))
            obj.Message(c4d.MSG_UPDATE)
        
            # close the undo encapsulation and tell c4d to update the gui
            doc.EndUndo()
            c4d.EventAdd()
        return True

    """ Set Unit Scale Script Tool """
    def SetUnit_Function(self, unit_type, pref_type, unit_str_type):
        global fileSeparator
        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False        
        # Set project scale
        unitScale = c4d.UnitScaleData()
        unitScale.SetUnitScale(1.0, unit_type)
        bc = c4d.BaseContainer()
        bc[c4d.DOCUMENT_DOCUNIT] = unitScale
        doc.SetDocumentData(c4d. DOCUMENTSETTINGS_DOCUMENT, bc)
        PREFS_Unit_ID = 465001627
        Unit_Prefs = c4d.plugins.FindPlugin(PREFS_Unit_ID, c4d.PLUGINTYPE_PREFS)
        Unit_Prefs[c4d.PREF_UNITS_BASIC]=pref_type
        doc = c4d.documents.GetActiveDocument()
        c4d.EventAdd()
        g_lib.fcsLog.INFO("You have just set whole C4d project unit system to " + unit_str_type, True, g_lib.DebugMode)
        return True
    def SetUnitScale(self):
        M0 = c4d.FIRST_POPUP_ID 
        M1 = c4d.FIRST_POPUP_ID+1
        M2 = c4d.FIRST_POPUP_ID+2
        M3 = c4d.FIRST_POPUP_ID+3
        M4 = c4d.FIRST_POPUP_ID+4
        M5 = c4d.FIRST_POPUP_ID+5
        M6 = c4d.FIRST_POPUP_ID+6
        M7 = c4d.FIRST_POPUP_ID+7
        M8 = c4d.FIRST_POPUP_ID+8
        M9 = c4d.FIRST_POPUP_ID+9
        M10 = c4d.FIRST_POPUP_ID+10

        menu = c4d.BaseContainer()
        menu.SetString(M0,'Select A Unit:')
        menu.SetString(0,'')
        menu.SetString(M1,'&i17972&Kilometers')
        menu.SetString(M2,'&i17972&Meters')
        menu.SetString(M3,'&i17972&Centimeters')
        menu.SetString(M4,'&i17972&Millimeters')
        menu.SetString(M5,'&i17972&Micrometers')
        menu.SetString(M6,'&i17972&Nanometers')
        menu.SetString(M7,'&i17972&Miles')
        menu.SetString(M8,'&i17972&Yards')
        menu.SetString(M9,'&i17972&Feet')
        menu.SetString(M10,'&i17972&Inches')

        result = gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)
        
        if result==M1:
            self.SetUnit_Function(unit_type=c4d.DOCUMENT_UNIT_KM, pref_type=1, unit_str_type='Kilometers (km).')

        if result==M2:
            self.SetUnit_Function(unit_type=c4d.DOCUMENT_UNIT_M, pref_type=2,  unit_str_type='Meters (m).')

        if result==M3:
            self.SetUnit_Function(unit_type=c4d.DOCUMENT_UNIT_CM, pref_type=3,  unit_str_type='Centimeters (cm).')

        if result==M4:
            self.SetUnit_Function(unit_type=c4d.DOCUMENT_UNIT_MM, pref_type=4,  unit_str_type='Millimeters (mm).')

        if result==M5:
            self.SetUnit_Function(unit_type=c4d.DOCUMENT_UNIT_MICRO, pref_type=5,  unit_str_type='Micrometers (micro).')

        if result==M6:
            self.SetUnit_Function(unit_type=c4d.DOCUMENT_UNIT_NM, pref_type=6,  unit_str_type='Nanometers (nm).')

        if result==M7:
            self.SetUnit_Function(unit_type=c4d.DOCUMENT_UNIT_MILE, pref_type=7,  unit_str_type='Miles (miles).')

        if result==M8:
            self.SetUnit_Function(unit_type=c4d.DOCUMENT_UNIT_YARD, pref_type=8,  unit_str_type='Yards (yard).')

        if result==M9:
            self.SetUnit_Function(unit_type=c4d.DOCUMENT_UNIT_FOOT, pref_type=9,  unit_str_type='Feet (ft).')

        if result==M10:
            self.SetUnit_Function(unit_type=c4d.DOCUMENT_UNIT_INCH, pref_type=10,  unit_str_type='Inches (in).')
        return True 

    """ Toggle Grid Axis Script Tool """
    def ToggleGridAxis(self):
        doc = c4d.documents.GetActiveDocument()
        T = doc.GetRenderBaseDraw()
        if T[c4d.BASEDRAW_DISPLAYFILTER_WORLDAXIS]==False:
            T[c4d.BASEDRAW_DISPLAYFILTER_WORLDAXIS]=True
            T[c4d.BASEDRAW_DISPLAYFILTER_GRID]=True
            T[c4d.BASEDRAW_DATA_TINTBORDER_OPACITY]=0.10000000149011612
        elif T[c4d.BASEDRAW_DISPLAYFILTER_WORLDAXIS]==True:
            T[c4d.BASEDRAW_DISPLAYFILTER_WORLDAXIS]=False
            T[c4d.BASEDRAW_DISPLAYFILTER_GRID]=False
            T[c4d.BASEDRAW_DATA_TINTBORDER_OPACITY]=0.0  
        c4d.EventAdd()
        return True

    """ Speckle Script Tool """
    def Speckle(self):
        #IDM1 = c4d.FIRST_POPUP_ID
        IDM2 = c4d.FIRST_POPUP_ID+2
        IDM3 = c4d.FIRST_POPUP_ID+3

        menu = c4d.BaseContainer()
        menu.SetString(IDM2, 'ON')
        menu.SetString(IDM3, 'OFF')

        result = gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)

        if result==IDM2:
            doc = c4d.documents.GetActiveDocument()
            c4d.CallCommand(100004766, 100004766) # Select All
            c4d.CallCommand(12112, 12112) # Select All
            c4d.CallCommand(100004768, 100004768) # Select Children
            list_objs = doc.GetActiveObjects(1)
            if not list_objs:
                g_lib.fcsLog.WARNING("Select an Object!", True, g_lib.DebugMode)
                return
            c4d.CallCommand(100004768, 100004768) # Select Children
            for E in list_objs:
                
                c4d.CallCommand(100004768, 100004768) # Select Children
                E[c4d.ID_BASEOBJECT_USECOLOR]=2
                E[c4d.ID_BASEOBJECT_COLOR] = Add_Vector_Random_Colors()
                c4d.CallCommand(12113, 12113) # Deselect All
                
            c4d.EventAdd()      
            return True

        if result==IDM3: 
            doc = c4d.documents.GetActiveDocument()
            c4d.CallCommand(100004766, 100004766) # Select All
            c4d.CallCommand(12112, 12112) # Select All
            c4d.CallCommand(100004768, 100004768) # Select Children
            list_objs = doc.GetActiveObjects(1)
            if not list_objs:
                g_lib.fcsLog.WARNING("Select an Object!", True, g_lib.DebugMode)
                return
            c4d.CallCommand(100004768, 100004768) # Select Children
            for E in list_objs:
                
                c4d.CallCommand(100004768, 100004768) # Select Children
                E[c4d.ID_BASEOBJECT_USECOLOR]=0
                c4d.CallCommand(12113, 12113) # Deselect All
            c4d.EventAdd()
        return True

    """ Fill Spline Shape Script Tool """
    def FillSplineShape(self):
        """ Fill Spline Shape Script Tool """
        doc = c4d.documents.GetActiveDocument()
        
        objs = doc.GetActiveObjects(1)

        if not objs:
            gui.MessageDialog("You must select an object or spline!")
            return False

        for i in objs:

            c4d.CallCommand(1011982) # Center Axis to

            i.SetName("SplineShape")

            # if needed
            #get_obj_loc = i.GetMg()

            # Make Loft Object
            getC4dObj = c4d.BaseObject(c4d.Oloft)
            getC4dObj[c4d.ID_BASEOBJECT_USECOLOR]=1
            getC4dObj[c4d.ID_BASEOBJECT_COLOR]=g_lib.Add_Vector_Random_Colors()
            g_lib.SetObjectName(name="Fill Shape", obj=getC4dObj, amount=400)
            #getC4dObj.SetMg(get_obj_loc)
            doc.InsertObject(getC4dObj)
            i.InsertUnder(getC4dObj)
            c4d.EventAdd()

            c4d.CallCommand(12298) # Model
            c4d.CallCommand(12113, 12113) # Deselect All

        c4d.EventAdd()
        return True      

    def TaxilineCoreFunc(self):
        doc = c4d.documents.GetActiveDocument()

        doc.StartUndo()

        objs = doc.GetActiveObjects(1)

        for i in objs:

            c4d.CallCommand(12298, 12298) # Model
            if i.CheckType(1053591):
                # Make Editable
                g_lib.ObjectToCurrentState(i)
                # Remove Extra User Path Child
                c4d.CallCommand(100004768, 100004768) # Select Children
                obj = doc.GetActiveObjects(1)
                d1 = obj[-2]
                g_lib.ObjectToCurrentState(d1)
                doc.AddUndo(c4d.UNDOTYPE_CHANGE, d1)
                #print(len(obj))
                d2 = obj[-1]
                d2.Remove()
                doc.AddUndo(c4d.UNDOTYPE_CHANGE, d2)
                doc.AddUndo(c4d.UNDOTYPE_NEW, i)
            else:
                pass

        doc.EndUndo()
        c4d.EventAdd()                    
        return True

    def CovertNurboTaxiLine(self):
        doc = c4d.documents.GetActiveDocument()

        doc.StartUndo()

        objs = doc.GetActiveObjects(1)

        if not objs:
            g_lib.fcsLog.WARNING("You must select an editable spline!", True, g_lib.DebugMode)
            return

        for i in objs:

            c4d.CallCommand(12298, 12298) # Model
            if i.CheckType(1053591):
                # Make Editable
                g_lib.ObjectToCurrentState(i)
                # Remove Extra User Path Child
                c4d.CallCommand(100004768, 100004768) # Select Children
                obj = doc.GetActiveObjects(1)
                d1 = obj[-2]
                g_lib.ObjectToCurrentState(d1)
                doc.AddUndo(c4d.UNDOTYPE_CHANGE, d1)
                #print(len(obj))
                d2 = obj[-1]
                d2.Remove()
                doc.AddUndo(c4d.UNDOTYPE_CHANGE, d2)
                doc.AddUndo(c4d.UNDOTYPE_NEW, i)

            else:
                pass
                #g_lib.fcsLog.WARNING("You need to select an Taxi object!", True, g_lib.DebugMode)
                #return

        doc.EndUndo()
        c4d.EventAdd()        
        return True    

""" The Plugin Data Type Classes Section, For Registration these  Global Scripts """
class CMDTool(CommandData):
    """ 
    The Plugin Data Type Classes Section, For Registration these  Global Scripts
    Main C4D GUI Dialog Operations Functions which are Methods to Override for CommandData 
    """

    def __init__(self, SelectedTool):
        super(CMDTool, self).__init__()
        self.CMDTool = SelectedTool

    def Init(self, op):
        return True

    def Message(self, type, data):
        return True
  
    def Execute(self, doc):


        if self.CMDTool == "FQS":
            menu = fcs_quick_scripts_popupmenu                      # Quick Commands Scripts Menu
            menu.FCS_OuickScripts()            

        if self.CMDTool == "AFL":                                    # FCS Layouts Menu Commands
            menu = fcs_layouts_popupmenu
            menu.FCS_Layouts_Script_PopupMenu()

        if self.CMDTool == "NDS":   
            Global_QuickScripts_Data().NewDocSelected()             # NewDoc + Selected Script Tool

        if self.CMDTool == "APC":
            Global_QuickScripts_Data().Auto_Poly_Checker()          # Auto Poly Checker Script Tool

        if self.CMDTool == "SMR":
            Global_QuickScripts_Data().SetMaterialRez()             # Set Material Rez Script Tool

        if self.CMDTool == "SCD":
            Global_QuickScripts_Data().SelectAllConnectDelete()     # Select All + Connect + Delete Script Tool

        if self.CMDTool == "AOM":
            Global_QuickScripts_Data().Add_AO2Mat()                 # Add AO-AmbientOcclusion To Material Script Tool

        if self.CMDTool == "CIt":
            Global_QuickScripts_Data().CompressIt()                 # Compress It Script Tool   

        if self.CMDTool == "D_P":
            Global_QuickScripts_Data().Delete_Poly()                # Delete Poly Script Tool

        if self.CMDTool == "OIt":
            Global_QuickScripts_Data().Optimize_It()                # Optimize It Script Tool

        if self.CMDTool == "ARN":
            Global_QuickScripts_Data().ARNormals()                  # AR Normals Script Tool

        if self.CMDTool == "A2Z":
            Global_QuickScripts_Data().Axis_2_Zero()                # Axis To Zero Script Tool

        if self.CMDTool == "SUS":
            Global_QuickScripts_Data().SetUnitScale()               # Set Unit Scale Script Tool

        if self.CMDTool == "TGA":
            Global_QuickScripts_Data().ToggleGridAxis()             # Toggle Grid Axis Script Tool

        if self.CMDTool == "S_O":
            Global_QuickScripts_Data().Speckle()                    # Speckle Script Tool

        if self.CMDTool == "FSS":
            Global_QuickScripts_Data().FillSplineShape()            # FillSplineShape Script Tool

        if self.CMDTool== "NTC":
            Global_QuickScripts_Data().CovertNurboTaxiLine()

        return True


class FCS_ProcessPopupWindow(g_lib.BaseWindowDialogUI):
    """ Process Running Window """
    """
    Window Dialog Main Properties
    _________________________________________________
    """
    windowMainTileName = "Process Window"
    windowDialogWidthSize = 400
    windowDialogHeightSize = 10
    windowDialogPluginID = 0
    """
    Variables & Properties
    _________________________________________________
    """
    IDUI_BAR = 1010
    IDUI_STR = 1001

    def __init__(self, name):
        """
        The __init__ is an Constuctor and help get or set data
        into the class and passes data on from the another class.
        """
        super(FCS_ProcessPopupWindow, self).__init__()
        self.processBAR = g_lib.AddCustomProgressBar_GUI(self)
        self.toolNameProcess = name
    
    def BuildUI(self):
        self.windowMainTileName = self.toolNameProcess
        self.processBAR.GUI( self.IDUI_STR, self.IDUI_BAR, 500, 15)
        return super(FCS_ProcessPopupWindow, self).BuildUI()





#PLUG_SP = {'ID':1054338, 'Icon':"tool_icon_SoloPoly.png", 'Name':StudioName+"Solo Poly",'flags':PF.HideTool, 'Data':SoloPoly_Data(), 'ToolWith':1051421, 'Info':'ToolInfo_Blank'}

#PLUG_MLiB = {'ID':1050002, 'Icon':"tool_icon_05.png", 'Name':StudioName+"MatLibrary", 'flags':PF.HideTool, 'Data':MatLibrary_Data(), 'ToolWith':1050002, 'Info':'ToolInfo_Blank'}

#PLUG_F_O = {'ID':1038352, 'Icon':"tool_icon_13.png", 'Name':StudioName+"Freeze Object",'flags':HideTool, 'Data':FCS_Toolset_Manager_Data(), 'ToolWith':regId.FQS['id'], 'Info':'ToolInfo_F_O'}

# class FreezeObject_Data(CommandData):
#     dialog = None
#     def Init(self, op):
#         return True
#     def Message(self, type, data):
#         return True
#     def Execute(self, doc):
#         if self.dialog is None:
#             self.dialog = FreezeObject_Ask_Script_Tool_Dialog()
#         return self.dialog.Open(dlgtype=c4d.DLG_TYPE_ASYNC, pluginid=PLUG_F_O["PlugID"], defaultw=300, defaulth=35)
#     def RestoreLayout(self, sec_ref):
#         return True
#     def ExecuteOptionID(self, doc, plugid, subid):
#         return True   