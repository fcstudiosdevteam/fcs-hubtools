"""
Tools Register ID and Name + GUI Enums ID's
"""
"""
F.C.S ToolKit Plugin for Maxon Cinema 4D

The FCS Tools Shelf Menu, The FCS ToolBox Plugin, Main Tools, Scripts Tools. 

Author: Asthon Rolle

Lead 3D Plugin Developer : Asthon Rolle AKA AP Asthon

Cinema 4D Python plugin
2015 Python Plugin For C4D
Plugin File Made: Aug 14, 2015.
Studio: Field Creators Studios
Last Upated : Feb 29, 2019

Add Plugin Version to add to UI and loading plugin and show/print in console.
VersionData = open(os.path.join(plugin_info_folder, 'FCS_ToolBox_CurrentVersion.txt'), "r").readline() | Might need it one day.

Compatible:
    - Win / Mac
    - R16, R17, R18, R19, R20, R21
    
Copyright: Field Creators Studios
"""
import c4d
import os 
import sys
import math 
import glob 
import time
import datetime
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as WindowDialog
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib

config_data = None


StudioName = "F.C.S - "
Toolset_State = "-Beta v"
__version_data__ = "2.40.09"
DataInfo = "InHouse" + Toolset_State + __version_data__
PLUGIN_Version = "RC - F.C.S C4D ToolKit | "+ DataInfo # Your Version Our Plugin


#  // GUI Enums ID's Tpyes //
class Global_Enums_IDs(object):
    """
    // GLOBAL STRINGS TEXT in the c4d_strings.str // 
    Calling on the IDs to get Text String

    // GLOBAL TOOLS UI ID's in the c4d_symbols.h file //

    ``Eg.:``

     In the c4d_symbols.h file :

     IDS_ID  = 1109

     In the c4d_strings.str file :

     IDS_ID   "HelloWorld";

    """

    GLOBAL_ID_VECTORIZER = 997022114
    GLOBAL_ID_ARC = 997022100
    GLOBAL_ID_CIRCLE = 997022101
    GLOBAL_ID_HELIX = 997022102
    GLOBAL_ID_NSIDE = 997022103
    GLOBAL_ID_RECTANGLE = 997022104
    GLOBAL_ID_STAR = 997022105
    GLOBAL_ID_TEXT = 997022106
    GLOBAL_ID_4SIDE = 997022107
    GLOBAL_ID_CISSOID = 997022108
    GLOBAL_ID_COGWHEEL = 997022109
    GLOBAL_ID_CYCLOID = 997022110
    GLOBAL_ID_FORMULA = 997022111
    GLOBAL_ID_FLOWER = 997022112
    GLOBAL_ID_PROFILE = 997022113
    
    fcs_nurbo_main_gui_dlg = 7001898
    ID_NURBO_DLG_TITLE = 7002
    ID_NURBO_OVERALL_GRP = 7003
    ID_NURBO_BANNER_GRP = 7049
    ID_NURBO_BANNER_GRP_BG = 7050
    ID_NURBO_TABS_GRP = 7004
    ID_NURBO_SWEEP_TAB_GRP = 7005
    ID_NURBO_REPLACER_TAB_GRP = 7100
    ID_NURBO_BUILDER_TAB_GRP = 7006
    ID_NURBO_SWEEP_GEN_TYPE_GRP = 70089
    ID_NURBO_SWEEP_GEN_TYPE_TITLE = 70090
    ID_NURBO_SWEEP_GEN_TYPE_STATIC = 70091
    ID_NURBO_SWEEP_GEN_TYPE_LIST = 70092     
    ID_NURBO_SWEEP_MODE_SETTINGS_GRP = 7007
    ID_NURBO_SWEEP_MODE_TITLE_GRP = 7008
    ID_NURBO_SWEEP_MODE_GRP = 7009
    ID_NURBO_SWEEP_MODE_CHK_SPINES = 7010
    ID_NURBO_SWEEP_MODE_CHK_POLYEDGES = 7011
    ID_NURBO_SWEEP_MODE_CHK_GEO_PATH_CUT = 7012
    ID_NURBO_SWEEP_CUSTOM_NAME_GRP = 7013
    ID_NURBO_SWEEP_CUSTOM_NAME_TITLE_GRP = 7014
    ID_NURBO_SWEEP_CUSTOM_NAME_CHK = 7015
    ID_NURBO_SWEEP_CUSTOM_NAME_EDITBOX = 7016
    ID_NURBO_SWEEP_SPLINE_SHAPES_GRP = 7018
    ID_NURBO_SWEEP_SPLINE_SHAPES_TITLE_GRP = 7019
    ID_NURBO_SWEEP_SPLINE_SHAPES_LIST = 7020
    ID_NURBO_SWEEP_L_CUSTOM = 7121
    ID_NURBO_SWEEP_SPLINE_MODE_LIST = 7023
    ID_NURBO_SWEEP_L_MAKEEDITABLE_MODE = 7124
    ID_NURBO_SWEEP_L_GENERATOR_MODE = 7125
    ID_NURBO_SWEEP_MODETYPE_STATIC = 7126
    ID_NURBO_SWEEP_CUSTOM_SPLINE_LINK = 7036
    ID_NURBO_SWEEP_PRESET_BTN_GRP = 7037
    ID_NURBO_SWEEP_SETTINGS_BTN_GRP = 7038
    ID_NURBO_SWEEP_ANGLE_BTN_GRP = 7039
    ID_NURBO_SWEEP_FIXROT_BTN_GRP = 7040
    ID_NURBO_SWEEP_ADDCUSTOMGRP_GRP = 7041
    ID_NURBO_SWEEP_ADDGRP_TITLE = 7042
    ID_NURBO_SWEEP_ADDGRP_CHK = 7043
    ID_NURBO_SWEEP_GRP_STATIC = 7044
    ID_NURBO_SWEEP_ADDGRP_EDIT = 7045
    ID_NURBO_SWEEP_ADDGRP_BTN = 7046
    ID_NURBO_SWEEP_NURBOIT_BTN = 7047

    ID_SNG_FCSWEBBANNER = 2044
    ID_SNG_STATUS_BAR_GRP = 2043
    ID_SNG_BOTTOM_GRP = 2042
    ID_SNG_TAKES_TAB_GRP = 204
    ID_SNG_OPT_EXPORT_IMPORT_BTN = 2040
    ID_SNG_OPT_PATH_DIR_BTN_GRP = 2039
    ID_SNG_OPT_PATH_EDITBOX = 2038
    ID_SNG_OPT_PATH_TXT = 2037
    ID_SNG_OPT_PATH_RF_BTN = 2036
    ID_SNG_OPT_PATH_PANEL_GRP = 2035
    ID_SNG_OPT_PATH_TITLE = 2034
    ID_SNG_OPT_PATH_GRP = 2033
    ID_SNG_OPT_BATCH_LIST = 2032
    ID_SNG_OPT_BATCH_FILETYPE_TXT = 2031
    ID_SNG_OPT_BATCH_FILENAME_TXT = 2030
    ID_SNG_OPT_BATCH_CHK_ALL = 2047
    ID_SNG_OPT_BATCH_LIST_GRP = 2029
    ID_SNG_OPT_SEARCH_DIR_BTN = 2028
    ID_SNG_OPT_SEARCH_EDITBOX = 2027
    ID_SNG_OPT_SEARCH_TXT = 2026
    ID_SNG_OPT_BATCH_SEARCHICON_GRP = 2048
    ID_SNG_OPT_BATCH_SEARCH_GRP = 2025
    ID_SNG_OPT_BATCHFITER_MENU = 2049
    ID_SNG_OPT_BATCH_DIR_BTN_GRP = 2024
    ID_SNG_OPT_BATCH_EDITBOX = 2023
    ID_SNG_OPT_BATCH_TXT = 2022
    ID_SNG_OPT_BATCH_TITLE_GRP = 2046
    ID_SNG_OPT_BATCH_DIR_GRP = 2021
    ID_SNG_OPT_BATCH_IMPORT_GRP = 2020
    ID_SNG_OPT_CHK_BATCH_IMPORT = 2019
    ID_SNG_OPT_CHK_OPENFOLDER = 2018
    ID_SNG_OPT_CHK_CENTER_OBJ = 2017
    ID_SNG_OPT_CHK_UNITY_AXIS = 2051
    ID_SNG_OPT_CHK_SINGLE_OBJ = 2016
    ID_SNG_OPT_OPTIONS_EXPORT_GRP = 2045
    ID_SNG_OPT_OPTIONS_TITLE_GRP = 2015
    ID_SNG_OPT_OPTIONS_GRP = 2014
    ID_SNG_OPT_FORMAT_GEAR_SETTINGS = 2013
    ID_SNG_OPT_FORMAT_DROPLIST_MENU = 2012
    ID_SNG_OPT_FORMAT_SETTINGS_TITLE_GRP = 2011
    ID_SNG_OPT_FORMAT_SETTINGS_GRP = 2010
    ID_SNG_OPT_MODE_IMPORT = 2009
    ID_SNG_OPT_MODE_EXPORT = 2008
    ID_SNG_OPT_MODE_TITLE_GRP = 2007
    ID_SNG_OPT_MODE_SETTINGS_GRP = 2006
    ID_SNG_OPTIONS_TAB_GRP = 2005
    ID_SNG_EXPORT_OPTIONS_TAB_GRP = 200532
    ID_SNG_IMPORT_OPTIONS_TAB_GRP = 200531  
    ID_SNG_OPTIONS_TABS = 200530  
    ID_SNG_TABS_GRP = 2004
    ID_SNG_OVERALL_GRP = 2003
    ID_SNG_DIALOG_TITLE = 2002
    fcs_select_n_go_gui_dlg = 2001


    ID_FS_QUICKTABHEADER_GRP2 = 1037
    ID_FS_QUICKTABHEADER_GRP1 = 51036
    ID_FS_MODEL_EXPORT_BTN = 51035
    ID_FS_FILE_PATH_BTN = 51034
    ID_FS_FILE_PATH_EDIT = 51033
    ID_FS_FILE_PATH_TXT = 51032
    ID_FS_FILE_PATH_GRP = 51031
    ID_FS_FILENAME_EDIT = 51030
    ID_FS_FILENAME_TXT = 51029
    ID_FS_FILENAME_GRP = 51028
    ID_FS_EXPORT_TO_MCX_CHK = 51027
    ID_FS_EXPORT_OPT_GRP = 51026
    ID_FS_GUID_BTN = 51025
    ID_FS_GUID_EDIT = 51024
    ID_FS_GUID_TXT = 51023
    ID_FS_GUID_GRP = 51022
    ID_FS_FRIENDLY_EDIT = 51021
    ID_FS_FRIENDLY_TXT = 51020
    ID_FS_FRIENDLY_GRP = 51019
    ID_FS_EXPORTER_GRP_PANEL = 51018
    ID_FSFX_LISTCONTAINER_GRP = 51017
    ID_FSFX_CONTAINER_SCROLLGRP = 51016
    ID_FSFX_SEARCH_EDIT_BAR = 51015
    ID_FSFX_SEARCH_TXT = 51014
    ID_FSFX_SEARCH_BAR_GRP = 51013
    ID_FSFX_RADIO_BTN_P3Dv4 = 51012
    ID_FSFX_RADIO_BTN_P3Dv3 = 51011
    ID_FSFX_RADIO_BTN_FSXSE = 51010
    ID_FSFX_RADIO_BTN_FSX = 51009
    ID_FSFX_RADIO_BTNS_GRP = 51008
    ID_FSFX_CHOOSE_TXT = 51007
    ID_FSFX_CHOOSE_SIM_GRP = 51006
    ID_FS_ATTACH_FX_TAB_GRP = 51005
    ID_FS_TABS_GRP = 51004
    ID_FS_OVERALL_GRP = 51003
    ID_FS_SDK_DIALOG_TITLE = 51002
    fcs_fs_sdk_attachfx_gui_dlg = 51001
    fcs_fs_fx_exportsettings_gui_dlg = 51000

    ID_OSP_WEB_BANNER_GRP = 4016
    ID_OSP_SET_BTN = 4015
    ID_OSP_VALUE_EDIT_INPUT = 4014
    ID_OSP_VALUE_TXT = 4012
    ID_OSP_VALUE_GRP = 4011
    ID_OSP_ROT_CHK_B = 4010
    ID_OSP_ROT_CHK_P = 4009
    ID_OSP_ROT_CHK_H = 4008
    ID_OSP_POS_CHK_Z = 4007
    ID_OSP_POS_CHK_Y = 4006
    ID_OSP_POS_CHK_X = 4005
    ID_OSP_ROTATION_GRP = 4004
    ID_OSP_POSTION_GRP = 4003
    ID_OSP_OVERALL_GRP = 4002
    fcs_obj_set_pos_gui_dlg = 4001

    ID_SMP_SPLIT_WHOLE_OBJECT = 3015
    ID_SMP_OVERALL_GRP1 = 3014
    ID_SMP_BTN_CANCEL = 3013
    ID_SMP_BTN_OK = 3011
    ID_SMP_STATIC_TXT1 = 3010
    ID_SMP_CENTER_AXIS = 3009
    ID_SMP_APPLY_COLOR = 3008
    ID_SMP_ATTACH_CHILD = 3007
    ID_SMP_KEEP_SOMETAGS = 3006
    ID_SMP_REMOVE_TAGS = 3005
    ID_SMP_OPTIMIZE_SPLITOBJ = 3004
    ID_SMP_OPTIMIZE_MAINOBJ = 3003
    ID_SMP_DEL_POLY = 3002
    fcs_split_my_poly_gui_dlg = 3001

    fcs_about_gui_dlg_file = 1001
    ABOUT_OVERALL_GRP_ID = 1002
    ABOUT_EMPTY_GRP_1 = 1003
    ABOUT_TXT_INFO_ID = 1004
    ABOUT_PLUG_STATIC_TXT_ID = 1005
    ABOUT_PLUG_VERSION_INFO_ID = 1006
    ABOUT_EMPTY_GRP_2 = 1007

    #// Tool Manger //
    fcs_toolset_manager_gui_dlg = 6001
    ID_TPM_DLG_TITLE = 6000
    ID_TPM_OVERALL_GRP = 6002
    ID_TPM_COL_HEADER_GRP = 6003
    ID_TPM_LIST_VIEW_GRP = 6004
    ID_TPM_ENABLE_All = 6005
    ID_TPM_DISABLE_ALL = 6006
    ID_TPM_STATIC_TXT1 = 6007
    ID_TRM_STATIC_TXT3 = 6009
    ID_TRM_LIST_GRP = 6010
    ID_TRM_EMPTY_BOTTOM_GRP = 6011
    ID_TPM_INFO_TEXT = 6012

    #// GLOBAL STRINGS TEXT in the c4d_strings.str  // 
    IDS_SWEEP_MODES = 6014
    IDS_ADD_CUSTOM_NAME = 6015
    IDS_ADD_GROUP = 6016
    IDS_ADD_TO_GROUP  = 6017
    IDS_CREATE_GRP = 6019
    IDS_SPLINES_SHAPES_TEMP = 6020
    IDS_OBJ_MODE_TYPE = 6021 
    IDS_OPEN = 6022
    IDS_SAVE = 6023     
    IDS_LOAD = 6024
    IDS_LOADFILE = 6025
    IDS_FILE = 6026
    IDS_CREATEPRESET = 6027
    IDS_CREATE_CUS_PRESET = 6028
    IDS_NEW = 6029
    IDS_TAGS = 6030
    IDS_NURBO_TIP1 = 6031
    IDS_NURBO_TIP2 = 6032
    IDS_NURBO_TIP3 = 6033
    IDS_NURBO_TIP4 = 6034     
    IDS_SWEEPOBJ_PARALLEL = 6035
    IDS_SWEEPOBJ_BANKING = 6036
    IDS_SWEEPOBJ_RAILDIRECTION = 6037
    IDS_SWEEPOBJ_RAILSCALE = 6038
    IDS_SWEEPOBJ_GROWUV = 6039
    IDS_SWEEPOBJ_CONSTANT = 6040
    IDS_SWEEPOBJ_KEEPSEGMENTS = 6041
    IDS_SWEEPOBJ_BIRAIL = 6042
    IDS_SWEEPOBJ_FLIPNORMALS = 6043
    IDS_SAVEAS = 6044
    IDS_ADD_PRESET_TEMP = 6045
    IDS_MSG_NOTHOME = 6046
    IDS_AUTORENAMER_TITLE = 6047
    IDS_TYPE = 6048
    IDS_TYPE_W_DOTS = 6049
    IDS_TYPE = 6048
    IDS_TYPE_W_DOTS = 6049
    IDS_OBJECTS = 6050
    IDS_MATERIALS = 6051
    IDS_PRESETS = 6052
    IDS_PRESET = 6053
    IDS_EDIT = 6054
    IDS_MESH = 6055
    IDS_NEW_DOC = 6056
    IDS_EDITMODE = 6057
    IDS_MODE = 6058
    IDS_MODES = 6059
    IDS_MAKEEDITABLE = 6060
    IDS_TAXILINE_CONVERT_ICON = 6061
    IDS_TAXILINE_OBJECT_ICON = 6062
    IDS_TAXILINE_OBJECT_STR = 6063
    IDS_SWEEP_OBJECT = 6064
    IDS_SWEEP_OBJECT_wICON = 6065
    IDS_CONVERSION_STR = 6066 
    IDS_TOOLNAME_FQS = 60678
    IDS_TOOLNAME_APC = 6068
    IDS_TOOLNAME_NDS = 6069
    IDS_TOOLNAME_SMR = 6070
    IDS_TOOLNAME_S_O = 6071
    IDS_TOOLNAME_AOM = 6072
    IDS_TOOLNAME_CIt = 6073
    IDS_TOOLNAME_D_P = 6074
    IDS_TOOLNAME_OIt = 6075
    IDS_TOOLNAME_ARN = 6076
    IDS_TOOLNAME_A2Z = 6077
    IDS_TOOLNAME_SUS = 6078
    IDS_TOOLNAME_TGA = 6079
    IDS_TOOLNAME_SCD = 6080
    IDS_TOOLNAME_TPM = 6081
    IDS_TOOLNAME_PEF = 6082   
    IDS_TOOLNAME_FRM = 6083
    IDS_TOOLNAME_DIS = 6084
    IDS_TOOLNAME_TUT = 6085    
    IDS_TOOLNAME_ABT = 6086
    IDS_TOOLNAME_MCT = 6087
    IDS_TOOLNAME_E_T = 6088
    IDS_TOOLNAME_AFL = 6089
    IDS_TOOLNAME_TLO = 6090
    IDS_TOOLNAME_FB  = 6091
    IDS_TOOLNAME_YT  = 6092
    IDS_TOOLNAME_GRS = 6093
    IDS_TOOLNAME_FSS = 6094
    IDS_TOOLNAME_T2C = 6095
    IDS_TOOLNAME_OSP = 6096
    IDS_TOOLNAME_A_R = 6097

fcsID = Global_Enums_IDs()

#  // Main C4D Register ID's //
class RegisterIDs(object):

    def __init__(self, global_strings):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """
        super(RegisterIDs, self).__init__()
        self.IDS = global_strings
        # Default FCS Helper Tools
        self.TPM = { 'id':1054372, 'i':"tool_icon_plugM.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_TPM) }    # Tools Manager
        self.FRM = { 'id':1039419, 'i':"tool_Forum_icon.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_FRM) }    # Forum
        self.DIS = { 'id':1050892, 'i':"tool_discord_icon.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_DIS) }  # Our Discord Tech-Help
        self.TUT = { 'id':1039242, 'i':"tool_Help_icon.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_TUT) }     # Offline Doc Help
        self.PEF = { 'id':1039465, 'i':"tool_Pref_icon.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_PEF) }     # Preferences
        self.ABT = { 'id':1050473, 'i':"tool_About_icon.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_ABT) }    # About...
        self.AFL = { 'id':1039852, 'i':"tool_icon_62.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_AFL) }       # Layouts
        self.MCT = { 'id':1040383, 'i':"tool_icon_60.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_MCT) }       # Modelling Command Tools
        self.TLO = { 'id':1052664, 'i':"tool_Help_icon.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_TLO) }     # Our Trello
        self.FB  = { 'id':1054277, 'i':"tool_Help_icon.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_FB) }      # Our Facebook Page
        self.YT  = { 'id':1054278, 'i':"tool_Help_icon.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_YT) }      # Our YouTube Page
        self.GRS = { 'id':1054279, 'i':"tool_Help_icon.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_GRS) }     # Our GumRoad Page

        self.GRS = { 'id':1054279, 'i':"tool_Help_icon.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_GRS) }     # Our GumRoad Page


        # FCS Sub Register Icons for Tools
        self.NURBO_TL_CENTER = { 'id':1053594, 'i':"tool_Nurbo_TaxiLineCenter_icon.png" }
        self.NURBO_TL_INNER  = { 'id':1053595, 'i':"tool_Nurbo_TaxiLineInner_icon.png" }
        self.NURBO_TL_OUTTER = { 'id':1053596, 'i':"tool_Nurbo_TaxiLineOutter_icon.png" }  

        self.GPP_1_MODE = { 'id':1054515, 'i':"tool_icon_gp4dmode1.png" }
        self.GPP_2_MODE = { 'id':1054516, 'i':"tool_icon_gp4dmode2.png" }
        self.GPP_3_MODE = { 'id':1054517, 'i':"tool_icon_gp4dmode3.png" }
        self.GPP_4_MODE = { 'id':1054518, 'i':"tool_icon_gp4dmode4.png" }


        # FCS Quick Scripts Tools Pack
        self.FQS = { 'id':1039801, 'i':"tool_icon_54.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_FQS) } # FCS Quick Scripts
        self.NDS = { 'id':1036188, 'i':"tool_icon_09.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_NDS) } # NewDoc + Selected
        self.APC = { 'id':1036394, 'i':"tool_icon_03.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_APC) } # Auto Poly Checker
        self.S_O = { 'id':1038971, 'i':"tool_icon_40.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_S_O) } # Speckle Objects
        self.CIt = { 'id':1038025, 'i':"tool_icon_02.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_CIt) } # Compress It
        self.D_P = { 'id':1036481, 'i':"tool_icon_01.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_D_P) } # Delete Poly
        self.SMR = { 'id':1038952, 'i':"tool_icon_39.png", 'str':self.IDS.ID(fcsID.IDS_TOOLNAME_SMR) }            # Set Material Rez
        self.AOM = { 'id':1039851, 'i':"tool_icon_56.png", 'str':self.IDS.ID(fcsID.IDS_TOOLNAME_AOM) }            # AmbientOcclusion(AO) to Material
        self.OIt = { 'id':1035756, 'i':"tool_icon_06.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_OIt) } # Optimize It
        self.ARN = { 'id':1038535, 'i':"tool_icon_26.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_ARN) } # Auto Reverse Normals
        self.A2Z = { 'id':1038536, 'i':"tool_icon_29.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_A2Z) } # Axis Zero
        self.SUS = { 'id':1038466, 'i':"tool_icon_25.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_SUS) } # Set Unit Scale
        self.TGA = { 'id':1039535, 'i':"tool_icon_52.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_TGA) } # Toggle Grid + Axis
        self.SCD = { 'id':1036583, 'i':"tool_icon_05.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_SCD) } # Select + Connect + Delete
        self.FSS = { 'id':1039227, 'i':"tool_icon_47.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_FSS) } # Fill Spline Shape
        self.T2C = { 'id':1038706, 'i':"tool_icon_31.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_T2C) } # Target to Constraint
        self.OSP = { 'id':1038011, 'i':"tool_icon_04.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_OSP) } # Object SetPos
        self.A_R = { 'id':1039524, 'i':"tool_icon_50.png", 'str':StudioName+self.IDS.ID(fcsID.IDS_TOOLNAME_A_R) } # Auto Renamer Config


# Create FCS Tools Prefs Directory.
def Create_FCS_Tools_Prefs_Folder():
    """ FCS Preferences Data Paths """
    """ Create F.C.S Tool-set Preferences Folder, If not in the Maxon Cinema 4D Preferences Folder. """
    path = os.path.join(g_lib.fcsDIR.c4d_prefs_folder, 'fcs_plugin_prefs')
    if os.path.exists(path):
        pass
    else:
        os.mkdir(path)
        path2 = os.path.join(g_lib.fcsDIR.c4d_prefs_folder, 'fcs_plugin_prefs', 'user_xml_saves')
        os.mkdir(path2)
        path3 = os.path.join(g_lib.fcsDIR.c4d_prefs_folder, 'fcs_plugin_prefs', 'user_presets_saves')
        os.mkdir(path3)
        path4 = os.path.join(g_lib.fcsDIR.c4d_prefs_folder, 'fcs_plugin_prefs', 'fcs_plugin_extension')
        os.mkdir(path4)
        g_lib.fcsLog.DEBUG("The FCS_Plugin_Prefs folder structure is created.", False, g_lib.DebugMode)
    return True

# Create FCS ID Tools To XML for FCS Manager to read.
def Create_FCS_ID_Tools_XML():
    """ Create Xml with tools Ids """
    jsonEdit = g_lib.jsonEdit
    JFile = g_lib.fcsDIR.FCS_Tools_IDs_CONIG2
    ToolsIDs_Config = {}

    if os.path.exists(g_lib.fcsDIR.FCS_Tools_IDs_CONIG2):
        return
    else:
        ToolsIDs_Config["OpenManager"]="MangerStart"

        set_tools = [
                    # Tool-Set Base
                    #PLUG_TPM, PLUG_ABT, PLUG_FRM, PLUG_TUT, PLUG_PEF, PLUG_AFL,
                    # Tools
                    PLUG_SNG, PLUG_A_R, PLUG_N_S, PLUG_T2C, PLUG_FSS, PLUG_O2P, PLUG_UCM, PLUG_APC, PLUG_RRP,
                    PLUG_PSC, PLUG_SMP, PLUG_A2L, PLUG_FST, PLUG_O2P, PLUG_SP, PLUG_FQS, PLUG_OSP, PLUG_SP, PLUG_C_P
                    ]

        for each_ele in set_tools:
            tool_id = "PLUGIN_ID"+str(each_ele['ID'])
            ToolsIDs_Config[tool_id]="Disable"

        jsonEdit.SaveFile(json_file=JFile, saveData=ToolsIDs_Config)
    return True


#  // Run Function C4D Console After Tools Loaded. //
def Print_FCS_ToolKit_Initialized():
    """
    Print To The C4D Console After Tools Loaded.
    """
    
    fcsLog = g_lib.fcsLog
    DebugMode = g_lib.DebugMode

    # // To show plugin is there and it will show it, and print inside the C4D Console.
    fcsLog.INFO("================================================================", False, DebugMode)
    fcsLog.INFO("   "+ PLUGIN_Version +" for Cinema 4D Initialized. This is a all Python Plugin.", False, DebugMode)
    fcsLog.INFO("   F.C.S ToolKit is a plugin created by FieldCreators Studios for MAXON CINEMA 4D.", False, DebugMode)
    fcsLog.INFO("   Lead Plugin Technical Developer : Asthon J. Rolle aka AP Asthon", False, DebugMode)
    fcsLog.INFO("   External Tool Developer : Creswell Gould aka CJ", False, DebugMode)
    fcsLog.INFO("   Studio Website: www.FieldCreatorsStudios.com", False, DebugMode)
    fcsLog.INFO("   copyright(c) Since 2015 FieldCreators Studios All rights reserved.", False, DebugMode)
    fcsLog.INFO("   Special Thanks to Maxon Plugin Development Support Forums.", False, DebugMode)
    fcsLog.INFO("   Compatible: Win / Mac  | R16, R17, R18, R19, R20, R21 ", False, DebugMode)
    if DebugMode:
        fcsLog.INFO("   Debug Mode is on.", False, DebugMode)
    else:
        fcsLog.INFO("   Debug Mode is off.", False, DebugMode)
    fcsLog.INFO("================================================================", False, DebugMode)
    return True


