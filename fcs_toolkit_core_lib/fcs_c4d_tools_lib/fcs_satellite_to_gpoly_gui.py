import c4d
from c4d import plugins, gui, bitmaps, documents, storage, utils
import os 
import sys
import math 
import glob 
import time
import datetime
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
#from ..fcs_c4d_dialogs import fcs_common_global_lib as g_lib
config_data = None
fcsID = g_lib.fcsID
jsonEdit = g_lib.jsonEdit


"""
Tool GUI Window User Interface Dialog.
_________________________________________________
"""
class SatToGPoly_GUI(g_lib.BaseWindowDialogUI):
    """ Satellite To GroundPoly GUI Window. """
    
    """
    Window Dialog Main Properties
    _________________________________________________
    """
    windowMainTileName = "Sat To G-Poly"
    windowDialogWidthSize = 200
    windowDialogHeightSize = 200
    windowDialogPluginID = 1053986

    """
    GUI Elements ID's.
    _________________________________________________
    """ 
    IDS_VER_ID = 1000
    UI_OVERALL_GRP_ID = 1001
    UI_SATELLITE_IMAGE_TXT_ID = 1002
    UI_SATELLITE_IMAGE_EB_ID = 1003
    UI_SATELLITE_IMAGE_BTN_ID = 1004
    UI_SATELLITE_IMAGE_ID = 1005
    UI_SATELLITE_DATA_TXT_ID = 1006
    UI_SATELLITE_DATA_EB_ID = 1007
    UI_SATELLITE_DATA_BTN_ID = 1008
    UI_GENERATE_SATELLITE_BTN_ID = 1009

    """
    Main GeDialog GUI Window Overrides.
    _________________________________________________
    """
    def __init__(self):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """
        super(SatToGPoly_GUI, self).__init__()
        self.CUSTOM_FILENAME = g_lib.AddCustomFileNamePath_GUI(self)

    def BuildUI(self):

        # Top Menu addinng Tool Version
        self.GroupBeginInMenuLine()
        self.AddStaticText(self.IDS_VER_ID, 0)
        self.SetString(self.IDS_VER_ID, " v1.0  ")
        self.GroupEnd()

        self.GroupBegin(self.UI_OVERALL_GRP_ID, c4d.BFH_SCALEFIT, 1, 0, "")

        self.AddStaticText(0, c4d.BFH_CENTER, 0, 15, "Satellite To GroundPoly", c4d.BORDER_WITH_TITLE_BOLD)
        
        self.AddSeparatorH(0, c4d.BFH_SCALEFIT) # Separator
        
        def SatelliteImage_PanelLayoutSection():
            self.GroupBegin(0, c4d.BFH_SCALEFIT, 2, 0, "")  
            self.AddStaticText(self.UI_SATELLITE_IMAGE_TXT_ID, c4d.BFV_TOP, 0, 15, " *Satellite Image : ", c4d.BORDER_WITH_TITLE_BOLD)
            self.GroupBegin(0, c4d.BFH_SCALEFIT, 1, 0, "")
            self.CUSTOM_FILENAME.GUI(string_id=self.UI_SATELLITE_IMAGE_EB_ID, btn_id=self.UI_SATELLITE_IMAGE_BTN_ID, size_w=250, size_l=10, custom_icon_btn=None)
            self.GroupBegin(0, c4d.BFH_MASK, 1, 0, "")
            self.GroupBegin(self.UI_SATELLITE_IMAGE_ID, c4d.BFH_MASK, 1, 0, "")
            self.GroupBorderNoTitle(c4d.BORDER_IN)
            self.GroupEnd()
            self.GroupEnd()
            self.GroupEnd()
            return self.GroupEnd()
        SatelliteImage_PanelLayoutSection()
               
        self.AddSeparatorH(0, c4d.BFH_SCALEFIT) # Separator

        def SatelliteData_PanelLayoutSection():
            self.GroupBegin(0, c4d.BFH_SCALEFIT, 2, 0, "")

            self.AddStaticText(self.UI_SATELLITE_DATA_TXT_ID, c4d.BFH_CENTER, 0, 15, " *Satellite Data : ", c4d.BORDER_WITH_TITLE_BOLD)
            self.CUSTOM_FILENAME.GUI(string_id=self.UI_SATELLITE_DATA_EB_ID, btn_id=self.UI_SATELLITE_DATA_BTN_ID, size_w=250, size_l=10, custom_icon_btn=None)

            
            return self.GroupEnd() 
        SatelliteData_PanelLayoutSection()        
        
        self.AddSeparatorH(0, c4d.BFH_SCALEFIT) # Separator
        
        self.AddButton(self.UI_GENERATE_SATELLITE_BTN_ID, c4d.BFH_SCALEFIT, 100, 15, name="Generate Satellite")  

        self.GroupEnd()

        return super(SatToGPoly_GUI, self).BuildUI()

    def UIsettings(self):
        """ Called when the dialog is initialized by the GUI / GUI's startup values basically. """        
        self.SetDefaultColor(self.IDS_VER_ID, c4d.COLOR_TEXT, g_lib.DARK_BLUE_TEXT_COL)
        self.SetDefaultColor(self.UI_OVERALL_GRP_ID, c4d.COLOR_BG, g_lib.BG_DARK)
        self.SetDefaultColor(self.UI_SATELLITE_IMAGE_TXT_ID, c4d.COLOR_TEXT, g_lib.DARK_BLUE_TEXT_COL)
        self.SetDefaultColor(self.UI_SATELLITE_DATA_TXT_ID, c4d.COLOR_TEXT, g_lib.DARK_BLUE_TEXT_COL)
        UpdateImageWigdetUI(uiIns=self,  widgetID=self.UI_SATELLITE_IMAGE_ID, image=g_lib.fcsDIR.I(i='RefFolder'))
        self.LayoutChanged(self.UI_OVERALL_GRP_ID)     
        return super(SatToGPoly_GUI, self).UIsettings()

    def Command(self, id, msg):
        """
        Excuting Commands for UI Elements Functions.
        """
        if id == self.UI_GENERATE_SATELLITE_BTN_ID:

            satelliteDataTxtB = self.GetString(self.UI_SATELLITE_DATA_EB_ID)
            satelliteImageTxtB = self.GetString(self.UI_SATELLITE_IMAGE_EB_ID)

            if not satelliteDataTxtB or not satelliteImageTxtB:
                gui.MessageDialog("Please fill in the requires (*) fields.")
                return

            if self.CUSTOM_FILENAME.VaildFileLocation(satelliteDataTxtB) != True:
                gui.MessageDialog("Cannot find the file specified {0}".format(satelliteDataTxtB))
                return

            if self.CUSTOM_FILENAME.VaildFileLocation(satelliteImageTxtB) != True:
                gui.MessageDialog("Cannot find the file specified {0}".format(satelliteImageTxtB))
                return

            GeneratePoly(satelliteDataTxtB, satelliteImageTxtB)

            c4d.EventAdd()
            
        if id == self.UI_SATELLITE_IMAGE_BTN_ID:  
            self.CUSTOM_FILENAME.SetStringData(self.UI_SATELLITE_IMAGE_EB_ID, "Satellite Image", c4d.FILESELECTTYPE_IMAGES, c4d.FILESELECT_LOAD, "", "")
            image11 = self.CUSTOM_FILENAME.GetStringData(self.UI_SATELLITE_IMAGE_EB_ID)
            #image11 = self.GetString(self.UI_SATELLITE_IMAGE_EB_ID)
            UpdateImageWigdetUI(uiIns=self,  widgetID=self.UI_SATELLITE_IMAGE_ID, image=image11)
            self.LayoutChanged(self.UI_OVERALL_GRP_ID)
            
        if id == self.UI_SATELLITE_DATA_BTN_ID:
            self.CUSTOM_FILENAME.SetStringData(self.UI_SATELLITE_DATA_EB_ID, "Satellite Data Json", c4d.FILESELECTTYPE_ANYTHING, c4d.FILESELECT_LOAD, ".json", 
                                                "You have selected a invaild satellite data.\nPlease select a satellite data (.json) file.")       

        return True

"""
Main Tool Operation Functions Hepler Methods.
_________________________________________________
"""
def Read_SatData(get_jsonfile, parentObjectName, childPropertyName, childPropertyValue):
    """
    Read .json file of the satellite data added.
    """
    get_jsonfile = get_jsonfile

    pointsList = jsonEdit.GetItem(get_jsonfile,parentObjectName)
    
    currentCorner = None
    print(pointsList)
    print(type(pointsList))
    for item in pointsList:
        if isinstance(item, dict):
            if item["CornerName"] == childPropertyValue:
                print("Found")
                currentCorner = item
                break


    if currentCorner == None:
        gui.MessageDialog("Corners returned None")     
        return None

    X = currentCorner['X']
    Y = 0
    Z = currentCorner['Y']
    data = c4d.Vector( X, Y, Z )
    print(data)
    return data
def SetObjPoints(doc, polygon, corners):
    """      
    Set polygon object Points in Postition Locations
    """
    polygon.SetPoint(2, corners[0])
    polygon.SetPoint(3, corners[1])
    polygon.SetPoint(0, corners[2])
    polygon.SetPoint(1, corners[3])

    return True  
def GetObjPoints(jsonfile):
    """
    Get Data from .Json file
    """
    topLeft = Read_SatData(jsonfile, "CornerPoints","CornerName","NW_Top left")
    topRight = Read_SatData(jsonfile, "CornerPoints","CornerName","NE_Top right")
    bottomLeft = Read_SatData(jsonfile,"CornerPoints","CornerName","SW_Bottom left")
    bottomRight = Read_SatData(jsonfile, "CornerPoints","CornerName","SE_Bottom right")

    if topLeft == None or topRight == None or bottomLeft == None or bottomRight == None:
        gui.MessageDialog("Failed to get all the corner points form the json file!")
        return None
    
    return [topLeft,topRight,bottomLeft,bottomRight]
def GeneratePoly(jsonfile, imagefile):
    _continue = True

    doc = c4d.documents.GetActiveDocument()

    pointData = GetObjPoints(jsonfile)

    if pointData == None:
        _continue = False

    if _continue:
        polygon = g_lib.MakePlanePolygonObject(doc, "Airport_Satellite_Poly")

        objPoints = SetObjPoints(doc=doc, polygon=polygon, corners=pointData)

        if not objPoints:
            gui.MessageDialog("Operation Failed")
            g_lib.RemoveObjectFromScene(polygon)
            return

        mat = g_lib.GetAndCheckMaterial(doc=doc, MatName="Sat_Image", remove_mat=True)

        g_lib.ApplyBitmapMaterial(doc=doc, 
                                obj=polygon, 
                                mat_name="Sat_Image", 
                                bitmap_image=imagefile, 
                                add_col=None, 
                                col=None, 
                                check_mat=mat, 
                                mat_prez_res=14,
                                add_tex_tag=True)
        polygon.Message(c4d.MSG_UPDATE)
    return True
def UpdateImageWigdetUI(uiIns,  widgetID, image):
    uiIns.LayoutFlushGroup(widgetID)
    g_lib.Add_SizeFit_Image_GUI(ui_instance=uiIns, id=101, image=image, width=160, height=160, info_text="") 
    c4d.EventAdd()
    print("UPADTE")
    return True