#  // Imports for Cinema 4D //
import c4d
import os, sys, subprocess, webbrowser, collections, math, random, urllib
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog, SubDialog as dlg
from random import randint
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib

# the selected number of objects
selected_num = 2
high_lvl_obj = None
low_lvl_obj = None




def LocalToGlobal(obj, local_pos):
    """
    Returns a point in local coordinate in global space.
    """
    obj_mg = obj.GetMg()
    return obj_mg * local_pos


def GetPolygonGlobal(obj, point_index):
    """
    Return the position of a point in Global Space
    """
    cal_pts_position = None
    poly_pts_data = None
    p3 = None
    poly = obj.GetPolygon(point_index) # Get the point in local coords
    #print(poly)
    """
    Get Points of Poly Face, Quad or Tri.
    _____________________________________________________
    """
    p0 = LocalToGlobal( obj, obj.GetPoint(poly.a) )
    p1 = LocalToGlobal( obj, obj.GetPoint(poly.b) )
    p2 = LocalToGlobal( obj, obj.GetPoint(poly.c) )
    p3 = LocalToGlobal( obj, obj.GetPoint(poly.d) )


    """
    Get Points of Poly Face, Quad or Tri Rotation.
    _____________________________________________________
    """
    e0 = p1 - p0
    e1 = p2 - p1
    #calculate polygon specific matrix
    zfn = e0.Cross(e1).GetNormalized()
    xfn = c4d.Vector(0,1,0).Cross(zfn).GetNormalized()
    yfn = xfn.Cross(zfn).GetNormalized()
    zfn = e0.Cross(e1).GetNormalized()
    cal_pts_rotation = c4d.utils.VectorToHPB(zfn)

    """
    Cal PolyFace Points
    ______________________________________________________
    """
    if p3 == p2:
        cal_pts_position = (p0 + p1 + p2)/3
        poly_pts_data = {'a':p0, 'b':p1, 'c':p2, 'd':p2}
    else:
        poly_pts_data = {'a':p0, 'b':p1, 'c':p2, 'd':p3}
        cal_pts_position = (p0 + p1 + p2 + p3 )/4

    #print(poly_pts_data)

    cal_data = {'pos':cal_pts_position, 'rot':cal_pts_rotation, 'pts':poly_pts_data}

    return cal_data



class MyDialog(dlg):
    """
    The GUI Elements ID's.
    _____________________________
    """
    LinkBox_SourceObj = 1001
    ALIGN = 1002
    MODE = 1004
    ID_PrerserveGroups = 1009
    IDS_VER_ID = 1000
    UI_OVERALL_GRP_ID = 1010
    """
    Variable Properties
    _____________________________
    """
    LinkBC = c4d.BaseContainer()
    objPtData = None
    ObjDefaultPntData = None
    linkObj = None
    get_obj_data = None
    GetlinkObj = None
    objData = None
    reset_axis_data = None


    def SetAxis(self, obj):

        mg     = obj.GetMg()
        pcount = obj.GetPointCount()
        self.reset_axis_data = {'pos':obj.GetAbsPos(), 'rot':obj.GetAbsPos()}

        # tell c4d to encapsulate the following steps into a single
        # undo step
        doc.StartUndo()


        for i in xrange(pcount):
            point = obj.GetPoint(i)
            doc.AddUndo(c4d.UNDOTYPE_CHANGE, obj)
            obj.SetPoint(i, point * mg)

        doc.AddUndo(c4d.UNDOTYPE_CHANGE, obj)
        pos = self.reset_axis_data['pos']
        obj.SetAbsPos(c4d.Vector( pos.x, 0, pos.z))
        #obj.SetAbsRot(c4d.Vector(0))
        obj.Message(c4d.MSG_UPDATE)

        # close the undo encapsulation and tell c4d to update the gui
        doc.EndUndo()

        c4d.EventAdd()
        return True

    def ReSetAxis(self, obj):

        mg     = obj.GetMg()
        pcount = obj.GetPointCount()
        current_axis_data = {'pos':obj.GetAbsPos(), 'rot':obj.GetAbsPos()}

        # tell c4d to encapsulate the following steps into a single
        # undo step
        doc.StartUndo()


        for i in xrange(pcount):
            point = obj.GetPoint(i)
            doc.AddUndo(c4d.UNDOTYPE_CHANGE, obj)
            obj.SetPoint(i, point * mg)

        doc.AddUndo(c4d.UNDOTYPE_CHANGE, obj)
        pos = self.reset_axis_data['pos']
        keeppos = current_axis_data['pos']
        obj.SetAbsPos(c4d.Vector( keeppos.x, pos.y, keeppos.z))
        obj.SetAbsRot(c4d.Vector(0))
        obj.Message(c4d.MSG_UPDATE)

        # close the undo encapsulation and tell c4d to update the gui
        doc.EndUndo()

        c4d.EventAdd()
        return True



    def If_PolygonSelected(self, PointObj):
        if PointObj is None:
            g_lib.fcsLog.DEBUG("Please Select one Point Based Object", True, g_lib.DebugMode)
            return
        elif not PointObj.CheckType(c4d.Opoint):
            g_lib.fcsLog.DEBUG("Please Select one Point Based Object", True, g_lib.DebugMode)
            return
        else:
            listy=[]
            maxEl = PointObj.GetPolygonCount()
            bs = PointObj.GetPolygonS()
            for index, selected in enumerate(bs.GetAll(maxEl)):
                if not selected:
                    continue
                else:
                    listy.append(index)
                    #print("PolySelected:"+str(index))
                    # Get the current point corr...  of the object.
                    self.objData = GetPolygonGlobal(PointObj, index)
                    #print(get_vertex_pnt)
            return listy
        c4d.EventAdd()
        return True

    def If_PointSelected(self, PointObj):
        if PointObj is None:
            g_lib.fcsLog.DEBUG("Please Select one Point Based Object", True, g_lib.DebugMode)
            return
        elif not PointObj.CheckType(c4d.Opoint):
            g_lib.fcsLog.DEBUG("Please Select one Point Based Object", True, g_lib.DebugMode)
            return
        else:
            listy=[]
            maxEl = PointObj.GetPointCount()
            bs = PointObj.GetPointS()
            for index, selected in enumerate(bs.GetAll(maxEl)):
                if not selected:
                    continue
                else:
                    listy.append(index)
                    #print("PointSelected:"+str(index))
                    # Get the current point corr...  of the object.
                    get_vertex_pnt = g_lib.GetPointGlobal(PointObj, index)
                    self.objPtData = get_vertex_pnt
                    self.ObjDefaultPntData = PointObj.GetPoint(index)
                    #print(self.ObjDefaultPntData)
            return listy
        c4d.EventAdd()
        return True

    def GhostPostionSelection(self, doc, obj, loc):

        get_merge_obj = doc.SearchObject("GhostPostionSelectionPoint")
        if get_merge_obj:
            get_merge_obj.SetAbsPos(loc)
            pass
        else:
            null = c4d.BaseObject(c4d.Onull)
            null.SetName("GhostPostionSelectionPoint")
            null[c4d.ID_BASEOBJECT_USECOLOR]=2
            null[c4d.ID_BASEOBJECT_COLOR]=c4d.Vector(0, 1, 0.933)
            null.SetAbsPos(loc)
            doc.InsertObject(null)
            # N-Bit Methods
            #Bit:_________________________________________________
            # c4d.NBIT_OHIDE   Hide object/tag in Object Manager
            #                  or material in Material Manager.
            #Bit mode:____________________________________________
            # c4d.NBITCONTROL_SET              Set bit.
            # c4d.NBITCONTROL_CLEAR            Clear bit.
            # c4d.NBITCONTROL_TOGGLE           Toggle bit.
            # c4d.NBITCONTROL_PRIVATE_NODIRTY  Private.
            null.ChangeNBit(c4d.NBIT_OHIDE,c4d.NBITCONTROL_SET)

        c4d.EventAdd()
        return True


    def GetTargetbjectModeData(self):
        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False
        obj = self.FindCustomGui(self.LinkBox_SourceObj, c4d.CUSTOMGUI_LINKBOX).GetLink()
        if obj == None:
            get_merge_obj = doc.SearchObject("GhostPostionSelectionPoint")
            if get_merge_obj:
                get_merge_obj.Remove()
            c4d.EventAdd()
        else:
            objGUID = obj.GetGUID()
            objN = obj.GetName()
            #print(objN)
            #print(objGUID)
            self.GetlinkObj = obj
            self.linkObj = objGUID

            if self.If_PointSelected(PointObj=obj):
                self.Enable(201, True)
            else:
                self.Enable(201, False)

            if self.If_PolygonSelected(PointObj=obj):
                self.Enable(203, True)
            else:
                self.Enable(203, False)

            if self.GetlinkObj:
                self.Enable(1, True)
            else:
                self.Enable(1, False)

            if self.GetLong(self.MODE) == 203:
                self.GhostPostionSelection(doc, obj=obj, loc=self.objData['pos'])
                #GhostPostionSelection(obj=obj, loc=self.objData['rot'])

            if self.GetLong(self.MODE) == 201:
                self.GhostPostionSelection(doc, obj=obj, loc=self.objPtData)

            if self.GetLong(self.MODE) == 1:
                self.GhostPostionSelection(doc, obj=obj, loc=self.GetlinkObj.GetAbsPos())



        c4d.EventAdd()
        return True


    def RunAlignSystemProcess_PolygonModeSel(self, PointObj):

        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False

        MainLinkObj = self.GetlinkObj

        # PointMode________________________
        if self.GetLong(self.MODE) == 201:

            if self.GetLong(self.ALIGN) == 2:

                self.SetAxis(obj=PointObj)

                data = self.ObjDefaultPntData.y
                data2 = self.objPtData.y
                print(data)

                if PointObj is None:
                    g_lib.fcsLog.DEBUG("Please Select one Point Based Object", True, g_lib.DebugMode)
                    return
                elif not PointObj.CheckType(c4d.Opoint):
                    g_lib.fcsLog.DEBUG("Please Select one Point Based Object", True, g_lib.DebugMode)
                    return
                else:
                    listy=[]
                    maxEl = PointObj.GetPolygonCount()
                    bs = PointObj.GetPolygonS()
                    for index, selected in enumerate(bs.GetAll(maxEl)):
                        if not selected:
                            continue
                        else:
                            listy.append(index)
                            poly = PointObj.GetPolygon(index)
                            print(poly)

                            p0 = PointObj.GetPoint(poly.a)
                            p1 = PointObj.GetPoint(poly.b)
                            p2 = PointObj.GetPoint(poly.c)
                            p3 = PointObj.GetPoint(poly.d)

                            newValData = data2

                            print(newValData)

                            PointObj.SetPoint(poly.a, c4d.Vector(p0.x, newValData, p0.z))
                            PointObj.SetPoint(poly.b, c4d.Vector(p1.x, newValData, p1.z))
                            PointObj.SetPoint(poly.c, c4d.Vector(p2.x, newValData, p2.z))
                            PointObj.SetPoint(poly.d, c4d.Vector(p3.x, newValData, p3.z))

                            PointObj.Message(c4d.MSG_UPDATE)


                            c4d.CallCommand(12298) # Model
                            doc.SetActiveObject(PointObj)
                            yu = doc.GetActiveObject()
                            print(yu)
                            c4d.CallCommand(1011982, 1011982) # Center Axis to
                            
                            #c4d.CallCommand(1019940) # Reset PSR

                            oi = yu.GetAbsPos()
                            yu.SetAbsPos(c4d.Vector(0, oi.y ,0))

                            self.ReSetAxis(obj=PointObj)


                            #yu.SetAbsPos(self.reset_axis_data['pos'])

                            c4d.EventAdd()

                    return listy
                c4d.EventAdd()

        c4d.EventAdd()
        return True


    def RunAlignSystemProcess_ModelModeSel(self, obj):

        MainLinkObj = self.GetlinkObj

        # PolyMode________________________
        if self.GetLong(self.MODE) == 203:

            if self.GetLong(self.ALIGN) == 1:
                data = self.objPtData.x
                obj[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_X]=data

            if self.GetLong(self.ALIGN) == 2:
                data = self.objPtData.y
                obj[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Y]=data

            if self.GetLong(self.ALIGN) == 3:
                data = self.objPtData.z
                obj[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Z]=data

            if self.GetLong(self.ALIGN) == 6:
                self.GetTargetbjectModeData()
                obj.SetAbsPos(self.objData['pos'])
                obj.SetAbsRot(self.objData['rot'])

        # PointMode________________________
        if self.GetLong(self.MODE) == 201:

            if self.GetLong(self.ALIGN) == 1:
                data = self.objData['pos'].x
                obj[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_X]=data

            if self.GetLong(self.ALIGN) == 2:
                data = self.objData['pos'].y
                obj[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Y]=data

            if self.GetLong(self.ALIGN) == 3:
                data = self.objData['pos'].z
                obj[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Z]=data

            if self.GetLong(self.ALIGN) == 6:
                self.GetTargetbjectModeData()
                obj.SetAbsPos(self.objPtData)
                #obj.SetAbsRot(self.objData['rot'])

        # ModelMode________________________
        if self.GetLong(self.MODE) == 1:

            if self.GetLong(self.ALIGN) == 1:
                data = MainLinkObj[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_X]
                obj[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_X]=data

            if self.GetLong(self.ALIGN) == 2:
                data = MainLinkObj[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Y]
                obj[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Y]=data

            if self.GetLong(self.ALIGN) == 3:
                data = MainLinkObj[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Z]
                obj[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Z]=data

            if self.GetLong(self.ALIGN) == 6:
                data = self.GetlinkObj.GetAbsPos()
                MainLinkObj.SetAbsPos(data)

        c4d.EventAdd()
        return True

    # // Main GeDialog Overrides //

    """
    The __init__ is an Constuctor and help get
    and passes data on from the another class.
    """
    def __init__(self):
        super(MyDialog, self).__init__()

    # UI Layout
    def CreateLayout(self):

        # Top Menu addinng Tool Version
        self.GroupBeginInMenuLine()
        self.AddStaticText(self.IDS_VER_ID, 0)
        self.SetString(self.IDS_VER_ID, " v1.0  ")
        self.GroupEnd()

        self.SetTitle("4D Align") # Dialog Title

        # Top Menu addinng Tool Version
        self.GroupBeginInMenuLine()
        self.AddStaticText(self.IDS_VER_ID, 0)
        self.SetString(self.IDS_VER_ID, " v1.0  ")
        self.GroupEnd()


        self.GroupBegin(self.UI_OVERALL_GRP_ID, c4d.BFH_SCALEFIT, 1, 0, "")

        #self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

        self.AddStaticText(0, c4d.BFH_CENTER, 0, 15, "4D Align Tool", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

        self.GroupBegin(0, c4d.BFV_TOP|c4d.BFH_SCALEFIT, 1, 0, "")

        self.GroupBorderSpace(3,3,3,3)

        self.GroupBegin(0, c4d.BFV_TOP|c4d.BFH_SCALEFIT, 2, 0, " Target", c4d.BFV_GRIDGROUP_EQUALCOLS)
        self.GroupBorder(c4d.BORDER_BLACK)
        self.GroupBorderSpace(3,3,3,3)
        self.AddStaticText(0, c4d.BFV_TOP, 0, 0, " Object")
        self.AddCustomGui(self.LinkBox_SourceObj, c4d.CUSTOMGUI_LINKBOX, "", c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 100, 0, self.LinkBC)
        self.AddStaticText(0, c4d.BFV_TOP|c4d.BFH_RIGHT, 0, 0, " Modes")
        self.GroupBegin(0, c4d.BFV_TOP|c4d.BFH_SCALEFIT, 1, 0, "")
        self.AddRadioGroup(self.MODE,  c4d.BFH_SCALEFIT, 1, 10)
        self.AddChild(self.MODE, 1, "Model")
        self.AddChild(self.MODE, 201, "Point Selected")
        self.AddChild(self.MODE, 202, "Edge Selected - W.I.P")
        self.AddChild(self.MODE, 203, "Polygon Selected")
        self.GroupEnd()

        self.GroupEnd()

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

        self.GroupBegin(0, c4d.BFV_TOP|c4d.BFH_SCALEFIT, 2, 0, " Alignment ", c4d.BFV_GRIDGROUP_EQUALCOLS)
        self.GroupBorder(c4d.BORDER_BLACK)
        self.GroupBorderSpace(3,3,3,3)

        self.AddStaticText(0, c4d.BFV_TOP, 0, 0, "Group Selection")
        self.AddCheckbox(self.ID_PrerserveGroups, c4d.BFH_MASK, 100, 0, "")

        self.AddStaticText(0, c4d.BFH_RIGHT, 0, 0, "Axis")
        self.AddComboBox(self.ALIGN,  c4d.BFH_SCALEFIT, 100, 10, specialalign=False)
        self.AddChild(self.ALIGN, 1, "X" )
        self.AddChild(self.ALIGN, 2, "Y" )
        self.AddChild(self.ALIGN, 3, "Z" )
        self.AddChild(self.ALIGN, 6, "XYZ")
        #self.AddChild(self.ALIGN, 904, "XY" )
        #self.AddChild(self.ALIGN, 905, "ZY" )

        self.GroupEnd()


        self.GroupEnd()

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

        self.AddButton(1007, c4d.BFH_MASK, 0, 10, " Align ")

        self.GroupEnd()

        return True

    # Called when the dialog is initialized by the GUI / GUI's startup values basically.
    def InitValues(self):
        self.Enable(201, False)
        self.Enable(202, False)
        self.Enable(203, False)
        self.Enable(1, False)
        self.SetDefaultColor(self.UI_OVERALL_GRP_ID, c4d.COLOR_BG, g_lib.BG_DARK)
        self.SetDefaultColor(self.IDS_VER_ID, c4d.COLOR_TEXT, g_lib.DARK_BLUE_TEXT_COL)

        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False
        if doc.GetMode()==c4d.Mpolygons:
            self.FreeChildren(self.ALIGN)
            self.AddChild(self.ALIGN, 2, "Y")

        if doc.GetMode()==c4d.Mmodel:
            self.FreeChildren(self.ALIGN)
            self.AddChild(self.ALIGN, 1, "X" )
            self.AddChild(self.ALIGN, 2, "Y" )
            self.AddChild(self.ALIGN, 3, "Z" )
            self.AddChild(self.ALIGN, 6, "XYZ")
        #self.add_image()
        return True

    # Excuting Commands for UI Elements Functions.
    def Command(self, id, msg):

        if id == self.LinkBox_SourceObj:
            self.GetTargetbjectModeData()



        if id == 1007:

            doc = c4d.documents.GetActiveDocument()
            if doc == None:
                return False

            # Get Objects form the Object Manager
            list_objs = doc.GetActiveObjects(1)
            if not list_objs:
                g_lib.fcsLog.DEBUG("Please select an object!", True, g_lib.DebugMode)
                return


            if self.GetBool(self.ID_PrerserveGroups):

                if doc.GetMode()==c4d.Mmodel:

                    c4d.CallCommand(100004772, 100004772) # Group Objects
                    sel_obj = doc.GetActiveObjects(1)
                    for i in sel_obj:
                        self.RunAlignSystemProcess_ModelModeSel(i)
                    c4d.CallCommand(1019951, 1019951) # Delete Without Children
                    for i in list_objs:
                        doc.SetActiveObject( i, c4d.SELECTION_ADD)

                if doc.GetMode()==c4d.Mpolygons:
                    for i in list_objs:
                        self.RunAlignSystemProcess_PolygonModeSel(i)


            else:
                for i in list_objs:
                    if i.GetGUID() == self.linkObj:
                        pass
                    else:

                        if doc.GetMode()==c4d.Mmodel:
                            self.RunAlignSystemProcess_ModelModeSel(i)

                        if doc.GetMode()==c4d.Mpaint:
                            return 0

                        if doc.GetMode()==c4d.Mpoints:
                            return g_lib.fcsLog.DEBUG("\nSorry but the selected Points function is\nnot ready yet.", True, g_lib.DebugMode)

                        if doc.GetMode()==c4d.Mpolygons:
                            self.RunAlignSystemProcess_PolygonModeSel(i)

                        if doc.GetMode()==c4d.Medges:
                            return g_lib.fcsLog.DEBUG("\nSorry but the selected Edges function is\nnot ready yet.", True, g_lib.DebugMode)

        c4d.EventAdd()
        return True

    # Override this function if you want to react to Cinema 4D core messages.
    # The original message is stored in msg
    def CoreMessage(self, id, msg):

        if id == c4d.EVMSG_CHANGE:

            doc = c4d.documents.GetActiveDocument()
            if doc == None:
                return False

            self.GetTargetbjectModeData()


        if id == c4d.EVMSG_TOOLCHANGED:

            doc = c4d.documents.GetActiveDocument()
            if doc == None:
                return False

            if doc.GetMode()==c4d.Mpolygons:
                self.FreeChildren(self.ALIGN)
                self.AddChild(self.ALIGN, 2, "Y")

            if doc.GetMode()==c4d.Mmodel:
                self.FreeChildren(self.ALIGN)
                self.AddChild(self.ALIGN, 1, "X" )
                self.AddChild(self.ALIGN, 2, "Y" )
                self.AddChild(self.ALIGN, 3, "Z" )
                self.AddChild(self.ALIGN, 6, "XYZ")



            pass
        return True

    def DestroyWindow(self):
        """
        DestroyWindow Override this method - this function is
        called when the dialog is about to be closed temporarily,
        for example for layout switching.
        """
        get_merge_obj = doc.SearchObject("GhostPostionSelectionPoint")
        if get_merge_obj:
            get_merge_obj.Remove()
        print "My C4D Dialog Close."




if __name__=='__main__':
    #main()
    class_dialog = MyDialog()
    class_dialog.Open(c4d.DLG_TYPE_ASYNC, defaultw=120, defaulth=180)
    c4d.EventAdd()