"""
Auto Renamer Tool Window GUI Dialog
"""
# ----------------------------------------------------
#               Imports for Cinema 4D
# ----------------------------------------------------
#  // Imports for Cinema 4D //
import c4d, os, sys, subprocess, webbrowser, collections, math, random, urllib, glob, shutil, time, datetime, weakref
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as WindowDialog        # eg:( class My_Dialog(c4d.gui.GeDialog): ) to ( class My_Dialog(WindowDialog): ) Just keeping code line short.
from c4d.plugins import CommandData, TagData, ObjectData
from random import randint
from os import urandom
# Regex Import
import re
# XML Imports is for Saving and Loading Project Data for Plugin UI / Xml good but long functions to make.
from xml.dom import minidom
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree
# Json Import to store or load descriptions or any data / .json is easy and alot flexable and short coding.
import json
# Python Plugin System Load Modules Dynamically With importlib.
import importlib
# Date System
DateT = str(datetime.date.today())
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
#from ..fcs_c4d_dialogs import fcs_common_global_lib as g_lib

config_data = None
fcsID = g_lib.fcsID
jsonEdit = g_lib.jsonEdit
fs_lib = g_lib.fs_lib

class AutoRenamer_Dialog(WindowDialog):
 ##### ID Numbers for UI Elements #####
    # Top Menu
    UI_BTN_CLEAR = {"id": 5067, "name": "Clear"}
    UI_BTN_SaveAsStrtup = {"id": 5087, "name": "Save Data As Startup"}
    UI_BTN_ReMoveStarup = {"id": 5097, "name": "Remove Startup Data"}    
    # Auto Renamer UI ID's
    EB_CName = 3001
    EB_ICAOAirport = 3002
    EB_ObjName = 3003
    CBL_Ext = 3004
    Ext_GP = 201 
    Ext_MEB = 202 
    Ext_EB = 203 
    Ext_MISC = 204 
    Ext_TB = 205
    Ext_VEG = 206
    Ext_VEH = 207 
    Ext_NONE = 208     
    BTN_Rename = 3005

    def __init__(self, global_strings):
        self.IDS = global_strings

 ##### UI Window Dialog #####
    #  Creating Panel UI 
    def CreateLayout(self):
        
        self.SetTitle(self.IDS.ID(fcsID.IDS_AUTORENAMER_TITLE))

        # Adding the top UI menu of the dialog.
        self.MenuFlushAll()
        self.MenuSubBegin(self.IDS.ID(fcsID.IDS_FILE))
        self.MenuAddString(self.UI_BTN_CLEAR["id"], self.UI_BTN_CLEAR["name"])
        self.MenuAddString(self.UI_BTN_SaveAsStrtup["id"], self.UI_BTN_SaveAsStrtup["name"])
        self.MenuAddString(self.UI_BTN_ReMoveStarup["id"], self.UI_BTN_ReMoveStarup["name"])
        self.MenuSubEnd()    
        self.MenuFinished()        

        self.GroupBegin(0, c4d.BFH_SCALEFIT, 0, 1, "Name Convension")
        self.GroupBorder(c4d.BORDER_BLACK)
        self.GroupBorderSpace(6, 6, 6, 6)
        self.AddEditText(self.EB_CName, c4d.BFH_SCALEFIT, 150, 0)
        self.AddStaticText(0, c4d.BFH_MASK, 0, 10, "+")         
        self.AddEditText(self.EB_ICAOAirport, c4d.BFH_SCALEFIT, 150, 0)
        self.AddStaticText(0, c4d.BFH_MASK, 0, 10, "+")
        self.AddComboBox(self.CBL_Ext, c4d.BFH_MASK, 100, 0, False)
        self.AddChild(self.CBL_Ext, self.Ext_NONE, 'None')
        self.AddChild(self.CBL_Ext, self.Ext_GP, 'GP')
        self.AddChild(self.CBL_Ext, self.Ext_EB, 'EB')
        self.AddChild(self.CBL_Ext, self.Ext_MEB, 'MEB')
        self.AddChild(self.CBL_Ext, self.Ext_TB, 'TB')
        self.AddChild(self.CBL_Ext, self.Ext_MISC, 'MISC')
        self.AddChild(self.CBL_Ext, self.Ext_VEG, 'VEG')
        self.AddChild(self.CBL_Ext, self.Ext_VEH, 'VEH')        
        self.GroupEnd()
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 0, 1, "Oject Name")
        self.GroupBorder(c4d.BORDER_BLACK)
        self.GroupBorderSpace(6, 6, 6, 6)
        self.AddEditText(self.EB_ObjName, c4d.BFH_SCALEFIT, 150, 0)
        self.AddButton(self.BTN_Rename, c4d.BFH_SCALEFIT, 20, 10, "Rename")
        self.GroupEnd()  

        return True 
 ##### Main UI Operations Functions #####
    # Load UI on Loading Startup as default.
    def InitValues(self):

        g_lib.fcsLog.DEBUG("F.C.S Auto Renamer is Open Now!", False, g_lib.DebugMode)
        self.config_data = g_lib.fcsDIR.presetLayout

        if not os.path.exists(self.config_data):
            self.SetString(self.EB_CName, "IDS")
            self.SetString(self.EB_ICAOAirport, "EGBB")
            self.SetString(self.EB_ObjName, "")
            self.SetLong(self.CBL_Ext, self.Ext_GP)
        else:
            # Run this Defaauult from a Xml in FCS Prefs
            doc = c4d.documents.GetActiveDocument()
            XmlFileTree = etree.parse(self.config_data)
            XmlRoot = XmlFileTree.getroot()
            for data in XmlRoot.findall('StartupUI_Data'):

                LoadData = data.get("CompanyName")
                self.SetString(self.EB_CName, LoadData)

                LoadData = data.get("ICAO")
                self.SetString(self.EB_ICAOAirport, LoadData)

                LoadData = data.get("ObjName")
                self.SetString(self.EB_ObjName, LoadData)

                ListData = data.get("Extension")
                if ListData == "GP":
                    D = self.Ext_NONE
                if ListData == "GP":
                    D = self.Ext_GP
                if ListData == "EB":
                    D = self.Ext_EB
                if ListData == "MEB":
                    D = self.Ext_MEB
                if ListData == "TB":
                    D = self.Ext_TB
                if ListData == "MISC":
                    D = self.Ext_MISC
                if ListData == "VEG":
                    D = self.Ext_VEG
                if ListData == "VEH":
                    D = self.Ext_VEH                                        
                self.SetLong(self.CBL_Ext, D)

        return True        
    # Core Operation
    def MainFunction(self):
        doc = c4d.documents.GetActiveDocument()

        objs = doc.GetActiveObjects(1)

        CompanyName = self.GetString(self.EB_CName)
        AirportICAO = self.GetString(self.EB_ICAOAirport)
        ObjectName = self.GetString(self.EB_ObjName)

        if self.GetLong(self.CBL_Ext) == self.Ext_NONE:
            Extension = ""
        if self.GetLong(self.CBL_Ext) == self.Ext_GP:
            Extension = "GP"
        if self.GetLong(self.CBL_Ext) == self.Ext_MEB:
            Extension = "MEB"
        if self.GetLong(self.CBL_Ext) == self.Ext_EB:
            Extension = "EB"
        if self.GetLong(self.CBL_Ext) == self.Ext_MISC:
            Extension = "MISC"    
        if self.GetLong(self.CBL_Ext) == self.Ext_TB:
            Extension = "TB"
        if self.GetLong(self.CBL_Ext) == self.Ext_VEG:
            Extension = "VEG"
        if self.GetLong(self.CBL_Ext) == self.Ext_VEH:
            Extension = "VEH"

        NewName = CompanyName + "_" + AirportICAO + "_" + Extension + "_" + ObjectName

        LimitName = doc.SearchObject( NewName + "_" +"3000000")
        if LimitName == None:
            LimitName = NewName + "_" + "0"

        if not objs:
            gui.MessageDialog("You must select an object or spline!")
            return False

        for Obj in objs:

            degit = int(filter(str.isdigit, LimitName))+1

            Obj.SetName(NewName + "_" + str(degit))

            for N in list(reversed(range(300))):
                findobj = doc.SearchObject(NewName + "_" + str(N)) 
                if findobj == None:            
                    Obj.SetName(NewName + "_" + str(N))

        c4d.EventAdd()            
        return True
    def ClearUI(self):
        self.SetString(self.EB_CName, "")
        self.SetString(self.EB_ICAOAirport, "")
        self.SetString(self.EB_ObjName, "")
        self.SetLong(self.CBL_Ext, self.CBL_Ext)
        c4d.EventAdd()            
        return True
    # Add StartUp or Remove.
    def SaveDataAsStartup(self):
        self.config_data = g_lib.fcsDIR.presetLayout
        doc = c4d.documents.GetActiveDocument()
        # Saving Data to XML File.
        MakeXml_root = minidom.Document()
        AddToXML = MakeXml_root.createElement('AutoRenamerUIData')
        MakeXml_root.appendChild(AddToXML)

        CompanyName = self.GetString(self.EB_CName)
        AirportICAO = self.GetString(self.EB_ICAOAirport)
        ObjectName = self.GetString(self.EB_ObjName)

        if self.GetLong(self.CBL_Ext) == self.Ext_NONE:
            Extension = ""
        if self.GetLong(self.CBL_Ext) == self.Ext_GP:
            Extension = "GP"
        if self.GetLong(self.CBL_Ext) == self.Ext_MEB:
            Extension = "MEB"
        if self.GetLong(self.CBL_Ext) == self.Ext_EB:
            Extension = "EB"
        if self.GetLong(self.CBL_Ext) == self.Ext_MISC:
            Extension = "MISC"    
        if self.GetLong(self.CBL_Ext) == self.Ext_TB:
            Extension = "TB"
        if self.GetLong(self.CBL_Ext) == self.Ext_VEG:
            Extension = "VEG"
        if self.GetLong(self.CBL_Ext) == self.Ext_VEH:
            Extension = "VEH"

        Data = MakeXml_root.createElement('StartupUI_Data')
        Data.setAttribute('CompanyName', CompanyName)
        Data.setAttribute('ICAO', AirportICAO)
        Data.setAttribute('ObjName', ObjectName)
        Data.setAttribute('Extension', Extension)        
        AddToXML.appendChild(Data)

        SaveXml = MakeXml_root.toprettyxml(indent="\t")
        with open(self.config_data, 'w') as f:
            f.write(SaveXml)
            f.close()
        c4d.EventAdd()            
        return True        
    def RemoveStartup(self):
        self.config_data = g_lib.fcsDIR.presetLayout
        doc = c4d.documents.GetActiveDocument()        
        files = glob.glob(self.config_data)
        for filename in files:
            os.remove(filename)        
        return True
 ##### Excuting Operations #####

    ###################################################################
    # Excuting Commands for Functions and UI Elements Functions.
    ###################################################################
    def Command(self, id, msg=None):
        if (id == self.BTN_Rename):
            self.MainFunction()

        if (id == 5067):
            self.ClearUI()

        if (id == 5087):
            self.SaveDataAsStartup()

        if (id == 5097):
            self.RemoveStartup()
        return True


