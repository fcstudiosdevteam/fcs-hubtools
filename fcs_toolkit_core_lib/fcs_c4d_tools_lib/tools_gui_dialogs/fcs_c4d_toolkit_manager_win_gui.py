"""
ToolKit Manager Tool Window GUI Dialog
"""
#  // Imports for Cinema 4D //
import c4d
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as WindowDialog        # eg:( class My_Dialog(c4d.gui.GeDialog): ) to ( class My_Dialog(WindowDialog): ) Just keeping code line short.
from c4d.plugins import CommandData, TagData, ObjectData
from random import randint
# Python Plugin System Load Modules Dynamically With importlib.
import importlib
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
#from res._fcs_lib.fcs_module_lib import fcs_common_global_lib as g_lib
fcsID = g_lib.fcsID
jsonEdit = g_lib.jsonEdit


class FCS_ToolsetManager_Dialog(WindowDialog):           # Toolset Manager Dialog.
 ##### \\ UI IDs \\ #####
    #####################################################################################
    # ID's for Dialog User Interface, UI Buttons Elements.
    # Check Buttons, and for Coding (self.ID - this is When the ID is in a Class).
    #####################################################################################
    fcs_link = "http://fieldcreatorsstudios.com/"
    fcs_text = "www.FieldCreatorsStudios.com"
    InfoTxt = "Lead Plugin Developer: \nAshton J. Rolle aka AP Ashton \n\nExternal Programer: \nCreswell Gould aka CJ"
    InfoTxt_Data = "Thank you for downloading our F.C.S Tool-set.\nThis is our Tool-set Manger.\nYou can Enable or Disable the tool you choose to use.\nThen restart your Cinema 4D for the F.C.S tools\nyou enable or disable."
    fcs_toolset_manager_gui_dlg         = 6001
    ID_TPM_OVERALL_GRP                  = 6002
    ID_TPM_COL_HEADER_GRP               = 6003
    ID_TPM_LIST_VIEW_GRP                = 6004
    ID_TPM_ENABLE_All                   = 6005
    ID_TPM_DISABLE_ALL                  = 6006
    ID_TPM_STATIC_TXT1                  = 6007
    ID_TRM_STATIC_TXT3                  = 6009
    ID_TRM_LIST_GRP                     = 6010
    ID_TRM_EMPTY_BOTTOM_GRP             = 6011
    ID_TPM_INFO_TEXT                    = 6012    

    FCS_WEBSITE_GUI_BANNER_ID = 1008

    GUI_ID_LIST = []
    Tools_IDs_LIST = []
 ##### \\ Tool Core Method Functions Operations \\   #####
    def Enable_GUI_BTN_if_ToolInstalled(self):
        if self.GUI_ID_LIST:
                    
            JsonEdit = jsonEdit
            JFile = PLUGIN_DIR().FCS_Tools_IDs_CONIG2

            for ui_ele in self.GUI_ID_LIST:

                tool_id = ui_ele.split('--')[0]
                mode_id = ui_ele.split('--')[1]
                btn_id = ui_ele.split('--')[2]

                # Get Data
                load_tool = "PLUGIN_ID"+tool_id

                print(load_tool)

                Get_TM_Data = JsonEdit.GetItemProperty(json_file=JFile, main_property=load_tool)                

                if Get_TM_Data == "Enable":
                    g_lib.Toggle_Image_GUI_BTN(func_instance=self, toggle_mode=True, btn_id=int(btn_id), ui_icon=I(i="tpm_Enable_icon"))
                    self.SetString(int(mode_id), "Enable")
                else:
                    pass
        return True
    # Toggle the .xml file by editing it function.
    def Toggle(self, element_string, State):
        JFile = PLUGIN_DIR().FCS_Tools_IDs_CONIG2
        jsonEdit.EditItemProperty(json_file=JFile, main_property=element_string, new_value=State)
        return True
 ##### \\ GUI Layout Panels | User Interface in Panels \\   #####
    def Tool_Slot_Item_Layout_Panel(self, tool_name, btn_id, layoutgrp_id, mode_id, mode_state, ui_icon):
        # Button Settings
        toolIcon_set = {'id':0, 'btn_look':c4d.BORDER_NONE, 'icon':I(i="tpm_icon")}
        enableORdisable_btn_set = {'id':btn_id, 'btn_look':c4d.BORDER_NONE, 'tip':"<b>" + "Enable/Disable" + "</b>\n" + tool_name + " Tool.", 'icon':I(i=ui_icon), 'clickable':True}
        
        # Slot Item Layout
        self.GroupBegin(layoutgrp_id, c4d.BFH_SCALEFIT, 0, 1, "")
        self.GroupBorder(c4d.BORDER_ROUND)
        self.GroupBorderNoTitle(c4d.BORDER_ROUND)

        self.GroupBorderSpace(3, 3, 3, 3)

        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=toolIcon_set)

        self.AddStaticText(0, c4d.BFH_SCALEFIT, 0, 15, tool_name, c4d.BORDER_WITH_TITLE_BOLD)

        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=enableORdisable_btn_set)
        
        self.AddStaticText(mode_id, c4d.BFH_SCALEFIT, 0, 15, mode_state)

        self.GroupEnd() # GroupBegin 1

        # Set GUI Color Values
        self.SetDefaultColor(layoutgrp_id, c4d.COLOR_BG, g_lib.BG_DARK)
        self.HideElement(mode_id, True)
        return True

    def Load_List_Items(self):
        self.LayoutFlushGroup(self.ID_TRM_LIST_GRP)

        degit = 0

        list_of_tools = [PLUG_SNG, PLUG_A_R, PLUG_N_S, PLUG_T2C, PLUG_FSS, PLUG_O2P, PLUG_UCM, PLUG_APC, PLUG_RRP, PLUG_PSC, PLUG_SMP, PLUG_A2L, PLUG_FST, PLUG_FQS, PLUG_OSP, PLUG_SP, PLUG_C_P]
        
        for each_tool in list_of_tools:
            degit+=1
            new_id_num = degit
            name_txt = each_tool['Name']
            tool_id = each_tool['ID']
            grp_id = 70+new_id_num
            mode_id = 300+new_id_num
            btn_id = 400+new_id_num

            gui_slot_item = str(tool_id) + "--" + str(mode_id) + "--" + str(btn_id)
            # Add to list.
            self.GUI_ID_LIST.append(gui_slot_item)

            self.Tool_Slot_Item_Layout_Panel(tool_name=name_txt, btn_id=btn_id, layoutgrp_id=grp_id, mode_id=mode_id, mode_state="Disable", ui_icon="tpm_Disable_icon")

        #self.LayoutChanged(self.ID_TPM_OVERALL_GRP)
        return True 

    def FCS_Web_Banner(self):
        self.LayoutFlushGroup(self.ID_TRM_EMPTY_BOTTOM_GRP) # Refresh Group UI
        g_lib.Bottom_FCS_Web_GUI_Banner(ui_instance=self, button_id=self.FCS_WEBSITE_GUI_BANNER_ID)
        self.LayoutChanged(self.ID_TRM_EMPTY_BOTTOM_GRP) # Update Group UI
        return True
 ##### \\ Main C4D GUI Dialog Operations Functions which are Methods to Override for GeDialog class. \\   #####
    # The __init__ is an Constuctor and help get and passes data on from the another class.
    def __init__(self):
        super(FCS_ToolsetManager_Dialog, self).__init__()
    # Creating Dialog UI Interface Layout 
    def CreateLayout(self):
        self.MenuFlushAll()
        # --------------------- #
        self.MenuSubBegin("Tools Release Notes")
        self.MenuAddString(0, '(Coming Soon!)')
        self.MenuSubEnd() # Help Menu End
        # --------------------- #
        self.LoadDialogResource(self.fcs_toolset_manager_gui_dlg) # Loads GUI stuff from the .res by the dialog ID that is in the .res & .str file.   
        self.SetDefaultColor(self.ID_TPM_OVERALL_GRP, c4d.COLOR_BG, g_lib.BG_DEEP_DARKER)
        self.SetDefaultColor(self.ID_TPM_COL_HEADER_GRP, c4d.COLOR_BG, g_lib.BG_DARKER_COL)
        self.SetDefaultColor(self.ID_TPM_INFO_TEXT, c4d.COLOR_BG, g_lib.BG_DEEP_DARKER)
        self.Load_List_Items()
        self.FCS_Web_Banner()        
        return True
    # This method will initialize all the values inside the dialog with default values
    def InitValues(self):
        self.SetString(self.ID_TPM_INFO_TEXT, self.InfoTxt_Data)
        self.Enable_GUI_BTN_if_ToolInstalled()
        return True
    # Excuting Commands for Functions 
    def Command(self, id, msg=None):

        if id == self.ID_TPM_INFO_TEXT:
            self.SetString(self.ID_TPM_INFO_TEXT, self.InfoTxt_Data)

        if id == self.FCS_WEBSITE_GUI_BANNER_ID:
            g_lib.OpenFCSWebLink(page="")

        if self.GUI_ID_LIST:

            for ui_ele in self.GUI_ID_LIST:

                tool_id = ui_ele.split('--')[0]

                mode_id = ui_ele.split('--')[1]

                btn_id = ui_ele.split('--')[2]

                if id == int(btn_id):
                    if self.GetString(int(mode_id)) == "Disable":
                        g_lib.Toggle_Image_GUI_BTN(func_instance=self, toggle_mode=False, btn_id=int(btn_id), ui_icon=I(i="tpm_Enable_icon"))
                        self.SetString(int(mode_id), "Enable")
                        self.Toggle(element_string="PLUGIN_ID"+tool_id, State="Enable")
                        #print("Enable")
                    else:
                        g_lib.Toggle_Image_GUI_BTN(func_instance=self, toggle_mode=True, btn_id=int(btn_id), ui_icon=I(i="tpm_Disable_icon"))
                        self.SetString(int(mode_id), "Disable")
                        self.Toggle(element_string="PLUGIN_ID"+tool_id, State="Disable")
                        #print("Disable")

        if id == self.ID_TPM_ENABLE_All:
            if self.GUI_ID_LIST:
                for ui_el in self.GUI_ID_LIST:
                    tool_id = ui_el.split('--')[0]                    
                    mode_id = ui_el.split('--')[1]
                    btn_id = ui_el.split('--')[2]
                    g_lib.Toggle_Image_GUI_BTN(func_instance=self, toggle_mode=False, btn_id=int(btn_id), ui_icon=I(i="tpm_Enable_icon"))
                    self.SetString(int(mode_id), "Enable")
                    self.Toggle(element_string="PLUGIN_ID"+tool_id, State="Enable", store_state="ToolMode")

        if id == self.ID_TPM_DISABLE_ALL:
            if self.GUI_ID_LIST:
                for ui_el2 in self.GUI_ID_LIST:
                    tool_id = ui_el2.split('--')[0]                    
                    mode_id = ui_el2.split('--')[1]
                    btn_id = ui_el2.split('--')[2]
                    g_lib.Toggle_Image_GUI_BTN(func_instance=self, toggle_mode=True, btn_id=int(btn_id), ui_icon=I(i="tpm_Disable_icon"))
                    self.SetString(int(mode_id), "Disable")
                    self.Toggle(element_string="PLUGIN_ID"+tool_id, State="Disable", store_state="ToolMode")

        return True 
    #------------------------------------------------------#
    def DestroyWindow(self):
        self.GUI_ID_LIST = []
        self.Tools_IDs_LIST = []
        g_lib.SendInstructions("Please restart Cinema 4D for the F.C.S tools you enable or disable.")            
