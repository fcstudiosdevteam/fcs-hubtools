"""
FCS Preferences Window GUI Dialog
"""
#  // Imports for Cinema 4D //
import c4d
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as WindowDialog        # eg:( class My_Dialog(c4d.gui.GeDialog): ) to ( class My_Dialog(WindowDialog): ) Just keeping code line short.
from c4d.plugins import CommandData, TagData, ObjectData
from random import randint
# XML Imports is for Saving and Loading Project Data for Plugin UI / Xml good but long functions to make.
from xml.dom import minidom
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree

# Python Plugin System Load Modules Dynamically With importlib.
import importlib
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
#from res._fcs_lib.fcs_module_lib import fcs_common_global_lib as g_lib
fcsID = g_lib.fcsID
jsonEdit = g_lib.jsonEdit

class FCS_ToolKit_Preferences_Dialog(WindowDialog):      # Preferences Dialog.
 #####  \\  GUI IDs and Strings \\   ##### 

    DialogTitleName = "F.C.S Preferences"

    MainGroupUI_ID = 1089

    ID_TREEVIEW = 909

    ## \\ C4D to MCX Ids \\ ## 
    C4D2MCX_GRP = {"grp":1001, "bar_id":1002, "FT_id":1012}
    MCX_Ids = {"str":"Model Converter X Path : ", "dir":1003, "dir_btn":1004, "tex_dir":1052, "tex_btn":1053, "tex_chk":1054, "TexPathsID":1060, "TexPathGrpID":1061, "TPStateID":1062} 
    Main_Exp_Ids = {"str":" Directory : ", "dir":1005, "dir_btn":1006, "chk":1007}
    BackupDir_Ids = {"str":"Backup Path : ", "dir":1008, "dir_btn":1009, "chk":1010, "open_btn":1011}
    C4D_MCXTex_List = []


    SNG_GRP = {"str":" Directory :", "dir":1013, "dir_btn":1014, "grp":1015, "TreeView_id":1016, "TreeViewGrp_id":1026, "bar_id":1017, "FT_id":1018, "CB":1033, "C1":1034, "C2":1035} 
    SNG_RF_List = [] # Add Recent Export Folders IDs as a lic4d.storage.

    PS4D_GRP = {"str":" Directory :", "dir":1020, "dir_btn":1021, "grp":1022, "TreeView_id":1023, "bar_id":1024, "FT_id":1025} 
    PS4D_RF_List1 = [] # Add Recent Export Folders IDs as a lic4d.storage.

    Nurbo_GRP = {"grp":1028, "TreeView_id":1029, "TreeViewGrp_id":1030, "bar_id":1031, "FT_id":1032}

    HistoryAboutInfoUI = 1040 

    FCS_STATION_LOG_BTN = 1042
    FCS_PLUG_DIR_BTN = 1043

    SAVE_FCS_PREFS = 1044

    PLUGIN_Version = 1045

    FCS_PLUG_PREFS_DIR_BTN = 1050
 #####  \\  GUI Utils Operations \\   #####

    ########################################
    # UI Utils Functions
    ########################################
    # Create Directory Slot Temp GUI Layout.  
    def Add_UserDirectory(self, Str, PathId, BrowserId, ExtraBtn, ExtBtnId, ExtIcon):
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 1, 0, "")
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 0, 1, "")
        self.AddStaticText(0, c4d.BFH_CENTER, 0, 0, Str) # "Directory#" + NumStr + ":"
        # Folder Path
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 0, 1, "")
        self.GroupSpace(0, 0)
        self.GroupBorderNoTitle(c4d.BORDER_BLACK)
        self.AddEditText(int(PathId), c4d.BFH_SCALEFIT, 170, 10)
        Vect = c4d.Vector(0.125, 0.125, 0.125)
        self.SetDefaultColor(int(PathId), c4d.COLOR_BG, Vect)

        fold_settings = {'id':int(BrowserId), 'btn_look':c4d.BORDER_NONE, 'tip':"Get Directory", 'icon':g_lib.fcsDIR.I(i="FIcon"), 'clickable':True, 'background_col':c4d.COLOR_TEXTFOCUS}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=fold_settings)
        #if ExtraBtn == True:
            #g_lib.Add_Image_Button_GUI(ui_instance=self, B_Id=int(ExtBtnId), BL=c4d.BORDER_NONE, TI="Add Directory", BI=ExtIcon, TB="No", CB="Yes", BC="Yes")
        self.GroupEnd()
        self.GroupEnd()
        self.GroupEnd()        
        return True

    # Create Auto IDs for List of GUI elements.
    def Add_CreateList_Ids_and_GUI_Generator(self, ListName, start_id_1, start_id_2, start_id_3, SlotAmount, AddExtraBtn, ExtraUI_Icon):
        # Create the Amount of Items.
        Amount = SlotAmount

        MainNumRange = range(Amount)

        degit = 0

        for each_num in MainNumRange:

            degit+=1

            new_id_num = str(degit)

            DirNum = new_id_num

            Path_id = str(start_id_1) + new_id_num

            BTN_id = str(start_id_2) + new_id_num

            if AddExtraBtn == True:
                Extra_BTN_id = str(start_id_3) + new_id_num
            else:
                Extra_BTN_id = str("0")


            Add_Data = DirNum + "--" + Path_id + "--" + BTN_id + "--" + Extra_BTN_id

            ListName.append(Add_Data)

            #print str(Add_Data)

        # If list created with IDs, then create GUI's layouts with Ids in the lic4d.storage. 
        if ListName:

            for E in ListName:

                Numlink = E.split('--')[0]
                DirPath = E.split('--')[1]
                DirBTN = E.split('--')[2]

                if AddExtraBtn == True:
                    ExtraBTN_Id = E.split('--')[3]
                else:
                    ExtraBTN_Id = 0

                # Create GUI Path Dir's with Generate List IDs.
                self.Add_UserDirectory(Str=" Path "+ Numlink +"", PathId=DirPath, BrowserId=DirBTN, ExtraBtn=AddExtraBtn, ExtBtnId=ExtraBTN_Id, ExtIcon=ExtraUI_Icon)
                #print str(E)

        L = len(ListName)
        #print "Amount Of Items:" + str(L)
        return True

    # Checking Checkbox GUI for state and get data of True & False.  
    def If_Checkbox_Checked(self, CHK_id, xml_Data, xml_str):
        if CHK_id == True:
            xml_Data.setAttribute(xml_str, "True")
        else:
            xml_Data.setAttribute(xml_str, "False")        
        return True

    # GUI Toggle System for QuickTab Bar Fold Icon Button.
    def FoldBar_BTN_Toggle(self, id, BTN, UI_Group, ToggleID):

        if (id == BTN):

            if self.GetString(ToggleID) == "c":
                self.HideElement(UI_Group, False) # Show
                self.SetString(ToggleID, "O")
                print("Open")
                self.LayoutChanged(self.MainGroupUI_ID) # Update

            else:
                self.HideElement(UI_Group, True) # Hide
                self.SetString(ToggleID, "c")
                print("Close")
                self.LayoutChanged(self.MainGroupUI_ID) # Update       
        return True        

    # Add and Update UI elements or Refresh UI.
    def Add_Reload_Refresh_Update_GUIs(self):
        g_lib.Add_TreeView_GUI(ui_instance=self, ui_id=self.SNG_GRP["TreeView_id"], group_id=self.SNG_GRP["TreeViewGrp_id"], tv_colums=1, get_folder=g_lib.fcsDIR.SNG_SaveFiles(), extension="SNG-Preset.xml")
        g_lib.Add_TreeView_GUI(ui_instance=self, ui_id=self.Nurbo_GRP["TreeView_id"], group_id=self.Nurbo_GRP["TreeViewGrp_id"], tv_colums=1, get_folder=g_lib.fcsDIR.plugin_Saves_folder, extension="NS-Preset.xml")
        self.LayoutChanged(self.MainGroupUI_ID) # Update
        return True

    # Enable or Disable GUI's
    def Enable_or_Disable(self, GUI_list, state_value):
        for each_GUI in GUI_list:
            self.Enable(each_GUI, state_value)
        return True

    # Adding Path to EditField Box, when Folder GUI icon pressed.  
    def GUI_FolderBTN_Add_PathDir(self, id, add_list, id_BTN, dirbox_id):
        if add_list == True:
            id_list = id_BTN
            for each_btn in id_list:
                # Break down data of each item of the lic4d.storage.
                DirPathNum_GUI = each_btn.split('--')[0]
                DirPath_GUI = int(each_btn.split('--')[1])
                DirPathBtn = int(each_btn.split('--')[2])

                if id == (DirPathBtn):
                    # User get path dir.
                    c_dir = c4d.storage.LoadDialog(type=c4d.FILESELECTTYPE_ANYTHING, title="Get Directory", flags=c4d.FILESELECT_DIRECTORY, force_suffix="")
                    if not c_dir:
                        return
                    self.SetString(DirPath_GUI, c_dir)

        # Run this if no list, and its only one GUI btn. 
        else:
            if id == (id_BTN):
                # User get path dir.
                c_dir = c4d.storage.LoadDialog(type=c4d.FILESELECTTYPE_ANYTHING, title="Get Directory", flags=c4d.FILESELECT_DIRECTORY, force_suffix="")
                if not c_dir:
                    return
                # set to the editbox gui.            
                self.SetString(dirbox_id, c_dir)
        return True  

    # Hiding GUI Elements on dialog startuos.path. 
    def Init_Hide_GUI_Elements(self):
        GUI_Elements = [ self.C4D2MCX_GRP["grp"], self.SNG_GRP["grp"], self.PS4D_GRP["grp"], self.PS4D_GRP["grp"], self.Nurbo_GRP["grp"], self.MCX_Ids["TexPathGrpID"] ]
        for GUI in GUI_Elements:
            self.HideElement(GUI, True) # Hide
        self.LayoutChanged(self.MainGroupUI_ID) # Update
        return True        

    def Create_Prefs_Config_XML(self):

        if not os.path.exists(g_lib.fcsDIR.ini_file_RecentSelNGO):
            pass
        else:
            os.remove(g_lib.fcsDIR.ini_file_RecentSelNGO)

        if not os.path.exists(g_lib.fcsDIR.FCS_PREF_CONIG):
            pass
        else:
            os.remove(g_lib.fcsDIR.FCS_PREF_CONIG)

        MCX_Dir = self.GetString(self.MCX_Ids["dir"])
        MCX_TexDir = self.GetString(self.MCX_Ids["tex_dir"])
        MCX_TexCHK = self.GetBool(self.MCX_Ids["tex_chk"])
        Export_Dir = self.GetString(self.Main_Exp_Ids["dir"])
        Export_CHK = self.GetBool(self.Main_Exp_Ids["chk"])
        Backup_Dir = self.GetString(self.BackupDir_Ids["dir"])
        Backup_CHK = self.GetBool(self.BackupDir_Ids["chk"])

        # //  Create Xml Structure. //
        MakeXml_root = minidom.Document()
        AddToXML = MakeXml_root.createElement('FCS_PREF_C4D_CONIG')
        MakeXml_root.appendChild(AddToXML)

        # FCS 

        # Save C4D to MCX Data
        mcxData = MakeXml_root.createElement('C4DtoMCX_Data')
        mcxData.setAttribute('ModelConverterX_Path', MCX_Dir)
        mcxData.setAttribute('Tex_for_MCX', MCX_TexDir)
        self.If_Checkbox_Checked(CHK_id=MCX_TexCHK, xml_Data=mcxData, xml_str='Tex_for_MCX_CHK')
        mcxData.setAttribute('ExportPath', Export_Dir)
        self.If_Checkbox_Checked(CHK_id=Export_CHK, xml_Data=mcxData, xml_str='Export_CHK')
        mcxData.setAttribute('Backup_Path', Backup_Dir)
        self.If_Checkbox_Checked(CHK_id=Backup_CHK, xml_Data=mcxData, xml_str='Backup_Path_CHK')
        AddToXML.appendChild(mcxData)

        #SP_Data = MakeXml_root.createElement('SwitchPlacer_Data')
        #SP_Data.setAttribute('UserCustom_Path', self.GetString(self.UI_Data_Dir_SP["P-id"]))
        #AddToXML.appendChild(SP_Data)

        #NS_Data = MakeXml_root.createElement('NurbSweep_Data')
        #NS_Data.setAttribute('UserCustom_Path', self.GetString(self.UI_Data_Dir_NS["P-id"]))
        #AddToXML.appendChild(NS_Data)            

        # Get Select_N_Go Recent Export Path Data and Create List of Paths
        # Remove List .txt file with the Export Paths

        # Save Texture_Paths Data 
        TextureP_Data = MakeXml_root.createElement('TexturePaths_Data')
        # Add Paths
        TP_Data = MakeXml_root.createElement('Texture_Paths')
        TextureP_Data.appendChild(TP_Data)
        for EportDirT in self.C4D_MCXTex_List:
            # Break down data of each item of the lic4d.storage.
            DirPathNum_GUI  = EportDirT.split('--')[0]
            DirPath_GUI = int(EportDirT.split('--')[1])
            DirPathBtn = int(EportDirT.split('--')[2])
            # Check if any data is in the editbox field.
            PathData = self.GetString(DirPath_GUI)
            if PathData == "":
                pass
            else: 
                # Add data to xml.
                AddPaths = MakeXml_root.createElement('TexturePath')
                AddPaths.setAttribute('Path_'+DirPathNum_GUI, PathData)
                TP_Data.appendChild(AddPaths)
        # Add Paths2
        TP_List_Data = MakeXml_root.createElement('TexturePathList')
        TP_Data.appendChild(TP_List_Data)
        for EportDir in self.C4D_MCXTex_List:
            # Break down data of each item of the lic4d.storage.
            DirPathNum_GUI  = EportDir.split('--')[0]
            DirPath_GUI = int(EportDir.split('--')[1])
            DirPathBtn = int(EportDir.split('--')[2])
            # Check if any data is in the editbox field.
            PathData = self.GetString(DirPath_GUI)
            if PathData == "":
                pass
            else: 
                # Add data to xml.
                AddPaths = MakeXml_root.createElement('All_Textures_Paths')
                AddPaths.setAttribute('Folder', PathData)
                TP_List_Data.appendChild(AddPaths)

        AddToXML.appendChild(TextureP_Data)

        # Save Select_N_Go Data
        SNG_DataRoot = MakeXml_root.createElement('Select_N_Go_Data')
        # Save Project Files
        SNG_Data = MakeXml_root.createElement('Select_N_Go_ProjectSaves')
        SNG_Data.setAttribute('UserCustom_Path', self.GetString(self.SNG_GRP["dir"]))
        SNG_DataRoot.appendChild(SNG_Data)        
        # Add Paths
        SNG_RFs_Data = MakeXml_root.createElement('SNG_Paths_Data')
        SNG_DataRoot.appendChild(SNG_RFs_Data)
        for EportDir in self.SNG_RF_List:
            # Break down data of each item of the lic4d.storage.
            DirPathNum_GUI  = EportDir.split('--')[0]
            DirPath_GUI = int(EportDir.split('--')[1])
            DirPathBtn = int(EportDir.split('--')[2])
            # Check if any data is in the editbox field.
            PathData = self.GetString(DirPath_GUI)
            if PathData == "":
                pass
            else:
                # Add data to xml. 
                AddPaths = MakeXml_root.createElement('ExportPath')    
                AddPaths.setAttribute('Path_'+DirPathNum_GUI, PathData)
                SNG_RFs_Data.appendChild(AddPaths)
                # Create List of Paths
                with open(g_lib.fcsDIR.ini_file_RecentSelNGO,'a') as t_file:
                    t_file.write(os.path.split(PathData)[1]+';'+PathData+'\n') 
        AddToXML.appendChild(SNG_DataRoot) 


        # Save Project Storage Data 
        PS4D_Data = MakeXml_root.createElement('ProjectStorage_Data')

        # Add Paths
        PS4D_RFs_Data = MakeXml_root.createElement('ProjectStorage_Paths')
        PS4D_Data.appendChild(PS4D_RFs_Data)
        for EportDir in self.PS4D_RF_List1:
            # Break down data of each item of the lic4d.storage.
            DirPathNum_GUI  = EportDir.split('--')[0]
            DirPath_GUI = int(EportDir.split('--')[1])
            DirPathBtn = int(EportDir.split('--')[2])
            # Check if any data is in the editbox field.
            PathData = self.GetString(DirPath_GUI)
            if PathData == "":
                pass
            else: 
                # Add data to xml.
                AddPaths = MakeXml_root.createElement('StoragePath')
                AddPaths.setAttribute('Path_'+DirPathNum_GUI, PathData)
                PS4D_RFs_Data.appendChild(AddPaths)

        # Add Paths
        PS4D_MenuList_Data = MakeXml_root.createElement('ProjectStorage_MenuList')
        PS4D_RFs_Data.appendChild(PS4D_MenuList_Data)
        for EportDir in self.PS4D_RF_List1:
            # Break down data of each item of the lic4d.storage.
            DirPathNum_GUI  = EportDir.split('--')[0]
            DirPath_GUI = int(EportDir.split('--')[1])
            DirPathBtn = int(EportDir.split('--')[2])
            # Check if any data is in the editbox field.
            PathData = self.GetString(DirPath_GUI)
            if PathData == "":
                pass
            else: 
                # Add data to xml.
                AddPaths = MakeXml_root.createElement('StoragePath')
                AddPaths.setAttribute('StorageFolder', PathData)
                PS4D_MenuList_Data.appendChild(AddPaths)

        AddToXML.appendChild(PS4D_Data)
        
        # Save XML CONIG  to Plugin Database Folder
        SaveXml = MakeXml_root.toprettyxml(indent="\t")
        with open(g_lib.fcsDIR.FCS_PREF_CONIG, 'w') as f:
            f.write(SaveXml)
            f.close() 
        #self.PS4D_RF_List = []
        return True

    def Loading_Prefs_Config_XML(self):

        # Check if folder with CONIG files exists. 
        if os.path.exists(g_lib.fcsDIR.FCS_PREF_CONIG):

            # To look in both files 
            XML_DATA_FILES = g_lib.fcsDIR.FCS_PREF_CONIG

            XmlFileTree = etree.parse(XML_DATA_FILES) 

            XmlRoot = XmlFileTree.getroot()

            # C4D To MCX XML Data
            # Set Data
            for get_C4D2MCX_data in XmlRoot.findall('C4DtoMCX_Data'):

                MCX_Dir = self.GetString(self.MCX_Ids["dir"])
                Export_Dir = self.GetString(self.Main_Exp_Ids["dir"])
                Export_CHK = self.GetBool(self.Main_Exp_Ids["chk"])
                Backup_Dir = self.GetString(self.BackupDir_Ids["dir"])
                Backup_CHK = self.GetBool(self.BackupDir_Ids["chk"])                

                GetMCXData = get_C4D2MCX_data.get("ModelConverterX_Path")
                self.SetString(self.MCX_Ids["dir"], GetMCXData)

                GetMCXData = get_C4D2MCX_data.get("Tex_for_MCX")
                self.SetString(self.MCX_Ids["tex_dir"], GetMCXData)

                GetMCXData = get_C4D2MCX_data.get("Tex_for_MCX_CHK")
                if GetMCXData == "False":
                    self.SetBool(self.MCX_Ids["tex_chk"], False)
                    L = [ self.MCX_Ids["tex_dir"], self.MCX_Ids["tex_btn"] ]
                    self.Enable_or_Disable(GUI_list=L, state_value=False)
                else:
                    self.SetBool(self.MCX_Ids["tex_chk"], True)
                    L = [ self.MCX_Ids["tex_dir"], self.MCX_Ids["tex_btn"]  ]
                    self.Enable_or_Disable(GUI_list=L, state_value=True)


                GetExportCHKData = get_C4D2MCX_data.get("Export_CHK")
                if GetExportCHKData == "False":
                    self.SetBool(self.Main_Exp_Ids["chk"], False)
                    L = [ self.Main_Exp_Ids["dir"], self.Main_Exp_Ids["dir_btn"] ]
                    self.Enable_or_Disable(GUI_list=L, state_value=False)
                else:
                    self.SetBool(self.Main_Exp_Ids["chk"], True)
                    L = [ self.Main_Exp_Ids["dir"], self.Main_Exp_Ids["dir_btn"] ]
                    self.Enable_or_Disable(GUI_list=L, state_value=True)


                GetExportDirData = get_C4D2MCX_data.get("ExportPath")
                self.SetString(self.Main_Exp_Ids["dir"], GetExportDirData)


                GetBackupCHKData = get_C4D2MCX_data.get("Backup_Path_CHK")
                if GetBackupCHKData == "False":
                    self.SetBool(self.BackupDir_Ids["chk"], False)
                    L = [ self.BackupDir_Ids["dir"], self.BackupDir_Ids["dir_btn"], self.BackupDir_Ids["open_btn"] ]
                    self.Enable_or_Disable(GUI_list=L, state_value=False) 
                else:
                    self.SetBool(self.BackupDir_Ids["chk"], True)
                    L = [ self.BackupDir_Ids["dir"], self.BackupDir_Ids["dir_btn"], self.BackupDir_Ids["open_btn"] ]
                    self.Enable_or_Disable(GUI_list=L, state_value=True)                                     

                GetBackupDirData = get_C4D2MCX_data.get("Backup_Path")
                self.SetString(self.BackupDir_Ids["dir"], GetBackupDirData)

            # Get Texture Folder Paths Data
            for EportDir in self.C4D_MCXTex_List:
                # Break down data of each item of the lic4d.storage.
                DirPathNum_GUI = EportDir.split('--')[0]
                DirPath_GUI = int(EportDir.split('--')[1])

                for get_root_data2 in XmlRoot.findall('TexturePaths_Data'):

                    for data2 in get_root_data2.findall('Texture_Paths'):

                        for all_ExportPaths in data2.findall('TexturePath'):

                            P_DATA = "Path_"+DirPathNum_GUI

                            GetData = all_ExportPaths.get(P_DATA)

                            if GetData:
                                self.SetString(DirPath_GUI, GetData)
                                #print GetData     
                            else:
                                pass



            # Select N Go XML Data
            # Get Select N Go Data
            for get_root_data in XmlRoot.findall('Select_N_Go_Data'):

                for data in get_root_data.findall('Select_N_Go_ProjectSaves'):

                    LoadData = data.get("UserCustom_Path")
                    if LoadData == "":
                        print("UserCustom_Path is Emty")
                    else:
                        self.SetString(self.SNG_GRP["dir"], LoadData)
            # Get Select N Go Recents Folder Paths Data
            for EportDir in self.SNG_RF_List:
                # Break down data of each item of the lic4d.storage.
                DirPathNum_GUI = EportDir.split('--')[0]
                DirPath_GUI = int(EportDir.split('--')[1])

                for get_root_data2 in XmlRoot.findall('Select_N_Go_Data'):

                    for data2 in get_root_data2.findall('SNG_Paths_Data'):

                        for all_ExportPaths in data2.findall('ExportPath'):

                            P_DATA = "Path_"+DirPathNum_GUI

                            GetData = all_ExportPaths.get(P_DATA)

                            if GetData:
                                self.SetString(DirPath_GUI, GetData)
                                #print GetData     
                            else:
                                pass

            # Set Project Storage C4D Data
            # Get StoragePaths Data
            for StoragePaths in self.PS4D_RF_List1:
                # Break down data of each item of the lic4d.storage.
                DirPathNum_GUI = StoragePaths.split('--')[0]
                DirPath_GUI = int(StoragePaths.split('--')[1])
                # Now look inside the .Xml file for data and set data to gui.
                for get_ProjectStorage_data in XmlRoot.findall('ProjectStorage_Data'):

                    for DataPath in get_ProjectStorage_data.findall('ProjectStorage_Paths'):                

                        for Path in DataPath.findall('StoragePath'):

                            P_DATA = "Path_"+DirPathNum_GUI

                            GetData = Path.get(P_DATA)

                            if GetData:
                                self.SetString(DirPath_GUI, GetData)
                                #print GetData     
                            else:
                                pass
        else:
            pass
        return True

    # Open folder for Tools. 
    def OpenFolders(self, FolderName):
        c4d.storage.ShowInFinder(FolderName, False)
        return True

    # Check for FCS tools that are installed , if not disable UI.
    def CheckToolInstalled(self, CheckTOOL, UI_ID):
        plug = plugins.FindPlugin(CheckTOOL, c4d.PLUGINTYPE_COMMAND)     
        if plug is None:
            self.Enable(UI_ID, False)   
        else:
            self.Enable(UI_ID, True)
        return True
 #####  \\  GUI Layout Panels \\   ##### 
    def C4D_TO_MCX_GUI_Panel(self):
        PanelName = " Cinema 4D To Model Converter X - Tool"
        Bar_ID = self.C4D2MCX_GRP["bar_id"]
        ToggleID = self.C4D2MCX_GRP["FT_id"]
        GroupID = self.C4D2MCX_GRP["grp"]
        g_lib.Add_QuickTab_Bar_GUI(ui_instance=self, bar_id=Bar_ID, bar_name=PanelName, fold=True, ui_state_id=ToggleID, width=150, height=15, color_mode=True, ui_color=c4d.Vector(0.125, 0.125, 0.125))
        self.GroupBegin(GroupID, c4d.BFH_SCALEFIT, 1, 0)
        self.GroupBorderSpace(10, 0, 10, 0)

        self.GroupBegin(0, c4d.BFH_SCALEFIT, 1, 0, " Export Settings")
        self.GroupBorderSpace(5, 5, 5, 5)
        self.GroupBorder(c4d.BORDER_BLACK)

        self.GroupBegin(0, c4d.BFH_MASK, 2, 0, "", c4d.BFV_GRIDGROUP_EQUALCOLS)
        self.GroupBorderSpace(5, 5, 5, 5)
        self.AddCheckbox(0, c4d.BFH_MASK, 0, 0, "PSR Object")
        self.AddCheckbox(0, c4d.BFH_MASK, 0, 0, "Center Object XZ")
        self.GroupEnd()

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT) 
        self.AddCheckbox(0, c4d.BFH_MASK, 0, 0, "0 Rotation on Y axis.")  
        self.GroupEnd()        

        self.AddSeparatorH(201, flags=c4d.BFH_SCALEFIT)           
        self.Add_UserDirectory(Str=self.MCX_Ids["str"], PathId=self.MCX_Ids["dir"], BrowserId=self.MCX_Ids["dir_btn"], ExtraBtn=False, ExtBtnId=0, ExtIcon=0)
        self.AddCheckbox(self.MCX_Ids["tex_chk"], c4d.BFH_MASK, 0, 0, "Export Textures to MCX Texture Folder")  
        self.Add_UserDirectory(Str="Path", PathId=self.MCX_Ids["tex_dir"], BrowserId=self.MCX_Ids["tex_btn"], ExtraBtn=False, ExtBtnId=0, ExtIcon=0)
        self.AddSeparatorH(201, flags=c4d.BFH_SCALEFIT)
        g_lib.Add_QuickTab_Bar_GUI(ui_instance=self, bar_id=self.MCX_Ids["TexPathsID"], bar_name="  Your Textures or Project Textures Folders", fold=True, ui_state_id=self.MCX_Ids["TPStateID"], width=150, height=15, color_mode=True, ui_color=g_lib.BG_DARKER_COL)
        self.GroupBegin(self.MCX_Ids["TexPathGrpID"], c4d.BFH_SCALEFIT, 1, 0, "")
        self.Add_CreateList_Ids_and_GUI_Generator(ListName=self.C4D_MCXTex_List, start_id_1=800, start_id_2=900, start_id_3=5050, SlotAmount=20, AddExtraBtn=False, ExtraUI_Icon=0)
        self.GroupEnd()        

        self.AddSeparatorH(201, flags=c4d.BFH_SCALEFIT)    
        self.AddCheckbox(self.Main_Exp_Ids["chk"], c4d.BFH_MASK, 0, 0, "Add Main Export Path")
        self.Add_UserDirectory(Str=self.Main_Exp_Ids["str"], PathId=self.Main_Exp_Ids["dir"], BrowserId=self.Main_Exp_Ids["dir_btn"], ExtraBtn=False, ExtBtnId=0, ExtIcon=0) 
        self.AddSeparatorH(201, flags=c4d.BFH_SCALEFIT)
        self.AddCheckbox(self.BackupDir_Ids["chk"], c4d.BFH_MASK, 0, 0, "Backup Export Model")      
        self.Add_UserDirectory(Str=self.BackupDir_Ids["str"], PathId=self.BackupDir_Ids["dir"], BrowserId=self.BackupDir_Ids["dir_btn"], ExtraBtn=False, ExtBtnId=0, ExtIcon=0)
        self.AddButton(self.BackupDir_Ids["open_btn"], c4d.BFH_MASK, 150, 10, "Open Backup Folder")

        self.SetDefaultColor(GroupID, c4d.COLOR_BG, g_lib.BG_LitDarkBlue_Col)
        self.GroupEnd()
        return True
    def Select_N_Go_GUI_Panel(self):
        PanelName = " Select N'Go"
        Bar_ID = self.SNG_GRP["bar_id"]
        ToggleID = self.SNG_GRP["FT_id"]
        GroupID = self.SNG_GRP["grp"]
        TreeViewID_GRP = self.SNG_GRP["TreeViewGrp_id"]
        ComboBoxId = self.SNG_GRP["CB"]
        g_lib.Add_QuickTab_Bar_GUI(ui_instance=self, bar_id=Bar_ID, bar_name=PanelName, fold=True, ui_state_id=ToggleID, width=150, height=15, color_mode=True, ui_color=c4d.Vector(0.125, 0.125, 0.125))
        self.GroupBegin(GroupID, c4d.BFH_SCALEFIT, 1, 0)
        self.GroupBorderSpace(10, 0, 10, 0)

        g_lib.Add_QuickTab_Bar_GUI(ui_instance=self, bar_id=0, bar_name="Select N'Go Saved Projects", fold=False, ui_state_id=0, width=150, height=15, color_mode=False, ui_color=0)
        #self.GroupBegin(1, c4d.BFH_SCALEFIT, 0, 1)
        #self.AddStaticText(0, c4d.BFH_SCALEFIT, 0, 10, " Saved Projects Directory :")
        #self.AddComboBox(ComboBoxId, c4d.BFH_SCALEFIT, 0, 10)
        #self.AddChild(ComboBoxId, self.SNG_GRP["C1"], "Custom Folder")
        #self.AddChild(ComboBoxId, self.SNG_GRP["C2"], "Default FCS Folder")
        #self.GroupEnd() 

        #self.Add_UserDirectory(Str=self.SNG_GRP["str"], PathId=self.SNG_GRP["dir"], BrowserId=self.SNG_GRP["dir_btn"], ExtraBtn=False, ExtBtnId=0, ExtIcon=0)

        self.GroupBegin(TreeViewID_GRP, c4d.BFH_MASK, 1, 0, "") # TreeView Group  
        self.GroupEnd() # TreeView GroupEnd

        g_lib.Add_QuickTab_Bar_GUI(ui_instance=self, bar_id=0, bar_name="Add Recent Folder Export Paths", fold=False, ui_state_id=0, width=150, height=15, color_mode=False, ui_color=0)
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 1, 0, "")
        self.GroupBorderSpace(5, 5, 5, 5)
        self.GroupBorderNoTitle(c4d.BORDER_BLACK)
        self.ScrollGroupBegin(0, c4d.BFH_SCALEFIT, c4d.SCROLLGROUP_VERT, 0, 100)
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 1, 0, "")
        self.Add_CreateList_Ids_and_GUI_Generator(ListName=self.SNG_RF_List, start_id_1=200, start_id_2=300, start_id_3=400, SlotAmount=100, AddExtraBtn=False, ExtraUI_Icon=0)
        self.GroupEnd()
        self.GroupEnd()

        self.GroupEnd()
        self.GroupEnd()
        return True        
    def ProjectStorage_GUI_Panel(self):
        PanelName = " ProjectStorage C4D - Tool"
        Bar_ID = self.PS4D_GRP["bar_id"]
        ToggleID = self.PS4D_GRP["FT_id"]
        GroupID = self.PS4D_GRP["grp"]
        g_lib.Add_QuickTab_Bar_GUI(ui_instance=self, bar_id=Bar_ID, bar_name=PanelName, fold=True, ui_state_id=ToggleID, width=150, height=15, color_mode=True, ui_color=c4d.Vector(0.125, 0.125, 0.125))

        self.GroupBegin(GroupID, c4d.BFH_SCALEFIT, 1, 0)
        self.GroupBorderSpace(10, 0, 10, 0)
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 1, 0, "")
        self.GroupBorderSpace(5, 5, 5, 5)
        self.GroupBorderNoTitle(c4d.BORDER_BLACK)
        self.ScrollGroupBegin(0, c4d.BFH_SCALEFIT, c4d.SCROLLGROUP_VERT, 0, 100)
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 1, 0, "")
        self.Add_CreateList_Ids_and_GUI_Generator(ListName=self.PS4D_RF_List1, start_id_1=500, start_id_2=600, start_id_3=700, SlotAmount=100, AddExtraBtn=False, ExtraUI_Icon=0)
        self.GroupEnd()
        self.GroupEnd()        

        self.GroupEnd() 
        self.GroupEnd()
        return True
    def Nurbo_GUI_Panel(self):
        PanelName = " Nurbo Generator - Tool"
        Bar_ID = self.Nurbo_GRP["bar_id"]
        ToggleID = self.Nurbo_GRP["FT_id"]
        GroupID = self.Nurbo_GRP["grp"]
        TreeViewID_GRP = self.Nurbo_GRP["TreeViewGrp_id"]
        g_lib.Add_QuickTab_Bar_GUI(ui_instance=self, bar_id=Bar_ID, bar_name=PanelName, fold=True, ui_state_id=ToggleID, width=150, height=15, color_mode=True, ui_color=c4d.Vector(0.125, 0.125, 0.125))
        self.GroupBegin(GroupID, c4d.BFH_SCALEFIT, 1, 0)
        self.GroupBorderSpace(10, 0, 10, 0)        
        g_lib.Add_QuickTab_Bar_GUI(ui_instance=self, bar_id=0, bar_name=" Nurbo Saved Projects", fold=False, ui_state_id=0, width=150, height=15, color_mode=False, ui_color=0)

        self.GroupBegin(TreeViewID_GRP, c4d.BFH_SCALEFIT, 1, 0, "") # TreeView Group  
        self.GroupEnd() # TreeView GroupEnd
 
        self.GroupEnd()        
        return True
    def FCS_OpenPluginAreas_GUI_Panel(self):
        self.AddSeparatorH(201, flags=c4d.BFH_SCALEFIT)
        self.AddButton(self.SAVE_FCS_PREFS, c4d.BFH_SCALEFIT, 0, 10, "Save Preferences Settings")
        self.AddSeparatorH(201, flags=c4d.BFH_SCALEFIT)
        self.AddButton(self.FCS_STATION_LOG_BTN, c4d.BFH_SCALEFIT, 0, 10, "Run FCS Station Log")
        self.AddButton(self.FCS_PLUG_DIR_BTN, c4d.BFH_SCALEFIT, 0, 10, "Open FCS Plugin Folder")
        self.AddButton(self.FCS_PLUG_PREFS_DIR_BTN, c4d.BFH_SCALEFIT, 0, 10, "Open FCS Plugin Prefs Folder")        
        self.GroupEnd()           
        return True
 #####  \\  Main Dialog Overall Panel Layout \\ #####
    # UI Layout with all different UI Panel Sections.
    def GUI_DialogLayout(self):

        self.GroupBegin(0, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 1, 0)

        self.GroupBegin(0, c4d.BFH_CENTER, 1, 0)
        self.AddStaticText(0, c4d.BFH_SCALEFIT, 0, 20, "F.C.S ToolKit Preferences Center v3.0", c4d.BORDER_WITH_TITLE_BOLD)
        self.GroupEnd()

        self.AddSeparatorH(201, flags=c4d.BFH_SCALEFIT)

        self.ScrollGroupBegin(0, c4d.BFH_SCALEFIT, c4d.SCROLLGROUP_VERT|c4d.SCROLLGROUP_AUTOVERT, 400, 225)  
        self.GroupBegin(self.MainGroupUI_ID, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 1, 0)

        self.GroupBorderSpace(0, 0, 0, 0) 

        # Add GUI Panels
        self.Select_N_Go_GUI_Panel()

        self.AddSeparatorH(201, flags=c4d.BFH_SCALEFIT)

        self.ProjectStorage_GUI_Panel()

        self.AddSeparatorH(201, flags=c4d.BFH_SCALEFIT)

        self.Nurbo_GUI_Panel()

        self.AddSeparatorH(201, flags=c4d.BFH_SCALEFIT)

        self.C4D_TO_MCX_GUI_Panel()

        self.GroupEnd()
        self.GroupEnd()

        self.FCS_OpenPluginAreas_GUI_Panel()
        return True
 #####  \\  UI Elements Functions \\ #####
    def GUI_Elements_Functions(self, id):
        self.GUI_FolderBTN_Add_PathDir(id, add_list=True, id_BTN=self.SNG_RF_List, dirbox_id=0)
        self.GUI_FolderBTN_Add_PathDir(id, add_list=True, id_BTN=self.C4D_MCXTex_List, dirbox_id=0)
        self.GUI_FolderBTN_Add_PathDir(id, add_list=True, id_BTN=self.PS4D_RF_List1, dirbox_id=0)
        self.FoldBar_BTN_Toggle(id, BTN=self.C4D2MCX_GRP["bar_id"], UI_Group=self.C4D2MCX_GRP["grp"], ToggleID=self.C4D2MCX_GRP["FT_id"])
        self.FoldBar_BTN_Toggle(id, BTN=self.MCX_Ids["TexPathsID"], UI_Group=self.MCX_Ids["TexPathGrpID"], ToggleID=self.MCX_Ids["TPStateID"])
        self.FoldBar_BTN_Toggle(id, BTN=self.SNG_GRP["bar_id"], UI_Group=self.SNG_GRP["grp"], ToggleID=self.SNG_GRP["FT_id"])
        self.FoldBar_BTN_Toggle(id, BTN=self.PS4D_GRP["bar_id"], UI_Group=self.PS4D_GRP["grp"], ToggleID=self.PS4D_GRP["FT_id"])
        self.FoldBar_BTN_Toggle(id, BTN=self.Nurbo_GRP["bar_id"], UI_Group=self.Nurbo_GRP["grp"], ToggleID=self.Nurbo_GRP["FT_id"])
        self.GUI_FolderBTN_Add_PathDir(id, add_list=False, id_BTN=self.MCX_Ids["dir_btn"], dirbox_id=self.MCX_Ids["dir"])
        self.GUI_FolderBTN_Add_PathDir(id, add_list=False, id_BTN=self.MCX_Ids["tex_btn"], dirbox_id=self.MCX_Ids["tex_dir"])
        self.GUI_FolderBTN_Add_PathDir(id, add_list=False, id_BTN=self.Main_Exp_Ids["dir_btn"], dirbox_id=self.Main_Exp_Ids["dir"])
        self.GUI_FolderBTN_Add_PathDir(id, add_list=False, id_BTN=self.BackupDir_Ids["dir_btn"], dirbox_id=self.BackupDir_Ids["dir"])
        self.Create_Prefs_Config_XML()
        return True
 #####  \\  Main C4D GUI Dialog Operations Functions which are Methods to Override for GeDialog class. \\   #####
    
    # UI Layout 
    def CreateLayout(self):
        self.SetTitle(self.DialogTitleName)   # <---| My Title for the MAIN Dialog
        self.GroupBegin(10477, c4d.BFH_SCALEFIT, 1, 0)
        Vect = c4d.Vector(0.125, 0.125, 0.125)
        self.SetDefaultColor(10477, c4d.COLOR_BG, Vect)

        self.GUI_DialogLayout()

        self.GroupEnd()         
        return True

    def InitValues(self):
        g_lib.fcsLog.DEBUG("FCS ToolKit Preferences Window is Open Now! From FCS ToolKit", False, g_lib.DebugMode)
        self.SetBool(self.BackupDir_Ids["chk"], False)
        self.SetBool(self.Main_Exp_Ids["chk"], False)
        self.SetBool(self.MCX_Ids["tex_chk"], False)
        L = [ self.Main_Exp_Ids["dir"], self.Main_Exp_Ids["dir_btn"] ]
        self.Enable_or_Disable(GUI_list=L, state_value=False)

        self.Loading_Prefs_Config_XML()

        self.Init_Hide_GUI_Elements()
        #self.SetLong(self.SNG_GRP["CB"], self.SNG_GRP["C2"])
        self.Add_Reload_Refresh_Update_GUIs()
        return True 

    # Excuting Commands for Functions
    def Command(self, id, msg):

        self.GUI_Elements_Functions(id)

        if id == self.SAVE_FCS_PREFS:
            self.Create_Prefs_Config_XML()            

        if id == self.FCS_STATION_LOG_BTN:
            g_lib.fcsEngine.RUN_FCS_STATION_LOGGER_APP()

        if id == self.FCS_PLUG_DIR_BTN:
            FolderName = g_lib.fcsDIR.plugin_database_folder
            self.OpenFolders(FolderName)            

        if id == self.FCS_PLUG_PREFS_DIR_BTN:
            FolderName = g_lib.fcsDIR.plugin_prefs_folder
            self.OpenFolders(FolderName)

        if id == self.BackupDir_Ids["open_btn"]:
            FolderName = self.GetString(self.BackupDir_Ids["dir"])
            if FolderName == "":
                gui.MessageDialog("No folder is added to the Backup edit box above the button.")
                return
            else:
                self.OpenFolders(FolderName)            

        if id == self.MCX_Ids["tex_chk"]:
            if self.GetBool(self.MCX_Ids["tex_chk"]) == True:
                L = [ self.MCX_Ids["tex_dir"], self.MCX_Ids["tex_btn"], self.MCX_Ids["TexPathGrpID"], self.MCX_Ids["TexPathsID"] ]
                self.Enable_or_Disable(GUI_list=L, state_value=True)
            else:
                L = [ self.MCX_Ids["tex_dir"], self.MCX_Ids["tex_btn"], self.MCX_Ids["TexPathGrpID"], self.MCX_Ids["TexPathsID"] ]
                self.Enable_or_Disable(GUI_list=L, state_value=False) 
            self.Create_Prefs_Config_XML()

        if id == self.Main_Exp_Ids["chk"]:

            if self.GetBool(self.Main_Exp_Ids["chk"]) == True:
                L = [ self.Main_Exp_Ids["dir"], self.Main_Exp_Ids["dir_btn"] ]
                self.Enable_or_Disable(GUI_list=L, state_value=True)
            else:
                #self.SetBool(self.Main_Exp_Ids["chk"], False)
                L = [ self.Main_Exp_Ids["dir"], self.Main_Exp_Ids["dir_btn"] ]
                self.Enable_or_Disable(GUI_list=L, state_value=False) 
            self.Create_Prefs_Config_XML() 

        if id == self.BackupDir_Ids["chk"]:

            if self.GetBool(self.BackupDir_Ids["chk"]) == True:
                L = [ self.BackupDir_Ids["dir"], self.BackupDir_Ids["dir_btn"], self.BackupDir_Ids["open_btn"] ]
                self.Enable_or_Disable(GUI_list=L, state_value=True)
            else:
                L = [ self.BackupDir_Ids["dir"], self.BackupDir_Ids["dir_btn"], self.BackupDir_Ids["open_btn"] ]
                self.Enable_or_Disable(GUI_list=L, state_value=False) 
            self.Create_Prefs_Config_XML()    

        return True

    def CoreMessage(self, id, msg):
        if id == c4d.EVMSG_CHANGE:
            self.Create_Prefs_Config_XML()
        return True

    # Auto Save User UI Settings.
    #def DestroyWindow(self):
        #self.Create_Prefs_Config_XML()
