"""
CollidePoints Tool Window GUI Dialog
"""
#  // Imports for Cinema 4D //
import c4d
from c4d import plugins, gui, bitmaps, documents, storage, utils
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
from random import randint
# Python Plugin System Load Modules Dynamically With importlib.
import importlib
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
#from res._fcs_lib.fcs_module_lib import fcs_common_global_lib as g_lib
fcsID = g_lib.fcsID
jsonEdit = g_lib.jsonEdit

class CollidePoints_Dialog(g_lib.BaseWindowDialogUI):
    """CollidePoints Tool Window GUI Dialog"""

    """
    Window Dialog Main Properties
    _________________________________________________
    """
    windowMainTileName = "CollidePoints"
    windowDialogWidthSize = 100
    windowDialogHeightSize = 100
    windowDialogPluginID = 1054054
    """
    GUI Elements ID's.
    _________________________________________________
    """
    IDS_VER_ID = 1000
    UI_OVERALL_GRP_ID = 1001
    UI_TXT_ID = 1002
    UI_PosX_BTN_ID = 1003
    UI_PosY_BTN_ID = 1004
    UI_PosZ_BTN_ID = 1005
    UI_NegX_BTN_ID = 1006
    UI_NegY_BTN_ID = 1007
    UI_NegZ_BTN_ID = 1008 
    UI_CAM_PROJECTION_BTN_ID = 1009
    UI_LINKBOX_LIST_ID = 1010

    def Projection_of_SelectedPoints(self, PointObj, axis, LinkCloneTargetObj):
        
        KeepCloneObj_Loc_MG = LinkCloneTargetObj.GetMg()
        KeepPntObj_Loc_MG = PointObj.GetMg()

        #if DebugMode == True:
        #    print( "Object Rotation : " + str(PointObj.GetAbsRot()) )

        if PointObj is None:
            g_lib.fcsLog.DEBUG( "Please Select one Point Based Object", True, g_lib.DebugMode )
            g_lib.c4d.gui.MessageDialog("Please Select one Point Based Object")
            return
        elif not PointObj.CheckType(c4d.Opoint):
            g_lib.fcsLog.DEBUG( "Please Select one Point Based Object", True, g_lib.DebugMode )
            return
        else:
            listy=[]
            maxEl = PointObj.GetPointCount()
            bs = PointObj.GetPointS()
            for index, selected in enumerate(bs.GetAll(maxEl)):
                if not selected: 
                    continue
                else:
                    listy.append(index)
                    # Get the current point corr...  of the object. 
                    get_vertex_pnt = g_lib.GetPointGlobal(PointObj, index)
                    g_lib.fcsLog.DEBUG( "Vertex Point Postion : " + str(get_vertex_pnt), False, g_lib.DebugMode)
                    """
                    Set Projection Temp Null Corr.. at Ray Start Point of Selected point
                    on the point object.
                    """
                    get_startpoint_pos = c4d.Vector(get_vertex_pnt.x, get_vertex_pnt.y, get_vertex_pnt.z)
                    """
                    Start projection rayCollide sys to model and set projection TargetPoint location by axis.
                    """
                    new_target_pos = g_lib.CollidePointToObject( axis, get_startpoint_pos, LinkCloneTargetObj )
                    if new_target_pos:  
                        """
                        Now Get New TargetPoint Location and Set PointObj Point to New TargetPoint.
                        """
                        g_lib.fcsLog.DEBUG( "Target New Currrent Point : " + str(new_target_pos), False, g_lib.DebugMode )
                        PointObj.SetPoint(index, c4d.Vector(new_target_pos))
                        """
                        Now get point object vertex point again and get from location object
                        and Cal... the point from the point_object by loc target obj.
                        """      
                        get_current_pnt = g_lib.GetPointGlobal(PointObj, index)
                        new_pntX = get_current_pnt.x - new_target_pos.x - new_target_pos.x
                        new_pntY = get_current_pnt.y - new_target_pos.y - new_target_pos.y
                        new_pntZ = get_current_pnt.z - new_target_pos.z - new_target_pos.z
                        """
                        Get point object and set the selected point to the target point location.
                        """
                        VertexNewPos = c4d.Vector(-new_pntX, -new_pntY, -new_pntZ)
                        PointObj.SetPoint(index, VertexNewPos)
                        get_vertex_pnt2 = g_lib.GetPointGlobal(PointObj, index)
                        g_lib.fcsLog.DEBUG( "Vertex New Point Postion : " + str(get_vertex_pnt2), False, g_lib.DebugMode ) 
                    PointObj.Message(c4d.MSG_UPDATE) 
            return listy
    
        PointObj.SetMg(KeepPntObj_Loc_MG)
        c4d.EventAdd()
        return True

    def ReEnable_CollisonObjectsList(self):
        """ Get and Merge Collision Objects as one object """
        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False        
        LinkList =  self.FindCustomGui(self.UI_LINKBOX_LIST_ID, c4d.CUSTOMGUI_INEXCLUDE_LIST).GetData()
        p = LinkList.GetObjectCount()
        if p:
            for i in xrange(p):
                collison_obj = LinkList.ObjectFromIndex(doc, i).GetName()
                get_temp = doc.SearchObject(collison_obj)
                get_temp[c4d.ID_BASEOBJECT_VISIBILITY_RENDER] = 2
                get_temp[c4d.ID_BASEOBJECT_VISIBILITY_EDITOR] = 2          
        return True    

    def Get_CollisonObjectsList(self):
        """ Get and Merge Collision Objects as one object """
        LinkList =  self.FindCustomGui(self.UI_LINKBOX_LIST_ID, c4d.CUSTOMGUI_INEXCLUDE_LIST).GetData()
        p = LinkList.GetObjectCount()
        if p:
            """
            Make a null group of the collison objects.
            """
            doc = c4d.documents.GetActiveDocument()
            CreateGroup = c4d.BaseObject(c4d.Onull)
            CreateGroup.SetName("cp_targets_collisons")
            doc.InsertObject(CreateGroup)
            for i in xrange(p):
                collison_obj = LinkList.ObjectFromIndex(doc, i).GetName()
                get_temp = doc.SearchObject(collison_obj)
                get_temp[c4d.ID_BASEOBJECT_VISIBILITY_RENDER] = 1
                get_temp[c4d.ID_BASEOBJECT_VISIBILITY_EDITOR] = 1
                cloneobject = get_temp.GetClone(c4d.COPYFLAGS_0)
                cloneobject.InsertUnder(CreateGroup)
                g_lib.ObjectToCurrentState(cloneobject)
            merge_grp = doc.SearchObject("cp_targets_collisons")
            merge_o = doc.SetActiveObject(merge_grp)
            c4d.CallCommand(1036583) # F.C.S - Select + Connect + Delete
            new_merge_grp = doc.GetActiveObject()
            new_merge_grp.SetName("cp_targets_collisons")
            g_lib.Convert_Object_Axis_To_Zero(new_merge_grp)
            g_lib.fcsLog.DEBUG( "collisons are ready", False, g_lib.DebugMode )
        else:
            gui.MessageDialog("Please add an collsion object!")
            return             
        return True

    def main_function(self, axis):
        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False
        # Get Objects form the Object Manager     
        list_objs = doc.GetActiveObjects(1)
        if not list_objs:
            g_lib.fcsLog.DEBUG( "Please select an Object!", True, g_lib.DebugMode )
            return
        
        self.Get_CollisonObjectsList()
        get_merge_obj = doc.SearchObject("cp_targets_collisons")
        if get_merge_obj:
            for obj in list_objs:
                self.Projection_of_SelectedPoints(PointObj=obj, axis=axis, LinkCloneTargetObj=get_merge_obj)

        #Re-Selection of objects the user as selected before.   
        for selobj in list_objs:
            doc.SetActiveObject( selobj, c4d.SELECTION_ADD)        
        get_merge_obj.Remove()
        self.ReEnable_CollisonObjectsList()
        #c4d.StopAllThreads()
        c4d.EventAdd()  
        return True

    def PostiveGroup_GUI_Layout(self):
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 3, 0, "")

        menu_btn_set = {'id':self.UI_PosX_BTN_ID, 
                        'btn_look':c4d.BORDER_NONE,
                        'tip':"<b>Postive X Axis</b>", 
                        'icon':os.path.join(g_lib.fcsDIR.plugin_ui_icons_folder, "tool_cp_+x_icon.png"), 
                        'clickable':True}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=menu_btn_set)

        menu_btn_set = {'id':self.UI_PosY_BTN_ID, 
                        'btn_look':c4d.BORDER_NONE,
                        'tip':"<b>Postive Y Axis</b>", 
                        'icon':os.path.join(g_lib.fcsDIR.plugin_ui_icons_folder, "tool_cp_+y_icon.png"), 
                        'clickable':True}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=menu_btn_set)
        
        menu_btn_set = {'id':self.UI_PosZ_BTN_ID, 
                        'btn_look':c4d.BORDER_NONE,
                        'tip':"<b>Postive Z Axis</b>", 
                        'icon':os.path.join(g_lib.fcsDIR.plugin_ui_icons_folder, "tool_cp_+z_icon.png"), 
                        'clickable':True}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=menu_btn_set)

        self.GroupEnd()
        return True

    def NegtiveGroup_GUI_Layout(self):
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 3, 0, "")
        
        self.GroupBegin(0, c4d.BFH_MASK, 1, 0, "")
        menu_btn_set = {'id':self.UI_NegX_BTN_ID, 
                        'btn_look':c4d.BORDER_NONE,
                        'tip':"<b>Menu</b>", 
                        'icon':os.path.join(g_lib.fcsDIR.plugin_ui_icons_folder, "tool_cp_-x_icon.png"), 
                        'clickable':True}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=menu_btn_set)         
        self.GroupEnd() 

        self.GroupBegin(0, c4d.BFH_MASK, 1, 0, "")
        menu_btn_set = {'id':self.UI_NegY_BTN_ID, 
                        'btn_look':c4d.BORDER_NONE,
                        'tip':"<b>Menu</b>", 
                        'icon':os.path.join(g_lib.fcsDIR.plugin_ui_icons_folder, "tool_cp_-y_icon.png"), 
                        'clickable':True}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=menu_btn_set)         
        self.GroupEnd() 
        
        self.GroupBegin(0, c4d.BFH_MASK, 1, 0, "")
        menu_btn_set = {'id':self.UI_NegZ_BTN_ID, 
                        'btn_look':c4d.BORDER_NONE,
                        'tip':"<b>Menu</b>", 
                        'icon':os.path.join(g_lib.fcsDIR.plugin_ui_icons_folder, "tool_cp_-z_icon.png"), 
                        'clickable':True}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=menu_btn_set)         
        self.GroupEnd() 

        self.GroupEnd()
        return True    

    def CamGroup_GUI_Layout(self):
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 3, 0, "")
        
        menu_btn_set = {'id':self.UI_CAM_PROJECTION_BTN_ID, 
                        'btn_look':c4d.BORDER_NONE,
                        'tip':"<b>Camera Projection</b>", 
                        'icon':os.path.join(g_lib.fcsDIR.plugin_ui_icons_folder, "tool_cp_cam_icon.png"), 
                        'clickable':True}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=menu_btn_set)         

        self.GroupEnd()
        return True      

    def LinkGroup_GUI_Layout(self):
        self.GroupBegin(0, c4d.BFH_MASK, 1, 0, "")
        self.AddStaticText(self.UI_TXT_ID, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 250, 0, "Collison Objects :", c4d.BORDER_WITH_TITLE_BOLD)
        #First create a container that will hold the items we will allow to be dropped into the INEXCLUDE_LIST gizmo
        acceptedObjs = c4d.BaseContainer()
        acceptedObjs.InsData(c4d.Obase, "")   # Accept all objects (change as desired)
        #Create another container for the INEXCLUDE_LIST gizmo's settings and add the above container to it
        bc_IEsettings = c4d.BaseContainer()
        bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_INIT_STATE, 1)
        bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_SEND_SELCHANGE_MSG, True)
        """
        bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_NUM_FLAGS, 2)
        bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_IMAGE_01_ON, 1039241)
        bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_IMAGE_01_OFF, 1039241)
        bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_IMAGE_02_ON, 1039801)
        bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_IMAGE_02_OFF, 1036394)
        """
        bc_IEsettings.SetData(c4d.DESC_ACCEPT, acceptedObjs)        
        self.LinkList = self.AddCustomGui(self.UI_LINKBOX_LIST_ID, c4d.CUSTOMGUI_INEXCLUDE_LIST, "Hi", c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 250, 80, bc_IEsettings)
        #self.LinkList.SetLayoutMode(c4d.LAYOUTMODE_MAXIMIZED)
        self.GroupEnd()    
        return True
    
    """
    Main GeDialog Overrides 
    """
    def __init__(self, global_strings):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """    
        super(CollidePoints_Dialog, self).__init__()
        self.IDS = global_strings

    def BuildUI(self):
        # Top Menu addinng Tool Version
        self.GroupBeginInMenuLine()
        self.AddStaticText(self.IDS_VER_ID, 0)
        self.SetString(self.IDS_VER_ID, " v1.0  ")
        self.GroupEnd()
        self.GroupBegin(self.UI_OVERALL_GRP_ID, c4d.BFH_SCALEFIT, 2, 0, "")
        self.GroupBegin(0, c4d.BFH_MASK, 1, 0, "")  
        self.GroupBorderSpace(2, 2, 2, 2)    
        self.PostiveGroup_GUI_Layout()
        self.NegtiveGroup_GUI_Layout()
        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)
        self.CamGroup_GUI_Layout()
        self.GroupEnd()
        self.LinkGroup_GUI_Layout()
        self.GroupEnd()
        return super(CollidePoints_Dialog, self).BuildUI()

    def UIsettings(self):
        self.SetDefaultColor(self.UI_OVERALL_GRP_ID, c4d.COLOR_BG, g_lib.BG_DARK)
        self.SetDefaultColor(self.UI_TXT_ID, c4d.COLOR_TEXT, g_lib.DARK_BLUE_TEXT_COL) 
        self.SetDefaultColor(self.IDS_VER_ID, c4d.COLOR_TEXT, g_lib.DARK_BLUE_TEXT_COL)  
        return super(CollidePoints_Dialog, self).UIsettings()

    def UIfunctions(self, id):
        if (id == self.UI_PosX_BTN_ID):
            self.main_function(axis="+X")

        if (id == self.UI_PosY_BTN_ID):
            self.main_function(axis="+Y")
            
        if (id == self.UI_PosZ_BTN_ID):
            self.main_function(axis="+Z")            

        if (id == self.UI_NegX_BTN_ID):
            self.main_function(axis="-X")

        if (id == self.UI_NegY_BTN_ID):
            self.main_function(axis="-Y")
            
        if (id == self.UI_NegZ_BTN_ID):
            self.main_function(axis="-Z")
        return super(CollidePoints_Dialog, self).UIfunctions(id)
