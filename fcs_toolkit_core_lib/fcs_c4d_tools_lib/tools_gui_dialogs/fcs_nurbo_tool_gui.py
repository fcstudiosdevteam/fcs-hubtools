"""
Nurbo Sweep Generator Tool Window GUI Dialog
"""
#  // Imports for Cinema 4D //
import c4d
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
import weakref
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as WindowDialog        # eg:( class My_Dialog(c4d.gui.GeDialog): ) to ( class My_Dialog(WindowDialog): ) Just keeping code line short.
from c4d.plugins import CommandData, TagData, ObjectData
from random import randint
# XML Imports is for Saving and Loading Project Data for Plugin UI / Xml good but long functions to make.
from xml.dom import minidom
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree
# Python Plugin System Load Modules Dynamically With importlib.
import importlib
# Date System
DateT = str(datetime.date.today())
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
#from res._fcs_lib.fcs_module_lib import fcs_common_global_lib as g_lib
fcsID = g_lib.fcsID
jsonEdit = g_lib.jsonEdit

class NurboGenerator_Dialog(g_lib.BaseWindowDialogUI):
    """ 
    Nurbo Generator Tool Dialog

    // ID Numbers for UI Elements Properties & Enums IDs //
        ID's for Dialog User Interface, UI Buttons Elements.
        Check Buttons, and for Coding (self.ID - this is When the ID is in a Class).
        and most of the UI ID's  are in in the GlobalEnumsTypesLib that is in c4d_symbols.h file.
    """
    windowMainTileName = "Nurbo"
    windowDialogWidthSize = 200
    windowDialogHeightSize = 200
    windowDialogPluginID = 1038594

    VerData = " v3.0 "
    ID_VER = 178

    ID_NURBO_SWEEP_PRESET_BTN = 105
    ID_NURBO_SWEEP_SETTINGS_BTN = 106
    ID_NURBO_SWEEP_ANGLE_BTN = 107
    ID_NURBO_SWEEP_FIXROT_BTN = 108

    SweepGeneratorID = 109
    TaxiLineGeneratorID = 110

    USERSHAPE_TAG_ID = 1040522
    USERPATH_TAG_ID = 1040523

    # Nurbo Random Replacer Id's
    AddSlot = 102
    GroupSelected_CHK = 119
    webpageBTN = 120 
    AboutBTN = 121
    ItemType_EB = 122
    ItemType_BTN = 123 
    ItemType_BTN2 = 124   
    ItemType_Info = 125
    ItemType_COL = 126
    ItemSelCol_CHK = 127
    ToolTitle = 128
    ItemSel_COMBOBOX = 130
    preset_EB = 132
    SlotLayoutGroup = 103    
    TOP_MENU_IDs = { 'demo1_id':167, 'demo2_id':168, 'demo3_id':169, 'onlineM':170, 'offlineM':172, 'discord_id':173 }
    SlotsPanelsLayoutGroup = 101
    AddSlots_GrpBTN = 117
    ClearListBTN = 118
    addpreset_BTN = 131
    Settings_Menu = 129
    UtilsPanel = {'toggle_btn':150, 'insert_id':151, 'grp_id':152, 't_state_id':153}  
    PresetPanel = {'toggle_btn':154, 'insert_id':155, 'grp_id':156, 't_state_id':157}
    TagPanel = {'toggle_btn':158, 'insert_id':159, 'grp_id':160, 't_state_id':161}    
    SelModePanel = {'toggle_btn':162, 'insert_id':163, 'grp_id':164, 't_state_id':165}
    AddSlot_preset_grp = 166



    def __init__(self, global_strings):
        self.IDS = global_strings
        self.Osplines = g_lib.C4DSplineShapesObjects(global_strings)
        #self.Nurbojsonfile = g_lib.fcsDIR.FCS_NURBO_CONIG

  ##### UI Window Dialog #####
    # ------------------------------------------
    #    UI Helpers Methods Of NurboSweep 
    # ------------------------------------------
    # Create a Bar Title for UI.
    def Add_QuickTab_Bar(self, BarId, BarName, ColorMode, AddFold, FoldToggle, Fold_Id):
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 0, 1, "")
        self.GroupSpace(0, 0)        
        self.CustomGui_UI = c4d.BaseContainer() 
        self.CustomGui_UI.FlushAll()
        self.CustomGui_UI.SetBool(c4d.QUICKTAB_BAR, True)
        self.CustomGui_UI.SetString(c4d.QUICKTAB_BARTITLE, BarName)
        if AddFold == True:
            self.CustomGui_UI.SetBool(c4d.QUICKTAB_BARSUBGROUP, True)
        if ColorMode == "Color":
            self.CustomGui_UI[c4d.QUICKTAB_BGCOLOR] = c4d.COLOR_DBARBG1 # c4d.Vector(0, 0, 0)           
        if ColorMode == "RandomColors":
            self.CustomGui_UI[c4d.QUICKTAB_BGCOLOR] = g_lib.Add_Vector_Random_Colors() 
        self._add_ui_bar = self.AddCustomGui(BarId, c4d.CUSTOMGUI_QUICKTAB, BarName, c4d.BFH_SCALEFIT, 150, 0, self.CustomGui_UI)
        self._add_ui_bar.Select(0, True) # Set the Fold Handle to Open or Close.
        if FoldToggle == True:
            # To Help with Toggle Mode of Open and Closing the Bar Grouos.path. / Its just a dummy to help with Toggling.
            # I have to StaticText set 1 , so u can not see it.
            self.AddStaticText(Fold_Id, c4d.BFH_MASK, 1, 1, "c")
        self.GroupEnd()
        return True 
    # Create a Bitmap Button Custom GUI.
    def Add_Image_Button(self, BTN_Id, Border, AddTip, Info, Image, Toggle, Click, BC):
        CustomGui_UI = c4d.BaseContainer()                            # Create a new container to store the button image.
        CustomGui_UI.SetFilename(BTN_Id, Image)                       # Add this location info to the conatiner
        CustomGui_UI.SetLong(c4d.BITMAPBUTTON_BORDER, Border)         # Sets the border flag to look like a button look. eg.( c4d.BORDER_NONE or c4d.BORDER_THIN_OUT )
        if Click == "Yes":
            CustomGui_UI.SetBool(c4d.BITMAPBUTTON_BUTTON, True)       # Clickable button.
        if Toggle == "Yes":
            CustomGui_UI.SetBool(c4d.BITMAPBUTTON_TOGGLE, True)       # Toggle button, like a checkbox.
        if AddTip == "Yes":
            CustomGui_UI.SetString(c4d.BITMAPBUTTON_TOOLTIP, Info)    # Add tooltios.path. eg.[self.SetString(c4d.BITMAPBUTTON_TOOLTIP, "<b>Bold Text</b><br>New line")]
        if BC == "Yes":
            CustomGui_UI.SetInt32(c4d.BITMAPBUTTON_BACKCOLOR, c4d.COLOR_DBARBG1) #  c4d.COLOR_TEXTFOCUS  int - Background color.
        self.SetBTN = self.AddCustomGui(BTN_Id, c4d.CUSTOMGUI_BITMAPBUTTON, "", c4d.BFH_MASK, 0, 0, CustomGui_UI)
        self.SetBTN.SetImage(Image, True)
        return True
    # Create a Title Bar with Custom Icon GUI. 
    def Add_Bar_Icon(self, bar_id, i_id, Name, Color, fold, tip, info, icon, i_bc, T_id):
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 2, 0, "")
        self.GroupSpace(0, 0)
        self.Add_QuickTab_Bar(BarId=bar_id, BarName=Name, ColorMode=Color, AddFold=fold, FoldToggle=True, Fold_Id=T_id)
        self.Add_Image_Button(BTN_Id=i_id, Border=c4d.BORDER_NONE, AddTip=tip, Info=info, Image=icon, Toggle="No", Click="Yes", BC=i_bc)
        self.GroupEnd()        
        return True          
    # To a custom bar gui
    def Add_TileCustomBarGUI(self, tile_str, ui_id):
        # BaseContainer for Ui Elements
        self.custom_ui_bc = c4d.BaseContainer()         
        self.custom_ui_bc.SetBool(c4d.QUICKTAB_BAR, True)
        self.custom_ui_bc.SetString(c4d.QUICKTAB_BARTITLE, tile_str)
        self.AddCustomGui(ui_id, c4d.CUSTOMGUI_QUICKTAB, "", c4d.BFH_SCALEFIT, 290, 0, self.custom_ui_bc)
        return True 
    # To Inject UI layout to a empty group.
    def Inject_GUI_Title(self, injected_group_id, str_title):
        """ Inject your UI layout or GUI element to a empty group. """
        self.LayoutFlushGroup(injected_group_id) # Refresh Group UI
        self.Add_TileCustomBarGUI(tile_str=str_title, ui_id=0)
        self.LayoutChanged(injected_group_id) # Update Group UI
        return True
    def Add_and_Inject_GUIs(self):

        g_lib.Inject_Image_GUI(self, fcsID.ID_NURBO_BANNER_GRP, g_lib.fcsDIR.I(i='nurbologo_banner'), 200, 45, "")
        #print(fcsDIR.I(i='nurbologo_banner'))

        self.Inject_GUI_Title(injected_group_id=fcsID.ID_NURBO_SWEEP_GEN_TYPE_TITLE, str_title=self.IDS.ID(fcsID.ID_NURBO_SWEEP_GEN_TYPE_TITLE))

        self.Inject_GUI_Title(injected_group_id=fcsID.ID_NURBO_SWEEP_MODE_TITLE_GRP, str_title=self.IDS.ID(fcsID.IDS_SWEEP_MODES))

        self.Inject_GUI_Title(injected_group_id=fcsID.ID_NURBO_SWEEP_CUSTOM_NAME_TITLE_GRP, str_title=self.IDS.ID(fcsID.IDS_ADD_CUSTOM_NAME))
        
        self.Inject_GUI_Title(injected_group_id=fcsID.ID_NURBO_SWEEP_ADDGRP_TITLE, str_title=self.IDS.ID(fcsID.IDS_ADD_TO_GROUP))

        self.Inject_GUI_Title(injected_group_id=fcsID.ID_NURBO_SWEEP_SPLINE_SHAPES_TITLE_GRP, str_title=self.IDS.ID(fcsID.IDS_SPLINES_SHAPES_TEMP))

        presetIcon_set = {'id':self.ID_NURBO_SWEEP_PRESET_BTN, 'btn_look':c4d.BORDER_NONE, 'tip':self.IDS.ID(fcsID.IDS_NURBO_TIP1), 'icon':g_lib.fcsDIR.I(i='nurbopreset_icon'), 'clickable':True}
        g_lib.Inject_GUI_Icon(func_instance=self, injected_group_id=fcsID.ID_NURBO_SWEEP_PRESET_BTN_GRP, data=presetIcon_set)

        sweepsetIcon_set = {'id':self.ID_NURBO_SWEEP_SETTINGS_BTN, 'btn_look':c4d.BORDER_NONE, 'tip':self.IDS.ID(fcsID.IDS_NURBO_TIP2), 'icon':g_lib.fcsDIR.I(i='nurbosweepset_icon'), 'clickable':True}
        g_lib.Inject_GUI_Icon(func_instance=self, injected_group_id=fcsID.ID_NURBO_SWEEP_SETTINGS_BTN_GRP, data=sweepsetIcon_set)

        AngleSetIcon_set = {'id':self.ID_NURBO_SWEEP_ANGLE_BTN, 'btn_look':c4d.BORDER_NONE, 'tip':self.IDS.ID(fcsID.IDS_NURBO_TIP3), 'icon':g_lib.fcsDIR.I(i='nurboangleset_icon'), 'clickable':True}
        g_lib.Inject_GUI_Icon(func_instance=self, injected_group_id=fcsID.ID_NURBO_SWEEP_ANGLE_BTN_GRP, data=AngleSetIcon_set)  

        cutIcon_set = {'id':self.ID_NURBO_SWEEP_FIXROT_BTN, 'btn_look':c4d.BORDER_NONE, 'tip':self.IDS.ID(fcsID.IDS_NURBO_TIP4), 'icon':g_lib.fcsDIR.I(i='nurbocut_icon'), 'clickable':True}
        g_lib.Inject_GUI_Icon(func_instance=self, injected_group_id=fcsID.ID_NURBO_SWEEP_FIXROT_BTN_GRP, data=cutIcon_set)         
        #
        return True
    
    # ----------------------------------
    #       Core Of NurboSweep 
    # ----------------------------------
    # Add To Group
    def AddingToCustomGroup(self, doc, obj):
        """ If group checked, then add object to group. """
        if self.GetBool(fcsID.ID_NURBO_SWEEP_ADDGRP_CHK)==True:
            group = doc.SearchObject(self.GetString(fcsID.ID_NURBO_SWEEP_ADDGRP_EDIT))
            obj.InsertUnder(group)
        return True

    # SweepObject Core
    def SweepObjGenerator(self):
        """ Add Create Custom Sweep Object """
        doc = c4d.documents.GetActiveDocument()
        doc.StartUndo()
        # Add Sweep Oject to C4d Scene and Object Manger with custom path and sweep settings.
        SweepGen = c4d.BaseObject(c4d.Osweep)
        doc.AddUndo(c4d.UNDOTYPE_CHANGE, SweepGen)
        # Add Colors
        SweepGen[c4d.ID_BASEOBJECT_USECOLOR] = 1
        SweepGen[c4d.ID_BASEOBJECT_COLOR] = g_lib.Add_Vector_Random_Colors()
        SweepGen[c4d.SWEEPOBJECT_BANKING] = jsonEdit.GetItemProperties(self.Nurbojsonfile, "SweepSettings", 'Banking')
        SweepGen[c4d.SWEEPOBJECT_RAILDIRECTION] = jsonEdit.GetItemProperties(self.Nurbojsonfile, "SweepSettings", 'Use Rail Direction')
        SweepGen[c4d.SWEEPOBJECT_RAILSCALE] = jsonEdit.GetItemProperties(self.Nurbojsonfile, "SweepSettings", 'Use Rail Scale')
        SweepGen[c4d.SWEEPOBJECT_CONSTANT] = jsonEdit.GetItemProperties(self.Nurbojsonfile, "SweepSettings", 'Constraint Cross Section')
        SweepGen[c4d.SWEEPOBJECT_BIRAIL] = jsonEdit.GetItemProperties(self.Nurbojsonfile, "SweepSettings", '2-Rail')
        SweepGen[c4d.SWEEPOBJECT_KEEPSEGMENTS] = jsonEdit.GetItemProperties(self.Nurbojsonfile, "SweepSettings", 'Keep Segements')
        SweepGen[c4d.SWEEPOBJECT_GROWUV] = jsonEdit.GetItemProperties(self.Nurbojsonfile, "SweepSettings", 'Stick UVs')
        SweepGen[c4d.SWEEPOBJECT_PARALLEL] = jsonEdit.GetItemProperties(self.Nurbojsonfile, "SweepSettings", 'Parallel Movement')
        SweepGen[c4d.SWEEPOBJECT_FLIPNORMALS] = jsonEdit.GetItemProperties(self.Nurbojsonfile, "SweepSettings", 'Flip Normals')
        SweepGen[c4d.CAP_START] = 0
        SweepGen[c4d.CAP_END] = 0
        SweepGen[c4d.CAP_CONNECT] = jsonEdit.GetItemProperties(self.Nurbojsonfile, "SweepSettings", 'CreateSingleObject')
        #SweepGenerator[c4d.SWEEPOBJECT_ROTATE] = 0.524

        # Add Sweep Object Settings.
        doc.AddUndo(c4d.UNDOTYPE_CHANGE, SweepGen)
        # Set Name of the SweepGenerator And checking if Name and Degit is already added to the object manager.
        if self.GetBool(fcsID.ID_NURBO_SWEEP_CUSTOM_NAME_CHK) == True:
            # The User Custom Name
            GetEditName = self.GetString(fcsID.ID_NURBO_SWEEP_CUSTOM_NAME_EDITBOX)
            #LogFile = PLUGIN_DIR().FCS_LOGGING_CONIG
            #fcsLog.DEBUG(LogFile, str(GetEditName))
        else:
            GetEditName = "Sweep"

        g_lib.SetObjectName(GetEditName, SweepGen, 100)

        doc.AddUndo(c4d.UNDOTYPE_NEW, SweepGen)
        # Add Phong Tag to Sweep Oject if Checked.
        tag = SweepGen.MakeTag(c4d.Tphong)
        doc.AddUndo(c4d.UNDOTYPE_CHANGE, tag)
        tag[c4d.PHONGTAG_PHONG_ANGLELIMIT]=True
        tag[c4d.PHONGTAG_PHONG_USEEDGES]=True
        doc.AddUndo(c4d.UNDOTYPE_NEW, tag)

        return SweepGen

    # NurboSweep Core Generate Run Mode!
    def NurboSweepCoreGenerator(self):
        self.Nurbojsonfile = g_lib.fcsDIR.FCS_NURBO_CONIG

        """
        Nurbro Sweep Nested Helpers
        _________________________________________
        """
        # Nurbo Core
        def NurboSweep_CoreGenerator(c4d_objType, AddTemp):

            doc = c4d.documents.GetActiveDocument()

            doc.StartUndo()

            objs = doc.GetActiveObjects(1)
                
            if not objs:
                g_lib.fcsLog.WARNING("You must select an editable spline!", True, g_lib.DebugMode)
                return

            for i in objs:

                invaild_items = [ c4d.Onull, c4d.Opolygon, c4d.Osweep, c4d.Ocube, c4d.Oconnector, c4d.Ocylinder, c4d.Ofloor, c4d.Osymmetry, c4d.Otorus, c4d.Otube, c4d.Opluginpolygon]
                for bad_items in invaild_items:
                    if i.CheckType(bad_items):
                        g_lib.fcsLog.WARNING("You need to select an editable spline object!", True, g_lib.DebugMode)
                        return

                if i.CheckType(c4d.Ospline):
                    invaild_Tag = i.GetTag(self.USERPATH_TAG_ID)
                    if invaild_Tag:
                        doc.SetActiveObject(i, c4d.SELECTION_SUB)
                        pass
                    else:
                        doc.SetActiveObject(i, c4d.SELECTION_ADD)
                        c4d.EventAdd()

            sel_objs = doc.GetSelection()

            for i in sel_objs:

                c4d.CallCommand(12298, 12298) # Model

                # | User Custom Path  | ----------------------------------------------- #
                """ Add the Nurbo Path Tag. """
                doc.AddUndo(c4d.UNDOTYPE_CHANGE, i)
                i.MakeTag(self.USERPATH_TAG_ID)
                g_lib.SetObjectName("User-Custom-PathGU", i, 100)
                # --------------------------------------------------------------------- #

                # | Add Create Custom Sweep Object | --------------------------------- #
                """ Add Sweep Oject to C4d Scene and Object Manger with custom path 
                    and sweep settings. """
                SweepGenerator = self.SweepObjGenerator()
                # --------------------------------------------------------------------- #

                # | User Custom Shape  | ---------------------------------------------- #
                """ Get and Add Custom Shape Spline Template """
                if AddTemp == True:
                    # Generate the User Splines Shape with Default Preset.
                    CustomShape = c4d.BaseObject(c4d_objType)
                    doc.AddUndo(c4d.UNDOTYPE_CHANGE, CustomShape)
                else:
                    # Generate the User Custom Splines Shape with Custom.
                    CustomShape = c4d_objType.GetClone(c4d.COPYFLAGS_0)
                    doc.AddUndo(c4d.UNDOTYPE_CHANGE, CustomShape)
                """ Add the Nurbo Shape Tag. """
                CustomShape.MakeTag(self.USERSHAPE_TAG_ID)
                doc.AddUndo(c4d.UNDOTYPE_CHANGE, CustomShape)
                g_lib.SetObjectName("CustomSplineShape", i, 100)
                # --------------------------------------------------------------------- #

                # | Insert Objects | -------------------------------------------------- #
                """ Insert Custom Sweep Object """
                doc.InsertObject(SweepGenerator)
                """ Insert Custom Shape Spline Object """
                doc.InsertObject(CustomShape)
                """ Insert Under Custom Path Object """
                i.InsertUnder(SweepGenerator)
                """ Insert Under Custom Shape Spline Object """
                CustomShape.InsertUnder(SweepGenerator)
                # --------------------------------------------------------------------- #

                doc.SetActiveObject(SweepGenerator)

                # | Set Objects in Order inside Object Manager. | --------------------- #
                self.AddingToCustomGroup(doc=doc, obj=SweepGenerator)

                # Add Undo System
                doc.AddUndo(c4d.UNDOTYPE_NEW, SweepGenerator)
                doc.AddUndo(c4d.UNDOTYPE_NEW, CustomShape) 
                doc.AddUndo(c4d.UNDOTYPE_NEW, i)

                #else:
                    #gui.MessageDialog("Attention!\nYou need to link or drag a editable spline object\ninto the custom spline template slot please.")
                    #return
            doc.EndUndo()
            c4d.CallCommand(100004767, 100004767) # Deselect All
            c4d.EventAdd()
            return True
        
        # Generate by the User Custom Sweep Mode.
        def Generate_With_Template(c4d_objType, AddTemp):
            """ Generate by the User Custom Sweep Selection Mode eg:(Splines, Edges or Geo Path) """
            self.Nurbojsonfile = g_lib.fcsDIR.FCS_NURBO_CONIG
            if self.GetBool(fcsID.ID_NURBO_SWEEP_MODE_CHK_SPINES)==True:
                    NurboSweep_CoreGenerator(c4d_objType, AddTemp)

            if self.GetBool(fcsID.ID_NURBO_SWEEP_MODE_CHK_POLYEDGES)==True:
                doc = c4d.documents.GetActiveDocument()
                objs = doc.GetActiveObject()
                if not objs:
                    g_lib.fcsLog.WARNING("You must select an object.", True)
                    return False
                doc.StartUndo()
                clone = objs.GetClone(c4d.COPYFLAGS_0)
                doc.AddUndo(c4d.UNDOTYPE_CHANGE, clone)
                clone.SetName("Temp404")
                doc.AddUndo(c4d.UNDOTYPE_CHANGE, clone)
                doc.AddUndo(c4d.UNDOTYPE_NEW, clone)
                doc.InsertObject(clone)
                doc.SetActiveObject(clone)
                doc.AddUndo(c4d.UNDOTYPE_NEW, clone)
                c4d.CallCommand(16351) # Edges    
                c4d.CallCommand(1009671, 1009671) # Edge to Spline
                c4d.CallCommand(1019951, 1019951) # Delete Without Children
                doc.AddUndo(c4d.UNDOTYPE_NEW, clone)
                f = doc.SearchObject("Temp404.Spline")
                doc.AddUndo(c4d.UNDOTYPE_CHANGE, f)
                doc.SetActiveObject(f)
                doc.EndUndo()          
                NurboSweep_CoreGenerator(c4d_objType, AddTemp)
                doc.AddUndo(c4d.UNDOTYPE_NEW, clone)
                doc.AddUndo(c4d.UNDOTYPE_NEW, f)             
                doc.EndUndo()
                c4d.EventAdd()
                #c4d.CallCommand(1019940) # Reset PSR
                #c4d.CallCommand(1021385) # Scale
                #c4d.CallCommand(200000089) # Scale
            return True

        # Nurbo Taxiline Object Core
        def Generate_Taxiline():

            doc = c4d.documents.GetActiveDocument()

            doc.StartUndo()

            objs = doc.GetActiveObjects(1)
                
            if not objs:
                g_lib.fcsLog.WARNING("You must select an editable spline!", True, g_lib.DebugMode)
                return

            for i in objs:

                invaild_items = [ c4d.Onull, c4d.Opolygon, c4d.Osweep, c4d.Ocube, c4d.Oconnector, c4d.Ocylinder, c4d.Ofloor, c4d.Osymmetry, c4d.Otorus, c4d.Otube, c4d.Opluginpolygon]
                for bad_items in invaild_items:
                    if i.CheckType(bad_items):
                        g_lib.fcsLog.WARNING("You need to select an editable spline object!", True, g_lib.DebugMode)
                        return

                if i.CheckType(c4d.Ospline):
                    invaild_Tag = i.GetTag(self.USERPATH_TAG_ID)
                    if invaild_Tag:
                        doc.SetActiveObject(i, c4d.SELECTION_SUB)
                        pass
                    else:
                        doc.SetActiveObject(i, c4d.SELECTION_ADD)
                        c4d.EventAdd()

            sel_objs = doc.GetSelection()
            for i in sel_objs:

                c4d.CallCommand(12298, 12298) # Model

                # | User Custom Path  | ----------------------------------------------- #
                """ Add the Nurbo Path Tag. """
                doc.AddUndo(c4d.UNDOTYPE_CHANGE, i)
                i.MakeTag(self.USERPATH_TAG_ID)
                g_lib.SetObjectName("User-Custom-PathGU", i, 100)
                # --------------------------------------------------------------------- #

                # | Set Name  | ----------------------------------------------- #
                TaxilineGen = c4d.BaseObject(1053591)

                doc.AddUndo(c4d.UNDOTYPE_CHANGE, TaxilineGen)

                if self.GetBool(fcsID.ID_NURBO_SWEEP_CUSTOM_NAME_CHK) == True:
                    # The User Custom Name
                    GetEditName = self.GetString(fcsID.ID_NURBO_SWEEP_CUSTOM_NAME_EDITBOX)
                else:
                    GetEditName = TaxilineGen.GetName()
    
                g_lib.SetObjectName(GetEditName, TaxilineGen, 3000)
                # --------------------------------------------------------------------- #


                # | Insert Objects | -------------------------------------------------- #
                """ Insert Custom Taxiline Sweep Object """
                doc.InsertObject(TaxilineGen)        

                TaxilineGen[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_X] = i[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_X]
                TaxilineGen[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Y] = i[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Y]
                TaxilineGen[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Z] = i[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Z]
                TaxilineGen[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_X] = i[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_X]
                TaxilineGen[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_Y] = i[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_Y]
                TaxilineGen[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_Z] = i[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_Z]            

                """ Insert Custom Path Object """
                i.InsertUnder(TaxilineGen)
                i[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_X] = 0
                i[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Y] = 0
                i[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Z] = 0
                i[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_X] = 0
                i[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_Y] = 0
                i[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_Z] = 0 
                # --------------------------------------------------------------------- #

                self.AddingToCustomGroup(doc=doc, obj=TaxilineGen)

                doc.AddUndo(c4d.UNDOTYPE_NEW, TaxilineGen)
                doc.AddUndo(c4d.UNDOTYPE_NEW, i)
            
            doc.EndUndo()
            c4d.CallCommand(100004767, 100004767) # Deselect All
            c4d.EventAdd()
            return True


        if self.GetLong(fcsID.ID_NURBO_SWEEP_GEN_TYPE_LIST)==self.SweepGeneratorID:

            for c4d_objType in self.Osplines.c4d_gen_splines:

                if self.GetLong(fcsID.ID_NURBO_SWEEP_SPLINE_SHAPES_LIST)==c4d_objType["ui_id"]:
                    self.Generate_With_Template(c4d_objType["otype"], AddTemp=True)

            if self.GetLong(fcsID.ID_NURBO_SWEEP_SPLINE_SHAPES_LIST)==fcsID.ID_NURBO_SWEEP_L_CUSTOM:
                c4d_objType = self.FindCustomGui(fcsID.ID_NURBO_SWEEP_CUSTOM_SPLINE_LINK, c4d.CUSTOMGUI_LINKBOX).GetLink()
                if c4d_objType == None:
                    g_lib.fcsLog.WARNING("Sorry!\nThe linked custom spline template slot is empty.\nPlease link or drag a editable spline object\ninto the custom spline template slot.", True, DebugMode)
                    return
                self.Generate_With_Template(c4d_objType, AddTemp=False)

        if self.GetLong(fcsID.ID_NURBO_SWEEP_GEN_TYPE_LIST)==self.TaxiLineGeneratorID:

            """ Generate by the User Custom Sweep Selection Mode eg:(Splines, Edges or Geo Path) """

            if self.GetBool(fcsID.ID_NURBO_SWEEP_MODE_CHK_SPINES)==True:
                Generate_Taxiline()

            if self.GetBool(fcsID.ID_NURBO_SWEEP_MODE_CHK_POLYEDGES)==True:
                doc = c4d.documents.GetActiveDocument()
                objs = doc.GetActiveObject()
                if not objs:
                    g_lib.fcsLog.WARNING("You must select an object.", True)
                    return False
                doc.StartUndo()
                clone = objs.GetClone(c4d.COPYFLAGS_0)
                doc.AddUndo(c4d.UNDOTYPE_CHANGE, clone)
                clone.SetName("Temp404")
                doc.AddUndo(c4d.UNDOTYPE_CHANGE, clone)
                doc.AddUndo(c4d.UNDOTYPE_NEW, clone)
                doc.InsertObject(clone)
                doc.SetActiveObject(clone)
                doc.AddUndo(c4d.UNDOTYPE_NEW, clone)
                c4d.CallCommand(16351) # Edges    
                c4d.CallCommand(1009671, 1009671) # Edge to Spline
                c4d.CallCommand(1019951, 1019951) # Delete Without Children
                doc.AddUndo(c4d.UNDOTYPE_NEW, clone)
                f = doc.SearchObject("Temp404.Spline")
                doc.AddUndo(c4d.UNDOTYPE_CHANGE, f)
                doc.SetActiveObject(f)
                doc.EndUndo()          
                Generate_Taxiline()
                doc.AddUndo(c4d.UNDOTYPE_NEW, clone)
                doc.AddUndo(c4d.UNDOTYPE_NEW, f)             
                doc.EndUndo()
                c4d.EventAdd()
                #c4d.CallCommand(1019940) # Reset PSR
                #c4d.CallCommand(1021385) # Scale
                #c4d.CallCommand(200000089) # Scale
            

        return True



 #####  \\  GUI Utils Operations \\   #####
    # To Add o Insert UI Group with elements and Remove.
    def Insert_GUI_Toggle_Area(self, data_id, attach_grp):
        # This Method function works like this to pass date or info to this function:
        # eg: [ self.Insert_GUI_Toggle_Area(data_id=[ID INFO], attach_grp=[DATA]) ]

        insert_grp_id = data_id['insert_id']
        toggle_id = data_id['t_state_id']

        self.GroupBegin(insert_grp_id, c4d.BFH_SCALEFIT, 1, 0)
        Attach_GUI = attach_grp # Attach UI Group
        self.GroupEnd()

        # Using the StaticText gui as a toggle state mode by a string text and have it hidden on UI as default.
        self.AddStaticText(toggle_id, c4d.BFH_SCALEFIT, 1, 1, "Hide_UI")
        self.HideElement(toggle_id, True) # (True) Hide UI element or (False) Show UI element.
        return True

    def Add_BarTitle_with_Icon_GUI(self, bar_title, info_btn_id, bar_ids, fold, state_id, col):
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 0, 1, "")
        self.GroupSpace(0, 0)
        g_lib.Add_QuickTab_Bar_GUI(ui_instance=self, bar_id=bar_ids, bar_name=bar_title, fold=fold, ui_state_id=state_id, width=150, height=10, color_mode=True, ui_color=col)
        info_btn_settings = {'id':info_btn_id, 'btn_look':c4d.BORDER_NONE, 'tip':"<b>Instructions</b>", 'icon':g_lib.fcsDIR.I(i='InfoIcon'), 'clickable':True}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=info_btn_settings)
        self.GroupEnd() # Group End            
        return True
    


    # ----------------------------------------
    #     Preset Window Dispay System 
    # ----------------------------------------
    # Open Nurbo Temp Preset Save Dialog
    def NurboTempPreset_SaveDialog(self):
        # Open Preset Save Tool Data Dialog
        self.dialog2 = NurboCreatePreset_Dialog()
        print("Preset saving at this time.")
        self.dialog2.Open(dlgtype=c4d.DLG_TYPE_MODAL, defaultw=10, defaulth=10) 
        return True 
    # Open Global Preset Save Tool Data Dialog
    def OpenPreset_SaveDialog(self):
        # Open Preset Save Tool Data Dialog
        self.dialog2 = Preset_Saver_Dialog()
        print("Preset saving at this time.")
        self.dialog2.Open(dlgtype=c4d.DLG_TYPE_ASYNC, defaultw=10, defaulth=10) 
        return True

    # --------------------------------------------------------
    #  Nurbo Config
    # --------------------------------------------------------
    def Nurbo_ConfigFile(self, config_state):

        self.Nurbojsonfile = g_lib.fcsDIR.FCS_NURBO_CONIG

        """ UI/UserInterface Save & Load Data Config State File """

        Nurbo_Config = {}
        
        if config_state == "createfile":
            """ 
            Create Save .Json Config File Stucture if the file exists. 
            """

            if not os.path.exists(self.Nurbojsonfile):

                Nurbo_Config["OptionMode"]="SweepSplines"
                Nurbo_Config["CustomNameChk"]=False
                Nurbo_Config["CustomName"]=""
                Nurbo_Config["GroupItChk"]=False
                Nurbo_Config["GroupName"]=""
                Nurbo_Config["PresetTemp"]="Arc"
                Nurbo_Config["AngleCurvesSettings"]="Default"
                Nurbo_Config["ObjectSplineModeType"]="Default"
                Nurbo_Config["TempSplineName"]="NoTempSpineObjData"
                Nurbo_Config["SweepSettings"]={"Parallel Movement":False, 
                                            "Banking":True, 
                                            "Use Rail Direction":True, 
                                            "Use Rail Scale":True, 
                                            "Stick UVs":False, 
                                            "Constraint Cross Section":True, 
                                            "Keep Segements":False, 
                                            "2-Rail":True,
                                            "Flip Normals":False, 
                                            "Cap_Start":"None", 
                                            "Cap_End":"None", 
                                            "CreateSingleObject":True,}
                Nurbo_Config["Nurbo_TaxilineObject_Type"]="Center"
                Nurbo_Config["Nurbo_TaxilineObject_Size"]=50
                Nurbo_Config["Nurbo_TaxilineObject_AngleCurvesNum"]=0.08726646259971647


                jsonEdit.SaveFile(self.Nurbojsonfile, Nurbo_Config)

        if config_state == "savefile":

            # Get UI Data
            jsonEdit.EditItemProperty(self.Nurbojsonfile, "CustomName", self.GetString(fcsID.ID_NURBO_SWEEP_CUSTOM_NAME_EDITBOX))
            jsonEdit.EditItemProperty(self.Nurbojsonfile, "CustomNameChk", self.GetBool(fcsID.ID_NURBO_SWEEP_CUSTOM_NAME_CHK))
            jsonEdit.EditItemProperty(self.Nurbojsonfile, "GroupName", self.GetString(fcsID.ID_NURBO_SWEEP_ADDGRP_EDIT))
            jsonEdit.EditItemProperty(self.Nurbojsonfile, "GroupItChk", self.GetBool(fcsID.ID_NURBO_SWEEP_ADDGRP_CHK))
            
            for c4dspline in self.Osplines.c4d_gen_splines:
                if self.GetLong(self.ID_NURBO_SWEEP_SPLINE_SHAPES_LIST) == c4dspline["ui_id"]:
                    jsonEdit.EditItemProperty(self.Nurbojsonfile, "PresetTemp", c4dspline["obj_name"])


        #if config_state == "loadfile":

        return True




    # -------------------------------------------------------
    #       Nurbo UI Sections 
    #       GUI Layout Panels | User Interface in Panels
    # -------------------------------------------------------
    def BuildUI(self):
        ui = self

        def WindoeTopMenuBar():
            """ Adding Top Toolbar UI Menu of the dialog."""
            ui.MenuFlushAll()
            
            # File Menu
            ui.MenuSubBegin("Data")
            ui.MenuAddString(fcsID.IDS_NEW, self.IDS.ID(fcsID.IDS_NEW))
            ui.MenuAddSeparator()  
            ui.MenuAddString(0, self.IDS.ID(fcsID.IDS_ADD_PRESET_TEMP))      
            ui.MenuAddString(fcsID.IDS_CREATE_CUS_PRESET, self.IDS.ID(fcsID.IDS_CREATE_CUS_PRESET))
            ui.MenuAddSeparator()
            ui.MenuAddString(fcsID.IDS_OPEN, self.IDS.ID(fcsID.IDS_OPEN))        
            ui.MenuAddSeparator()
            ui.MenuAddString(fcsID.IDS_SAVE, self.IDS.ID(fcsID.IDS_SAVE))
            ui.MenuAddString(fcsID.IDS_SAVEAS, self.IDS.ID(fcsID.IDS_SAVEAS) + "...")
            ui.MenuSubEnd() # End of File Menu   

            # Objects & Mesh Tools Menu
            ui.MenuSubBegin( self.IDS.ID(fcsID.IDS_OBJECTS) + " & " + self.IDS.ID(fcsID.IDS_MESH))
            # Objects SubMenu
            ui.MenuSubBegin(self.IDS.ID(fcsID.IDS_OBJECTS))
            ui.MenuAddCommand(5118)
            ui.MenuAddSeparator()  
            ui.MenuAddCommand(1053591)
            ui.MenuSubEnd()
            # Mesh Tools SubMenu
            ui.MenuSubBegin(self.IDS.ID(fcsID.IDS_MESH) + " " + self.IDS.ID(fcsID.IDS_CONVERSION_STR))
            ui.MenuAddCommand(1053608)
            ui.MenuSubEnd()
            ui.MenuSubEnd() # End of Objects & Mesh Tools Menu

            # Help Menu
            ui.MenuSubBegin("Help")           # Help Menu
            ui.MenuSubBegin("Demo Projects")  # Sub Menu for Demo Projects
            ui.MenuAddString(self.TOP_MENU_IDs["demo1_id"], "Trees Demo")
            ui.MenuAddSeparator()
            ui.MenuAddString(self.TOP_MENU_IDs["demo2_id"], "Cars Demo")
            ui.MenuAddSeparator()
            ui.MenuAddString(self.TOP_MENU_IDs["demo3_id"], "Boxes Demo")
            ui.MenuSubEnd()                   # Demo Projects Sub Menu End
            ui.MenuAddSeparator()
            ui.MenuAddString(self.TOP_MENU_IDs["discord_id"], 'Discord Support Help')
            ui.MenuAddSeparator()
            ui.MenuAddString(self.TOP_MENU_IDs["onlineM"], 'Online Manual')
            ui.MenuAddSeparator() 
            ui.MenuAddString(self.TOP_MENU_IDs["offlineM"], 'Offline Manual')
            ui.MenuSubEnd() # Help Menu End

            ui.MenuFinished()

            ui.GroupBeginInMenuLine()
            ui.AddStaticText(self.ID_VER, 0)
            ui.SetString(self.ID_VER, self.VerData)
            ui.GroupEnd()

            return True
        WindoeTopMenuBar()

        def NurboBanner_Panel_GUI():
            self.GroupBegin(fcsID.ID_NURBO_BANNER_GRP_BG, c4d.BFH_SCALEFIT, 1, 0, "")

            self.GroupBegin(fcsID.ID_NURBO_BANNER_GRP, c4d.BFH_CENTER, 1, 0, "")
            self.GroupEnd()

            self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

            return self.GroupEnd()
        NurboBanner_Panel_GUI()

        ui.GroupBegin(fcsID.ID_NURBO_OVERALL_GRP, c4d.BFH_SCALEFIT, 1, 0, "") # Overall Group
        ui.GroupBorderSpace(3, 3, 3, 3)

        def NurboTabs_Planels_GUI():
            ui.TabGroupBegin(fcsID.ID_NURBO_TABS_GRP, c4d.BFH_SCALEFIT, c4d.TAB_TABS)

            def NurboSweepTab_GUI():

                ui.GroupBegin(fcsID.ID_NURBO_SWEEP_TAB_GRP, c4d.BFH_SCALEFIT, 1, 0, self.IDS.ID(fcsID.ID_NURBO_SWEEP_TAB_GRP) )
                ui.GroupBorderSpace(3, 3, 3, 3) 

                ui.AddStaticText(fcsID.ID_NURBO_SWEEP_GRP_STATIC, c4d.BFH_CENTER, 0, 10, self.IDS.ID(fcsID.ID_NURBO_SWEEP_GRP_STATIC) )

                ui.AddSeparatorH(0, c4d.BFH_SCALEFIT)

                self.GroupBegin(fcsID.ID_NURBO_SWEEP_GEN_TYPE_GRP, c4d.BFH_SCALEFIT, 1, 0, "")

                self.GroupBegin(fcsID.ID_NURBO_SWEEP_GEN_TYPE_TITLE, c4d.BFH_SCALEFIT, 1, 0, "")
                self.GroupEnd()  

                self.GroupBegin(0, c4d.BFH_SCALEFIT, 2, 0, "")
                self.AddStaticText(fcsID.ID_NURBO_SWEEP_GEN_TYPE_STATIC, c4d.BFH_CENTER, 0, 10, self.IDS.ID(fcsID.ID_NURBO_SWEEP_GEN_TYPE_STATIC) )
                self.AddComboBox(fcsID.ID_NURBO_SWEEP_GEN_TYPE_LIST, c4d.BFH_SCALEFIT, 100, 0, False)
                self.AddChild( fcsID.ID_NURBO_SWEEP_GEN_TYPE_LIST, self.SweepGeneratorID,  self.IDS.ID(fcsID.IDS_SWEEP_OBJECT_wICON))
                self.AddChild( fcsID.ID_NURBO_SWEEP_GEN_TYPE_LIST, self.TaxiLineGeneratorID, self.IDS.ID(fcsID.IDS_TAXILINE_OBJECT_ICON))      
                self.GroupEnd() 

                self.GroupEnd()

                self.GroupBegin(fcsID.ID_NURBO_SWEEP_MODE_SETTINGS_GRP, c4d.BFH_SCALEFIT, 1, 0, "")
                self.GroupBegin(fcsID.ID_NURBO_SWEEP_MODE_TITLE_GRP, c4d.BFH_SCALEFIT, 1, 0, "")
                self.GroupEnd() 

                self.AddRadioGroup(fcsID.ID_NURBO_SWEEP_MODE_GRP, c4d.BFH_CENTER, 3, 0)
                self.GroupSpace(3, 3)
                self.AddChild(fcsID.ID_NURBO_SWEEP_MODE_GRP, fcsID.ID_NURBO_SWEEP_MODE_CHK_SPINES, self.IDS.ID(fcsID.ID_NURBO_SWEEP_MODE_CHK_SPINES))
                self.AddChild(fcsID.ID_NURBO_SWEEP_MODE_GRP, fcsID.ID_NURBO_SWEEP_MODE_CHK_POLYEDGES, self.IDS.ID(fcsID.ID_NURBO_SWEEP_MODE_CHK_POLYEDGES))
                self.AddChild(fcsID.ID_NURBO_SWEEP_MODE_GRP, fcsID.ID_NURBO_SWEEP_MODE_CHK_GEO_PATH_CUT, self.IDS.ID(fcsID.ID_NURBO_SWEEP_MODE_CHK_GEO_PATH_CUT))
                self.GroupEnd() 


                self.GroupBegin(fcsID.ID_NURBO_SWEEP_CUSTOM_NAME_GRP, c4d.BFH_SCALEFIT, 1, 0, "")

                self.GroupBegin(fcsID.ID_NURBO_SWEEP_CUSTOM_NAME_TITLE_GRP, c4d.BFH_SCALEFIT, 1, 0, "")
                self.GroupEnd() 

                self.GroupBegin(0, c4d.BFH_SCALEFIT, 3, 0, "")
                self.AddCheckbox(fcsID.ID_NURBO_SWEEP_CUSTOM_NAME_CHK, c4d.BFH_MASK, 0, 0, "")
                self.AddSeparatorV(0, c4d.BFV_SCALEFIT)
                self.AddEditText(fcsID.ID_NURBO_SWEEP_CUSTOM_NAME_EDITBOX, c4d.BFH_SCALEFIT, 0, 10)
                self.GroupEnd()

                self.GroupEnd()


                self.GroupBegin(fcsID.ID_NURBO_SWEEP_ADDCUSTOMGRP_GRP, c4d.BFH_SCALEFIT, 1, 0, "")

                self.GroupBegin(fcsID.ID_NURBO_SWEEP_ADDGRP_TITLE, c4d.BFH_SCALEFIT, 1, 0, "")
                self.GroupEnd() 

                self.GroupBegin(0, c4d.BFH_SCALEFIT, 5, 0, "")
                self.AddCheckbox(fcsID.ID_NURBO_SWEEP_ADDGRP_CHK, c4d.BFH_MASK, 0, 0, "")
                self.AddSeparatorV(0, c4d.BFV_SCALEFIT)
                self.AddEditText(fcsID.ID_NURBO_SWEEP_ADDGRP_EDIT, c4d.BFH_SCALEFIT, 0, 10)        
                self.AddButton(fcsID.ID_NURBO_SWEEP_ADDGRP_BTN, c4d.BFH_SCALEFIT, 20, 10, self.IDS.ID(fcsID.ID_NURBO_SWEEP_ADDGRP_BTN))
                self.GroupEnd() 

                self.GroupEnd() 


                self.GroupBegin(fcsID.ID_NURBO_SWEEP_SPLINE_SHAPES_GRP, c4d.BFH_SCALEFIT, 1, 0, "")

                self.GroupBegin(fcsID.ID_NURBO_SWEEP_SPLINE_SHAPES_TITLE_GRP, c4d.BFH_SCALEFIT, 1, 0, "")
                self.GroupEnd() 

                self.GroupBegin(0, c4d.BFH_SCALEFIT, 1, 0, "")
                self.GroupBorderSpace(3, 3, 3, 3) 
                #
                self.GroupBegin(0, c4d.BFH_SCALEFIT, 4, 0, "") 
                self.AddComboBox(fcsID.ID_NURBO_SWEEP_SPLINE_SHAPES_LIST, c4d.BFH_SCALEFIT, 100, 0, False)
                self.AddChild(fcsID.ID_NURBO_SWEEP_SPLINE_SHAPES_LIST, fcsID.ID_NURBO_SWEEP_L_CUSTOM, self.IDS.ID(fcsID.ID_NURBO_SWEEP_L_CUSTOM))
                for _spline_ in g_lib.C4DSplineShapesObjects(self.IDS).c4d_gen_splines:
                    self.AddChild(fcsID.ID_NURBO_SWEEP_SPLINE_SHAPES_LIST, _spline_["ui_id"], _spline_["obj_name"])
                self.AddSeparatorV(0, c4d.BFV_SCALEFIT)
                self.GroupBegin(0, c4d.BFH_SCALEFIT, 1, 0, "")
                self.AddStaticText(fcsID.ID_NURBO_SWEEP_MODETYPE_STATIC, c4d.BFH_CENTER, 0, 10, self.IDS.ID(fcsID.ID_NURBO_SWEEP_MODETYPE_STATIC))
                self.AddComboBox(fcsID.ID_NURBO_SWEEP_SPLINE_MODE_LIST, c4d.BFH_SCALEFIT, 100, 0, False)
                self.AddChild(fcsID.ID_NURBO_SWEEP_SPLINE_MODE_LIST, fcsID.ID_NURBO_SWEEP_L_GENERATOR_MODE, self.IDS.ID(fcsID.ID_NURBO_SWEEP_L_GENERATOR_MODE))        
                self.AddChild(fcsID.ID_NURBO_SWEEP_SPLINE_MODE_LIST, fcsID.ID_NURBO_SWEEP_L_MAKEEDITABLE_MODE, self.IDS.ID(fcsID.ID_NURBO_SWEEP_L_MAKEEDITABLE_MODE))
                self.GroupEnd()
                self.GroupEnd()
                #
                self.GroupEnd()

                self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

                self.rootLinkBaseContainer = c4d.BaseContainer()
                self.linkBoxForName = self.AddCustomGui(fcsID.ID_NURBO_SWEEP_CUSTOM_SPLINE_LINK, c4d.CUSTOMGUI_LINKBOX, "", c4d.BFH_SCALEFIT, 320, 10, self.rootLinkBaseContainer)        

                self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

                self.GroupBegin(0, c4d.BFH_SCALEFIT, 4, 0, "")
                self.GroupBorderSpace(3, 3, 3, 3) 
                self.GroupSpace(3, 3)

                # Preset BUTTON
                self.GroupBegin(fcsID.ID_NURBO_SWEEP_PRESET_BTN_GRP, c4d.BFH_SCALEFIT, 1, 0, "")
                self.GroupEnd()         

                # Sweep Settings BUTTON
                self.GroupBegin(fcsID.ID_NURBO_SWEEP_SETTINGS_BTN_GRP, c4d.BFH_SCALEFIT, 1, 0, "")
                self.GroupEnd() 

                # Angle Types BUTTON
                self.GroupBegin(fcsID.ID_NURBO_SWEEP_ANGLE_BTN_GRP, c4d.BFH_SCALEFIT, 1, 0, "")
                self.GroupEnd() 

                # GEO Shapes TO GEO BUTTON
                self.GroupBegin(fcsID.ID_NURBO_SWEEP_FIXROT_BTN_GRP, c4d.BFH_SCALEFIT, 1, 0, "")
                self.GroupEnd()                 

                self.GroupEnd() 

                self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

                self.AddButton(fcsID.ID_NURBO_SWEEP_NURBOIT_BTN, c4d.BFH_SCALEFIT, 20, 15, self.IDS.ID(fcsID.ID_NURBO_SWEEP_NURBOIT_BTN))


                self.GroupEnd() 
                self.GroupEnd()
                return True
            NurboSweepTab_GUI()

            def NurborRePlacerTab_GUI():
                ui.GroupBegin( fcsID.ID_NURBO_REPLACER_TAB_GRP, c4d.BFH_SCALEFIT, 1, 0, self.IDS.ID(fcsID.ID_NURBO_REPLACER_TAB_GRP) )
                ui.AddStaticText(0, c4d.BFH_CENTER, 0, 10, "Nurbo Random Re-Placer Generator")

                def AddToolBarHelperButtons_GUI():

                    # UI Icons
                    # Adding custom GUI image button settings ([set] an shot way for saying [settings]).
                    utils_btn_set = {'id':self.UtilsPanel['toggle_btn'], 
                                    'btn_look':c4d.BORDER_NONE, 
                                    'tip':"<b>Tag Utils</b>\nAdding a tag and tag settings.", 
                                    'icon':g_lib.fcsDIR.I(i="rp_tag_icon"), 
                                    'clickable':True}
                    addS_btn_set = {'id':self.AddSlot, 
                                    'btn_look':c4d.BORDER_NONE, 
                                    'tip':"<b>Add a Template Object/s</b>\nAdding a single object or multiple selection of ojects to the list", 
                                    'icon':g_lib.fcsDIR.I(i="rp_add_icon"), 
                                    'clickable':True}
                    addGrp_btn_set = {'id':self.AddSlots_GrpBTN, 
                                    'btn_look':c4d.BORDER_NONE, 
                                    'tip':"<b>Auto Add a Group with Templates</b>\nAdding all the template objects from the group, to the lic4d.storage.", 
                                    'icon':g_lib.fcsDIR.I(i="rp_grp_icon"), 
                                    'clickable':True}
                    clr_btn_set = {'id':self.ClearListBTN, 
                                'btn_look':c4d.BORDER_NONE,
                                'tip':"<b>Clear List</b>\n To remove all templates objects from the lic4d.storage.", 
                                'icon':g_lib.fcsDIR.I(i="rp_clr_icon"), 
                                'clickable':True}
                    settings_btn_set = {'id':self.Settings_Menu, 
                                        'btn_look':c4d.BORDER_NONE, 
                                        'tip':"<b>Settings</b>\nTo simulate the different options you want to do,\nbefore pressing the Randomize button.", 
                                        'icon':g_lib.fcsDIR.I(i="rp_set_icon"), 
                                        'clickable':True}
                    addPreset_btn_set = {'id':self.AddSlot_preset_grp, 
                                        'btn_look':c4d.BORDER_NONE, 
                                        'tip':"<b>Add a Preset Group</b>", 
                                        'icon':g_lib.fcsDIR.I(i="rp_pre_icon"), 
                                        'clickable':True}


                    def AddPresetTag_PanelTab_GUI(grp_id):
                        self.GroupBegin(grp_id, c4d.BFH_SCALEFIT, 0, 1, "")
                        #self.AddEditText(self.presetName_EB, c4d.BFH_SCALEFIT, 190, 10)
                        self.AddButton(self.addpreset_BTN, c4d.BFH_SCALEFIT, 0, 10, "Add Preset Group Tag")
                        self.GroupEnd() # Group End 
                        # Hide on ui element on default startuos.path.
                        self.HideElement(grp_id, True) # (True) Hide UI element or (False) Show UI element.               
                        return True

                    def AddTag_PanelTab_GUI(grp_id):
                        self.GroupBegin(grp_id, c4d.BFH_SCALEFIT, 0, 1, "")
                        self.AddEditText(self.ItemType_EB, c4d.BFH_SCALEFIT, 190, 10)
                        self.AddColorField(self.ItemType_COL, c4d.BFH_CENTER, 20, 15, c4d.DR_COLORFIELD_NO_COLOR|c4d.DR_COLORFIELD_NO_BRIGHTNESS)
                        self.AddButton(self.ItemType_BTN, c4d.BFH_SCALEFIT, 0, 10, "Add Tag")
                        self.GroupEnd() # Group End 
                        # Hide on ui element on default startuos.path.
                        self.HideElement(grp_id, True) # (True) Hide UI element or (False) Show UI element.        
                        return True

                    def SelectionMode_PanelTab_GUI(grp_id):
                        self.GroupBegin(grp_id, c4d.BFH_SCALEFIT, 1, 0, "")
                        self.GroupBegin(0, c4d.BFH_SCALEFIT, 0, 1, "")
                        self.AddStaticText(0, c4d.BFH_CENTER, 0, 15, " Item Type :", c4d.BORDER_WITH_TITLE_BOLD)
                        self.AddComboBox(self.ItemSel_COMBOBOX, c4d.BFH_SCALEFIT, 0, 15, False)
                        self.GroupEnd() # Group End
                        self.AddButton(self.ItemType_BTN2, c4d.BFH_SCALEFIT, 0, 10, "Select Object/s")
                        self.AddCheckbox(self.ItemSelCol_CHK, c4d.BFH_CENTER|c4d.BFH_SCALEFIT, 0, 0, "Auto Toggle Item Selection Color.")
                        self.GroupEnd() # Group End
                        # Hide on ui element on default startuos.path.
                        self.HideElement(grp_id, True) # (True) Hide UI element or (False) Show UI element.        
                        return True        

                    def Utils_Panel_GUI(grp_id):
                    
                        self.GroupBegin(grp_id, c4d.BFH_SCALEFIT, 1, 0, "") # Overall Main Group
                        self.GroupBorderSpace(2, 2, 2, 2)

                        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)
                    
                        self.Add_BarTitle_with_Icon_GUI(bar_title="Tag Utils", info_btn_id=self.ItemType_Info, bar_ids=0, fold=False, state_id=0, col=g_lib.BG_DEEP_DARKER)

                        self.Add_BarTitle_with_Icon_GUI(bar_title="Create a Preset Group Tag", info_btn_id=self.ItemType_Info, bar_ids=self.PresetPanel['toggle_btn'], fold=True, state_id=0, col=g_lib.BG_LitDarkBlue_Col)
                        self.Insert_GUI_Toggle_Area(self.PresetPanel, AddPresetTag_PanelTab_GUI(self.PresetPanel['grp_id']))

                        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

                        self.Add_BarTitle_with_Icon_GUI(bar_title="Add ItemType Tag", info_btn_id=self.ItemType_Info, bar_ids=self.TagPanel['toggle_btn'], fold=True, state_id=0, col=g_lib.BG_LitDarkBlue_Col)
                        self.Insert_GUI_Toggle_Area(self.TagPanel, AddTag_PanelTab_GUI(self.TagPanel['grp_id']))

                        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

                        self.Add_BarTitle_with_Icon_GUI(bar_title="Selection Mode", info_btn_id=self.ItemType_Info, bar_ids=self.SelModePanel['toggle_btn'], fold=True, state_id=0, col=g_lib.BG_LitDarkBlue_Col)
                        self.Insert_GUI_Toggle_Area(self.SelModePanel, SelectionMode_PanelTab_GUI(self.SelModePanel['grp_id']))

                        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

                        self.GroupEnd() # Overall Main Group End 

                        # Hide on ui element on default startuos.path.
                        self.HideElement(grp_id, True) # (True) Hide UI element or (False) Show UI element.
                        return True


                    self.GroupBegin(0, c4d.BFH_SCALEFIT, 1, 0, "") # Overall Main GRP

                    self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

                    # ---------------------------------------------------------------------------------------- #
                    self.GroupBegin(0, c4d.BFH_CENTER, 0, 1, "")
                    g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=utils_btn_set)
                    self.AddSeparatorV(0, c4d.BFV_SCALEFIT)
                    g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=addS_btn_set)
                    g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=addGrp_btn_set)
                    g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=addPreset_btn_set)
                    g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=clr_btn_set)
                    self.AddSeparatorV(0, c4d.BFV_SCALEFIT)
                    g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=settings_btn_set)
                    self.GroupEnd() # Group End
                    # ---------------------------------------------------------------------------------------- #

                    self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

                    self.ScrollGroupBegin(4540, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, c4d.SCROLLGROUP_VERT|c4d.SCROLLGROUP_AUTOVERT, 420, 150) # Overall Main ScrollGroup
                    self.GroupBegin(0, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 1, 0, "") # Inner-Group

                    # To inject or insert gui group or ui element.
                    self.Insert_GUI_Toggle_Area(self.UtilsPanel, Utils_Panel_GUI(self.UtilsPanel['grp_id']))

                    # The List of Items Group Area
                    #self.AddSeparatorH(0, c4d.BFH_SCALEFIT)        
                    self.ScrollGroupBegin(498, c4d.BFH_SCALEFIT, c4d.SCROLLGROUP_VERT|c4d.SCROLLGROUP_AUTOVERT, 320, 150) # ScrollGroup
                    self.GroupBegin(self.SlotsPanelsLayoutGroup, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 1, 0, "") # Group
                    self.GroupEnd() # Group End
                    self.GroupEnd() # ScrollGroup End

                    self.GroupEnd() # Inner-Group End
                    self.GroupEnd() # Overall Main ScrollGroup End

                    self.GroupEnd() # GRP End
                    return True
                AddToolBarHelperButtons_GUI()

                ui.GroupEnd()
                return True
            NurborRePlacerTab_GUI()

            def NurboBuilderTab_GUI():
                ui.GroupBegin( fcsID.ID_NURBO_BUILDER_TAB_GRP, c4d.BFH_SCALEFIT, 1, 0, self.IDS.ID(fcsID.ID_NURBO_BUILDER_TAB_GRP) )
                ui.GroupEnd()
                return True
            NurboBuilderTab_GUI()

            return self.GroupEnd()
        NurboTabs_Planels_GUI()

        self.GroupEnd() # Overall Group
        return True

    def UIsettings(self):
        g_lib.fcsLog.DEBUG("F.C.S Nurbo Generator Tool is Open Now!", False, g_lib.DebugMode)
        self.Add_and_Inject_GUIs()
        self.SetDefaultColor(fcsID.ID_NURBO_OVERALL_GRP, c4d.COLOR_BG, g_lib.BG_LightDarker_COL)
        self.SetDefaultColor(fcsID.ID_NURBO_BANNER_GRP_BG, c4d.COLOR_BG, g_lib.BG_DEEP_DARKER)
        self.Nurbo_ConfigFile(config_state="createfile")        
        return super(NurboGenerator_Dialog, self).UIsettings()

    #  Excuting Operations
    def Command(self, id, msg=None): 

        #if id == self.ID_NURBO_SWEEP_SETTINGS_BTN:
        #    g_lib.PopupSettings_Checklist(self.Nurbojsonfile, C4D_OBJS.UI_CHK_SWEEPSETTINGS)

        if id == fcsID.ID_NURBO_SWEEP_NURBOIT_BTN:
            self.NurboSweepCoreGenerator()

        if (id == fcsID.ID_NURBO_SWEEP_ADDGRP_BTN):
            g_lib.Generate_Null_Group(str_name=self.GetString(fcsID.ID_NURBO_SWEEP_ADDGRP_EDIT))
            
        return True






class NurboCreatePreset_Dialog(WindowDialog):
    """ Nurbo Preset Save Dialog for all tools that need to have tool data as preset. """
 ##### UI IDs #####
    EB_Data1 = 1001
    EB_Data2 = 1004
    BTN_OK = 1002
    STATIC_TXT = 1005
    BTN_Close = 1003
 ##### UI Window Dialog #####
    def CreateLayout(self):
        self.SetTitle("Save Preset")
        self.AddStaticText(0, c4d.BFH_CENTER, 0, 0, "Template Preset")
        self.GroupBegin(0, c4d.BFH_CENTER, 1, 0, "")
        self.GroupBorderSpace(10, 10, 10, 10) 
        self.GroupBegin(0, c4d.BFH_CENTER, 0, 1, "")
        self.AddStaticText(0, c4d.BFH_CENTER, 0, 0, "Preset Name :")   
        self.AddEditText(self.EB_Data1, c4d.BFH_CENTER, 250, 0)       
        self.GroupEnd()
        self.AddStaticText(0, c4d.BFH_RIGHT, 0, 0, "Maximum of characters limit is (12).")
        #"Attention:\nYou have reached the maximum character limit for the present name field (max character limit:12)\nPlease use a shorter name."
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 1, 0, "")
        self.AddStaticText(0, c4d.BFH_MASK, 0, 0, "Description :")
        self.AddMultiLineEditText(self.EB_Data2, c4d.BFH_SCALEFIT, 100, 100, 0)      
        self.GroupEnd()        
        self.GroupBegin(0, c4d.BFH_CENTER, 0, 1, "")
        self.AddButton(self.BTN_OK, c4d.BFH_CENTER, 0, 0, "Save Preset")
        self.AddButton(self.BTN_Close, c4d.BFH_CENTER, 0, 0, "Close")
        self.GroupEnd()
        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)
        self.AddMultiLineEditText(self.STATIC_TXT, c4d.BFH_SCALEFIT, 100, 60, c4d.DR_MULTILINE_READONLY)
        self.Enable(self.BTN_OK, False)
        #self.Enable(self.STATIC_TXT, False) 
        self.GroupEnd()          
        return True
 ##### Main UI Operations Functions #####
    def MakePreset(self):
        doc = c4d.documents.GetActiveDocument()
        fName = self.GetString(self.EB_Data1)
        fDesc = self.GetString(self.EB_Data2)
        GetDocActiveUnit = doc.GetDocumentData(c4d.DOCUMENTSETTINGS_DOCUMENT)
        ExportSceneSaverID = 1001026

        # Start Process 

        PresetObj = doc.GetActiveObject()
        if not PresetObj:
            c4d.gui.MessageDialog("Sorry?\nNo object selected to save the preset.")
            return    

        ObjectName = PresetObj.GetName()

        GetObjName = PresetObj.GetName()
        MakeClone = PresetObj.GetClone(c4d.COPYFLAGS_0)
        MakeClone.SetName(GetObjName+"_NurbTemp")
        doc.InsertObject(MakeClone)
        doc.SetActiveObject(MakeClone)
        objs = doc.GetActiveObjects(c4d.GETACTIVEOBJECTFLAGS_CHILDREN)
        if objs == None:
            print("No Objects Selected")
            return
        # Get a fresh new document, temporary document with only the selected objects
        docTemp = c4d.documents.IsolateObjects(doc, objs)
        if docTemp == None:
            return False

        doc = c4d.documents.GetActiveDocument() # Get Doc again.

        # Check for export format plugin ID is there.
        plug = plugins.FindPlugin(ExportSceneSaverID, c4d.PLUGINTYPE_SCENESAVER)
        if plug is None:
            print("Export failed!")
            return 

        # Set Document Data for the doc Temos.path.   <------------{PROBLEM HERE! \ It don't set data I have below}
        docTemp.SetDocumentData(c4d.DOCUMENTSETTINGS_DOCUMENT, GetDocActiveUnit)

        # Path and File Name
        path = os.path.join(g_lib.fcsDIR.plugin_CP_folder, fName+".c4d")

        # Exporting Format     
        if c4d.documents.SaveDocument(docTemp, path, c4d.SAVEDOCUMENTFLAGS_DONTADDTORECENTLIST, ExportSceneSaverID) == False:
            c4d.gui.MessageDialog("Directory field is empty.")
            return
        #doc.SetActiveObject(objs)
        c4d.CallCommand(100004787, 100004787) # Delete 
        doc.SetActiveObject(PresetObj) 
        c4d.EventAdd()  

        # Saving Preset Data to XML File.
        MakeXml_root = minidom.Document()
        AddToXML = MakeXml_root.createElement('NurboTempPreset')
        MakeXml_root.appendChild(AddToXML)
        AddTo = MakeXml_root.createElement('Preset')
        AddTo.setAttribute('PresetName', fName)
        AddTo.setAttribute('ObjectTempName', ObjectName)
        AddTo.setAttribute('Description', fDesc) 
        AddToXML.appendChild(AddTo)
        SaveXml = MakeXml_root.toprettyxml(indent="\t")
        SavePresetData = os.path.join(g_lib.fcsDIR.plugin_CP_folder, fName+'.xml')
        with open(SavePresetData, 'w') as f:
            f.write(SaveXml)
            f.close()    

        return True
 ##### Excuting Operations #####
    def Command(self, id, msg=None):

        # Okay Button
        if (id == self.BTN_OK):

            empty = ""
            if self.GetString(self.EB_Data1)==empty:
                c4d.gui.MessageDialog("Attention:\nThere is no name in the edit box field.\nPlease enter a name and press okay.")
                return
            self.MakePreset()
            print("Done")
            self.Close()

        if (id == self.EB_Data1):
            StrPreset = self.GetString(self.EB_Data1)
            GetStrMax = len(StrPreset)                
            if GetStrMax > 13:
                self.Enable(self.BTN_OK, False)
                self.SetString(self.STATIC_TXT, "Attention:\nYou have reached the maximum \ncharacter limit for the present name field\n(max character limit:12).")
                return

            empty = ""
            if self.GetString(self.EB_Data1)==empty:
                self.SetString(self.STATIC_TXT, "Attention:\nThere is no preset name in the edit box field.")
                self.Enable(self.BTN_OK, False)

            else:
                self.SetString(self.STATIC_TXT, "")
                self.Enable(self.BTN_OK, True)



        # Close Button
        if (id == self.BTN_Close):
            self.Close()
        return True       
