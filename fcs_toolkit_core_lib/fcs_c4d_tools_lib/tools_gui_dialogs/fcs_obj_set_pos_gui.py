"""
SetPos Tool Window GUI Dialog
"""
#  // Imports for Cinema 4D //
import c4d
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as WindowDialog        # eg:( class My_Dialog(c4d.gui.GeDialog): ) to ( class My_Dialog(WindowDialog): ) Just keeping code line short.
from c4d.plugins import CommandData, TagData, ObjectData
from random import randint
# Python Plugin System Load Modules Dynamically With importlib.
import importlib
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
fcsID = g_lib.fcsID
jsonEdit = g_lib.jsonEdit

class SetPos_Dialog(WindowDialog):
    """ SetPos Tool Dialolg """
 ##### ID Numbers for UI Elements ######
    def __init__(self, global_strings):
        self.IDS = global_strings

 ##### UI Window Dialog #####
    def CreateLayout(self):

        self.SetTitle(self.IDS.ID(fcsID.fcs_obj_set_pos_gui_dlg))

        self.GroupBegin(fcsID.ID_OSP_OVERALL_GRP, c4d.BFH_SCALEFIT, 1, 0, "") # Overall Group
        self.GroupBorderSpace(3, 3, 3, 3)

        self.GroupBegin(fcsID.ID_OSP_POSTION_GRP, c4d.BFH_SCALEFIT, 3, 0, self.IDS.ID(fcsID.ID_OSP_POSTION_GRP)) # Start POSTION_GRP
        self.GroupBorderSpace(3, 3, 3, 3)
        self.GroupBorder(c4d.BORDER_BLACK)
        self.AddCheckbox(fcsID.ID_OSP_POS_CHK_X, c4d.BFH_MASK, 100, 20, self.IDS.ID(fcsID.ID_OSP_POS_CHK_X)) 
        self.AddCheckbox(fcsID.ID_OSP_POS_CHK_Y, c4d.BFH_MASK, 100, 20, self.IDS.ID(fcsID.ID_OSP_POS_CHK_Y))
        self.AddCheckbox(fcsID.ID_OSP_POS_CHK_Z, c4d.BFH_MASK, 100, 20, self.IDS.ID(fcsID.ID_OSP_POS_CHK_Z))
        self.GroupEnd() # End POSTION_GRP

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT) # Separator

        self.GroupBegin(fcsID.ID_OSP_ROTATION_GRP, c4d.BFH_SCALEFIT, 3, 0, self.IDS.ID(fcsID.ID_OSP_ROTATION_GRP)) # Start ROTATION_GRP
        self.GroupBorderSpace(3, 3, 3, 3)
        self.GroupBorder(c4d.BORDER_BLACK)
        self.AddCheckbox(fcsID.ID_OSP_ROT_CHK_H, c4d.BFH_MASK, 100, 20, self.IDS.ID(fcsID.ID_OSP_ROT_CHK_H)) 
        self.AddCheckbox(fcsID.ID_OSP_ROT_CHK_P, c4d.BFH_MASK, 100, 20, self.IDS.ID(fcsID.ID_OSP_ROT_CHK_P))
        self.AddCheckbox(fcsID.ID_OSP_ROT_CHK_B, c4d.BFH_MASK, 100, 20, self.IDS.ID(fcsID.ID_OSP_ROT_CHK_B))
        self.GroupEnd() # End ROTATION_GRP

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT) # Separator

        self.GroupBegin(fcsID.ID_OSP_VALUE_GRP, c4d.BFH_SCALEFIT, 2, 0, self.IDS.ID(fcsID.ID_OSP_VALUE_TXT)) # Start VALUE_GRP
        self.GroupBorderSpace(3, 3, 3, 3)
        self.GroupBorder(c4d.BORDER_BLACK)
        #self.AddStaticText(fcsID.ID_OSP_VALUE_TXT, c4d.BFH_CENTER, 70, 20, self.IDS.ID(fcsID.ID_OSP_VALUE_TXT), c4d.BORDER_WITH_TITLE_BOLD)
        self.AddEditNumberArrows(fcsID.ID_OSP_VALUE_EDIT_INPUT, c4d.BFH_SCALEFIT, 140, 0)
        self.GroupEnd() # End VALUE_GRP

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT) # Separator

        self.AddButton(fcsID.ID_OSP_SET_BTN, c4d.BFH_SCALEFIT, 100, 15, self.IDS.ID(fcsID.ID_OSP_SET_BTN))

        self.GroupEnd() # End Overall Group
        return True 

    def InitValues(self):
        self.SetInt32(fcsID.ID_OSP_VALUE_EDIT_INPUT, 0)
        Vect = c4d.Vector(0.0625, 0.05859375, 0.0625)
        self.SetDefaultColor(fcsID.ID_OSP_OVERALL_GRP, c4d.COLOR_BG, Vect)   
        return True             
 ##### Main UI Operations Functions #####
    def SetPos_Function(self):
        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False 
        ValNum = self.GetReal(fcsID.ID_OSP_VALUE_EDIT_INPUT) 
        doc.StartUndo()
        SelectObject = doc.GetActiveObjects(1)
        for SO in SelectObject:
            if not SO:
                gui.MessageDialog("You must need to select an object")
                return False  
            # Postion 
            if self.GetBool(fcsID.ID_OSP_POS_CHK_Y) == True:
                SO[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Y]=ValNum          
            if self.GetBool(fcsID.ID_OSP_POS_CHK_X) == True:
                SO[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_X]=ValNum  
            if self.GetBool(fcsID.ID_OSP_POS_CHK_Z) == True:
                SO[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Z]=ValNum
            # Rotation
            if self.GetBool(fcsID.ID_OSP_ROT_CHK_H) == True:
                SO[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_X]=ValNum
            if self.GetBool(fcsID.ID_OSP_ROT_CHK_P) == True:
                SO[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_Y]=ValNum
            if self.GetBool(fcsID.ID_OSP_ROT_CHK_B) == True:
                SO[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_Z]=ValNum
        doc.EndUndo()
        print("Just use SetPos")
        c4d.EventAdd()      
        return True
 ##### Excuting Operations #####
    def Command (self, id, msg):
        if (id == fcsID.ID_OSP_SET_BTN):
            #print("Workiing")
            self.SetPos_Function()
        return True
