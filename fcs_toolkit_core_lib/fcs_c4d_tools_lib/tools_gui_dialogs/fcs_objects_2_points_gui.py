"""
Objects 2 Points Tool Window GUI Dialog
"""
#  // Imports for Cinema 4D //
import c4d
from c4d import plugins, gui, bitmaps, documents, storage, utils
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
from random import randint
# Python Plugin System Load Modules Dynamically With importlib.
import importlib
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
#from res._fcs_lib.fcs_module_lib import fcs_common_global_lib as g_lib
fcsID = g_lib.fcsID
jsonEdit = g_lib.jsonEdit
fcsDIR = None
fcsLog = None
DebugMode = None

# ------------------------------
#  Objects 2 Points Tool
# ------------------------------
class GUI_Window(g_lib.BaseWindowDialogUI):
    """ Object to Points Tool Dialolg. """
    """
    Window Dialog Main Properties
    _________________________________________________
    """
    windowMainTileName = "F.C.S Object to Points"
    windowDialogWidthSize = 10
    windowDialogHeightSize = 10
    windowDialogPluginID = 1038730

    ##### ID Numbers for UI Elements  #####
    LinkBox_SourceObj = 1001
    BTN_Info = 1002
    BTN_Covert = 1003
    LinkBC = c4d.BaseContainer()
    
    def __init__(self, global_strings):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """    
        super(GUI_Window, self).__init__()
        self.IDS = global_strings

    def BuildUI(self):
        """ Creating Dialog UI Interface Layout   """
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 0, 1, "")
        BTN_INFO_SETTINGS = { 'btn_look':c4d.BORDER_NONE, 'tip':"<b>Instructions</b>", 'icon':g_lib.fcsDIR.I(i='InfoIcon'), 'clickable':True}
        g_lib.Add_Image_Button(ui_instance=self, button_id=self.BTN_Info, btn_settings=BTN_INFO_SETTINGS)
        self.AddStaticText(0, c4d.BFH_CENTER, 0, 0, "Source:")
        self.AddCustomGui(self.LinkBox_SourceObj, c4d.CUSTOMGUI_LINKBOX, "", c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 320, 0, self.LinkBC)
        self.AddButton(self.BTN_Covert, c4d.BFH_SCALEFIT | c4d.BFH_CENTER, 100, 10, "Set to Points")
        self.GroupEnd()        
        return super(GUI_Window, self).BuildUI()

    def UIsettings(self):
        g_lib.fcsLog.DEBUG("F.C.S Objects 2 Points is Open Now!", False, g_lib.DebugMode)
        return super(GUI_Window, self).UIsettings()

    def UIfunctions(self, id):
        if (id == self.BTN_Covert):
            self.Run_Operation()
        return super(GUI_Window, self).UIfunctions(id)

    def Run_Operation(self):

        doc = c4d.documents.GetActiveDocument()

        doc.StartUndo()

        obj = doc.GetActiveObject()
        if not obj:
            gui.MessageDialog("You must select an object!")
            return False

        objName = obj.GetName()

        doc.SearchObject(objName)

        obj_pnts = obj.GetAllPoints() # GetPointS()

        # Get object Name from SourceObj Linkbox.
        SourceLinkName = self.FindCustomGui(self.LinkBox_SourceObj, c4d.CUSTOMGUI_LINKBOX).GetLink().GetName()
                 
        for i in xrange(len(obj_pnts)):
                    
            pntX = obj_pnts[i].x
            pntY = obj_pnts[i].y
            pntZ = obj_pnts[i].z 

            SourceLinkClone = self.FindCustomGui(self.LinkBox_SourceObj, c4d.CUSTOMGUI_LINKBOX).GetLink()
            CloneObjSource = SourceLinkClone.GetClone(c4d.COPYFLAGS_0)

            CloneObjSource[c4d.ID_BASEOBJECT_USECOLOR]=1
            CloneObjSource[c4d.ID_BASEOBJECT_COLOR] = g_lib.Add_Vector_Random_Colors()

            g_lib.SetObjectName(name=SourceLinkName, obj=CloneObjSource, amount=500000)

            doc.InsertObject(CloneObjSource)

            CloneObjSource.SetAbsPos(c4d.Vector(pntX,pntY,pntZ))

            under = doc.SearchObject(objName)

            CloneObjSource.InsertUnder(under)
        #c4d.CallCommand(12139) # Points         
        doc.EndUndo()        
        c4d.EventAdd()
        return True
