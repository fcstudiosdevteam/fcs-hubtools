"""
CollidePoints Tool Window GUI Dialog
"""
#  // Imports for Cinema 4D //
import c4d
from c4d import plugins, gui, bitmaps, documents, storage, utils
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
from c4d import plugins, gui, bitmaps, documents, storage, utils
from random import randint
# XML Imports is for Saving and Loading Project Data for Plugin UI / Xml good but long functions to make.
from xml.dom import minidom
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree

# Python Plugin System Load Modules Dynamically With importlib.
import importlib
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
#from res._fcs_lib.fcs_module_lib import fcs_common_global_lib as g_lib
fcsID = g_lib.fcsID
jsonEdit = g_lib.jsonEdit


class C4D_ProjectStorage_Dialog(g_lib.BaseWindowDialogUI):
    """ F.C.S ProjectStorage C4D Window Dialog """

    """
    Window Dialog Main Properties
    _________________________________________________
    """
    windowMainTileName = "F.C.S ProjectStorage C4D"
    windowDialogWidthSize = 200
    windowDialogHeightSize = 200
    windowDialogPluginID = 1050001       

    #####  \\  GUI IDs  \\   ##### 
    Add_RecentPath_Menu = 200
    RecentPath_List = []
    Add_StoragePath_BTN = 201 
    StorageContainerGRP = 202
    PresetsContainerGRP = 203   
    StorageSearchBar = 205
    StorageFiles_List = []
    GRP_Files_Container = 206
    Add_ObjToStorage = 207
    StoragePath = "C:/Users/AP_Ashton_TheCreator/Desktop/testing"
    PROGRESSBAR = 208
    ProgressAmount = 209 
    AmountStr = 210
    Refresh_RecentPath_Menu = 211
    SettingsBtn = 212
    C4D_Filter = {'id':1017, 'export_id':1001026, 'import_id':1001025, 'str':"CINEMA 4D Project(*.c4d)", 'ext':".c4d"}
    #PSC4Djsonfile = PLUGIN_DIR().PSC4D_CONIG

    def BuildUI(self):
        self.ProjectStorage_GUI_Layout()
        return super(C4D_ProjectStorage_Dialog, self).BuildUI()

    def UIsettings(self):
        g_lib.fcsLog.DEBUG("F.C.S C4D ProjectStorage is Open Now!", False, g_lib.DebugMode)
        self.StorageFiles_List = []
        self.RecentPath_List = []
        self.GenerateGUI(Group_Id=self.StorageContainerGRP)
        self.Add_User_StorageFolders_to_ComboBoxMenu()
        self.PSC4D_ConfigFile()        
        return super(C4D_ProjectStorage_Dialog, self).UIsettings()

    def UIfunctions(self, id):
        self.MainGUI_Fuctions(id)
        self.StorageFile_GUI_Functions(id)        
        return super(C4D_ProjectStorage_Dialog, self).UIfunctions(id)
    """___________________________________________________________________________________
    """
 #####  \\  GUI Utils Operations \\   #####

    ########################################
    # UI Utils Functions
    ########################################
    #   //   Adding to GUI   //

    # Create StorageFile Temp GUI Layout.  
    def Add_StorageFile(self, Str, TN, T, DB, OB, IB, DelB, Path_info, DI, TitleN, ThumN, GRP):

        self.GroupBegin(int(GRP), c4d.BFH_MASK, 1, 0, "")
        Vect = c4d.Vector(0.125, 0.125, 0.125)
        self.SetDefaultColor(int(GRP), c4d.COLOR_BG, Vect)

        self.GroupSpace(1, 1)

        self.GroupBorderNoTitle(c4d.BORDER_BLACK) 

        # Tile 
        Title = " [" + Str +"] " + TitleN
        g_lib.Add_QuickTab_Bar_GUI(ui_instance=self, bar_id=int(T), bar_name=Title, fold=False, ui_state_id=0, width=150, height=10, color_mode=True, ui_color=c4d.Vector(0.125, 0.125, 0.125))

        # Thumnail Image
        self.GroupBegin(0, c4d.BFH_MASK, 2, 0, "")
        Image_thum = Path_info + "/" + ThumN #PLUGIN_DIR().Icon_Path_Two(i=Image_thum)
        Image = g_lib.fcsDIR.Global_Icon_Path(i=Image_thum)
        #decs_icon_settings = {'id':int(TN), 'btn_look':c4d.BORDER_NONE, 'tip':"Description:"+"\n"+DI, 'icon':Image, 'clickable':True}
        g_lib.Add_SizeFit_Image_GUI(ui_instance=self, image=Image, width=160, height=120, info_text="", id=int(TN))

        # Description
        self.GroupBegin(0, c4d.BFH_MASK, 1, 0, "")
        self.GroupSpace(1, 1) 
        self.AddStaticText(0, c4d.BFH_MASK, 0, 0, "Description:")
        self.AddMultiLineEditText(int(DB), c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 230, 60, c4d.DR_MULTILINE_MONOSPACED)

        self.SetString(int(DB), DI) # Set EditBox with data.

        self.AddSeparatorH(0, flags=c4d.BFH_SCALEFIT)

        self.GroupBegin(0, c4d.BFH_RIGHT, 0, 1, "")
        self.GroupSpace(0, 0)
        # Open Button 
        open_icon_settings = {'id':int(OB), 'btn_look':c4d.BORDER_NONE, 'tip':"Open Object", 'icon':g_lib.fcsDIR.I(i='MergeIt'), 'clickable':True}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=open_icon_settings)
        self.AddSeparatorV(0, flags=c4d.BFV_SCALEFIT)
        # Merge wwth Scene Button
        merge_icon_settings = {'id':int(IB), 'btn_look':c4d.BORDER_NONE, 'tip':"Merge to Scene", 'icon':g_lib.fcsDIR.I(i='Openbox'), 'clickable':True}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=merge_icon_settings)        
        self.AddSeparatorV(0, flags=c4d.BFV_SCALEFIT)
        # Delete Button
        del_icon_settings = {'id':int(DelB), 'btn_look':c4d.BORDER_NONE, 'tip':"Delete Object", 'icon':g_lib.fcsDIR.I(i='DelTrash'), 'clickable':True}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=del_icon_settings) 
        self.AddSeparatorV(0, flags=c4d.BFV_SCALEFIT)
        self.GroupEnd()
        self.GroupEnd()
        self.GroupEnd()        

        self.GroupEnd()
        return True
    # Create ProgressBar
    def Add_ProgressBar_GUI(self):
        # PROGRESSBAR
        self.GroupBegin(0, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 0, 1)   
        self.GroupBorderNoTitle(c4d.BORDER_THIN_IN)
        #self.GroupBorderSpace(1, 1, 1, 1) 
        self.AddCustomGui(self.PROGRESSBAR, c4d.CUSTOMGUI_PROGRESSBAR, "", c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 100, 10)
        self.AddSeparatorV(0, c4d.BFV_SCALEFIT)
        self.AddStaticText(self.ProgressAmount, c4d.BFH_MASK, 50, 10, "", c4d.BORDER_WITH_TITLE_BOLD)
        self.AddSeparatorV(0, c4d.BFV_SCALEFIT)
        self.AddStaticText(self.AmountStr, c4d.BFH_RIGHT, 0, 0, "Amount of Files : ")      
        self.GroupEnd() # Group End           
        return True
    def Run_PrcoessBar(self, currentNum, amountOfObjects):
        # Prcoess Bar
        percent = float(currentNum)/amountOfObjects*100
        #print str(percent)
        # Set Data to PROGRESSBAR
        progressMsg = c4d.BaseContainer(c4d.BFM_SETSTATUSBAR)
        progressMsg[c4d.BFM_STATUSBAR_PROGRESSON] = True
        progressMsg[c4d.BFM_STATUSBAR_PROGRESS] = percent/100.0 
        self.SendMessage(self.PROGRESSBAR, progressMsg)
        self.SetString(self.ProgressAmount, str(int(percent))+"%")   
        return True   
    def StopProgress(self):
        #self.SetTimer(0)
        progressMsg = c4d.BaseContainer(c4d.BFM_SETSTATUSBAR)
        progressMsg.SetBool(c4d.BFM_STATUSBAR_PROGRESSON, False)
        self.SendMessage(self.PROGRESSBAR, progressMsg)
        return True 

    # Create Auto IDs for List of GUI elements.
    def Add_CreateList_Ids_and_GUI_Generator(self, Path, ListName, Thum_id, Title_id, Desc, open_id, InjectId, DelId, GRPId, xmlext):
        # Create the Amount of Items.
        #Amount = 10
        #MainNumRange = range(Amount)

        degit = 0

        if os.path.exists(Path):
            # Looking for all files that are .xml    
            os.chdir(Path)
            for each_file in glob.glob(xmlext):
                # Get all files and split or cut off the extension from it.
                for files in each_file.splitlines():
                    # Get the Object Doc Name.
                    ObjectfileName = files.split('_C4D.xml')
                    # Ceeate IDs FOR UI
                    degit+=1
                    new_id_num = str(degit)
                    # Create ID  
                    Add_Data = {'DirNum':new_id_num,
                                'Thumbnail_ID':str(Thum_id) + new_id_num,
                                'TitleID':str(Title_id) + new_id_num,
                                'DescBoxID':str(Desc) + new_id_num,
                                'OpenBTN':str(open_id) + new_id_num,
                                'InjectBTN':str(InjectId) + new_id_num,
                                'DelBTN':str(DelId) + new_id_num,
                                'Group_Id':str(GRPId) + new_id_num,
                                'ObjectName':ObjectfileName[0],
                                'FilePath':Path }
                    # Add to lic4d.storage.
                    ListName.append(Add_Data)

                    print(Add_Data)

        # If list created with IDs, then create GUI's layouts with Ids in the lic4d.storage. 
        if ListName:

            L = len(ListName) 
            currentNum = 1
            amountOfObjects = L

            for E in ListName:

                self.Run_PrcoessBar(currentNum, amountOfObjects)

                currentNum += 1

                # Get Ids from  Lic4d.storage.
                Numlink = E['DirNum']
                Thumbnail_ID = E['Thumbnail_ID']
                TitleID = E['TitleID']
                DescBoxID = E['DescBoxID']
                OpenBTN = E['OpenBTN']
                InjectBTN = E['InjectBTN']
                DelBTN =  E['DelBTN']
                ObjectName = E['ObjectName']
                FilePath = E['FilePath']
                Group_Id = E['Group_Id']

                # Get Data From File XML
                ReadXmlFile = os.path.join(os.path.split(__file__)[0], FilePath, ObjectName+'_C4D.xml')
                XmlFileTree = etree.parse(ReadXmlFile)
                XmlRoot = XmlFileTree.getroot()
                for data in XmlRoot.findall('Preset'):
                    GetFileTitle = data.get("TitleName")
                    GetFileThumbnail = data.get("Thumbnail_Image")
                    GetFileDesc = data.get("Description")  

                DescInfo = open(GetFileDesc, "r")

                TitleName = GetFileTitle
                Thumbnail = GetFileThumbnail

                # Get User Str Editbox Search Bar. 
                SearchBar = self.GetString(self.StorageSearchBar).lower()
                if SearchBar == "":
                    #print "Search Bar is empty."
                    pass

                # Create GUI Path Dir's with Generate List IDs.
                self.Add_StorageFile(Str=Numlink, 
                                     TN=Thumbnail_ID, 
                                     T=TitleID, 
                                     DB=DescBoxID, 
                                     OB=OpenBTN, 
                                     IB=InjectBTN, 
                                     DelB=DelBTN, 
                                     Path_info=FilePath, 
                                     DI=DescInfo.read(), 
                                     TitleN=TitleName, 
                                     ThumN=Thumbnail, 
                                     GRP=Group_Id )
                #print str(E) + " + " + str(Thumbnail)
                #print str(Thumbnail)   
            #print "Amount Of Items:" + str(L)
            self.SetString(self.AmountStr, "Amount of Files : "+str(amountOfObjects)) 
        self.StopProgress()
        return True  
    def Add_User_StorageFolders_to_ComboBoxMenu(self):

        # Refesh or clear the ComboBox Menu.
        self.FreeChildren(self.Add_RecentPath_Menu)

        # Read XML for list of ProjectStoragePaths
        degit = 0

        ReadXmlFile = etree.parse(g_lib.fcsDIR.FCS_PREF_CONIG)

        GetRoot = ReadXmlFile.getroot()
        #print "I got the root."
        if ReadXmlFile:
            for root in GetRoot.findall('ProjectStorage_Data'):
                for ListRoot in root.findall('ProjectStorage_Paths'):
                    for datas in ListRoot.findall('ProjectStorage_MenuList'):
                        for data in datas.findall('StoragePath'):
                            GetData = data.get("StorageFolder")
                            if GetData:
                                foldername = os.path.split(GetData)[1]
                                F_Str = str(foldername +" Storage ")
                                #print F_Str

                                # Create ID for GUI. 
                                degit+=1
                                new_id_num = degit
                                id_StorageFolder = 7000+new_id_num

                                # Add to ComboBox Menu
                                self.AddChild(self.Add_RecentPath_Menu, id_StorageFolder, F_Str)

                                # Add to the lic4d.storage.
                                Adding_Path_N_id = str(id_StorageFolder) + "--" + GetData 
                                #print str(id_StorageFolder)
                                self.RecentPath_List.append(Adding_Path_N_id) 
                            else:
                                gui.MessageDialog("Error:\nSorry! You didn't provide the Project Storage directory for exporting the object to storage folder.\nPlease add a path to ProjectStorage C4D in the settings.\n\nTo Find the settings, go to F.C.S menu -> Preferences Center and you will see ProjectStorage C4D - Tool.")
                                return
        else:
            gui.MessageDialog("Error:\nSorry! You didn't provide the Project Storage directory for exporting the object to storage folder.\nPlease add a path to ProjectStorage C4D in the settings.\n\nTo Find the settings, go to F.C.S menu -> Preferences Center and you will see ProjectStorage C4D - Tool.")
            return
        return True
    # Get Users Files and Create Files with ID + GUI-Elements.
    def Add_StorageFiles(self):
        # // Reading Xml and adding the list of the storage folders paths thats the user added by the fcs prefs center.
        # // Adding to list of paths to gui, which is the drop down gui combobox menu.

        #print "----| List of StorageFolders |----"

        # //  Adding Files from the Project Storage Folders to GUI //
        self.SetString(self.AmountStr, "Amount of Files : "+ "0")

        # Get User Str Editbox Search Bar. 
        SearchBar = self.GetString(self.StorageSearchBar).lower()
        if SearchBar == "":
            #print "Search Bar is empty."
            pass
        # Get the Recent Storage Folders Lic4d.storage.
        for each_path in self.RecentPath_List:

            for path in each_path.splitlines():
                Path_id = path.split('--')[0]
                Path_data = path.split('--')[1]

                if self.GetLong(self.Add_RecentPath_Menu) == int(Path_id):
                    #c4d.CallCommand(5159, 5159) # Cube
                    #print str(Path_id) + " - " + str(Path_data)
                    Folder = Path_data #"C:/Users/AP_Ashton_TheCreator/Desktop/testing" #"C:/Users/the_a/Desktop/testing"
                    if SearchBar == "":
                        self.Add_CreateList_Ids_and_GUI_Generator(Path=Folder, ListName=self.StorageFiles_List, Thum_id=100, Title_id=200, Desc=300, open_id=400, InjectId=500, DelId=600, GRPId=700, xmlext="*_C4D.xml")
                    else:
                        self.Add_CreateList_Ids_and_GUI_Generator(Path=Folder, ListName=self.StorageFiles_List, Thum_id=100, Title_id=200, Desc=300, open_id=400, InjectId=500, DelId=600, GRPId=700, xmlext=SearchBar+"*_C4D.xml")
        return True
    #   //   Adding to Files   //
    # Create thumbnail of the object, for gui when adding a new file to the project storage.
    def CreateThumbnail(self, doc, ObjName, icon_path):

        StoragePath = icon_path

        FileName = StoragePath + "/" + ObjName + "_Thum_C4D" + ".png"

        g_lib.C4D_CreateThumbnail_Image(imagePath=FileName,
                                        imageFormat=g_lib.ImageFilters.PNG
                                        )

        c4d.EventAdd()    
        return True
    def CreateDefaultThumbnail(self, doc, ObjName, icon_path):

        StoragePath = icon_path

        FileName = ObjName + "_Thum_C4D" + ".png"

        g_lib.C4D_DefaultLockThumbnail_Image(imagePath=FileName,
                                            imageTempPath=I(i='c4dfile'),
                                            path = StoragePath,
                                            imageName = g_lib.fcsDIR.GUI_Icons['c4dfile']
                                            )

        c4d.EventAdd()    
        return True       
    # Adding C4D object to the Project Storage Folder.    
    def Adding_Object_To_Storage(self):

        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False

        GetDocActiveUnit = doc.GetDocumentData(c4d.DOCUMENTSETTINGS_DOCUMENT)
        
        # Get Object form the Object Manager     
        obj = doc.GetActiveObject()
        if not obj:
            gui.MessageDialog("Select an Object!")
            return


        active_solo = jsonEdit.GetItemProperties(g_lib.fcsDIR.PSC4Djsonfile, "Settings", "Solo_Obj")
        if active_solo == True:
            c4d.CallCommand(431000060, 431000060)                 # Viewport Solo Hierarchy
            c4d.CallCommand(12151, 12151)                         # Center Object to Camera
        
        # Get c4d object name.
        Name = obj.GetName()

        # Get the Recent Storage Folders Lic4d.storage.
        for each_path in self.RecentPath_List:

            for path in each_path.splitlines():
                Path_id = path.split('--')[0]
                Path_data = path.split('--')[1]

                if self.GetLong(self.Add_RecentPath_Menu) == int(Path_id):


                    FolderPATH = Path_data

                    # 
                    # The Thumbnail of object.
                    c4dfileProjectThumbnail = jsonEdit.GetItemProperties(g_lib.fcsDIR.PSC4Djsonfile, "Settings", "ProjectThumbnail")
                    if c4dfileProjectThumbnail == True:
                        self.CreateThumbnail(doc=doc, ObjName=Name, icon_path=FolderPATH)
                    else:
                        self.CreateDefaultThumbnail(doc=doc, ObjName=Name, icon_path=FolderPATH)

                        
                    # Export Model C4D obj file and send to Stroage folder.
                    ExportPLUGIN_ID = self.C4D_Filter['export_id']
                    
                    objs = doc.GetSelection()
                    if objs == None:
                        print("No Objects Selected")
                        return
                    # Get a fresh Blank new document, temporary document with only the selected objects
                    docTemp = c4d.documents.IsolateObjects(doc, objs)
                    if docTemp == None:
                        return False
                    doc = c4d.documents.GetActiveDocument() # Get Doc again.    
                    # Set Document Data for the doc Temos.path.   <------------{PROBLEM HERE! \ It don't set data I have below}
                    docTemp.SetDocumentData(c4d.DOCUMENTSETTINGS_DOCUMENT, GetDocActiveUnit)
                    #  Find C4D Format Plugin.
                    plug = plugins.FindPlugin(ExportPLUGIN_ID, c4d.PLUGINTYPE_SCENESAVER)
                    if plug is None:
                        return

                    # Send file to user path.
                    Newpath = os.path.join(FolderPATH, Name+self.C4D_Filter['ext'])
                    ###########################################################
                    # Exporting Format
                    ########################################################### 
                    if c4d.documents.SaveDocument(docTemp, Newpath, c4d.SAVEDOCUMENTFLAGS_DONTADDTORECENTLIST, ExportPLUGIN_ID) == False:
                        c4d.gui.MessageDialog("Directory field is empty.")
                        print("Couldn't export, due to directory field is empty.")
                        return False
                    if os.path.exists(Newpath):
                        print(Newpath +" - Export Finish successfully")   
                    
                    # Make Xml fle wih data.
                    # Make Description Txt
                    DescriptionFile = os.path.join(FolderPATH, Name + "_C4D" +'.txt')
                    with open(DescriptionFile, 'w') as f:
                        f.write("")
                        f.close()                     
                    # Saving Preset Data to XML File.
                    MakeXml_root = minidom.Document()
                    AddToXML = MakeXml_root.createElement('PresetData')
                    MakeXml_root.appendChild(AddToXML)
                    AddTo = MakeXml_root.createElement('Preset')
                    AddTo.setAttribute('TitleName', Name)
                    AddTo.setAttribute('Description', DescriptionFile)
                    AddTo.setAttribute('Thumbnail_Image', Name + "_Thum_C4D" + ".png") 
                    AddToXML.appendChild(AddTo)
                    SaveXml = MakeXml_root.toprettyxml(indent="\t")
                    SavePresetData = os.path.join(FolderPATH, Name + "_C4D" +'.xml')
                    with open(SavePresetData, 'w') as f:
                        f.write(SaveXml)
                        f.close()
                    # 
        if active_solo == True:
            c4d.CallCommand(431000058, 431000058)                 # Viewport Solo Off            
        return True
    def PSC4D_ConfigFile(self):
        """ UI/UserInterface Save & Load Data Config State File """

        PSC4D_Config = {}
        
        """ 
        Create Save .Json Config File Stucture if the file exists. 
        """

        if not os.path.exists(g_lib.fcsDIR.PSC4Djsonfile):

            PSC4D_Config["Settings"]={"Solo_Obj":False, 
                                      "ProjectThumbnail":True, }
            
            jsonEdit.SaveFile(g_lib.fcsDIR.PSC4Djsonfile, PSC4D_Config)

        return True
    #  //  Create and Update GUI  //
    # GUI Dialog Layout
    def ProjectStorage_GUI_Layout(self):
        
        self.GroupBegin(self.StorageContainerGRP, c4d.BFH_MASK, 1, 0) # Main Group
        Vect = c4d.Vector(0.1875, 0.1875, 0.1875)
        self.SetDefaultColor(self.StorageContainerGRP, c4d.COLOR_BG, Vect)

        self.AddSeparatorH(0, flags=c4d.BFH_SCALEFIT)

        self.GroupBegin(0, c4d.BFH_MASK, 0, 1)
        self.AddStaticText(0, c4d.BFH_CENTER, 0, 0, " Your Storage Folder:")
        self.AddComboBox(self.Add_RecentPath_Menu, c4d.BFH_SCALEFIT, 250, 10)
        self.AddSeparatorV(0, flags=c4d.BFV_SCALEFIT) 

        refresh_icon_settings = {'id':self.Refresh_RecentPath_Menu, 'btn_look':c4d.BORDER_NONE, 'tip':"Refresh Storage List", 'icon':g_lib.fcsDIR.I(i='RefFolder'), 'clickable':True}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=refresh_icon_settings)       
        self.GroupEnd()

        self.AddSeparatorH(0, flags=c4d.BFH_SCALEFIT)

        self.GroupBegin(0, c4d.BFH_MASK, 0, 1)
        self.AddStaticText(0, c4d.BFH_CENTER, 0, 0, " Search")
        self.AddEditText(self.StorageSearchBar, c4d.BFH_SCALEFIT, 250, 10)
        self.AddSeparatorV(0, flags=c4d.BFV_SCALEFIT)


        refresh_icon_settings = {'id':self.Add_ObjToStorage, 'btn_look':c4d.BORDER_NONE, 'tip':"Add Object", 'icon':g_lib.fcsDIR.I(i='AddBox'), 'clickable':True}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=refresh_icon_settings)

        self.AddSeparatorV(0, flags=c4d.BFV_SCALEFIT)

        settings_icon_settings = {'id':self.SettingsBtn, 'btn_look':c4d.BORDER_NONE, 'tip':"Settings", 'icon':g_lib.fcsDIR.I(i='SettingsIcon'), 'clickable':True}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=settings_icon_settings)

        self.GroupEnd()

        self.AddSeparatorH(0, flags=c4d.BFH_SCALEFIT)

        self.ScrollGroupBegin(0, c4d.BFH_SCALEFIT, c4d.SCROLLGROUP_VERT|c4d.SCROLLGROUP_AUTOVERT, 450, 290)  # Scroll Group

        self.GroupBegin(self.PresetsContainerGRP, c4d.BFV_SCALEFIT, 1, 0)
        # Add Files in this Grouos.path.
        self.GroupEnd()

        self.GroupEnd() # Scroll Group

        self.AddSeparatorH(0, flags=c4d.BFH_SCALEFIT)

        # PROGRESSBAR
        self.Add_ProgressBar_GUI() 
        self.GroupEnd() # Main Group

        return True 
    # Add Settings Item.
    def add_item_(self, ui, ui_id, str_info, xml_data):
        add_item = ui
        CHECKBOX = '&c&'
        DISABLE_UI = '&d&' # Disable this item    

        if xml_data == True:
            add_item.InsData(ui_id, str_info + CHECKBOX)
        #elif xml_mode == "Disable":
        #    add_item.InsData(ui_id, str_info + DISABLE_UI)
        else:
            add_item.InsData(ui_id, str_info)
            
        return True
    # Generate GUI Eelements Or Update GUI
    def GenerateGUI(self, Group_Id):

        self.LayoutFlushGroup(self.PresetsContainerGRP) # Refresh Group UI

        self.GroupBegin(self.GRP_Files_Container, c4d.BFH_SCALEFIT, 1, 0)
        #Build_It = Build_GUI 

        self.Add_StorageFiles()

        self.GroupEnd() 

        self.LayoutChanged(Group_Id) # Update Group UI
        return True
    #  //  User Interaction with GUI Elements Fuctions for Command Fuction  //
    def MainGUI_Fuctions(self, id):
        # When user using the search bar to find a file.
        if id == self.StorageSearchBar:
            g_lib.fcsLog.DEBUG("Search Bar in Used", False, g_lib.DebugMode)
            self.StorageFiles_List = []
            self.GenerateGUI(Group_Id=self.StorageContainerGRP)

        
        if id == self.Refresh_RecentPath_Menu:
            g_lib.fcsLog.DEBUG("Refresh Button Pressed", False, g_lib.DebugMode)
            self.StorageFiles_List = []
            self.RecentPath_List = []
            self.Add_User_StorageFolders_to_ComboBoxMenu()

        # When user adding a object to the storage folder.
        if id == self.Add_ObjToStorage:
            g_lib.fcsLog.DEBUG("Adding Object Button Pressed", False, g_lib.DebugMode)
            self.StorageFiles_List = []
            self.Adding_Object_To_Storage()
            self.GenerateGUI(Group_Id=self.StorageContainerGRP)


        if id == self.SettingsBtn:
            g_lib.fcsLog.DEBUG("Settings Button Pressed", False, g_lib.DebugMode)

            xml_data = jsonEdit.GetItemProperties(g_lib.fcsDIR.PSC4Djsonfile, "Settings", "Solo_Obj")
            xml_data2 = jsonEdit.GetItemProperties(g_lib.fcsDIR.PSC4Djsonfile, "Settings", "ProjectThumbnail")

            IDM_MENU1 = c4d.FIRST_POPUP_ID
            IDM_MENU2 = c4d.FIRST_POPUP_ID+2

            menu = c4d.BaseContainer()

            self.add_item_(ui=menu, ui_id=IDM_MENU1, str_info="Auto Solo Object for Thumbnail", xml_data=xml_data)
            menu.InsData(0, '') # Append separator

            self.add_item_(ui=menu, ui_id=IDM_MENU2, str_info="Project Default Thumbnail", xml_data=xml_data2)
            menu.InsData(0, '') # Append separator

            # Finally show popup dialog
            result = gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)
            #fcsLog.DEBUG(result, False)
            
            if result == IDM_MENU1:

                if xml_data == True:
                    jsonEdit.EditItemProperties(g_lib.fcsDIR.PSC4Djsonfile, "Settings", "Solo_Obj", False)
                else:
                    jsonEdit.EditItemProperties(g_lib.fcsDIR.PSC4Djsonfile, "Settings", "Solo_Obj", True)

            if result == IDM_MENU2:
 
                if xml_data2 == True:
                    jsonEdit.EditItemProperties(g_lib.fcsDIR.PSC4Djsonfile, "Settings", "ProjectThumbnail", False)
                else:
                    jsonEdit.EditItemProperties(g_lib.fcsDIR.PSC4Djsonfile, "Settings", "ProjectThumbnail", True)

        # When user switching the storage folder paths that they want or choose.
        if id == self.Add_RecentPath_Menu:
            print("Drop down menu in Used")
            self.StorageFiles_List = []
            self.GenerateGUI(Group_Id=self.StorageContainerGRP)      

        return True 
    def StorageFile_GUI_Functions(self, id):

        StorageFiles = self.StorageFiles_List

        if StorageFiles:

            for Each_StorageFile in StorageFiles:

                E = Each_StorageFile

                # Get StorageFile Ids from Lic4d.storage.
                Numlink = E['DirNum']
                Thumbnail_ID = E['Thumbnail_ID']
                TitleID = E['TitleID']
                DescBoxID = E['DescBoxID']
                OpenBTN = E['OpenBTN']
                InjectBTN = E['InjectBTN']
                DelBTN =  E['DelBTN']
                ObjectName = E['ObjectName']
                FilePath = E['FilePath']
                Group_Id = E['Group_Id']
  

                if id == int(DescBoxID):
                    Info = self.GetString(int(DescBoxID))
                    DescInfoFile = os.path.join(FilePath, ObjectName + "_C4D" +'.txt')
                    if os.path.exists(DescInfoFile):
                        os.remove(DescInfoFile)
                    SaveOut_Desc = os.path.join(FilePath, ObjectName + "_C4D" +'.txt')
                    with open(SaveOut_Desc, 'w') as f:
                        f.write(Info)
                        f.close()                     

                # // To Open StorageFile in a new project. //
                if id == int(OpenBTN):
                    ObjectFile = os.path.join(FilePath, ObjectName +".c4d")
                    doc = c4d.documents.GetActiveDocument()
                    file_Load = c4d.documents.LoadFile(ObjectFile)
                    c4d.EventAdd()                    

                # // To Merge or Inject StorageFile to scene. //
                if id == int(InjectBTN):
                    ObjectFile = os.path.join(FilePath, ObjectName +".c4d")
                    doc = c4d.documents.GetActiveDocument()
                    file_Load = c4d.documents.MergeDocument(doc, ObjectFile, c4d.SCENEFILTER_OBJECTS | c4d.SCENEFILTER_MATERIALS | c4d.SCENEFILTER_MERGESCENE)
                    c4d.documents.BaseDocument(file_Load)
                    c4d.EventAdd()

                # // To Delete a StorageFile. //
                if id == int(DelBTN):

                    #Check if user sure if they want to delete file.
                    Ask = c4d.gui.QuestionDialog("Are you sure you want to delete this file from your Project Storage?")
                    if Ask == True:
                        # Delete Files
                        ObjectFile = os.path.join(FilePath, ObjectName +".c4d")
                        ThumbnailFile = os.path.join(FilePath, ObjectName + "_Thum_C4D" + ".png")
                        DescInfoFile = os.path.join(FilePath, ObjectName + "_C4D" +'.xml') 
                        DescInfoTXTFile = os.path.join(FilePath, ObjectName + "_C4D" +'.txt') 

                        SFile = [ ObjectFile, ThumbnailFile, DescInfoFile, DescInfoTXTFile]

                        files = SFile #glob.glob(SFile)
                        for filename in files:
                            os.remove(filename) 
                        # Refesh Files
                        self.StorageFiles_List = []
                        self.GenerateGUI(Group_Id=self.StorageContainerGRP)     

        return True 


 