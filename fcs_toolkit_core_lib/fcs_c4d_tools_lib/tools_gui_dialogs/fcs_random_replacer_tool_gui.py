"""
FCS Random Replacer Tool Window GUI Dialog
"""
#  // Imports for Cinema 4D //
import c4d
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as WindowDialog        # eg:( class My_Dialog(c4d.gui.GeDialog): ) to ( class My_Dialog(WindowDialog): ) Just keeping code line short.
from c4d.plugins import CommandData, TagData, ObjectData
from random import randint
# XML Imports is for Saving and Loading Project Data for Plugin UI / Xml good but long functions to make.
from xml.dom import minidom
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree

# Python Plugin System Load Modules Dynamically With importlib.
import importlib
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
#from res._fcs_lib.fcs_module_lib import fcs_common_global_lib as g_lib
fcsID = g_lib.fcsID
jsonEdit = g_lib.jsonEdit

class Random_Replacer_Dialog(WindowDialog):
    """ Random_Replacer  """
 #####  \\  Random_Replacer GUI IDs and Strings \\   #####
    Toggle_State = False


    ReplaceBTN = 104
    null = 105
    countBar = 106
    InProgress = 107
    PROGRESSBAR = 108
    ProgressAmount = 109
    AutoFliping = 110
    AutoBackup = 111
    RandomRot = 112
    SettingsBTN = 113
    TemplatesBTN = 114
    GRP_T = 115
    RandomizeCHK = 116

    AddSlot = 102
    SlotLayoutGroup = 103
    SlotsPanelsLayoutGroup = 101
    AddSlots_GrpBTN = 117
    ClearListBTN = 118
    addpreset_BTN = 131
    Settings_Menu = 129
    UtilsPanel = {'toggle_btn':150, 'insert_id':151, 'grp_id':152, 't_state_id':153}  
    PresetPanel = {'toggle_btn':154, 'insert_id':155, 'grp_id':156, 't_state_id':157}
    TagPanel = {'toggle_btn':158, 'insert_id':159, 'grp_id':160, 't_state_id':161}    
    SelModePanel = {'toggle_btn':162, 'insert_id':163, 'grp_id':164, 't_state_id':165}

    GroupSelected_CHK = 119
    webpageBTN = 120 
    AboutBTN = 121
    ItemType_EB = 122
    ItemType_BTN = 123 
    ItemType_BTN2 = 124   
    ItemType_Info = 125
    ItemType_COL = 126
    ItemSelCol_CHK = 127
    ToolTitle = 128
    ItemSel_COMBOBOX = 130
    preset_EB = 132


    AddSlot_preset_grp = 166

    TOP_MENU_IDs = { 'demo1_id':167, 'demo2_id':168, 'demo3_id':169, 'onlineM':170, 'offlineM':172, 'discord_id':173 }

    AmountSlotsData = []
    ItemTypeList = []

    def __init__(self):
        super(Random_Replacer_Dialog, self).__init__()
 #####  \\  Tool Core Method Functions Operations \\   #####

    # //   Generate GUI's and ID's | Main Functions  //
    
    # UI Layouts of Each Object in a LinkBox.
    def Slot_Temp_Layout(self, Numlink, LinkBox_id, DelBTN, GRP):
        self.CustomGui_UI = c4d.BaseContainer() 
        self.GroupBegin(int(GRP), c4d.BFH_SCALEFIT, 1, 0, "")
        self.GroupSpace(0, 0)
        Vect = c4d.Vector(0.125, 0.125, 0.125)
        self.SetDefaultColor(int(GRP), c4d.COLOR_BG, Vect)
        # 
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 0, 1, "")
        #self.AddStaticText(0, c4d.BFH_CENTER, 0, 0, Numlink + ":")
        preset_icon_settings = {'id':0, 'btn_look':c4d.BORDER_NONE, 'icon':g_lib.fcsDIR.I(i='rp_temp_tag_icon')}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=preset_icon_settings)
        self.AddSeparatorV(0, c4d.BFV_SCALEFIT)        
        self.AddCustomGui(int(LinkBox_id), c4d.CUSTOMGUI_LINKBOX, "9", c4d.BFH_SCALEFIT, 200, 10, self.CustomGui_UI)
        # Del BTN
        del_btn_settings = {'id':int(DelBTN), 'btn_look':c4d.BORDER_NONE, 'tip':"<b>Delete</b>", 'icon':g_lib.fcsDIR.I(i='rp_del_icon'), 'clickable':True}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=del_btn_settings)
        self.GroupEnd()
        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)
        self.GroupEnd()
        return True 
    def Slot_Preset_Layout(self, Numlink, LinkBox_id, DelBTN, GRP):
        self.CustomGui_UI = c4d.BaseContainer() 
        self.GroupBegin(int(GRP), c4d.BFH_SCALEFIT, 1, 0, "")
        self.GroupSpace(0, 0)
        self.SetDefaultColor(int(GRP), c4d.COLOR_BG, g_lib.BG_LitDarkBlue_Col)
        # 
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 0, 1, "")
        #self.AddStaticText(0, c4d.BFH_CENTER, 0, 0, Numlink + ":")
        preset_icon_settings = {'id':0, 'btn_look':c4d.BORDER_NONE, 'icon':g_lib.fcsDIR.I(i='rp_pre_grp_tag_icon')}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=preset_icon_settings)
        self.AddSeparatorV(0, c4d.BFV_SCALEFIT)
        self.AddCustomGui(int(LinkBox_id), c4d.CUSTOMGUI_LINKBOX, "9", c4d.BFH_SCALEFIT, 200, 10, self.CustomGui_UI)
        # Del BTN
        del_btn_settings = {'id':int(DelBTN), 'btn_look':c4d.BORDER_NONE, 'tip':"<b>Delete Directory</b>", 'icon':g_lib.fcsDIR.I(i='rp_del_icon'), 'clickable':True}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=del_btn_settings)
        self.GroupEnd()
        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)
        self.GroupEnd()
        return True 
    def Slot_PresetChild_Layout(self, LinkBox_id, DelBTN, GRP):
        self.CustomGui_UI = c4d.BaseContainer() 
        self.GroupBegin(int(GRP), c4d.BFH_SCALEFIT, 1, 0, "")
        self.GroupSpace(0, 0)
        self.SetDefaultColor(int(GRP), c4d.COLOR_BG, g_lib.BG_LitDarkBlue_Col)
        # 
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 0, 1, "")
        #self.AddStaticText(0, c4d.BFH_CENTER, 0, 0, Numlink + ":")
        self.AddCustomGui(int(LinkBox_id), c4d.CUSTOMGUI_LINKBOX, "9", c4d.BFH_SCALEFIT, 200, 10, self.CustomGui_UI)
        # Del BTN
        #del_btn_settings = {'id':int(DelBTN), 'btn_look':c4d.BORDER_NONE, 'tip':"<b>Delete Directory</b>", 'icon':I(i='DelTrash'), 'clickable':True}
        #g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=del_btn_settings)
        self.GroupEnd()
        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)
        self.GroupEnd()

        self.HideElement(int(GRP), True) # Hide
        return True 

    # Generate IDs for files or GUI.
    def CreateUI_ID(self, Num):
        self.newfieldindex += 1
        for newid in range(1, self.newfieldindex+1):
            newid = self.newfieldindex
            idi = newid + Num
        new_id = idi
        #print new_id
        return new_id

    # Generate Degits
    def CreateDegit_Str(self):
        new_id = degit+1 
        #print str(new_id)    
        return new_id

    # Add GUI Links
    def Create_Slot(self):

        degit = 0

        if self.AmountSlotsData:

            for each_slot in self.AmountSlotsData:

                doc = c4d.documents.GetActiveDocument()

                degit+=1

                Numlink = str(degit)

                ObjName = each_slot.split('--')[0] 
                ObjId = each_slot.split('--')[1]
                LinkBox_id = each_slot.split('--')[2]
                DelBTN = each_slot.split('--')[3]
                GRP = each_slot.split('--')[4]
                GetGUI_type = each_slot.split('--')[5]
                GetGUI_ID = each_slot.split('--')[6]

                if GetGUI_type == "Normal":
                    self.Slot_Temp_Layout(Numlink, LinkBox_id, DelBTN, GRP)
                    print("Slot - " + str(Numlink) + " | " + "Linked Object Name: " + ObjName + " = " + GetGUI_type + ", Temp ID = " +  GetGUI_ID) 

                if GetGUI_type == "PresetGrp":
                    self.Slot_Preset_Layout(Numlink, LinkBox_id, DelBTN, GRP)
                    print("Slot - " + str(Numlink) + " | " + "Linked Object Name: " + ObjName + " = " + GetGUI_type + ", Group ID = " +  GetGUI_ID)

                if GetGUI_type == "PresetChild":
                    self.Slot_PresetChild_Layout(LinkBox_id, DelBTN, GRP) 
                    print("Slot - " + "Child" + " | " + "Linked Object Name: " + ObjName + " = " + GetGUI_type + ", Child Of Group ID = " +  GetGUI_ID)              

                linkBox = self.FindCustomGui(int(LinkBox_id), c4d.CUSTOMGUI_LINKBOX)
                linkBox.SetLink(doc.SearchObject(ObjName))

            #self.degit = 0

        #SlotAdded =  str(SlotId)
        #self.AmountSlotsData.append(SlotAdded)
        # Added_Link = LinkNum + "--" + objname + "--" + objid + "--" + linkboxId + "--" + SlotDel
        return True

    # To ReBuild and Refresh UI Elements function.  
    def _ReBuild_ReFresh_GUIs_(self):
        # ReBuild and Refresh TreeViews
        self.LayoutFlushGroup(id=self.SlotsPanelsLayoutGroup) # Refresh Group Area with UI Elements.

        self.Create_Slot()

        self.LayoutChanged(id=self.SlotsPanelsLayoutGroup)

        return True 

    # Adding Slots
    def AddingSlotsGUI(self):
        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False

        # Get Objects form the Object Manager     
        list_objs = doc.GetActiveObjects(1)
        if not list_objs:
            gui.MessageDialog("Please select an Object!") 
            return

        for E in list_objs:
            objname = str(E.GetName())
            objid = str(E.GetGUID())
            linkboxId = str(self.CreateUI_ID(Num=19630))
            SlotDel = str(self.CreateUI_ID(Num=26630))
            GRPId = str(self.CreateUI_ID(Num=37630))
            item_type = "Normal"
            Temp_ID = str(self.CreateUI_ID(Num=49630))
            Added_Link = objname + "--" + objid + "--" + linkboxId + "--" + SlotDel + "--" + GRPId + "--" + item_type + "--" + Temp_ID
            self.AmountSlotsData.append(Added_Link)  
            #print "Link" + "(" + LinkNum + ")" + "-" + Added_Link  
            
        # Refresh Panel
        self._ReBuild_ReFresh_GUIs_()        
        return True

    # Adding Slots Group
    def AddingSlotsGUI_FromGroup(self):

        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False

        G = doc.GetActiveObject()
        if not G:
            gui.MessageDialog("Please select an Object!")
            return

        c4d.CallCommand(100004768, 100004768) # Select Children   

        doc.SetActiveObject(G, c4d.SELECTION_SUB)
  
        # Get Objects form the Object Manager     
        list_objs = doc.GetSelection()
        if not list_objs:
            gui.MessageDialog("Please select an Object!")
            return
        for E in list_objs:
            objname = str(E.GetName())
            objid = str(E.GetGUID())
            linkboxId = str(self.CreateUI_ID(Num=9630))
            SlotDel = str(self.CreateUI_ID(Num=6630))
            GRPId = str(self.CreateUI_ID(Num=7630))
            item_type = "Normal"
            Temp_ID = str(self.CreateUI_ID(Num=8630))
            Added_Link = objname + "--" + objid + "--" + linkboxId + "--" + SlotDel + "--" + GRPId + "--" + item_type + "--" + Temp_ID
            self.AmountSlotsData.append(Added_Link)  
            #print "Link" + "(" + LinkNum + ")" + "-" + Added_Link

        # Refresh Panel
        self._ReBuild_ReFresh_GUIs_() 
        c4d.CallCommand(100004767, 100004767) # Deselect All 
        return True  

    # Adding Preset Group
    def AddingSlotsGUI_PresetGroup(self):

        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False

        g_obj = doc.GetActiveObject()
        if not g_obj:
            g_lib.SendInstructions(Message="Please select an Object!")
            return

        else:

            if g_obj.CheckType(c4d.Onull):

                CheckChkTag = g_obj.GetTag(1050361)
                if not CheckChkTag:
                    g_lib.SendInstructions(Message="Please add a preset-group tag.\nTo add the preset-group tag go the Tag Utils panel\nthat is in the Random Re-Placer tool window.")
                    return

                objname = str(g_obj.GetName())
                objid = str(g_obj.GetGUID())
                linkboxId = str(self.CreateUI_ID(Num=96300))
                SlotDel = str(self.CreateUI_ID(Num=66300))
                GRPId = str(self.CreateUI_ID(Num=76300))
                item_type = "PresetGrp"
                PresetGrp_ID = str(self.CreateUI_ID(Num=86300))
                Added_Link = objname + "--" + objid + "--" + linkboxId + "--" + SlotDel + "--" + GRPId + "--" + item_type + "--" + PresetGrp_ID
                self.AmountSlotsData.append(Added_Link)            
                
                doc.SetActiveObject(g_obj)
                
                active_obj = doc.GetActiveObject()

                c4d.CallCommand(100004768, 100004768) # Select Children   
                
                doc.SetActiveObject(active_obj, c4d.SELECTION_SUB)

                # Get Objects form the Object Manager     
                list_objs = doc.GetSelection()
                for E in list_objs:
                    objname = str(E.GetName())
                    objid = str(E.GetGUID())
                    linkboxId = str(self.CreateUI_ID(Num=906300))
                    SlotDel = str(self.CreateUI_ID(Num=606300))
                    GRPId = str(self.CreateUI_ID(Num=706300))
                    item_type = "PresetChild"
                    PresetGrp_TagID = PresetGrp_ID
                    Added_Link = objname + "--" + objid + "--" + linkboxId + "--" + SlotDel + "--" + GRPId + "--" + item_type + "--" + PresetGrp_TagID
                    self.AmountSlotsData.append(Added_Link)
                    #print "Link" + "(" + LinkNum + ")" + "-" + Added_Link
            else:
                pass

        # Refresh Panel
        self._ReBuild_ReFresh_GUIs_() 
        c4d.CallCommand(100004767, 100004767) # Deselect All 
        return True

    def Remove_All_Matching_Items_IDs_From_List(self, main_list, match_id, item_num):

        list_amount = len(main_list)

        ranger_list = range(list_amount)

        for item in reversed(ranger_list):

            list_Item_ID = main_list[item].split('--')[item_num]

            if list_Item_ID == match_id:

                del main_list[item]

        return True

    # Delete Slots
    def DeleteSlotBTN(self, id, SlotItem):
        each_slot = SlotItem
        # Check for which Delete Button been press.
        DeleteId = each_slot.split('--')[3]
        GetGUI_type = each_slot.split('--')[5]
        Grp_PresetID = each_slot.split('--')[6]
        # Turn string Id to Int.
        Delete_BTN = int(DeleteId)
        if id == Delete_BTN:
            if GetGUI_type == "PresetGrp":
                # Remove Slot from lic4d.storage.
                self.Remove_All_Matching_Items_IDs_From_List(main_list=self.AmountSlotsData, match_id=Grp_PresetID, item_num=6)

            if GetGUI_type == "Normal":
                # Get Slot Id from where the Delete is from.
                #NumStr = each_slot.split('--')[1]
                # Remove Slot from lic4d.storage.
                self.AmountSlotsData.remove(each_slot)

            # Refresh Panel
            self._ReBuild_ReFresh_GUIs_()
            #print "\n\nJust Deleted - " + DeleteId + " - " + "Slot Deleted" + "(" + NumStr + ") - Old Data: " + each_slot + "\n"
            # Show New Lic4d.storage.
            Newlist = self.AmountSlotsData
            for e in Newlist:
                print(e)       
        return True

    # Random Rotation Function (If Checked Run this)
    def RandomRotation(self, ReplacerObj):

        OBJ_Rot = ReplacerObj.GetRelRot()
        removeOpenBracket = str(OBJ_Rot).split('(')
        removeCloseBracket = removeOpenBracket[1].split(')')
        values = removeCloseBracket[0].split(',')
        H_value = float(values[0])
        P_value = float(values[1])
        B_value = float(values[2])
        
        #print str(H_value)
        
        #rad = H_value
        RamdomValue1 = 4.241
        RamdomValue2 = 1.096
        RamdomValue3 = 2.265
        RamdomValue4 = 6.016
        RamdomValue5 = 2.695
        RamdomValue6 = 5.358     
        RamdomValue7 = 7.648
        RamdomValue8 = 3.458
        RamdomValue9 = 4.483                         
        RandomRadList = [H_value, H_value, RamdomValue1, RamdomValue2, RamdomValue3, RamdomValue3, RamdomValue4, RamdomValue5, RamdomValue6, RamdomValue7, RamdomValue8, H_value, RamdomValue9 ]

        H = random.choice(RandomRadList)

        deg = H*180/math.pi
        
        new_deg = deg + 180
        
        newvalue = round(new_deg)
        
        #print "RAD: " + str(H_value) + "  /  " + "DEG: " +str(deg) + "  /  " + "NewDEG: " + str(new_deg) + " = " + str(newvalue)
        
        #c4d.utils.RadToDeg(r)
    
        New_H_value = c4d.utils.DegToRad(newvalue)

        SetRot = c4d.Vector(New_H_value, P_value, B_value)

        ReplacerObj.SetRelRot(SetRot) 
        c4d.EventAdd()        
        return True

    # Auto Flipping Function (If Checked Run this)
    def AutoFlip(self, ReplacerObj):
        doc = c4d.documents.GetActiveDocument()
        OBJ_Rot = ReplacerObj.GetRelRot()
        removeOpenBracket = str(OBJ_Rot).split('(')
        removeCloseBracket = removeOpenBracket[1].split(')')
        values = removeCloseBracket[0].split(',')
        H_value = float(values[0])
        P_value = float(values[1])
        B_value = float(values[2])
        
        #print str(H_value)
        
        rad = H_value
        
        deg = H_value*180/math.pi
        
        new_deg = deg + 180

        newvalue = round(new_deg)
        
        #print "RAD: " + str(H_value) + "  /  " + "DEG: " +str(deg) + "  /  " + "NewDEG: " + str(new_deg) + " = " + str(newvalue)
        
        #c4d.utils.RadToDeg(r)
    
        H = c4d.utils.DegToRad(newvalue)

        AutoFlipList = [ H, H_value, H_value ]

        New_H_value = random.choice(AutoFlipList)
        
        SetRot = c4d.Vector(New_H_value, P_value, B_value)
        
        #print str(SetRot)
        
        ReplacerObj.SetRelRot(SetRot) 
        c4d.EventAdd()        
        return True

    # Ramdom Replacer Main Core Fuction 
    def RamdomObjects(self):
        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False

        # Get Objects form the Object Manager     
        list_objs = doc.GetActiveObjects(1)
        if not list_objs:
            gui.MessageDialog("Please select an Object!")
            return

        items = self.AmountSlotsData
        amountOfObjects = len(list_objs)
        currentNum = 1

        # Auto Backup

        XML_DATA_FILES = g_lib.fcsDIR.FCS_RRP_CONFIG_SETTINGS
        XmlFileTree = etree.parse(XML_DATA_FILES) 
        XmlRoot = XmlFileTree.getroot()
        for rp_data in XmlRoot.findall('Settings_Data'):
            
            xml_AB = rp_data.get("CBC")
            xml_RR = rp_data.get("RR")
            xml_Random = rp_data.get("Random")
            xml_AF = rp_data.get("AF")
            xml_GS = rp_data.get("GS")            

            if xml_AB == "True":
                # Create A Backup Group and Disabe It. # For User Undo
                CreateGroup = c4d.BaseObject(c4d.Onull)
                CreateGroup[c4d.ID_BASEOBJECT_VISIBILITY_RENDER] = 1
                CreateGroup[c4d.ID_BASEOBJECT_VISIBILITY_EDITOR] = 1
                CustomNullName = doc.SearchObject("Replacer_Backup"+".300000000")
                if CustomNullName == None:
                    CustomNullName = "Replacer_Backup"
                degit = +1
                CreateGroup.SetName(CustomNullName + "." + str(degit))
                for N in list(reversed(range(100))):
                    obj = doc.SearchObject(CustomNullName+ "." + str(N)) 
                    if obj == None:
                        CreateGroup.SetName(CustomNullName + "." + str(N))
                doc.InsertObject(CreateGroup)        


            for E in list_objs:

                # \\  Progress Log Area  \\
                # Auto Backup
                if xml_AB == "True":
                    BackupOBj = E.GetClone(c4d.COPYFLAGS_0)
                    BackupOBj.InsertUnder(CreateGroup)

                # Show To Log
                self.SetString(self.InProgress,"In Progress") 
                self.SetString(self.countBar,"Object Count: " + str(currentNum)+" : "+ str(amountOfObjects))
                c4d.EventAdd()

                # Prcoess Bar
                percent = float(currentNum)/amountOfObjects*100
                print(str(percent))
                # Set Data to PROGRESSBAR
                progressMsg = c4d.BaseContainer(c4d.BFM_SETSTATUSBAR)
                progressMsg[c4d.BFM_STATUSBAR_PROGRESSON] = True
                progressMsg[c4d.BFM_STATUSBAR_PROGRESS] = percent/100.0 
                self.SendMessage(self.PROGRESSBAR, progressMsg)
                self.SetString(self.ProgressAmount, str(int(percent))+"%")            

                currentNum += 1          

                # If the selected object is a Null object then "Contuine Process".
                #if E.GetType() == c4d.Opolygon: 
                if xml_Random == "True":
                    # This is Random Picker Function picks different objs temps from list that as been made.
                    RandomItemPick  = random.choice(items)
                    objname = RandomItemPick.split('--')[0]
                    LinkTempObj = int(RandomItemPick.split('--')[2])
                    # Print what as been done so far.
                    #print str(LinkTempObj)+"-"+objname

                    # This Get the Linked Obj Temp Replacer from the Linkbox UI. 
                    MainLinkObj = self.FindCustomGui(LinkTempObj, c4d.CUSTOMGUI_LINKBOX).GetLink()
                    if MainLinkObj is None:
                        return

                    CHK_TAG = MainLinkObj.GetTag(1050361)
                    if CHK_TAG:
                        pass # pass this model

                    else:

                        # This Get the TempReplacerObj Name to find it in the C4D Object List Manger.
                        GetTemp = MainLinkObj.GetName()

                        # Get the Selected Object Name that is going to be Replace.
                        get_placerName = E.GetName()

                        # Get Local Position Data From Object 
                        local = E.GetMg()

                        # Get Object From the Linkbox and Name of Object and Search for Object in the C4D Object List Manger. 
                        get_replacer = doc.SearchObject(GetTemp)

                        # Get the Temp object Name in the Linkbox that is going to be Replacer Object.
                        get_replacerName = get_replacer.GetName()

                        # Set Object Active
                        fineobj = doc.SetActiveObject(get_replacer)

                        # Get Selection of the ActiveObject which is the Replacer.
                        get = doc.GetSelection()
                        for i in get:
                            # Make a clone or copy of the replacer that is in the Linkbox. 
                            LinkObj = i.GetClone(c4d.COPYFLAGS_0)
                            # Set the Local Position Data from the Selected Object.
                            LinkObj.SetMg(local)

                            # Set Name of the Replacer from the Selected Object.
                            LimitName = doc.SearchObject(get_replacerName + "." +"3000000")
                            if LimitName == None:
                                LimitName = get_replacerName + "." + "0"

                            degit = int(filter(str.isdigit, LimitName))+1

                            LinkObj.SetName(get_replacerName + "." + str(degit))

                            for N in list(reversed(range(300))):
                                findobj = doc.SearchObject(get_replacerName + "." + str(N)) 
                                if findobj == None:            
                                    LinkObj.SetName(get_replacerName + "." + str(N))

                            # Insert the Replacer in C4D Object List Manger.
                            doc.InsertObject(LinkObj)

                            # Switching Tags and with settings
                            tempTag = LinkObj.GetTag(1050362)
                            if tempTag:
                                # Get Random RePlacer Temp Tag Settings
                                GET_ITEM_TYPE = tempTag[c4d.FCS_RP_TEMP_TAG_ITEMTYPE]
                                GET_ITEM_COL1 = tempTag[c4d.FCS_RP_TEMP_TAG_COLOR_1]
                                # Add The Random RePlacer Select Tag and add Temp Tag Settings
                                tagID = 1050211 # Random RePlacer Select Tag ID
                                ItemType_Tag = c4d.BaseTag(tagID)
                                ItemType_Tag[c4d.FCS_RP_TAG_ITEMTYPE] = GET_ITEM_TYPE
                                ItemType_Tag[c4d.FCS_RP_TAG_COLOR_1] = GET_ITEM_COL1          
                                LinkObj.InsertTag(ItemType_Tag)
                                # Remove Tag when done
                                tempTag.Remove()
                                c4d.EventAdd()
                            else:
                                pass

                            # Insert the Replacer under Selected Object.
                            LinkObj.InsertUnder(E)
                            # Set Replacer Object Active
                            doc.SetActiveObject(LinkObj)
                            # Reset Replacer Local Position Data 
                            c4d.CallCommand(1019940) # Reset PSR
                            # Set Selected Object Active
                            doc.SetActiveObject(E)
                            # Delete Selected Object but not Replacer 
                            c4d.CallCommand(1019951, 1019951) # Delete Without Children 

                            # Run Function Event
                            c4d.EventAdd()

                            # Auto Flipping
                            if xml_AF == "True":
                                # Auto Fliping
                                # Select and set obj as active.
                                doc.SetActiveObject(LinkObj)
                                obj = doc.GetActiveObject()
                                self.AutoFlip(ReplacerObj=obj)
                                c4d.EventAdd()

                            # Random Rotation
                            if xml_RR == "True":
                                # Auto Fliping
                                # Select and set obj as active.
                                doc.SetActiveObject(LinkObj)
                                obj = doc.GetActiveObject()
                                self.RandomRotation(ReplacerObj=obj)
                                # Run Function Even
                                c4d.EventAdd()
                else:

                    # Auto Flipping
                    if xml_AF == "True":
                        self.AutoFlip(ReplacerObj=E)
                        c4d.EventAdd()

                    # Random Rotation
                    if xml_RR == "True":
                        self.RandomRotation(ReplacerObj=E)
                        # Run Function Even
                        c4d.EventAdd()

        self.SetString(self.InProgress,"Completed!")
        # Run Function Event
        c4d.EventAdd()            
        return True

    # Ramdomzie Button Fuction
    def RandomizeBTN(self):
        XML_DATA_FILES = g_lib.fcsDIR.FCS_RRP_CONFIG_SETTINGS
        XmlFileTree = etree.parse(XML_DATA_FILES) 
        XmlRoot = XmlFileTree.getroot()
        for rp_data in XmlRoot.findall('Settings_Data'):
            
            xml_data = rp_data.get("GS")

            if xml_data == "True":
                doc = c4d.documents.GetActiveDocument()
                if doc == None:
                    return False

                G = doc.GetActiveObject()
                if not G:
                    gui.MessageDialog("Please select an Object!")
                    return

                c4d.CallCommand(100004768, 100004768) # Select Children   

                doc.SetActiveObject(G, c4d.SELECTION_SUB)

                self.RamdomObjects()

            else:      
                self.RamdomObjects()
        return True         

    # Toggle Checkbox Functions
    def CheckBoxToggle(self, Mode):
        # This Method function works like this, to pass data or info to this function:
        # Toggle = { "Checked":[ID Info], "Disable":[ID Info]} 
        # self.CheckBoxToggle(Mode=Toggle)

        if self.GetBool(Mode["Checked"]) == True:
            self.SetBool(Mode["Disable"], False)
            self.Enable(Mode["Disable"], False)
        else:
            self.Enable(Mode["Disable"], True)        
        return True

    # Add ItemType Tag
    def Add_Tag_to_Object(self):
        # Get the Live Active Document
        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False

        # Get all active children objects of the "SelectedRootNode".
        SelectedChildrenObjects = doc.GetActiveObjects(1)
        if not SelectedChildrenObjects:
            print("No ojects selected")
            return

        # Get str from Edit Text box.
        str_data = self.GetString(self.ItemType_EB)
        if str_data == "":
            gui.MessageDialog("Please enter a (Item Name Type) for the Tag")
            return

        userColor = self.GetColorField(self.ItemType_COL)
        #print userColor

        col = userColor['color']
        #print col

        removeOpenBracket = str(col).split('(')
        removeCloseBracket = removeOpenBracket[1].split(')')
        values = removeCloseBracket[0].split(',')
        val1 = float(values[0])
        val2 = float(values[1])
        val3 = float(values[2])

        # If Models Selected, Then Run Progress. 
        for each_anim_obj in SelectedChildrenObjects:        
            tagID = 1050362 # The Random RePlacer Template Tag ID
            ItemType_Tag = c4d.BaseTag(tagID)
            ItemType_Tag[c4d.FCS_RP_TEMP_TAG_ITEMTYPE] = str_data
            ItemType_Tag[c4d.FCS_RP_TEMP_TAG_COLOR_1] = c4d.Vector(val1, val2, val3)           
            each_anim_obj.InsertTag(ItemType_Tag)
        c4d.EventAdd()          
        return True

    # Add Preset-Group Tag
    def Add_PresetTag_to_Object(self):
        # Get the Live Active Document
        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False

        # Get all active children objects of the "SelectedRootNode".
        SelectedChildrenObjects = doc.GetActiveObjects(1)
        if not SelectedChildrenObjects:
            print("No ojects selected")
            return

        # If Models Selected, Then Run Progress. 
        for each_anim_obj in SelectedChildrenObjects:        
            tagID = 1050361 # The Random RePlacer Template Tag ID
            ItemType_Tag = c4d.BaseTag(tagID)        
            each_anim_obj.InsertTag(ItemType_Tag)
        c4d.EventAdd()          
        return True

    # Selection of Objects with ItemTypes
    def Get_ItemTypes(self):
        # Get the Live Active Document
        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False

        # Get str from Edit Text box.
        #str_data = self.GetString(self.ItemType_EB)
        #if str_data == "":
            #gui.MessageDialog("Please enter a (Item Name Type) for the Tag")
            #return
        c4d.CallCommand(12112, 12112) # Select All
        c4d.CallCommand(100004768, 100004768) # Select Children

        if self.ItemTypeList:

            for i in self.ItemTypeList:

                item_id = i.split('--')[0]
                item_str = i.split('--')[1]

                if self.GetLong(self.ItemSel_COMBOBOX) == int(item_id):

                    c4d.CallCommand(12112, 12112) # Select All
                    c4d.CallCommand(100004768, 100004768) # Select Children


                    # Get all active children objects of the "SelectedRootNode".
                    SelectedChildrenObjects = doc.GetActiveObjects(1)
                    if not SelectedChildrenObjects:
                        print("No ojects selected")
                        return

                    # If Models Selected, Then Run Progress. 
                    for each_anim_obj in SelectedChildrenObjects:
                        
                        RP_TAG = each_anim_obj.GetTag(1050211)
                        
                        if RP_TAG: 

                            e = RP_TAG[c4d.FCS_RP_TAG_ITEMTYPE]#[c4d.ID_RP_Type_FIELD]

                            if e == item_str:
                                
                                doc = c4d.documents.GetActiveDocument()
                                doc.SetActiveObject(each_anim_obj, c4d.SELECTION_ADD)
                            else:
                                doc.SetActiveObject(each_anim_obj, c4d.SELECTION_SUB)
                        else:
                            doc.SetActiveObject(each_anim_obj, c4d.SELECTION_SUB)
        c4d.EventAdd()        
        return True

    # Show ItemTypes Selection of Objects Color
    def Show_ItemTypes_Color(self):
        # Get the Live Active Document
        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False

        if self.ItemTypeList:

            for i in self.ItemTypeList:

                for item in i.splitlines():

                    item_id = item.split('--')[0]
                    item_str = item.split('--')[1]

                    if self.GetLong(self.ItemSel_COMBOBOX) == int(item_id):

                        c4d.CallCommand(12112, 12112) # Select All
                        c4d.CallCommand(100004768, 100004768) # Select Children

                        # Get all active children objects of the "SelectedRootNode".
                        SelectedChildrenObjects = doc.GetActiveObjects(1)
                        if not SelectedChildrenObjects:
                            print("No ojects selected")
                            return

                        # If Models Selected, Then Run Progress. 
                        for each_anim_obj in SelectedChildrenObjects:
                            
                            RP_TAG = each_anim_obj.GetTag(1050211)
                            
                            if RP_TAG: 

                                e = RP_TAG[c4d.FCS_RP_TAG_ITEMTYPE]#[c4d.ID_RP_Type_FIELD]
                                col = RP_TAG[c4d.FCS_RP_TAG_COLOR_1]
                                
                                if e == item_str:

                                    if self.GetBool(self.ItemSelCol_CHK) == True:
                                        doc = c4d.documents.GetActiveDocument()
                                        each_anim_obj[c4d.ID_BASEOBJECT_USECOLOR] = 2
                                        each_anim_obj[c4d.ID_BASEOBJECT_COLOR] = col                                        
                                else:
                                    each_anim_obj[c4d.ID_BASEOBJECT_USECOLOR] = 0
                                    doc.SetActiveObject(each_anim_obj, c4d.SELECTION_SUB)
                            else:
                                doc.SetActiveObject(each_anim_obj, c4d.SELECTION_SUB)

        c4d.EventAdd()        
        return True
    def Hide_ItemTypes_Color(self):
        # Get the Live Active Document
        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False

        c4d.CallCommand(12112, 12112) # Select All
        c4d.CallCommand(100004768, 100004768) # Select Children


        if self.ItemTypeList:

            for i in self.ItemTypeList:

                item_id = i.split('--')[0]
                item_str = i.split('--')[1]

                if self.GetLong(self.ItemSel_COMBOBOX) == int(item_id):                     

                    c4d.CallCommand(12112, 12112) # Select All
                    c4d.CallCommand(100004768, 100004768) # Select Children                    

                    # Get all active children objects of the "SelectedRootNode".
                    SelectedChildrenObjects = doc.GetActiveObjects(1)
                    if not SelectedChildrenObjects:
                        print("No ojects selected")
                        return

                    # If Models Selected, Then Run Progress. 
                    for each_anim_obj in SelectedChildrenObjects:
                        
                        RP_TAG = each_anim_obj.GetTag(1050211)
                        
                        if RP_TAG: 

                            e = RP_TAG[c4d.FCS_RP_TAG_ITEMTYPE]#[c4d.ID_RP_Type_FIELD]

                            if e == item_str:
                                
                                doc = c4d.documents.GetActiveDocument()
                                each_anim_obj[c4d.ID_BASEOBJECT_USECOLOR] = 0

                            else:
                                each_anim_obj[c4d.ID_BASEOBJECT_USECOLOR] = 0
                                doc.SetActiveObject(each_anim_obj, c4d.SELECTION_SUB)
                        else:
                            each_anim_obj[c4d.ID_BASEOBJECT_USECOLOR] = 0
                            doc.SetActiveObject(each_anim_obj, c4d.SELECTION_SUB)             
        c4d.EventAdd()        
        return True

    ###########################################
    # Settings UI Popup Menu with a Checklist
    ###########################################
    # Create .XML config.. settings file.
    def CreatConfigFile(self):
        if not os.path.exists(g_lib.fcsDIR.FCS_RRP_CONFIG_SETTINGS):
            # //  Create Xml Structure. //
            MakeXml_root = minidom.Document()
            AddToXML = MakeXml_root.createElement('FCS_RP_CONIG')
            MakeXml_root.appendChild(AddToXML)
            
            rpData = MakeXml_root.createElement('Settings_Data')
            # Toggle Data
            rpData.setAttribute('Random', "True")
            rpData.setAttribute('AF', "False")
            rpData.setAttribute('RR', "False")
            rpData.setAttribute('GS', "False")
            rpData.setAttribute('CBC', "False")

            rpData.setAttribute('R_Mode', "Enable")
            rpData.setAttribute('AF_Mode', "Enable")
            rpData.setAttribute('RR_Mode', "Enable")
            rpData.setAttribute('GS_Mode', "Enable")
            rpData.setAttribute('CBC_Mode', "Enable")

            AddToXML.appendChild(rpData)
            
            # Save XML CONIG
            SaveXml = MakeXml_root.toprettyxml(indent="\t")
            with open(g_lib.fcsDIR.FCS_RRP_CONFIG_SETTINGS, 'w') as f:
                f.write(SaveXml)
                f.close()
        return True  
    # Toggle the .xml file by editing it function.
    def Toggle(self, State, store_state):
        # Parse the xml file and get the data (Creates an object of the xml file to minipulate)
        tree = etree.parse(g_lib.fcsDIR.FCS_RRP_CONFIG_SETTINGS)
        # Now Edit the xml attribute by providing the tree elementName, attribute value and the new value in a string
        g_lib.fcsEngine.EditXmlAttribute(tree,"Settings_Data", store_state, str(State))
        # Now lets get the root of the xml
        root = tree.getroot()
        # Fix the xml structure from the root level
        g_lib.fcsEngine.indent(root)
        # and finally save the xml to the file
        with open(g_lib.fcsDIR.FCS_RRP_CONFIG_SETTINGS, 'w') as f:
            f.write(etree.tostring(root))
        return
    # Check item when toggle.
    def Checked_Toggle(self, store_state):
        XML_DATA_FILES = g_lib.fcsDIR.FCS_RRP_CONFIG_SETTINGS
        XmlFileTree = etree.parse(XML_DATA_FILES) 
        XmlRoot = XmlFileTree.getroot()
        for rp_data in XmlRoot.findall('Settings_Data'):
            
            xml_data = rp_data.get(store_state)
            if xml_data == "True":
                self.Toggle(State=False, store_state=store_state)

            else:
                self.Toggle(State=True, store_state=store_state)
        return True
    def Checked_ModeToggle(self, store_mode):
        XML_DATA_FILES = g_lib.fcsDIR.FCS_RRP_CONFIG_SETTINGS
        XmlFileTree = etree.parse(XML_DATA_FILES) 
        XmlRoot = XmlFileTree.getroot()
        for rp_data in XmlRoot.findall('Settings_Data'):

            xml_mode = rp_data.get(store_mode)
            if xml_mode == "Enable":
                self.Toggle(State="Disable", store_state=store_mode)
            else:
                self.Toggle(State="Enable", store_state=store_mode)
        return True
    # Add Settings Item.
    def add_item_(self, ui, ui_id, str_info, store_state, store_mode):
        add_item = ui
        CHK = '&c&'
        DISABLE_UI = '&d&' # Disable this item        

        XML_DATA_FILES = g_lib.fcsDIR.FCS_RRP_CONFIG_SETTINGS
        XmlFileTree = etree.parse(XML_DATA_FILES) 
        XmlRoot = XmlFileTree.getroot()
        for rp_data in XmlRoot.findall('Settings_Data'):
            
            xml_data = rp_data.get(store_state)
            xml_mode = rp_data.get(store_mode)

            if xml_data == "True":
                add_item.InsData(ui_id, str_info + CHK)

            elif xml_mode == "Disable":
                add_item.InsData(ui_id, str_info + DISABLE_UI)
                
            else:
                add_item.InsData(ui_id, str_info)
        return True
    # Create Popup Settings Menu.     
    def PopupSettings_Checklist(self):
        # Declare menu items ID
        IDM_MENU1 = c4d.FIRST_POPUP_ID
        IDM_MENU2 = c4d.FIRST_POPUP_ID+1
        IDM_MENU3 = c4d.FIRST_POPUP_ID+2
        IDM_MENU4 = c4d.FIRST_POPUP_ID+3
        IDM_MENU5 = c4d.FIRST_POPUP_ID+4                
        
        # Main menu
        menu = c4d.BaseContainer()
        self.add_item_(ui=menu, ui_id=IDM_MENU1, str_info='Randomize', store_state="Random", store_mode="R_Mode")
        menu.InsData(0, '')     # Append separator
        self.add_item_(ui=menu, ui_id=IDM_MENU2, str_info='Auto Fliping', store_state="AF", store_mode="AF_Mode")
        menu.InsData(0, '')     # Append separator
        self.add_item_(ui=menu, ui_id=IDM_MENU3, str_info='Random Rotation', store_state="RR", store_mode="RR_Mode")
        menu.InsData(0, '')     # Append separator
        self.add_item_(ui=menu, ui_id=IDM_MENU4, str_info='Group Selected', store_state="GS", store_mode="GS_Mode")
        menu.InsData(0, '')     # Append separator
        self.add_item_(ui=menu, ui_id=IDM_MENU5, str_info='Create Backup Copy', store_state="CBC", store_mode="CBC_Mode")

        # Finally show popup dialog
        result = gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)
        print(result)
        
        if result == IDM_MENU1:
            self.Checked_Toggle(store_state="Random")
            
        if result == IDM_MENU2:
            self.Checked_Toggle(store_state="AF")

            XML_DATA_FILES = g_lib.fcsDIR.FCS_RRP_CONFIG_SETTINGS
            XmlFileTree = etree.parse(XML_DATA_FILES) 
            XmlRoot = XmlFileTree.getroot()
            for rp_data in XmlRoot.findall('Settings_Data'):
                xml_data = rp_data.get("RR")
                if xml_data == "True":
                    self.Toggle(State=False, store_state="RR")

            self.Checked_ModeToggle(store_mode="RR_Mode")

        if result == IDM_MENU3:
            self.Checked_Toggle(store_state="RR")

            XML_DATA_FILES = g_lib.fcsDIR.FCS_RRP_CONFIG_SETTINGS
            XmlFileTree = etree.parse(XML_DATA_FILES) 
            XmlRoot = XmlFileTree.getroot()
            for rp_data in XmlRoot.findall('Settings_Data'):
                xml_data = rp_data.get("AF")
                if xml_data == "True":
                    self.Toggle(State=False, store_state="AF")

            self.Checked_ModeToggle(store_mode="AF_Mode")            

        if result == IDM_MENU4:
            self.Checked_Toggle(store_state="GS")

        if result == IDM_MENU5:
            self.Checked_Toggle(store_state="CBC")                        

        return True
    # ------------------------------------------ #

    # Startup UI Values.
    def Store_UI_Values_and_Data(self):
        if not os.path.exists(g_lib.fcsDIR.FCS_RRP_CONFIG_SETTINGS):
            self.CreatConfigFile()        
        self.newfieldindex = 0
        #self.degit = 0
        return True
 #####  \\  GUI Utils Operations \\   #####
    # Adding the top UI menu of the dialog.
    def TopMenu_GUI(self):

        self.MenuFlushAll()
        # --------------------- #
        self.MenuSubBegin("Help")           # Help Menu
        self.MenuSubBegin("Demo Projects")  # Sub Menu for Demo Projects
        self.MenuAddString(self.TOP_MENU_IDs["demo1_id"], "Trees Demo")
        self.MenuAddSeparator()
        self.MenuAddString(self.TOP_MENU_IDs["demo2_id"], "Cars Demo")
        self.MenuAddSeparator()
        self.MenuAddString(self.TOP_MENU_IDs["demo3_id"], "Boxes Demo")
        self.MenuSubEnd()                   # Demo Projects Sub Menu End
        self.MenuAddSeparator()
        self.MenuAddString(self.TOP_MENU_IDs["discord_id"], 'Discord Support Help')
        self.MenuAddSeparator()
        self.MenuAddString(self.TOP_MENU_IDs["onlineM"], 'Online Manual')
        self.MenuAddSeparator() 
        self.MenuAddString(self.TOP_MENU_IDs["offlineM"], 'Offline Manual')
        self.MenuSubEnd() # Help Menu End
        # --------------------- #

        self.MenuFinished()
        # --------------------- #
        # Adding to menu Example:
        # eg: self.MenuAddString(self.["id"], self.["name"])
        return True

    def Add_BarTitle_with_Icon_GUI(self, bar_title, info_btn_id, bar_ids, fold, state_id, col):
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 0, 1, "")
        self.GroupSpace(0, 0)
        g_lib.Add_QuickTab_Bar_GUI(ui_instance=self, bar_id=bar_ids, bar_name=bar_title, fold=fold, ui_state_id=state_id, width=150, height=10, color_mode=True, ui_color=col)
        info_btn_settings = {'id':info_btn_id, 'btn_look':c4d.BORDER_NONE, 'tip':"<b>Instructions</b>", 'icon':g_lib.fcsDIR.I(i='InfoIcon'), 'clickable':True}
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=info_btn_settings)
        self.GroupEnd() # Group End            
        return True
    
    # To Add o Insert UI Group with elements and Remove.
    def Insert_GUI_Toggle_Area(self, data_id, attach_grp):
        # This Method function works like this to pass date or info to this function:
        # eg: [ self.Insert_GUI_Toggle_Area(data_id=[ID INFO], attach_grp=[DATA]) ]

        insert_grp_id = data_id['insert_id']
        toggle_id = data_id['t_state_id']

        self.GroupBegin(insert_grp_id, c4d.BFH_SCALEFIT, 1, 0)
        Attach_GUI = attach_grp # Attach UI Group
        self.GroupEnd()

        # Using the StaticText gui as a toggle state mode by a string text and have it hidden on UI as default.
        self.AddStaticText(toggle_id, c4d.BFH_SCALEFIT, 1, 1, "Hide_UI")
        self.HideElement(toggle_id, True) # (True) Hide UI element or (False) Show UI element.
        return True
    # Auto adding items to ComboBox drop-down Menu.
    def Add_Templates_to_ComboBoxMenu_GUI(self, menu_id):
        # Get the Live Active Document
        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False

        # Refesh or clear the ComboBox Menu.
        self.FreeChildren(menu_id)

        ComboBoxMenu_List = []
        self.ItemTypeList = []

        c4d.CallCommand(12112, 12112) # Select All
        c4d.CallCommand(100004768, 100004768) # Select Children

        # Get all active children objects that are Templates Models.
        SelectedChildrenObjects = doc.GetActiveObjects(1)

        #  Models Selected
        for each_anim_obj in SelectedChildrenObjects:
            
            RP_TAG = each_anim_obj.GetTag(1050362) # The Random RePlacer Template Tag ID\1050362
            
            if RP_TAG:

                data = RP_TAG[c4d.FCS_RP_TEMP_TAG_ITEMTYPE]
                # Check if item is in the list before adding.
                if data in ComboBoxMenu_List:
                    pass
                # If item not in the list then add the item.
                else:
                    ComboBoxMenu_List.append(data)

        c4d.CallCommand(12113, 12113) # Deselect All

        # Now add items to the drop-down menu ComboBox GUI.
        if ComboBoxMenu_List:

            degit = 0

            for item_type in ComboBoxMenu_List:

                # Create ID for GUI. 
                degit+=1
                new_id_num = degit
                item_id = 7000+new_id_num

                temp_item = '&i1050362&'+ " " + item_type

                item_data = str(item_id) + "--" + item_type 

                # Add to new the lic4d.storage.
                self.ItemTypeList.append(item_data)

                # Add to ComboBox Menu.
                self.AddChild(menu_id, item_id, temp_item)

        c4d.EventAdd()
        return True
 #####  \\  GUI Layout Panels | User Interface in Panels \\   #####

    def AddPresetTag_PanelTab_GUI(self, grp_id):
        self.GroupBegin(grp_id, c4d.BFH_SCALEFIT, 0, 1, "")
        #self.AddEditText(self.presetName_EB, c4d.BFH_SCALEFIT, 190, 10)
        self.AddButton(self.addpreset_BTN, c4d.BFH_SCALEFIT, 0, 10, "Add Preset Group Tag")
        self.GroupEnd() # Group End 
        # Hide on ui element on default startuos.path.
        self.HideElement(grp_id, True) # (True) Hide UI element or (False) Show UI element.               
        return True

    def AddTag_PanelTab_GUI(self, grp_id):
        self.GroupBegin(grp_id, c4d.BFH_SCALEFIT, 0, 1, "")
        self.AddEditText(self.ItemType_EB, c4d.BFH_SCALEFIT, 190, 10)
        self.AddColorField(self.ItemType_COL, c4d.BFH_CENTER, 20, 15, c4d.DR_COLORFIELD_NO_COLOR|c4d.DR_COLORFIELD_NO_BRIGHTNESS)
        self.AddButton(self.ItemType_BTN, c4d.BFH_SCALEFIT, 0, 10, "Add Tag")
        self.GroupEnd() # Group End 
        # Hide on ui element on default startuos.path.
        self.HideElement(grp_id, True) # (True) Hide UI element or (False) Show UI element.        
        return True

    def SelectionMode_PanelTab_GUI(self, grp_id):
        self.GroupBegin(grp_id, c4d.BFH_SCALEFIT, 1, 0, "")
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 0, 1, "")
        self.AddStaticText(0, c4d.BFH_CENTER, 0, 15, " Item Type :", c4d.BORDER_WITH_TITLE_BOLD)
        self.AddComboBox(self.ItemSel_COMBOBOX, c4d.BFH_SCALEFIT, 0, 15, False)
        self.GroupEnd() # Group End
        self.AddButton(self.ItemType_BTN2, c4d.BFH_SCALEFIT, 0, 10, "Select Object/s")
        self.AddCheckbox(self.ItemSelCol_CHK, c4d.BFH_CENTER|c4d.BFH_SCALEFIT, 0, 0, "Auto Toggle Item Selection Color.")
        self.GroupEnd() # Group End
        # Hide on ui element on default startuos.path.
        self.HideElement(grp_id, True) # (True) Hide UI element or (False) Show UI element.        
        return True        

    def Utils_Panel_GUI(self, grp_id):

        self.GroupBegin(grp_id, c4d.BFH_SCALEFIT, 1, 0, "") # Overall Main Group
        self.GroupBorderSpace(2, 2, 2, 2)

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)
    
        self.Add_BarTitle_with_Icon_GUI(bar_title="Tag Utils", info_btn_id=self.ItemType_Info, bar_ids=0, fold=False, state_id=0, col=g_lib.BG_DEEP_DARKER)

        self.Add_BarTitle_with_Icon_GUI(bar_title="Create a Preset Group Tag", info_btn_id=self.ItemType_Info, bar_ids=self.PresetPanel['toggle_btn'], fold=True, state_id=0, col=g_lib.BG_LitDarkBlue_Col)
        self.Insert_GUI_Toggle_Area(self.PresetPanel, self.AddPresetTag_PanelTab_GUI(self.PresetPanel['grp_id']))

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

        self.Add_BarTitle_with_Icon_GUI(bar_title="Add ItemType Tag", info_btn_id=self.ItemType_Info, bar_ids=self.TagPanel['toggle_btn'], fold=True, state_id=0, col=g_lib.BG_LitDarkBlue_Col)
        self.Insert_GUI_Toggle_Area(self.TagPanel, self.AddTag_PanelTab_GUI(self.TagPanel['grp_id']))

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

        self.Add_BarTitle_with_Icon_GUI(bar_title="Selection Mode", info_btn_id=self.ItemType_Info, bar_ids=self.SelModePanel['toggle_btn'], fold=True, state_id=0, col=g_lib.BG_LitDarkBlue_Col)
        self.Insert_GUI_Toggle_Area(self.SelModePanel, self.SelectionMode_PanelTab_GUI(self.SelModePanel['grp_id']))

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

        self.GroupEnd() # Overall Main Group End 

        # Hide on ui element on default startuos.path.
        self.HideElement(grp_id, True) # (True) Hide UI element or (False) Show UI element.
        return True

    def ProgressBar_and_Count_Panel_GUI(self):

        self.GroupBegin(0, c4d.BFH_SCALEFIT, 1, 0, "") 

        # Count Info
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 2, 0, "")
        self.AddStaticText(self.countBar, c4d.BFH_SCALEFIT, 200, 10, "Object Count: 0 : 0", c4d.BORDER_WITH_TITLE_BOLD|c4d.BORDER_THIN_IN)
        self.AddStaticText(self.InProgress, c4d.BFH_MASK, 160, 10, "", c4d.BORDER_WITH_TITLE_BOLD|c4d.BORDER_THIN_IN)
        self.GroupEnd() # Group End         
        # PROGRESSBAR
        self.GroupBegin(0, c4d.BFH_SCALEFIT, 0, 1)   
        self.GroupBorderNoTitle(c4d.BORDER_THIN_IN)
        #self.GroupBorderSpace(1, 1, 1, 1) 
        self.AddCustomGui(self.PROGRESSBAR, c4d.CUSTOMGUI_PROGRESSBAR, "", c4d.BFH_SCALEFIT, 100, 10)
        self.AddSeparatorV(0, c4d.BFV_SCALEFIT)
        self.AddStaticText(self.ProgressAmount, c4d.BFH_MASK, 50, 10, "", c4d.BORDER_WITH_TITLE_BOLD|c4d.BORDER_THIN_IN)       
        self.GroupEnd() # Group End 

        self.GroupEnd() # Group End                 
        return True

    def AddTemplates_Panel_GUI(self):

        # UI Icons
        # Adding custom GUI image button settings ([set] an shot way for saying [settings]).
        utils_btn_set = {'id':self.UtilsPanel['toggle_btn'], 
                        'btn_look':c4d.BORDER_NONE, 
                        'tip':"<b>Tag Utils</b>\nAdding a tag and tag settings.", 
                        'icon':g_lib.fcsDIR.I(i="rp_tag_icon"), 
                        'clickable':True}
        addS_btn_set = {'id':self.AddSlot, 
                        'btn_look':c4d.BORDER_NONE, 
                        'tip':"<b>Add a Template Object/s</b>\nAdding a single object or multiple selection of ojects to the list", 
                        'icon':g_lib.fcsDIR.I(i="rp_add_icon"), 
                        'clickable':True}
        addGrp_btn_set = {'id':self.AddSlots_GrpBTN, 
                          'btn_look':c4d.BORDER_NONE, 
                          'tip':"<b>Auto Add a Group with Templates</b>\nAdding all the template objects from the group, to the lic4d.storage.", 
                          'icon':g_lib.fcsDIR.I(i="rp_grp_icon"), 
                          'clickable':True}
        clr_btn_set = {'id':self.ClearListBTN, 
                       'btn_look':c4d.BORDER_NONE,
                       'tip':"<b>Clear List</b>\n To remove all templates objects from the lic4d.storage.", 
                       'icon':g_lib.fcsDIR.I(i="rp_clr_icon"), 
                       'clickable':True}
        settings_btn_set = {'id':self.Settings_Menu, 
                            'btn_look':c4d.BORDER_NONE, 
                            'tip':"<b>Settings</b>\nTo simulate the different options you want to do,\nbefore pressing the Randomize button.", 
                            'icon':g_lib.fcsDIR.I(i="rp_set_icon"), 
                            'clickable':True}
        addPreset_btn_set = {'id':self.AddSlot_preset_grp, 
                             'btn_look':c4d.BORDER_NONE, 
                             'tip':"<b>Add a Preset Group</b>", 
                             'icon':g_lib.fcsDIR.I(i="rp_pre_icon"), 
                             'clickable':True}


        self.GroupBegin(0, c4d.BFH_SCALEFIT, 1, 0, "") # Overall Main GRP

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)


        # ---------------------------------------------------------------------------------------- #
        self.GroupBegin(0, c4d.BFH_CENTER, 0, 1, "")
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=utils_btn_set)
        self.AddSeparatorV(0, c4d.BFV_SCALEFIT)
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=addS_btn_set)
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=addGrp_btn_set)
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=addPreset_btn_set)
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=clr_btn_set)
        self.AddSeparatorV(0, c4d.BFV_SCALEFIT)
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=settings_btn_set)
        self.GroupEnd() # Group End
        # ---------------------------------------------------------------------------------------- #

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

        self.ScrollGroupBegin(4540, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, c4d.SCROLLGROUP_VERT|c4d.SCROLLGROUP_AUTOVERT, 420, 150) # Overall Main ScrollGroup
        self.GroupBegin(0, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 1, 0, "") # Inner-Group

        # To inject or insert gui group or ui element.
        self.Insert_GUI_Toggle_Area(self.UtilsPanel, self.Utils_Panel_GUI(self.UtilsPanel['grp_id']))

        # The List of Items Group Area
        #self.AddSeparatorH(0, c4d.BFH_SCALEFIT)        
        self.ScrollGroupBegin(498, c4d.BFH_SCALEFIT, c4d.SCROLLGROUP_VERT|c4d.SCROLLGROUP_AUTOVERT, 320, 150) # ScrollGroup
        self.GroupBegin(self.SlotsPanelsLayoutGroup, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 1, 0, "") # Group
        self.GroupEnd() # Group End
        self.GroupEnd() # ScrollGroup End

        self.GroupEnd() # Inner-Group End
        self.GroupEnd() # Overall Main ScrollGroup End

        self.GroupEnd() # GRP End
        return True
 #####  \\  Main C4D GUI Dialog Operations Functions which are Methods to Override for GeDialog class. \\   #####

    # Set GUI Elements with values for when UI dialog open and startup with the set values. 
    def InitValues(self):
        self.CreatConfigFile()
        self.Add_Templates_to_ComboBoxMenu_GUI(menu_id=self.ItemSel_COMBOBOX) 
        self.LayoutChanged(self.SlotLayoutGroup) # Update
        self.Store_UI_Values_and_Data()
        return True

    # // UI Panel Layout //
    def CreateLayout(self):

        self.SetTitle("Random Re-Placer v1.2") # Dialog Title

        self.TopMenu_GUI()

        self.GroupBegin(self.SlotLayoutGroup, c4d.BFH_MASK, 1, 0, "") # Overall Main Group
        self.SetDefaultColor(self.SlotLayoutGroup, c4d.COLOR_BG, g_lib.BG_DARKER_COL)
        self.GroupSpace(0, 0)

        self.GroupBegin(self.ToolTitle, c4d.BFH_SCALEFIT, 1, 0, "")
        self.SetDefaultColor(self.ToolTitle, c4d.COLOR_BG, g_lib.BG_DEEP_DARKER)
        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)
        self.AddStaticText(0, c4d.BFH_CENTER, 0, 15, "Random Re-Placer v1.2", c4d.BORDER_WITH_TITLE_BOLD)
        self.GroupEnd() # Group End        

        self.AddTemplates_Panel_GUI()

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

        self.AddButton(self.ReplaceBTN, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 100, 15, "Randomize Items")

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

        self.ProgressBar_and_Count_Panel_GUI()

        self.GroupBegin(self.AboutBTN, c4d.BFH_SCALEFIT, 1, 0, "")
        self.SetDefaultColor(self.AboutBTN, c4d.COLOR_BG, g_lib.BG_DEEP_DARKER)
        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)
        g_lib.Add_Icon_Button_GUI(self, self.webpageBTN, "www.FieldCreatorsStudios.com", None, g_lib.IconButton.M_NOICON)
        self.GroupEnd() # Group End

        self.GroupEnd() # Overall Main Group End
        return True

    # Command for GUI Elements Functions
    def Command(self, id, msg):

        if id == self.null:
            doc = c4d.documents.GetActiveDocument()
            if doc == None:
                return False
            list_objs = doc.GetActiveObjects(1)
            if not list_objs:
                gui.MessageDialog("Please select an Object!")
                return

            for E in list_objs:
                if E.GetType() == c4d.Onull:
                    doc.SetActiveObject(E)
                    # Delete Selected Object but not Replacer 
                    c4d.CallCommand(1019951, 1019951) # Delete Without Children
                    c4d.EventAdd()

        if id == self.UtilsPanel['toggle_btn']:
            g_lib.Toggle_Image_BTN_GUI(self, self.UtilsPanel['toggle_btn'], self.UtilsPanel['t_state_id'], g_lib.fcsDIR.I(i="rp_tag_icon"), g_lib.fcsDIR.I(i="rp_tagE_icon"), "Hide_UI")
            g_lib.Toggle_On_and_Off_GUI(self, self.UtilsPanel['grp_id'], self.UtilsPanel['t_state_id'], self.SlotLayoutGroup)

        if id == self.TagPanel['toggle_btn']:
            g_lib.Toggle_On_and_Off_GUI(self, self.TagPanel['grp_id'], self.TagPanel['t_state_id'], self.SlotLayoutGroup)

        if id == self.SelModePanel['toggle_btn']:
            g_lib.Toggle_On_and_Off_GUI(self, self.SelModePanel['grp_id'], self.SelModePanel['t_state_id'], self.SlotLayoutGroup)

        if id == self.PresetPanel['toggle_btn']:
            g_lib.Toggle_On_and_Off_GUI(self, self.PresetPanel['grp_id'], self.PresetPanel['t_state_id'], self.SlotLayoutGroup)

        if id == self.AddSlot:
            self.AddingSlotsGUI()

        if id == self.AddSlots_GrpBTN:
            self.AddingSlotsGUI_FromGroup()   

        if id == self.AddSlot_preset_grp:
            self.AddingSlotsGUI_PresetGroup()

        if id == self.Settings_Menu:
            self.PopupSettings_Checklist()

        if id == self.ItemType_BTN:
            self.Add_Tag_to_Object()
            self.Add_Templates_to_ComboBoxMenu_GUI(menu_id=self.ItemSel_COMBOBOX)               

        if id == self.addpreset_BTN:
            self.Add_PresetTag_to_Object()

        if id == self.ItemType_BTN2:
            self.Get_ItemTypes()

        if id == self.ItemSelCol_CHK:
            if self.GetBool(self.ItemSelCol_CHK) == True:
                self.Show_ItemTypes_Color()
            else:
                self.Hide_ItemTypes_Color()

        if id == self.ItemSel_COMBOBOX:

            if self.GetLong(self.ItemSel_COMBOBOX) == self.ItemSel_COMBOBOX:
                pass
            else:
                self.Show_ItemTypes_Color() 

        if id == self.ReplaceBTN:
            self.RandomizeBTN()
        
        if id == self.ItemType_Info:
            info = "Random Replacer v1.0 Instructions :" + "\n\n" + "" 
            g_lib.SendInstructions(Message=info)

        if id == self.SettingsBTN:
            info = "Random Replacer v1.0 Instructions :" + "\n\n" + ""
            g_lib.SendInstructions(Message=info)

        if id == self.TemplatesBTN:
            info = "Random Replacer v1.0 Instructions :" + "\n\n" + ""  
            g_lib.SendInstructions(Message=info)

        if id == self.ClearListBTN:
            self.AmountSlotsData = []
            self._ReBuild_ReFresh_GUIs_()

        if id == id:

            if self.AmountSlotsData:
                for each_slot in self.AmountSlotsData:
                    self.DeleteSlotBTN(id, SlotItem=each_slot)

        if id == self.webpageBTN:
            g_lib.OpenFCSWebLink(page="https://gumroad.com/fcstudios")
        return True            

    # Keeping dialog updated and refresh if any GUI elements as changes to the UI or in the UI.
    def CoreMessage(self, id, msg):
        if c4d.EVMSG_CHANGE:
            pass
        return True
                        
    # If GUI dialog window close or c4d layout changes then auto this function. eg(Auto Saving UI data or Removing data functions)and etc..
    # NOTE!: No need to add a (return True or return False, or return) at the of this C4D DestroyWindow function.
    #def DestroyWindow(self):
        #if os.path.exists(TxtF):
            #os.remove(TxtF)
