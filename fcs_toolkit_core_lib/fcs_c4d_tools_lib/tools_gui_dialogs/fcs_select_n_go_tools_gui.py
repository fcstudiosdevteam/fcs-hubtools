"""
Select N'Go Tool Window GUI Dialog and GameDev Pipline Tools
"""
#  // Imports for Cinema 4D //
import c4d
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as WindowDialog        # eg:( class My_Dialog(c4d.gui.GeDialog): ) to ( class My_Dialog(WindowDialog): ) Just keeping code line short.
from c4d.plugins import CommandData, TagData, ObjectData
# XML Imports is for Saving and Loading Project Data for Plugin UI / Xml good but long functions to make.
from xml.dom import minidom
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree

# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
import fcs_satellite_to_gpoly_gui
import fcs_flightsim_tools_popupmenu
import fcs_station_engine_app_lib as fcsEngine

#from ..fcs_c4d_dialogs import fcs_common_global_lib as g_lib
config_data = None
fcsID = g_lib.fcsID
jsonEdit = g_lib.jsonEdit
fs_lib = g_lib.fs_lib

# ------------------------------
#  Select N'Go Tool
# ------------------------------
class Select_N_Go_Dialog(WindowDialog):
    """ Select N'Go Tool / Our first 3d plugin tool. """
 #####  \\  GUI IDs and Strings \\   #####
    VerData = " v3.10 "
    ID_VER = 178

    OpenDir_UI_BTN = 46554
    AddDir_UI_BTN = 19897
    Center_Obj = 195654

    CHK_ExportTogether = 19877
    OBJName = 19866

    #--| ID's for Dialog User Interface, UI Buttons, and Check Buttons |--#
    UI_FormatsDropDownMenu = 1001
    UI_Execute_ExportButton = 1002
    UI_FormatsDropDownMenu2 = 1003
    UI_FormatSettingsGearButton = 1004
    UI_SEP = 10009

    #---------------------------------------------------------#
    DirName=133448
    NoneError404=16656
    NoneError404F = 16356
    UI_BTN_DirLoad = 178337

    Path_Group_ID = 90096

    ESave   =   {"id":647665, "name":"Save Project Data"}
    ELoad   =   {"id":64556,  "name":"Open Project Data"}
    NewLoad =   {"id":64558,  "name":"New"}

    STATUS_UI_BAR_ID = 9009845
    STATUS_UI_TXT_ID = 9009945
    ID_SNG_OPT_BATCH_CUSTOM_BTN = 9009846
    fcs_select_n_go_gui_dlg = 2001
    ID_SNG_OVERALL_GRP = 2003
    # // Take Clips Tab IDs //
    ID_SNG_TAKES_TAB_GRP = 2041
    ID_SNG_BOTTOM_GRP = 2042
    ID_SNG_STATUS_BAR_GRP = 2043
    ID_SNG_FCSWEBBANNER = 2044

    ID_SNG_OPT_BATCHFITER_MENU_ALL = 10202
    FCS_WEBSITE_GUI_BANNER_ID = 10201

    BatchList = []
    BatchListItemsUIs = []
    CheckedOnBatchList = []

    sub_points = []

    # BaseContainer for Ui Elements
    custom_ui_bc = c4d.BaseContainer()
 #####  \\  Tool Core Heler Method Functions Operations \\   #####

    # This function is check for export filter IDs and then add it to the combox formats lic4d.storage.
    def AddExportFilter(self, ComboID, ChildID, FilterString, FilterID):
        plug = plugins.FindPlugin(FilterID, c4d.PLUGINTYPE_SCENESAVER)     
        if plug is None:
            #print FilterString + " This Plugin Is Not Install."
            pass   
        else:
            self.AddChild(ComboID, ChildID, FilterString)
        return True        

    def Add_TileCustomBarGUI(self, tile_str, ui_id):
        # BaseContainer for Ui Elements
        self.custom_ui_bc = c4d.BaseContainer()         
        self.custom_ui_bc.SetBool(c4d.QUICKTAB_BAR, True)
        self.custom_ui_bc.SetString(c4d.QUICKTAB_BARTITLE, tile_str)
        self.AddCustomGui(ui_id, c4d.CUSTOMGUI_QUICKTAB, "", c4d.BFH_SCALEFIT, 290, 0, self.custom_ui_bc)
        return True 

    # To Inject UI layout to a empty group.
    def Inject_GUI_Title(self, injected_group_id, str_title):
        """ Inject your UI layout or GUI element to a empty group. """
        self.LayoutFlushGroup(injected_group_id) # Refresh Group UI
        self.Add_TileCustomBarGUI(tile_str=str_title, ui_id=0)
        self.LayoutChanged(injected_group_id) # Update Group UI
        return True

    def Inject_GUI_Icon(self, injected_group_id, data):
        """ Inject your UI layout or GUI element to a empty group. """
        self.LayoutFlushGroup(injected_group_id) # Refresh Group UI
        g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=data)
        self.LayoutChanged(injected_group_id) # Update Group UI
        return True

    def Inject_GUI_Status(self):
        """ Inject your UI layout or GUI element to a empty group. """
        self.LayoutFlushGroup(fcsID.ID_SNG_STATUS_BAR_GRP) # Refresh Group UI
        g_lib.Add_StatusBar_GUI(ui_instance=self, ui_id=self.STATUS_UI_BAR_ID, state_col=g_lib.BG_DARK, str_id=self.STATUS_UI_TXT_ID, message="OK")
        self.LayoutChanged(self.ID_SNG_STATUS_BAR_GRP) # Update Group UI
        return True

    def Inject_GUI_FCS_Web_Banner(self):
        self.LayoutFlushGroup(self.ID_SNG_FCSWEBBANNER) # Refresh Group UI
        g_lib.Bottom_FCS_Web_GUI_Banner(ui_instance=self, button_id=self.FCS_WEBSITE_GUI_BANNER_ID)
        self.LayoutChanged(self.ID_SNG_FCSWEBBANNER) # Update Group UI
        return True

    def Add_and_Inject_GUIs(self):
        # Set GUI Elements to UI.
        self.Inject_GUI_Title(injected_group_id=fcsID.ID_SNG_OPT_MODE_TITLE_GRP, str_title=" Export Settings")
        self.Inject_GUI_Title(injected_group_id=fcsID.ID_SNG_OPT_FORMAT_SETTINGS_TITLE_GRP, str_title=" Format Settings")
        self.Inject_GUI_Title(injected_group_id=fcsID.ID_SNG_OPT_OPTIONS_TITLE_GRP, str_title=" Opttion Settings")
        self.Inject_GUI_Title(injected_group_id=fcsID.ID_SNG_OPT_BATCH_TITLE_GRP, str_title=" Batch Importing")
        self.Inject_GUI_Title(injected_group_id=fcsID.ID_SNG_OPT_PATH_TITLE, str_title="Export Path")

        for eachFilter in g_lib.c4dSceneFilter.ExportFiltersC1:##ExportFiltersC1
            self.AddExportFilter(ComboID=fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU, ChildID=eachFilter['id'], FilterString=eachFilter['str'], FilterID=eachFilter['export_id'])
        self.AddChild(fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU, -1, "---------------------------------") # Separator
        for eachFilter in g_lib.c4dSceneFilter.ExportFiltersC2:
            self.AddExportFilter(ComboID=fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU, ChildID=eachFilter['id'], FilterString=eachFilter['str'], FilterID=eachFilter['export_id'])        
        self.AddChild(fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU, -1, "---------------------------------") # Separator
        for eachFilter in g_lib.c4dSceneFilter.ExportFiltersC3:
            self.AddExportFilter(ComboID=fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU, ChildID=eachFilter['id'], FilterString=eachFilter['str'], FilterID=eachFilter['export_id'])
        self.AddChild(fcsID.ID_SNG_OPT_BATCHFITER_MENU, self.ID_SNG_OPT_BATCHFITER_MENU_ALL, " Show All ")
        self.AddChild(fcsID.ID_SNG_OPT_BATCHFITER_MENU, -1, "---------------------------------")
        for filter_extension in g_lib.c4dSceneFilter.ImportFilters:
            self.AddExportFilter(ComboID=fcsID.ID_SNG_OPT_BATCHFITER_MENU, ChildID=filter_extension['import_id'], FilterString=filter_extension['extstr'], FilterID=filter_extension['export_id'])

        settingsIcon_set = {'id':self.UI_FormatSettingsGearButton, 'btn_look':c4d.BORDER_NONE, 'tip':"<b>Settings</b>\nTo open your export formats settings.", 'icon':g_lib.fcsDIR.I(i='SettingsIcon'), 'clickable':True}
        self.Inject_GUI_Icon(injected_group_id=fcsID.ID_SNG_OPT_FORMAT_GEAR_SETTINGS, data=settingsIcon_set)
        
        dir1Icon_set = {'id':self.ID_SNG_OPT_BATCH_CUSTOM_BTN, 'btn_look':c4d.BORDER_NONE, 'tip':"<b>Settings</b>\nTo open your export formats settings.", 'icon':g_lib.fcsDIR.I(i='Folder'), 'clickable':True}
        self.Inject_GUI_Icon(injected_group_id=fcsID.ID_SNG_OPT_BATCH_DIR_BTN_GRP, data=dir1Icon_set)
        
        dir2Icon_set = {'id':self.UI_BTN_DirLoad, 'btn_look':c4d.BORDER_NONE, 'tip':"<b>Settings</b>\nAdding export path directory.", 'icon':g_lib.fcsDIR.I(i='Folder'), 'clickable':True}
        self.Inject_GUI_Icon(injected_group_id=fcsID.ID_SNG_OPT_PATH_DIR_BTN_GRP, data=dir2Icon_set)
        
        searchIcon_set = {'id':0, 'btn_look':c4d.BORDER_NONE, 'icon':g_lib.fcsDIR.I(i='searchIcon')}
        self.Inject_GUI_Icon(injected_group_id=fcsID.ID_SNG_OPT_BATCH_SEARCHICON_GRP, data=searchIcon_set)

        rf1Icon_set = {'id':self.AddDir_UI_BTN, 'btn_look':c4d.BORDER_NONE, 'tip': "<b>Recent Folder</b>\nTo add your recent export folder path for exporting,\nthat you added from the browser button before.", 'icon':g_lib.fcsDIR.I(i='RecentIcon'), 'clickable':True}
        self.Inject_GUI_Icon(injected_group_id=fcsID.ID_SNG_OPT_PATH_RF_BTN, data=rf1Icon_set)        

        self.Inject_GUI_Status()

        self.Inject_GUI_FCS_Web_Banner()
        return True

    def FileNodeGUI_ItemLayout(self, itemData):
        """ 
        Item slot UI layout.
        ui_data = { 'GRP_ID':data, 'GRP2_ID':data, 'CHK_ID':data, 'FILE_STR':data, 'EXT_STR':data }
        self.FileNodeGUI_ItemLayout(itemData=ui_data)
        """
        self.GroupBegin(itemData["GRP_ID"], c4d.BFH_SCALEFIT, 3, 0, "") # c4d.BFV_CMD_EQUALCOLUMNS,
        #self.GroupSpace(0, 0)
        Vect = c4d.Vector(0.125, 0.125, 0.125)
        self.SetDefaultColor(itemData["GRP_ID"], c4d.COLOR_BG, Vect)

        self.AddCheckbox(itemData["CHK_ID"], c4d.BFH_SCALEFIT, 30, 0, "") # File Check

        self.AddStaticText(0, c4d.BFH_SCALEFIT, 0, 15, itemData["FILE_STR"]+"  ", c4d.BORDER_WITH_TITLE_BOLD) # File Name

        self.GroupBegin(itemData["GRP2_ID"], c4d.BFH_LEFT, 1, 0, "")
        self.SetDefaultColor(itemData["GRP2_ID"], c4d.COLOR_BG, g_lib.BG_GREEN_COL)
        self.AddStaticText(0, c4d.BFH_LEFT, 80, 15, "  "+itemData["EXT_STR"]+"  ", c4d.BORDER_WITH_TITLE_BOLD) # File Type
        self.GroupEnd()

        self.GroupEnd()
        return True

    def GetFilesFromDirectory(self, FileData):
        """ Get files from directory path, by their file extension."""
        path = self.GetString(fcsID.ID_SNG_OPT_BATCH_EDITBOX) #"C:/Users/AP_Ashton_TheCreator/Desktop/0_TestFilesZone/NOOBS MODELS" # self.GetString(self.ID_SNG_OPT_BATCH_EDITBOX)
        if os.path.exists(path):
            # Looking for all files in folder.    
            os.chdir(path)
            # Get all files with extension from the folder.
            for each_file in glob.glob(FileData["File"]):
                # Get all files and split or cut off the extension from it.
                for files in each_file.splitlines():
                    ObjectfileName = each_file.split(FileData["Ext"])
                    ObjectfileExtension = FileData["Ext"]
                    ObjectStrExt = FileData["Str"]
                    ObjectExtId = FileData["ID"]
                    ObjectfileData = ObjectfileName[0] + "--" + ObjectfileExtension + "--" + ObjectStrExt + "--" + str(ObjectExtId)
                    self.BatchList.append(ObjectfileData)
        return True

    def SearchFilterBatchFileSystem(self):
        DataAndExtension = None
        SearchBar = self.GetString(fcsID.ID_SNG_OPT_SEARCH_EDITBOX).lower()
        # Get file 3D format filter extension from the (fcs_Global_EnumsTypes_LIB).
        for filter_extension in g_lib.c4dSceneFilter.ImportFilters:
            itemId = filter_extension['import_id']
            itemId_ShowAll = self.ID_SNG_OPT_BATCHFITER_MENU_ALL

            if self.GetLong(fcsID.ID_SNG_OPT_BATCHFITER_MENU) == itemId_ShowAll:
                #print("On Show All")
                DataAndExtension = {'File':SearchBar+"*"+filter_extension['ext'], 'Ext':filter_extension['ext'], 'Str':filter_extension['extstr'], 'ID':filter_extension['import_id']}
                self.GetFilesFromDirectory(FileData=DataAndExtension)
                #print(filter_extension['ext'])
                #print(DataAndExtension)

            if self.GetLong(fcsID.ID_SNG_OPT_BATCHFITER_MENU) == itemId:
                DataAndExtension = {'File':SearchBar+"*"+filter_extension['ext'], 'Ext':filter_extension['ext'], 'Str':filter_extension['extstr'], 'ID':filter_extension['import_id']}
                self.GetFilesFromDirectory(FileData=DataAndExtension)
                #print(filter_extension['ext'])

        return DataAndExtension

    def LoadFiles_To_BatchListViewGUI(self):

        SearchBar = self.GetString(fcsID.ID_SNG_OPT_SEARCH_EDITBOX).lower()

        self.LayoutFlushGroup(fcsID.ID_SNG_OPT_BATCH_LIST) # Refresh Group UI

        degit = 0
        
        if self.BatchList:

            amountOfBathFiles = len(self.BatchList)
            currentNum = 1

            for batchfile in self.BatchList:

                percent = float(currentNum)/amountOfBathFiles*100
                currentNum+=1

                # Create ID for GUI. 
                degit+=1
                FNAME = batchfile.split('--')[0]
                FEXT = batchfile.split('--')[1]
                FEXT_STR = batchfile.split('--')[2]
                FFILTER_ID = batchfile.split('--')[3]
                chk_id = 7000+degit
                ui_data = { 'GRP_ID':9000+degit, 'GRP2_ID':10000+degit, 'CHK_ID':chk_id, 'FILE_STR':FNAME, 'EXT_STR':FEXT_STR }
                self.FileNodeGUI_ItemLayout(itemData=ui_data)

                ItemUIData = str(chk_id) + "--" + FNAME + "--" + FEXT + "--" + str(FFILTER_ID)
                self.BatchListItemsUIs.append(ItemUIData)

                g_lib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, g_lib.BG_GREEN_COL, "Loading Batch Files...."+str(percent)+"%")

        self.LayoutChanged(self.ID_SNG_OPT_BATCH_LIST) # Update Group UI
        return True

    def CheckedItems(self):
        if self.CheckedOnBatchList:
            for itemfile in self.CheckedOnBatchList:
                chk_id = itemfile.split('--')[0]
                FNAME = itemfile.split('--')[1]
                FEXT = itemfile.split('--')[2]                            
                self.SetBool(int(chk_id), True)
        return True

    def CheckAllBatchFileItem(self, CheckAll):
        if self.BatchListItemsUIs:
            if CheckAll == True:
                for itemfile in self.BatchListItemsUIs:
                    chk_id = itemfile.split('--')[0]
                    FNAME = itemfile.split('--')[1]
                    FEXT = itemfile.split('--')[2]        
                    self.SetBool(int(chk_id), True)
            if CheckAll == False:
                for itemfile in self.BatchListItemsUIs:
                    chk_id = itemfile.split('--')[0]
                    FNAME = itemfile.split('--')[1]
                    FEXT = itemfile.split('--')[2]        
                    self.SetBool(int(chk_id), False)                

    def ConvertingtoUnityAxis(self, obj, obj_name, SNG_COMPLIER):
        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False

        # Get Objects form the Object Manager     
        list_obj = obj

        list_obj[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_X]=3.141592653589793

        Obj_name = list_obj.GetName()
        
        # Check object for children
        ChildrenObjs = list_obj.GetChildren()

        get_amount = len(ChildrenObjs)
        #print(str(get_amount))

        if get_amount > 0:
            
            list_obj.InsertUnder(SNG_COMPLIER)
            
            # Make Temp Null Group.
            UnityAxisNull = c4d.BaseObject(c4d.Onull)
            UnityAxisNull.SetName("UnityAxisNull")
            doc.InsertObject(UnityAxisNull)
            UnityAxisNull.InsertUnder(SNG_COMPLIER)        
            
            childrenHolderNull = c4d.BaseObject(c4d.Onull)
            childrenHolderNull.SetName("obj_Children_holder")
            doc.InsertObject( childrenHolderNull)
            childrenHolderNull.InsertUnder(SNG_COMPLIER)
            for e in ChildrenObjs:
                e.InsertUnder(childrenHolderNull)

            list_obj.InsertUnder(UnityAxisNull)
            
            doc.SetActiveObject(UnityAxisNull)
            
            c4d.CallCommand(1036583)

            get_obj_sys = doc.GetActiveObject()
            get_obj_sys.SetName(Obj_name+"_sng")
            
            doc.SetActiveObject(childrenHolderNull)
            get_obj = doc.GetActiveObject()
            get_obj[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_X]=3.141592653589793
            c4d.EventAdd()
            
            doc.SetActiveObject(get_obj_sys)
            
            doc.SetActiveObject(childrenHolderNull)
            get_objs2 = doc.GetActiveObject()
            get_objs2.InsertUnder(get_obj_sys)
            c4d.CallCommand(1019951, 1019951) # Delete Without Children
            #get_objs2.Remove()

            doc.SetActiveObject(get_obj_sys)

        else:
            list_obj.InsertUnder(SNG_COMPLIER)
            
            # Make Temp Null Group.
            UnityAxisNull = c4d.BaseObject(c4d.Onull)
            UnityAxisNull.SetName("UnityAxisNull")
            doc.InsertObject(UnityAxisNull)
            UnityAxisNull.InsertUnder(SNG_COMPLIER)             

            list_obj.InsertUnder(UnityAxisNull)
            
            doc.SetActiveObject(UnityAxisNull)
            
            c4d.CallCommand(1036583)

            get_obj_sys = doc.GetActiveObject()
            get_obj_sys.SetName(Obj_name+"_sng")

            doc.SetActiveObject(get_obj_sys)

        return True

    def newSolo_docTemp(self, doc, docData):
        
        # Get Models form the Object Manager.
        objs = doc.GetSelection()
        
        newdoc = c4d.documents.GetActiveDocument()
        
        # Solo the selected object.
        docTemp = c4d.documents.IsolateObjects(newdoc, objs)
        if docTemp == None:
            return False
        
        # Set Document Data.
        docTemp.SetDocumentData(c4d.DOCUMENTSETTINGS_DOCUMENT, docData) 
        
        get_docTemp_Data = c4d.documents.GetActiveDocument()   
        
        return get_docTemp_Data

    def Exporting(self):
        """ For File Exporting Function """
        ###########################################################
        # Start Export Function 
        ###########################################################
        doc = c4d.documents.GetActiveDocument()
        if doc == None:
            return False
        # Get Active Document Data.     
        docActiveUnit = doc.GetDocumentData(c4d.DOCUMENTSETTINGS_DOCUMENT)
        # Get Objects form the Object Manager     
        list_objs = doc.GetActiveObjects(1)
        if not list_objs:
            g_lib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, g_lib.BG_RED_COL, "Please select an Object!")
            return

        #global fileSeparator 
        ###########################################################
        # Get Path and Make File Extention of Export File
        ###########################################################
        for E in list_objs:

            # Create the complier group.
            SNG_COMPLIER = c4d.BaseObject(c4d.Onull)
            SNG_COMPLIER.SetName("SNG_OPERATING")
            doc.InsertObject(SNG_COMPLIER)  

            # Get Object and make a clone of the object
            CloneTemp = E.GetClone(c4d.COPYFLAGS_0)
            doc.InsertObject(CloneTemp)
            CloneTemp.InsertUnder(SNG_COMPLIER)

            # Get Oject Name, Get Object and Set Object as Active.
            get_E = E.GetName()

            # Center object on export is Checked.
            if self.GetBool(fcsID.ID_SNG_OPT_CHK_CENTER_OBJ)==True:
                doc.SetActiveObject(CloneTemp) 
                c4d.CallCommand(1019940) # Reset PSR

            if self.GetBool(fcsID.ID_SNG_OPT_CHK_UNITY_AXIS)==True:
                self.ConvertingtoUnityAxis(obj=CloneTemp, obj_name=get_E, SNG_COMPLIER=SNG_COMPLIER)

            if self.GetBool(fcsID.ID_SNG_OPT_CHK_UNITY_AXIS)==False:
                doc.SetActiveObject(CloneTemp)
                get_obj_sys = doc.GetActiveObject()
                get_obj_sys.SetName(get_E+"_sng")

            doc = c4d.documents.GetActiveDocument()

            ################################################################################
            # Check for Export ID , has been chosse in the list function.
            # Set FileName with Get Object Name and Make File Extention of Export File. 
            ################################################################################
            for SelFilter in g_lib.c4dSceneFilter.ExportFilters:

                if self.GetLong(fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU)==SelFilter['id']:

                    #print(SelFilter['id'])

                    ExportPLUGIN_ID = SelFilter['export_id']

                    filename = get_E + SelFilter['ext']

                    fobj = doc.SearchObject(get_E+"_sng")
                    sobj = doc.SetActiveObject(fobj)
                    l_obj = doc.GetActiveObject()

                    docTemp = self.newSolo_docTemp(doc=doc, docData=docActiveUnit)

                    # Get export plugin, For Example : DAR1.4:1022316 or FBX:1026370 is its ID
                    plug = plugins.FindPlugin(ExportPLUGIN_ID, c4d.PLUGINTYPE_SCENESAVER)
                    if plug is None:
                        raise RuntimeError("Failed to retrieve the format scene_saver_filter.")
                    #print(plug)

                    data = dict()
                    # Sends MSG_RETRIEVEPRIVATEDATA to fbx export plugin
                    if not plug.Message(c4d.MSG_RETRIEVEPRIVATEDATA, data):
                        return False

                    # # Get User file export folder path from Directory for file.
                    userpath = os.path.join(self.GetString(fcsID.ID_SNG_OPT_PATH_EDITBOX), filename)
                    if not userpath:
                        return

                    # Exporting Format
                    if not c4d.documents.SaveDocument(docTemp, userpath, c4d.SAVEDOCUMENTFLAGS_DONTADDTORECENTLIST, ExportPLUGIN_ID):
                        g_lib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, g_lib.BG_RED_COL, "Couldn't export, due to Directory field is empty.")
                        raise RuntimeError("Couldn't export, due to Directory field is empty.")

                    g_lib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, g_lib.BG_GREEN_COL, filename +" Export Finish successfully")
                    self.OpenExportFolder()

            # Check and Remove Temp Clone Object.
            doc.SetActiveObject(SNG_COMPLIER)
            ActObj = doc.GetActiveObject()
            ActObj.Remove()

        c4d.EventAdd()
        return True

    def BatchImporting(self, fn, filterId, extensionStr):
        """ For Batch Import Function """
        doc = c4d.documents.GetActiveDocument()
        path = self.GetString(fcsID.ID_SNG_OPT_BATCH_EDITBOX)
        fileName = fn + extensionStr

        getImportFile = os.path.join(path, fileName)

        importPLUGIN_ID = int(filterId)

        plug = plugins.FindPlugin(importPLUGIN_ID, c4d.PLUGINTYPE_SCENELOADER)
        if plug is None:
           return

        c4d.documents.MergeDocument(doc, getImportFile, c4d.SCENEFILTER_OBJECTS|c4d.SCENEFILTER_MATERIALS|c4d.SCENEFILTER_PROGRESSALLOWED|c4d.SCENEFILTER_MERGESCENE, None)

        c4d.EventAdd()
        return True

    def Importing(self):
        """ Basic File Import Function """
        doc = c4d.documents.GetActiveDocument()
        # Check for Export ID , has been chosse in the list function 
        for SelFilter in g_lib.c4dSceneFilter.ImportFilters:
            if self.GetLong(fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU)==SelFilter['id']:
                importPLUGIN_ID = SelFilter['import_id']
                plug = plugins.FindPlugin(importPLUGIN_ID, c4d.PLUGINTYPE_SCENELOADER)
                if plug is None:
                    return
                # Get a path to load the imported file
                selectedFile = c4d.storage.LoadDialog(title="Load File for Import", type=c4d.FILESELECTTYPE_ANYTHING, force_suffix=".fbx")
                if selectedFile is None:
                    return
                c4d.documents.MergeDocument(doc, selectedFile, c4d.SCENEFILTER_OBJECTS|c4d.SCENEFILTER_MATERIALS|c4d.SCENEFILTER_PROGRESSALLOWED|c4d.SCENEFILTER_MERGESCENE, None)
        c4d.EventAdd()
        return True

    def OpenExportFolder(self):
        """ Opening Export Folder After File Export """
        if self.GetBool(fcsID.ID_SNG_OPT_CHK_OPENFOLDER)==True:
            Dir = self.GetString(fcsID.ID_SNG_OPT_PATH_EDITBOX)
            openEF = os.path.join(Dir)
            if openEF is False:
                g_lib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, g_lib.BG_RED_COL, "You dont have a directory added.")
                return
            else:
                c4d.storage.ShowInFinder(openEF, False)
        return True

    def Add_RecentFolder(self):

        menu = c4d.BaseContainer()

        sub_points = []

        if os.path.exists(g_lib.fcsDIR.ini_file_RecentSelNGO):
            with open(g_lib.fcsDIR.ini_file_RecentSelNGO,'r') as t_file:
                data = t_file.read()
            for line in data.splitlines():
                name,n_dir = line.split(';')
                sub_points.append((name,n_dir))

        subID = c4d.FIRST_POPUP_ID+10001

        for item in sub_points:
            menu.SetString(subID+len(menu), item[0])

        result = gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)-c4d.FIRST_POPUP_ID
        print(result)

        if result > 1000:
            self.SetString(fcsID.ID_SNG_OPT_PATH_EDITBOX, sub_points[result-10001][1])
            return

        return True

    def Add_RecentFolderToList(self):
        c_dir = c4d.storage.LoadDialog(type=c4d.FILESELECTTYPE_ANYTHING, title="Get Export Directory", flags=c4d.FILESELECT_DIRECTORY, force_suffix="")
        self.SetString(fcsID.ID_SNG_OPT_PATH_EDITBOX, c_dir)
        #if c_dir:
            #with open(PLUGIN_DIR().ini_file_RecentSelNGO,'a') as t_file:
            #    t_file.write(os.path.split(c_dir)[1]+';'+c_dir+'\n')    
            #return
        return True            

    def Add_Batch_Dir(self):
        self.BatchList = []
        self.BatchListItemsUIs = []
        c_dir = c4d.storage.LoadDialog(type=c4d.FILESELECTTYPE_ANYTHING, title="Get Export Directory", flags=c4d.FILESELECT_DIRECTORY, force_suffix="")
        self.SetString(fcsID.ID_SNG_OPT_BATCH_EDITBOX, c_dir)
        p = self.GetString(fcsID.ID_SNG_OPT_BATCH_EDITBOX)
        g_lib.fcsLog.INFO(p, False)
        self.SetString(fcsID.ID_SNG_OPT_SEARCH_EDITBOX, "")
        return True  

    def OpenExports_or_Imports_Settings(self):

        if self.GetLong(fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU)==fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU:
            g_lib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, g_lib.BG_RED_COL, "Please select a format.")

        for SelFilter in g_lib.c4dSceneFilter.ExportFilters:

            if self.GetLong(fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU)==SelFilter['id']:

                if self.GetInt32(fcsID.ID_SNG_OPTIONS_TABS) == fcsID.ID_SNG_IMPORT_OPTIONS_TAB_GRP:
                    importPLUGIN_ID = SelFilter['import_id']
                    c4d.PrefsLib_OpenDialog(importPLUGIN_ID)

                if self.GetInt32(fcsID.ID_SNG_OPTIONS_TABS) == fcsID.ID_SNG_EXPORT_OPTIONS_TAB_GRP:
                    ExportPLUGIN_ID = SelFilter['export_id']
                    c4d.PrefsLib_OpenDialog(ExportPLUGIN_ID)
        return True    

    def EnableExportMultiTogether(self):
        """ Multi Obj Selection Export Together """
        doc = c4d.documents.GetActiveDocument()
        c4d.CallCommand(100004772, 100004772) # Group Objects 
        Obj = doc.GetActiveObject()
        name = self.GetString(self.OBJName)
        Obj.SetName(name)
        return True
 #####  \\  Main C4D GUI Dialog Operations Functions which are Methods to Override for GeDialog class. \\   #####
    def __init__(self, global_strings):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """    
        super(Select_N_Go_Dialog, self).__init__()
        self.IDS = global_strings
        #self.HightLight_GIcon = os.path.join(g_lib.fcsDIR.plugin_ui_icons_folder, "UI_HL_Good.png")
        #self.HightLight_NIcon = os.path.join(g_lib.fcsDIR.plugin_ui_icons_folder, "UI_HL_Notting.png")
        #self.HightLight_BIcon = os.path.join(g_lib.fcsDIR.plugin_ui_icons_folder, "UI_HL_Bad.png")
        #self.playbuttonIcon = os.path.join(g_lib.fcsDIR.plugin_ui_icons_folder, "tool_icon_playbutton.png")  # The path to the image 
        #self.stopbuttonIcon = os.path.join(g_lib.fcsDIR.plugin_ui_icons_folder, "tool_icon_playbuttonON.png")

        #self.recentFolderIcon = os.path.join(g_lib.fcsDIR.plugin_ui_icons_folder, "tool_icon_Refolder.png")
        #self.SettingsIcon = os.path.join(g_lib.fcsDIR.plugin_ui_icons_folder, "UI_Settings_BTN.png")

        #self.ExportGoIcon = os.path.join(g_lib.fcsDIR.plugin_ui_icons_folder, "UI_GO_BTN.png")
        #self.SelNGOIcon = os.path.join(g_lib.fcsDIR.plugin_ui_icons_folder, "tool_icon_22.png")

    def CreateLayout(self):
        self.MenuFlushAll()
        self.MenuSubBegin("File")
        self.MenuAddString(self.NewLoad["id"], self.NewLoad["name"])
        self.MenuAddString(self.ESave["id"], self.ESave["name"])
        self.MenuAddString(self.ELoad["id"], self.ELoad["name"])
        self.MenuSubEnd()   
        self.MenuSubBegin("Info")
        self.MenuSubEnd()
        self.MenuFinished()

        self.GroupBeginInMenuLine()
        self.AddStaticText(self.ID_VER, 0)
        self.SetString(self.ID_VER, self.VerData)
        self.GroupEnd()

        g_lib.fcsResDLG.ResGUI(self, fcsID.fcs_select_n_go_gui_dlg)

        self.Add_and_Inject_GUIs()
        self.SetDefaultColor(fcsID.ID_SNG_OVERALL_GRP, c4d.COLOR_BG, g_lib.BG_DARKER_COL)
        return True
    ##### Load UI on Loading Startup as Default #####
    def InitValues(self):
        g_lib.fcsLog.DEBUG("F.C.S Select N' Go is Open Now", False, g_lib.DebugMode)
        self.SetLong(fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU, fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU)
        self.SetLong(fcsID.ID_SNG_OPT_BATCHFITER_MENU, self.ID_SNG_OPT_BATCHFITER_MENU_ALL)
        self.SetString(fcsID.ID_SNG_OPT_PATH_EDITBOX, "Add directory....")
        self.SetString(self.OBJName, "File Name")
        self.Enable(self.OBJName, False)
        self.SetString(fcsID.ID_SNG_OPT_SEARCH_EDITBOX, "Search....")
        self.SetBool(fcsID.ID_SNG_OPT_MODE_EXPORT, True)
        self.Enable(fcsID.ID_SNG_OPT_BATCH_IMPORT_GRP, False)
        self.SetDefaultColor(self.ID_VER, c4d.COLOR_TEXT, g_lib.DARK_BLUE_TEXT_COL)    
        return True

    def Command(self, id, msg):

        if self.BatchListItemsUIs:
            for itemfile in self.BatchListItemsUIs:
                chk_id = itemfile.split('--')[0]
                FNAME = itemfile.split('--')[1]
                FEXT = itemfile.split('--')[2]
                EXT_id = itemfile.split('--')[3]                            
                if id == int(chk_id):
                    AppendCheckFile = chk_id + "--" + FNAME + "--" + FEXT
                    self.CheckedOnBatchList.append(AppendCheckFile)

        if(id==fcsID.ID_SNG_OPT_BATCH_CHK_ALL):
            if self.GetBool(fcsID.ID_SNG_OPT_BATCH_CHK_ALL)==True:
                self.CheckAllBatchFileItem(CheckAll=True)
            else:
                self.CheckAllBatchFileItem(CheckAll=False)

        if(id==fcsID.ID_SNG_OPT_BATCHFITER_MENU):
            self.BatchList = []
            self.BatchListItemsUIs = []
            self.SearchFilterBatchFileSystem()
            self.LoadFiles_To_BatchListViewGUI()
            self.CheckedItems()

        if(id==fcsID.ID_SNG_OPT_SEARCH_EDITBOX):
            SearchBar = self.GetString(fcsID.ID_SNG_OPT_SEARCH_EDITBOX).lower()
            print(SearchBar)
            self.BatchList = []
            self.BatchListItemsUIs = []
            self.SearchFilterBatchFileSystem()
            self.LoadFiles_To_BatchListViewGUI()
            self.CheckedItems()

        if (id==self.ID_SNG_OPT_BATCH_CUSTOM_BTN):
            self.Add_Batch_Dir()
            self.BatchListItemsUIs = []
            self.CheckedOnBatchList = []
            self.SearchFilterBatchFileSystem()
            self.LoadFiles_To_BatchListViewGUI()

        if (id==self.UI_BTN_DirLoad):
            self.Add_RecentFolderToList()

        if (id==19897):
            self.Add_RecentFolder()
            
        if (id == self.UI_FormatSettingsGearButton):
            self.OpenExports_or_Imports_Settings()

        # User choose exporting as a single object file.
        if (id==fcsID.ID_SNG_OPT_CHK_SINGLE_OBJ):
            if self.GetBool(fcsID.ID_SNG_OPT_CHK_SINGLE_OBJ)==True:
                self.Enable(self.OBJName, True)
            if self.GetBool(fcsID.ID_SNG_OPT_CHK_SINGLE_OBJ)==False:
                self.Enable(self.OBJName, False)

        # Use Chosse Mode between Export or Import.      

        if self.GetInt32(fcsID.ID_SNG_OPTIONS_TABS) == fcsID.ID_SNG_EXPORT_OPTIONS_TAB_GRP:
            #
            print("Export Tab")
            self.SetString(fcsID.ID_SNG_OPT_EXPORT_IMPORT_BTN, "Export")

        if self.GetInt32(fcsID.ID_SNG_OPTIONS_TABS) == fcsID.ID_SNG_IMPORT_OPTIONS_TAB_GRP:
            # 
            print("Import Tab")
            self.SetString(fcsID.ID_SNG_OPT_EXPORT_IMPORT_BTN, "Import")


        if (id == fcsID.ID_SNG_OPT_CHK_BATCH_IMPORT):
            if self.GetBool(fcsID.ID_SNG_OPT_CHK_BATCH_IMPORT)==False:
                self.Enable(fcsID.ID_SNG_OPT_BATCH_IMPORT_GRP, False)
            else:
                self.Enable(fcsID.ID_SNG_OPT_BATCH_IMPORT_GRP, True)

        # Exporting Objects or Importing Objects
        if (id == fcsID.ID_SNG_OPT_EXPORT_IMPORT_BTN):

            if self.GetInt32(fcsID.ID_SNG_OPTIONS_TABS) == fcsID.ID_SNG_IMPORT_OPTIONS_TAB_GRP:
                if self.GetBool(fcsID.ID_SNG_OPT_CHK_BATCH_IMPORT)==True:
                    if self.BatchListItemsUIs:
                        amountOfBathFiles = len(self.BatchList)
                        currentNum = 1
                        for itemfile in self.BatchListItemsUIs:
                            percent = float(currentNum)/amountOfBathFiles*100
                            currentNum+=1                            
                            chk_id = itemfile.split('--')[0]
                            FNAME = itemfile.split('--')[1]
                            FEXT = itemfile.split('--')[2]
                            EXT_id = itemfile.split('--')[3]                            
                            if self.GetBool(int(chk_id))==True:
                                self.BatchImporting(fn=FNAME, filterId=EXT_id, extensionStr=FEXT)
                            else:
                                pass
                            g_lib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, g_lib.BG_GREEN_COL, "Importing Batch Files....."+str(percent)+"%")

                if self.GetBool(fcsID.ID_SNG_OPT_CHK_BATCH_IMPORT)==False:
                    self.Importing()

            if self.GetInt32(fcsID.ID_SNG_OPTIONS_TABS) == fcsID.ID_SNG_EXPORT_OPTIONS_TAB_GRP:
                # Just put Selected Objects Together and Export 
                if self.GetBool(fcsID.ID_SNG_OPT_CHK_SINGLE_OBJ)==True:
                    self.EnableExportMultiTogether()
                    self.Exporting()
                    c4d.CallCommand(1019951, 1019951) # Delete Without
                    self.OpenExportFolder()
                # Just Expot Selected Ojects Seprated. 
                if self.GetBool(fcsID.ID_SNG_OPT_CHK_SINGLE_OBJ)==False:                                      
                    self.Exporting()
                    
        return True

    def CoreMessage(self, id, msg):
        if id == c4d.EVMSG_CHANGE:
            pass
            #self.GetFilesFromDirectory()
            #self.LoadFiles_To_BatchListViewGUI()
        return True


# -----------------------------------------------------
#  GameDev Pipline Tools for Select N'Go Tool Addon
# -----------------------------------------------------

# ----- | Unity Tools | ------- # 
# Unity Collison Object
class Unity_UCX_Data(CommandData):      # Unity Object Collison Command Go Button with Setup GUI Settings

    dialog = None

    def Execute_Settings_Menu(self):

        IDM0 = c4d.FIRST_POPUP_ID
        IDM1 = c4d.FIRST_POPUP_ID+1
        IDM2 = c4d.FIRST_POPUP_ID+2

        menu = c4d.BaseContainer()
        menu.SetString(IDM0,'&i1000474&'+'Collison Setup')
        menu.InsData(0, '') # Append separator
        menu.SetString(IDM1,'&i12236&'+'Convert Collsion')
        menu.InsData(0, '') # Append separator
        menu.SetString(IDM2,'&i12112&'+'Select All Collsion')        

        result = gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)

        if result==IDM0:
            pass

        if result==IDM1:

            doc = c4d.documents.GetActiveDocument()

            c4d.CallCommand(100004768, 100004768) # // Select Children

            SEL_C4D_OBJS = doc.GetActiveObjects(1)

            if not SEL_C4D_OBJS:
                fcsLog.ERROR("Select an object or group.", True, g_lib.DebugMode)
                pass

            elif SEL_C4D_OBJS:

                for obj in SEL_C4D_OBJS:

                    if obj.CheckType(1053288):

                        doc.SetActiveObject(obj)

                        if obj[c4d.ID_COLLISON_TYPE_COMBOBOX] == 0:
                            obj.SetName(obj.GetName()+"_fcsUnityBC")

                        if obj[c4d.ID_COLLISON_TYPE_COMBOBOX] == 1:
                            obj.SetName(obj.GetName()+"_fcsUnityBCT")

                        if obj[c4d.ID_COLLISON_TYPE_COMBOBOX] == 2:
                            obj.SetName(obj.GetName()+"_fcsUnityMC")

                        if obj[c4d.ID_COLLISON_TYPE_COMBOBOX] == 3:
                            obj.SetName(obj.GetName()+"_fcsUnityMCT")

                        c4d.CallCommand(12236) #// Make Editable

                    else:
                        pass  

                fcsLog.INFO("All Unity collisons as been coverted", True, g_lib.DebugMode)

            else:
                fcsLog.ERROR("No Unity collisons was found to convert.", True, g_lib.DebugMode)                                       

        if result==IDM2:

            doc = c4d.documents.GetActiveDocument()

            c4d.CallCommand(100004768, 100004768) # // Select Children

            SEL_C4D_OBJS = doc.GetActiveObjects(1)

            if not SEL_C4D_OBJS:
                fcsLog.ERROR("Select an object or group.", True, g_lib.DebugMode)
                pass

            elif SEL_C4D_OBJS:

                for obj in SEL_C4D_OBJS:

                    if obj.CheckType(1053288):

                        if obj:
                            doc.SetSelection(obj, c4d.SELECTION_ADD)
                    else:
                        doc.SetSelection(obj, c4d.SELECTION_SUB)

                SEL_UCX_OBJS = doc.GetSelection()
                if not SEL_UCX_OBJS:
                    fcsLog.ERROR("No Unity collisons was found in the scene.", True, g_lib.DebugMode)
                    return
                else:
                    pass     

            else:
                fcsLog.ERROR("No Unity collisons was found in the scene.", True, g_lib.DebugMode)             
                        
        return True

 #####  \\  Main C4D CommandData Operations Functions which are Methods to Override for CommandData class. \\   #####
    def Init(self, op):
        return True

    def Message(self, type, data):
        return True

    def Execute(self, doc):
        doc = c4d.documents.GetActiveDocument()

        SEL_C4D_OBJ = doc.GetActiveObjects(1)
        if not SEL_C4D_OBJ:
            #fcsLog.ERROR("Select an Object!", True)
            return g_lib.Run_Tool_ID(1053288)

        elif SEL_C4D_OBJ:

            for obj in SEL_C4D_OBJ:
                add_ucx = c4d.BaseObject(1053288)
                g_lib.SetObjectName("Unity Collison Object", add_ucx, 10000)
                add_ucx.InsertUnder(obj)
                doc.SetActiveObject(add_ucx)
                # Reset Replacer Local Position Data 
                c4d.CallCommand(1019940) # Reset PSR
        else:
            g_lib.Run_Tool_ID(1053288)

        return True

    def RestoreLayout(self, sec_ref):      
        return True

    def ExecuteOptionID(self, doc, plugid, subid):
        self.Execute_Settings_Menu()
        return True
class O_Unity_Object_Data(ObjectData):  # Unity Object Collison

    CONTROL_HANDLES_COUNT = 6

    #------------------------------------
    #   CUSTOM HELPER METHODS FUNCTIONS
    #------------------------------------    
    def get_draw_screen_uvcoords(self, bmp, x, y, w, h):
        bmpw, bmph = map(float, bmp.GetSize())
        corners = [x / bmpw, y / bmph, (x + w) / bmpw, (y + h) / bmph]
        return [
            c4d.Vector(corners[0], corners[1], 0.0),
            c4d.Vector(corners[2], corners[1], 0.0),
            c4d.Vector(corners[2], corners[3], 0.0),
            c4d.Vector(corners[0], corners[3], 0.0)]
    def get_draw_screen_padr(self, pos, w, h, align):
        if align & self.ALIGN_LEFT:
            xoff = 0
        elif align & self.ALIGN_RIGHT:
            xoff = w
        elif align & self.ALIGN_CENTERH or True:
            xoff = w / 2.0
        if align & self.ALIGN_TOP:
            yoff = 0
        elif align & self.ALIGN_BOTTOM:
            yoff = h
        elif align & self.ALIGN_CENTERV or True:
            yoff = h / 2.0

        x, y = (pos.x - xoff, pos.y - yoff)
        return [
            c4d.Vector(x,     y, 0.0),
            c4d.Vector(x + w, y, 0.0),
            c4d.Vector(x + w, y + h, 0.0),
            c4d.Vector(x,     y + h, 0.0)]
    def LookatCamera(self, iconobj, axis_sys):

        doc = c4d.documents.GetActiveDocument()

        bd = doc.GetRenderBaseDraw()
        if bd is None: 
            return c4d.EXECUTIONRESULT_OK
        cp = bd.GetSceneCamera(doc)
        if cp is None: 
            cp = bd.GetEditorCamera()
        if cp is None: 
            return c4d.EXECUTIONRESULT_OK
        local = cp.GetMg().off * (~(iconobj.GetUpMg() * iconobj.GetFrozenMln())) - iconobj.GetRelPos()
        hpb = utils.VectorToHPB(local)
        if axis_sys == "y":
            hpb.y = iconobj.GetRelRot().y
        if axis_sys == "x":
            hpb.x = iconobj.GetRelRot().x
        hpb.z = iconobj.GetRelRot().z
        #----------------------------
        # Switching Between Y and X which is above (axis_sys)
        """
        hpb.y = iconobj.GetRelRot().y
        hpb.x = iconobj.GetRelRot().x        
        """
        #----------------------------
        iconobj.SetRelRot(hpb) 
        iconobj.Message(c4d.MSG_UPDATE)
        return c4d.EXECUTIONRESULT_OK

    def __init__(self):
        self.SetOptimizeCache(True) 

    def Init(self, op):

        FRONT_CTRL = {'ID_NAME':op[c4d.ID_UNITY_UCX_FRONT_CTRL], 'DefaultValue':-200.0, 'Param':c4d.ID_UNITY_UCX_FRONT_CTRL, }
        BACK_CTRL = {'ID_NAME':op[c4d.ID_UNITY_UCX_BACK_CTRL], 'DefaultValue':200.0, 'Param':c4d.ID_UNITY_UCX_BACK_CTRL, }
        LEFT_CTRL = {'ID_NAME':op[c4d.ID_UNITY_UCX_LEFT_CTRL], 'DefaultValue':-200.0, 'Param':c4d.ID_UNITY_UCX_LEFT_CTRL, }
        RIGHT_CTRL = {'ID_NAME':op[c4d.ID_UNITY_UCX_RIGHT_CTRL], 'DefaultValue':200.0, 'Param':c4d.ID_UNITY_UCX_RIGHT_CTRL,}
        TOP_CTRL = {'ID_NAME':op[c4d.ID_UNITY_UCX_TOP_CTRL], 'DefaultValue':200.0, 'Param':c4d.ID_UNITY_UCX_TOP_CTRL, }
        BOTTOM_CTRL = {'ID_NAME':op[c4d.ID_UNITY_UCX_BOTTOM_CTRL], 'DefaultValue':-200.0, 'Param':c4d.ID_UNITY_UCX_BOTTOM_CTRL, }
        ObjectParams = [FRONT_CTRL, BACK_CTRL, LEFT_CTRL, RIGHT_CTRL, TOP_CTRL, BOTTOM_CTRL]

        for param in ObjectParams:
            self.InitAttr(op, float, param['Param'])

        op[c4d.ID_UNITY_UCX_FRONT_CTRL] = FRONT_CTRL['DefaultValue']+100.0
        op[c4d.ID_UNITY_UCX_BACK_CTRL] = BACK_CTRL['DefaultValue']-100.0
        op[c4d.ID_UNITY_UCX_LEFT_CTRL] = LEFT_CTRL['DefaultValue']+100.0
        op[c4d.ID_UNITY_UCX_RIGHT_CTRL] = RIGHT_CTRL['DefaultValue']-100.0
        op[c4d.ID_UNITY_UCX_TOP_CTRL] = TOP_CTRL['DefaultValue']-100.0
        op[c4d.ID_UNITY_UCX_BOTTOM_CTRL] = BOTTOM_CTRL['DefaultValue']+100.0

        op[c4d.ID_COLOR_PICKER_COL1] = c4d.Vector(0.08203125, 0.1015625, 0.12109375)
        op[c4d.ID_COLOR_PICKER_COL2] = c4d.Vector(0.99609375, 0.26953125, 0)
        op[c4d.ID_COLOR_PICKER_COL3] = c4d.Vector(0.99609375, 0.26953125, 0)

        op[c4d.ID_COLLISON_TYPE_COMBOBOX] = 0
        #self.SetLong(1014, 1015)

        return True

    def GetHandleCount(op):
        return O_Unity_Object_Data.CONTROL_HANDLES_COUNT

    def GetHandle(self, op, i, info):

        num = 100.0

        front_c = op[c4d.ID_UNITY_UCX_FRONT_CTRL]
        back_c = op[c4d.ID_UNITY_UCX_BACK_CTRL]
        left_c = op[c4d.ID_UNITY_UCX_LEFT_CTRL]
        right_c = op[c4d.ID_UNITY_UCX_RIGHT_CTRL]
        top_c = op[c4d.ID_UNITY_UCX_TOP_CTRL]
        bottom_c = op[c4d.ID_UNITY_UCX_BOTTOM_CTRL]

        deltaX = 0 - 0
        deltaY = 0 - 0
        deltaZ = float(front_c) - float(back_c)
        distanceZ = math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ)

        deltaX = 0 - 0
        deltaY = float(bottom_c) - float(top_c)
        deltaZ =  0 - 0
        distanceY = math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ)

        deltaX = float(left_c) - float(right_c)
        deltaY = 0 - 0
        deltaZ =  0 - 0
        distanceX = math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ)

        XLoc = ( float(left_c) + float(right_c) ) / 2
        YLoc = ( float(top_c) + float(bottom_c) ) / 2
        ZLoc = ( float(front_c) + float(back_c) ) / 2


        front_rad = op[c4d.ID_UNITY_UCX_FRONT_CTRL]
        if front_rad is None: 
            front_rad = num

        back_rad = op[c4d.ID_UNITY_UCX_BACK_CTRL]
        if back_rad is None: 
            back_rad = num

        left_rad = op[c4d.ID_UNITY_UCX_LEFT_CTRL]
        if left_rad is None: 
            left_rad = -num

        right_rad = op[c4d.ID_UNITY_UCX_RIGHT_CTRL]
        if right_rad is None: 
            right_rad = num

        top_rad = op[c4d.ID_UNITY_UCX_TOP_CTRL]
        if top_rad is None: 
            top_rad = num

        bottom_rad = op[c4d.ID_UNITY_UCX_BOTTOM_CTRL]
        if bottom_rad is None: 
            bottom_rad = -num          

        if i is 0:
            info.position = c4d.Vector(XLoc, YLoc, front_rad)
            info.direction = c4d.Vector(0.0, 0.0, -1.0)
            info.type = c4d.HANDLECONSTRAINTTYPE_LINEAR

        if i is 1:
            info.position = c4d.Vector(XLoc, YLoc, back_rad)
            info.direction = c4d.Vector(0.0, 0.0, 1.0)
            info.type = c4d.HANDLECONSTRAINTTYPE_LINEAR

        if i is 2:
            info.position = c4d.Vector(left_rad, YLoc, ZLoc)
            info.direction = c4d.Vector(1.0, 0.0, 0.0) 
            info.type = c4d.HANDLECONSTRAINTTYPE_LINEAR

        if i is 3:
            info.position = c4d.Vector(right_rad, YLoc, ZLoc)
            info.direction = c4d.Vector(1.0, 0.0, 0.0)
            info.type = c4d.HANDLECONSTRAINTTYPE_LINEAR 

        if i is 4:
            info.position = c4d.Vector(XLoc, top_rad, ZLoc)
            info.direction = c4d.Vector(0.0, 1.0, 0.0)
            info.type = c4d.HANDLECONSTRAINTTYPE_LINEAR 

        if i is 5:
            info.position = c4d.Vector(XLoc, bottom_rad, ZLoc)
            info.direction = c4d.Vector(0.0, -1.0, 0.0)
            info.type = c4d.HANDLECONSTRAINTTYPE_LINEAR

    def CalulateDis(self, Xpt1, Xpt2, Ypt1, Ypt2, Zpt1, Zpt2):
        """
        Caluate Dis Between Two points.

        Code Example:

            deltaX = 0 - 0
            deltaY = 0 - 0
            deltaZ = float(-200.0) - float(200.0)
            distance = math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ)
            print(distance)

            LOG: 
                400.0
        """

        deltaX = Xpt1 - Xpt2
        deltaY = Ypt1 - Ypt2
        deltaZ = Zpt1 - Zpt1
        
        distance = math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ)

        return distance       

    def Draw(self, op, drawpass, bd, bh):
        front_c = op[c4d.ID_UNITY_UCX_FRONT_CTRL]
        back_c = op[c4d.ID_UNITY_UCX_BACK_CTRL]
        left_c = op[c4d.ID_UNITY_UCX_LEFT_CTRL]
        right_c = op[c4d.ID_UNITY_UCX_RIGHT_CTRL]
        top_c = op[c4d.ID_UNITY_UCX_TOP_CTRL]
        bottom_c = op[c4d.ID_UNITY_UCX_BOTTOM_CTRL]

        BG_FCS_ORANGE_COL = op[c4d.ID_COLOR_PICKER_COL1]
        BG_FCS_FRAME_COL = op[c4d.ID_COLOR_PICKER_COL2]
        BG_FCS_HANDLE_COL = op[c4d.ID_COLOR_PICKER_COL3]


        deltaX = 0 - 0
        deltaY = 0 - 0
        deltaZ = float(front_c) - float(back_c)
        distanceZ = math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ)

        deltaX = 0 - 0
        deltaY = float(bottom_c) - float(top_c)
        deltaZ =  0 - 0
        distanceY = math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ)

        deltaX = float(left_c) - float(right_c)
        deltaY = 0 - 0
        deltaZ =  0 - 0
        distanceX = math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ)

        XLoc = ( float(left_c) + float(right_c) ) / 2
        YLoc = ( float(top_c) + float(bottom_c) ) / 2
        ZLoc = ( float(front_c) + float(back_c) ) / 2

        v1 = c4d.Vector(0.0, 0.0, distanceZ)
        v2 = c4d.Vector(0.0, distanceY, 0.0)
        v3 = c4d.Vector(distanceX, 0.0, 0.0)
        off = c4d.Vector(XLoc, YLoc, ZLoc)
        mat2 = c4d.Matrix(off,v1,v2,v3)


        info2 = c4d.HandleInfo()

        if drawpass == c4d.DRAWPASS_HANDLES:

            bd.SetPen(BG_FCS_FRAME_COL)
            
            hitid = op.GetHighlightHandle(bd)
            bd.SetMatrix_Matrix(op, bh.GetMg())

            for i in xrange(self.CONTROL_HANDLES_COUNT):

                if i==hitid:
                    bd.SetPen(c4d.GetViewColor(c4d.VIEWCOLOR_SELECTION_PREVIEW))

                else:
                    bd.SetPen(BG_FCS_HANDLE_COL)
                
                info = c4d.HandleInfo()
                self.GetHandle(op, i, info)

                bd.DrawHandle(info.position, c4d.DRAWHANDLE_BIG, 0)

                v1_x = -20
                v1_y = 0
                v1_z = 0

                v2_x = 0
                v2_y = 20
                v2_z = 0           

                v3_x = 0
                v3_y = 0
                v3_z = 20

                v1 = c4d.Vector(v1_x, v1_y, v1_z)
                v2 = c4d.Vector(v2_x, v2_y, v2_z)
                v3 = c4d.Vector(v3_x, v3_y, v3_z)
                off = c4d.Vector(info.position)
                mat = c4d.Matrix(off,v1,v2,v3)
                bd.DrawBox(mat, 0.5, BG_FCS_HANDLE_COL, True)

                bd.DrawBox(mat, 0.5, BG_FCS_HANDLE_COL, True)

                bd.SetPen(BG_FCS_FRAME_COL)

            self.GetHandle(op, 0, info2)
            bd.DrawLine(c4d.Vector(XLoc, YLoc, ZLoc), info2.position, 0)
            self.GetHandle(op, 1, info2)
            bd.DrawLine(c4d.Vector(XLoc, YLoc, ZLoc), info2.position, 0)
            self.GetHandle(op, 2, info2)
            bd.DrawLine(c4d.Vector(XLoc, YLoc, ZLoc), info2.position, 0)
            self.GetHandle(op, 3, info2)
            bd.DrawLine(c4d.Vector(XLoc, YLoc, ZLoc), info2.position, 0)
            self.GetHandle(op, 4, info2)
            bd.DrawLine(c4d.Vector(XLoc, YLoc, ZLoc), info2.position, 0)
            self.GetHandle(op, 5, info2)
            bd.DrawLine(c4d.Vector(XLoc, YLoc, ZLoc), info2.position, 0)

            bd.DrawBox(mat2, 0.5, BG_FCS_FRAME_COL, True)

            """
            # ------------------------------------------------------
            # Draw a box outline for the object shape
            # with DrawLine(), Eg. bd.DrawLine(pt1, pt2, flags)
            # ------------------------------------------------------
            # Front CONTROL FACE DRAWLINE FOR OUTLINE SHAPE
            self.GetHandle(op, 0, info2) 
            #          (            POINT 1              )  (            POINT 2              )
            bd.DrawLine(c4d.Vector(left_c, top_c, front_c), c4d.Vector(right_c, top_c, front_c), 0)
            bd.DrawLine(c4d.Vector(left_c, bottom_c, front_c), c4d.Vector(right_c, bottom_c, front_c), 0)
            bd.DrawLine(c4d.Vector(right_c, top_c, front_c), c4d.Vector(right_c, bottom_c, front_c), 0)
            bd.DrawLine(c4d.Vector(left_c, top_c, front_c), c4d.Vector(left_c, bottom_c, front_c), 0)

            # Back CONTROL FACE DRAWLINE FOR OUTLINE SHAPE.
            self.GetHandle(op, 1, info2) 
            #          (            POINT 1              )  (            POINT 2              )
            bd.DrawLine(c4d.Vector(left_c, top_c, back_c), c4d.Vector(right_c, top_c, back_c), 0)
            bd.DrawLine(c4d.Vector(left_c, bottom_c, back_c), c4d.Vector(right_c, bottom_c, back_c), 0)
            bd.DrawLine(c4d.Vector(right_c, top_c, back_c), c4d.Vector(right_c, bottom_c, back_c), 0)
            bd.DrawLine(c4d.Vector(left_c, top_c, back_c), c4d.Vector(left_c, bottom_c, back_c), 0)
            """
            return c4d.DRAWRESULT_OK


        return c4d.DRAWRESULT_SKIP

    def DetectHandle(self, op, bd, x, y, qualifier):
        if qualifier&c4d.QUALIFIER_CTRL: 
            return c4d.NOTOK
        mg = op.GetMg()
        ret = c4d.NOTOK
        for i in xrange(self.GetHandleCount()):
            info = c4d.HandleInfo()
            self.GetHandle(op, i, info)
            if bd.PointInRange(info.position*mg, x, y):
                ret = i
                if not qualifier&c4d.QUALIFIER_SHIFT: break
        return ret

    def MoveHandle(self, op, undo, mouse_pos, hit_id, qualifier, bd):

        dst = op.GetDataInstance();

        info = c4d.HandleInfo()
        val = mouse_pos.x
        self.GetHandle(op, hit_id, info)
        
        if bd is not None:
            mg = op.GetUpMg()*undo.GetMl()
            pos = bd.ProjectPointOnLine(info.position*mg, info.direction^mg, mouse_pos.x, mouse_pos.y)
            val = (pos[0]*~mg)*info.direction
        
        if hit_id==0:
            dst.SetReal( c4d.ID_UNITY_UCX_FRONT_CTRL, -val )
        elif hit_id==1:
            dst.SetReal( c4d.ID_UNITY_UCX_BACK_CTRL, val )  #         
        elif hit_id==2:
            dst.SetReal( c4d.ID_UNITY_UCX_LEFT_CTRL, val )
        elif hit_id==3:
            dst.SetReal( c4d.ID_UNITY_UCX_RIGHT_CTRL, val ) #
        elif hit_id==4:
            dst.SetReal( c4d.ID_UNITY_UCX_TOP_CTRL, val ) #
        elif hit_id==5:
            dst.SetReal( c4d.ID_UNITY_UCX_BOTTOM_CTRL, -val )

            #dst.SetReal(c4d.PYSPHERIFYDEFORMER_RADIUS, utils.FCut(val, 0.0, sys.maxint))
        #elif hit_id==1:  
            #dst.SetReal(c4d.PYSPHERIFYDEFORMER_STRENGTH, utils.FCut(val*0.001, 0.0, 1.0))
        return True

    def GetVirtualObjects(self, op, hh):

        doc = c4d.documents.GetActiveDocument()

        #op.SetName("Unity_Collison")
        front_c = op[c4d.ID_UNITY_UCX_FRONT_CTRL]
        back_c = op[c4d.ID_UNITY_UCX_BACK_CTRL]
        left_c = op[c4d.ID_UNITY_UCX_LEFT_CTRL]
        right_c = op[c4d.ID_UNITY_UCX_RIGHT_CTRL]
        top_c = op[c4d.ID_UNITY_UCX_TOP_CTRL]
        bottom_c = op[c4d.ID_UNITY_UCX_BOTTOM_CTRL]

        deltaX = 0 - 0
        deltaY = 0 - 0
        deltaZ = float(front_c) - float(back_c)
        distanceZ = math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ)

        deltaX = 0 - 0
        deltaY = float(bottom_c) - float(top_c)
        deltaZ =  0 - 0
        distanceY = math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ)

        deltaX = float(left_c) - float(right_c)
        deltaY = 0 - 0
        deltaZ =  0 - 0
        distanceX = math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ)

        XLoc = ( float(left_c) + float(right_c) ) / 2
        YLoc = ( float(top_c) + float(bottom_c) ) / 2
        ZLoc = ( float(front_c) + float(back_c) ) / 2

        op[c4d.ID_BASEOBJECT_XRAY] = True
        op[c4d.ID_BASEOBJECT_USECOLOR] = 1
        op[c4d.ID_BASEOBJECT_COLOR] = op[c4d.ID_COLOR_PICKER_COL1]

        UCX_box = c4d.BaseObject(c4d.Ocube)
        UCX_box[c4d.PRIM_CUBE_LEN,c4d.VECTOR_X] = distanceX
        UCX_box[c4d.PRIM_CUBE_LEN,c4d.VECTOR_Y] = distanceY
        UCX_box[c4d.PRIM_CUBE_LEN,c4d.VECTOR_Z] = distanceZ
        UCX_box[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_X] = XLoc
        UCX_box[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Y] = YLoc
        UCX_box[c4d.ID_BASEOBJECT_REL_POSITION,c4d.VECTOR_Z] = ZLoc
        # Make Display Tag for the Outline frame.
        Display_TagID = 5613
        DisplayWireframe_Tag = c4d.BaseTag(Display_TagID)        
        DisplayWireframe_Tag[c4d.DISPLAYTAG_AFFECT_DISPLAYMODE] = True        
        #DisplayWireframe_Tag[c4d.DISPLAYTAG_AFFECT_DISPLAYMODE] = False  
        DisplayWireframe_Tag[c4d.DISPLAYTAG_SDISPLAYMODE] = 4  #[c4d.DISPLAYTAG_SDISPLAYMODE] = 6 #DISPLAYTAG_SDISPLAYMODE = 4
        DisplayWireframe_Tag[c4d.DISPLAYTAG_WDISPLAYMODE] = 0
        #UCX_box.InsertTag(DisplayWireframe_Tag)


        #self.LookatCamera(iconobj=p, axis_sys="y")
        #self.LookatCamera(iconobj=p, axis_sys="x")

        if UCX_box is None: 
            return None

        UCX_box.SetName(op.GetName())

        return UCX_box

    def Execute(op, doc, bt, priority, flags):
        return c4d.EXECUTIONRESULT_OK



# Satellite To GroundPoly Tool
class SatelliteToGpolyTool_Data(CommandData):

    DLG = fcs_satellite_to_gpoly_gui
    pluginid = 1053986
    dialog = None

    def __init__(self):
        super(SatelliteToGpolyTool_Data, self).__init__()    

    def Init(self, op):
        return True

    def Message(self, type, data):
        return True

    def Execute(self, doc):
        if self.dialog is None:
            self.dialog = self.DLG.SatToGPoly_GUI()
            g_lib.fcsLog.DEBUG("F.C.S Satellite To Ground Poly is Open Now!", False, g_lib.DebugMode)
        return self.dialog.Open(dlgtype=c4d.DLG_TYPE_ASYNC, pluginid=self.pluginid, defaultw=10, defaulth=10)

    def RestoreLayout(self, sec_ref):
        if self.dialog is None:
            self.dialog =self.DLG.SatToGPoly_GUI()
        return self.dialog.Restore(pluginid=self.pluginid, secret=sec_ref)

    def ExecuteOptionID(self, doc, plugid, subid):
        return True

