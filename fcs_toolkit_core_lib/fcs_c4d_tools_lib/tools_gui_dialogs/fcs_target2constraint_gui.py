"""
Target to Constraint Tool Window GUI Dialog
"""
#  // Imports for Cinema 4D //
import c4d
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as WindowDialog        # eg:( class My_Dialog(c4d.gui.GeDialog): ) to ( class My_Dialog(WindowDialog): ) Just keeping code line short.
from c4d.plugins import CommandData, TagData, ObjectData
from random import randint
# Python Plugin System Load Modules Dynamically With importlib.
import importlib
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
fcsID = g_lib.fcsID
jsonEdit = g_lib.jsonEdit


class Target2Constraint_Dialog(WindowDialog):
    """ Target to Constraint Tool Dialog. """
 ##### ID Numbers for UI Elements #####
    GroupID = 1001
    UI_Linkbox1 = 1003
    UI_BTN_T2C = 1004
    rootLinkBaseContainer = c4d.BaseContainer()

    def __init__(self, global_strings):
        self.IDS = global_strings
        
 ##### UI Window Dialog #####
    def CreateLayout(self):
        self.SetTitle("Target to Constraint")   # <---| My Title for the MAIN Dialog
        self.GroupBegin(self.GroupID, c4d.BFH_MASK, 2, 0, "Object")
        self.GroupBorder(c4d.BORDER_GROUP_IN)
        self.GroupBorderSpace(10, 4, 10, 4)
        self.linkBoxForName = self.AddCustomGui(self.UI_Linkbox1, c4d.CUSTOMGUI_LINKBOX, "", c4d.BFH_CENTER, 320, 10, self.rootLinkBaseContainer)
        BTN_INFO_SETTINGS = { 'btn_look':c4d.BORDER_NONE, 'tip':"<b>Generate</b>", 'icon':g_lib.fcsDIR.I(i='constraint_icon'), 'clickable':True}
        g_lib.Add_Image_Button(ui_instance=self, button_id=self.UI_BTN_T2C, btn_settings=BTN_INFO_SETTINGS)
        self.GroupEnd()
        return True 
 ##### Main UI Operations Functions #####
    def RandomColor(self):
        r = randint(0,255) / 256.0
        g = randint(0,255) / 256.0
        b = randint(0,255) / 256.0
        return c4d.Vector(r, g, b)

    def Generate(self):
        doc = c4d.documents.GetActiveDocument()
        objs = doc.GetActiveObjects(1)
        if not objs:
            gui.MessageDialog("You must select an object!")
            return False
        for Link_Object in objs:
            #Link_Object = doc.GetActiveObject()
            LinkBox = self.linkBoxForName.GetLink().GetName()
            Sel_Target = doc.SearchObject(LinkBox)
            tagID = 1018074
            SplineConstraint = c4d.BaseTag(tagID)
            SplineConstraint[c4d.HAIR_CONSTRAINTS_TAG_DRAW_ANCHOR]=True
            SplineConstraint[c4d.HAIR_CONSTRAINTS_TAG_ANCHOR_LINK]=Link_Object
            Sel_Target.InsertTag(SplineConstraint)
            SplineConstraint.SetBit(c4d.BIT_AOBJ)
            doc.SetActiveTag(SplineConstraint, c4d.SELECTION_ADD)
            #c4d.CallButton(SplineConstraint, c4d.HAIR_CONSTRAINTS_TAG_SET_ANCHOR)            
            c4d.EventAdd()
        return True
 ##### Excuting Operations #####
    def Command(self, id, msg=None):

        if id==self.UI_BTN_T2C:
            self.Generate() 

        return True                        
