import c4d
import os 
import sys
import math 
import glob 
import time
import datetime
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as WindowDialog        # eg:( class My_Dialog(c4d.gui.GeDialog): ) to ( class My_Dialog(WindowDialog): ) Just keeping code line short.
from c4d.plugins import CommandData, TagData, ObjectData
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
#from ..fcs_c4d_dialogs import fcs_common_global_lib as g_lib

fcsID = g_lib.fcsID
jsonEdit = g_lib.jsonEdit
config_data = None


class Pesets_Popup(WindowDialog):
    # 
    textures = [ ]
    item_menu_ids = [ ]

    def AddUVCheckersMapMat_Script_Tool(self, filename_TEXpath): # Add UV Checkers Map Material Script
        doc = c4d.documents.GetActiveDocument()
        MatName = "UV"
        doc = c4d.documents.GetActiveDocument()
        # Open this image from the res folder.
        #filename_TEXpath = c4d.storage.LoadDialog(type=c4d.FILESELECTTYPE_ANYTHING, title="Save Your Project Data in a Directory.", flags=c4d.FILESELECT_LOAD, force_suffix="png", def_path=PLUGIN_DIR().plugin_UVMaps_folder)
        
        shdBitmap = c4d.BaseShader(c4d.Xbitmap)
        if shdBitmap is None:
            print("Error: Shader allocation failed")
            return
        shdBitmap[c4d.BITMAPSHADER_FILENAME] = filename_TEXpath
        PREVIEWSIZE=1
        ActTag = doc.GetActiveTag()
        # If not Selected
        if ActTag is None:
            Material_Tex = c4d.BaseMaterial(5703)               # Generate Material
            Material_Tex[c4d.ID_BASELIST_NAME] = MatName        # Generate Material Name                  
            Material_Tex[c4d.MATERIAL_PREVIEWSIZE]=PREVIEWSIZE  # Set Material Size
            Material_Tex[c4d.MATERIAL_USE_REFLECTION]=False
            Material_Tex[c4d.MATERIAL_COLOR_SHADER] = shdBitmap # Set Material Shader Tex
            Material_Tex.InsertShader(shdBitmap)                # Insert Material Shader 
            doc.InsertMaterial(Material_Tex)                    # Insert Material
            c4d.EventAdd()
            return True
        # If Selected
        Material_Tex = c4d.BaseMaterial(5703)               # Generate Material
        Material_Tex[c4d.ID_BASELIST_NAME] = MatName        # Generate Material Name                  
        Material_Tex[c4d.MATERIAL_PREVIEWSIZE]=PREVIEWSIZE  # Set Material Size
        Material_Tex[c4d.MATERIAL_USE_REFLECTION]=False
        Material_Tex[c4d.MATERIAL_COLOR_SHADER] = shdBitmap # Set Material Shader Tex
        Material_Tex.InsertShader(shdBitmap)                # Insert Material Shader 
        doc.InsertMaterial(Material_Tex)                    # Insert Material
        ActTag[c4d.TEXTURETAG_MATERIAL]=Material_Tex
        ActTag[c4d.TEXTURETAG_PROJECTION]=6                    
        c4d.EventAdd()        
        return True

    def GetFilesFromDirectory(self, FileData):
        """ Get files from directory path, by their file extension."""
        path_dir = g_lib.fcsDIR.plugin_c4d_texture_presets_folder
        if os.path.exists(path_dir):
            # Looking for all files in folder.    
            os.chdir(path_dir)
            # Get all files with extension from the folder.
            for each_file in glob.glob(FileData+"*"):
                # Get all files and split or cut off the extension from it.
                for files in each_file.splitlines():

                    fileNameStart = each_file.split(FileData+"_")[1]

                    fileNameEnd = fileNameStart.split("Tex.png")

                    if int(fileNameEnd[0]) == 512:
                        texture_data = str(1) + "--" + fileNameEnd[0] + "--" + each_file
                        self.textures.append(texture_data)

                    if int(fileNameEnd[0]) == 1024:
                        texture_data = str(2) + "--" + fileNameEnd[0] + "--" + each_file
                        self.textures.append(texture_data)

                    if int(fileNameEnd[0]) == 2048:
                        texture_data = str(3) + "--" + fileNameEnd[0] + "--" + each_file
                        self.textures.append(texture_data)

                    if int(fileNameEnd[0]) == 4096:
                        texture_data = str(4) + "--" + fileNameEnd[0] + "--" + each_file
                        self.textures.append(texture_data)

                    if int(fileNameEnd[0]) == 8192:
                        texture_data = str(5) + "--" + fileNameEnd[0] + "--" + each_file
                        self.textures.append(texture_data)

                    if int(fileNameEnd[0]) == 16384:
                        texture_data = str(6) + "--" + fileNameEnd[0] + "--" + each_file
                        self.textures.append(texture_data)
        return True

    def MaterialSizePopupMenu(self, texture_name):
        # Clear lists.
        self.textures = [ ]
        self.item_menu_ids = [ ]

        # Add Textures + texture size to list.
        self.GetFilesFromDirectory(FileData=texture_name)

        self.textures.sort()
        
        menu = c4d.BaseContainer()

        degit = 0

        for i in self.textures:

            degit+=1 

            for files in i.splitlines():

                file_size = i.split("--")[1]

            IDM = c4d.FIRST_POPUP_ID+degit

            menu.SetString( IDM, str(file_size) + " x " + str(file_size) )

            self.item_menu_ids.append(i+"--"+ str(IDM))

            print(i+" - "+ str(IDM))


        result = gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)            

        if self.item_menu_ids:

            for i in self.item_menu_ids:

                for files in i.splitlines():

                    ITEM_ID = i.split("--")[3]
                    ITEM_FILENAME = i.split("--")[2]

                    if (result == int(ITEM_ID)):
                        fn_path = os.path.join(g_lib.fcsDIR.plugin_c4d_texture_presets_folder, ITEM_FILENAME)
                        self.AddUVCheckersMapMat_Script_Tool(filename_TEXpath=fn_path)
                        print(ITEM_ID)



        return True


    def ToolUI(self):
        self.GroupBegin(180, c4d.BFH_CENTER, 1, 0, "")

        self.AddStaticText(0, c4d.BFH_CENTER, 0, 15, "UV Materials", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text


        presetIcon_set6 = {'id':101, 'btn_look':c4d.BORDER_NONE, 'tip':"Grid Checkers", 'icon':g_lib.fcsDIR.I(i='checker_6'), 'clickable':True}
        presetIcon_set5 = {'id':102, 'btn_look':c4d.BORDER_NONE, 'tip':"Grid Checkers", 'icon':g_lib.fcsDIR.I(i='checker_5'), 'clickable':True}
        presetIcon_set4 = {'id':103, 'btn_look':c4d.BORDER_NONE, 'tip':"Grid Checkers", 'icon':g_lib.fcsDIR.I(i='checker_4'), 'clickable':True}
        presetIcon_set3 = {'id':104, 'btn_look':c4d.BORDER_NONE, 'tip':"Grid Checkers", 'icon':g_lib.fcsDIR.I(i='checker_3'), 'clickable':True}
        presetIcon_set2 = {'id':105, 'btn_look':c4d.BORDER_NONE, 'tip':"Grid Checkers", 'icon':g_lib.fcsDIR.I(i='checker_2'), 'clickable':True}
        presetIcon_set1 = {'id':106, 'btn_look':c4d.BORDER_NONE, 'tip':"Grid Checkers", 'icon':g_lib.fcsDIR.I(i='checker_1'), 'clickable':True}
        presetIconButtons = [ presetIcon_set1, presetIcon_set2, presetIcon_set3, presetIcon_set4, presetIcon_set5, presetIcon_set6 ]

        self.GroupBegin(188, c4d.BFH_CENTER, 2, 0, "")

        for i in presetIconButtons:
            g_lib.Add_Image_Button_GUI(ui_instance=self, btn_settings=i)

        self.GroupEnd()
        return True


    # // Main GeDialog Overrides //
    """
    The __init__ is an Constuctor and help get 
    and passes data on from the another class.
    """
    def __init__(self):
        super(Pesets_Popup, self).__init__()




    # UI Layout
    def CreateLayout(self):
        """ Window GUI Layout """
        self.ToolUI()
        return True
    
    # Called when the dialog is initialized by the GUI / GUI's startup values basically.
    def InitValues(self):
        g_lib.fcsLog.DEBUG("F.C.S UV Checkers Map Materials is Open Now!", False, g_lib.DebugMode)

        col = c4d.Vector(0.125, 0.125, 0.125)
        self.SetDefaultColor(188, c4d.COLOR_BG, col) 

        col2 = c4d.Vector(0.0234375, 0.53515625, 0.765625)
        self.SetDefaultColor(180, c4d.COLOR_BG, col2)  
        return True
 
    # Excuting Commands for UI Elements Functions.
    def Command(self, id, msg):

        if id == id:
            print("hello code ID: "+ str(id))

        if (id == 106):
            self.MaterialSizePopupMenu(texture_name="UV_CB")

        if (id == 105):
            self.MaterialSizePopupMenu(texture_name="UV_CCNB")

        if (id == 104):
            self.MaterialSizePopupMenu(texture_name="UV_CNB")

        if (id == 103):
            self.MaterialSizePopupMenu(texture_name="UV_Default")

        if (id == 102):
            self.MaterialSizePopupMenu(texture_name="UV_GameDevMap1")

        if (id == 101):
            self.MaterialSizePopupMenu(texture_name="UV_GameDevMap2")

        #self.Close()
        return True

    def CoreMessage(self, id, msg):

        if id == c4d.EVMSG_CHANGE:
            self.Close()

        if id == c4d.EVMSG_UPDATEHIGHLIGHT:
            self.Close()

        if id == c4d.EVMSG_ACTIVEVIEWCHANGED:
            self.Close()            

        if id == c4d.EVMSG_MATERIALSELECTION:
            self.Close()   

        if id == c4d.EVMSG_DOCUMENTRECALCULATED:
            self.Close()  

        if id == c4d.EVMSG_RAYTRACER_FINISHED:
            self.Close()  

        if id == c4d.EVMSG_UPDATEBASEDRAW:
            self.Close()  

        if id == c4d.EVMSG_TIMECHANGED:
            self.Close()  

        return True  
