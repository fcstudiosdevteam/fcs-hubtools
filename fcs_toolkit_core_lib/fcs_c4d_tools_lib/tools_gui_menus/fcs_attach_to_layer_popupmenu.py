#  // Imports for Cinema 4D //
import c4d
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as WindowDialog        # eg:( class My_Dialog(c4d.gui.GeDialog): ) to ( class My_Dialog(WindowDialog): ) Just keeping code line short.
from c4d.plugins import CommandData, TagData, ObjectData
from random import randint
# Python Plugin System Load Modules Dynamically With importlib.
import importlib
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
#from res._fcs_lib.fcs_module_lib import fcs_common_global_lib as g_lib
fcsID = g_lib.fcsID
jsonEdit = g_lib.jsonEdit


LayerListItems = []
LayerIDsItems = []

def Add_Objet_To_Layer(LAYER_NAME):
    g_lib.GetLayer(layername=LAYER_NAME)
    print(g_lib.MatchedLayer)
    if(g_lib.MatchedLayer == None):
        print("we have a problem")
    LayorCol = g_lib.MatchedLayer[c4d.ID_LAYER_COLOR]
    doc = c4d.documents.GetActiveDocument()
    objs = doc.GetActiveObjects(1)
    for obj in objs:
        obj[c4d.ID_BASEOBJECT_USECOLOR]=1
        obj[c4d.ID_LAYER_LINK] = g_lib.MatchedLayer
        obj[c4d.ID_BASEOBJECT_COLOR] = LayorCol
    c4d.EventAdd()
    return True
#
def GetAll_Childen_Of_Layers(AllLayers):
    for layer in AllLayers:
        # Recursive Checking System
        if(layer.GetChildren > 0):
            # Get Name Of Layer.
            layer_name = layer.GetName()
            LayerListItems.append(layer_name)
            # Check Again.
            GetAll_Childen_Of_Layers(layer.GetChildren())
    return True

# Get the List of all object layers in the layer manager and Add to Lic4d.storage.
def GetAll_ObjectLayers():
    global LayerListItems
    LayerListItems = []
    # Get the List of the layer manager
    doc = c4d.documents.GetActiveDocument()
    root = doc.GetLayerObjectRoot() #Gets the layer manager
    LayersList = root.GetChildren()
    GetAll_Childen_Of_Layers(AllLayers=LayersList)
    return True

# Create Popup Menu.
def PopupMenu_ShowLayers():
    
    GetAll_ObjectLayers()
    LayerIDsItems =[]      # to clear the List every time its excute.

    # Main menu
    menu = c4d.BaseContainer()

    if LayerListItems:
        degit = 0
        #L = len(self.LayerListItems)
        for i in LayerListItems:
            # Make Item ID
            degit+=1
            Item_ID = c4d.FIRST_POPUP_ID+degit
            # Layer Name.
            str_name = i
            # Add item to menu.
            menu.InsData(Item_ID, '&i1050427&'+str_name)
            menu.InsData(0, '')     # Append separator
            # Now Add to ID list to excute the item that been added to the menu lic4d.storage.
            item = str(Item_ID) + "--" + str_name
            LayerIDsItems.append(item)

    # Finally show popup dialog
    result = gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)
    #print result
    for layer_item in LayerIDsItems:
        layer_id = layer_item.split("--")[0]
        layer_name = layer_item.split("--")[1]
        if result == int(layer_id):
            Add_Objet_To_Layer(LAYER_NAME=layer_name)
    if result:
        g_lib.fcsLog.DEBUG("F.C.S Attach 2 Layer is Just Open !", False, g_lib.DebugMode)

    return True