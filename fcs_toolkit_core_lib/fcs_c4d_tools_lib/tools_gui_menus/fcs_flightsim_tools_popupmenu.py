import c4d
import os 
import sys
import math 
import glob 
import time
import datetime
from c4d import plugins, gui, bitmaps, documents, storage, utils
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib

config_data = None
fcsID = g_lib.fcsID
jsonEdit = g_lib.jsonEdit
fs_lib = g_lib.fs_lib
    
def FlightSimTools():
    IDM0 = c4d.FIRST_POPUP_ID

    menu = c4d.BaseContainer()
    fsxtools = [ 1039663, 1039518, 1053986, 1051207 ]
    for tool in fsxtools:
        menu.InsData(0, '') # Append separator
        menu.InsData(tool, "CMD")

    result = gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)

    return True