import c4d
import os 
import sys
import math 
import glob 
import time
import datetime
from c4d import plugins, gui, bitmaps, documents, storage, utils
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
#from ..fcs_c4d_dialogs import fcs_common_global_lib as g_lib

fcsID = g_lib.fcsID
jsonEdit = g_lib.jsonEdit

def FCS_Layouts_Script_PopupMenu(): # FCS Layouts
    IDM1 = c4d.FIRST_POPUP_ID
    IDM2 = c4d.FIRST_POPUP_ID+2
    IDM3= c4d.FIRST_POPUP_ID+6
    IDM4 = c4d.FIRST_POPUP_ID+3
    IDM5 = c4d.FIRST_POPUP_ID+5
    IDM6 = c4d.FIRST_POPUP_ID+5
    M1 = c4d.FIRST_POPUP_ID+4

    menu = c4d.BaseContainer()

    submenu = c4d.BaseContainer()
    submenu.SetString(1, "FCS C4D Layouts")
    submenu.SetString(IDM3, 'FCS Studio Layout')
    submenu.SetString(IDM4, 'FCS UV Editor Layout')
    submenu.SetString(IDM5, 'FCS Scripting Editor Layout')
    submenu.SetString(IDM6, 'FCS Scripting Editor Quick Layout')
    menu.SetContainer(M1, submenu)

    menu.SetString(0, '')
    menu.SetString(IDM2, "Add Viewport Camera Views Palette")

    result = gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)

    #if result==IDM1:
    #    self.OpenLayouts()

    if result==IDM2:
        g_lib.Load_C4D_Preset_File( os.path.join(g_lib.fcsDIR.plugin_c4d_presets_folder, "FCS_Viewport_Views.l4d") )

    if result==IDM3:
        g_lib.Load_C4D_Preset_File( os.path.join(g_lib.fcsDIR.plugin_c4d_presets_folder, "FCS Studio Pro Layout.l4d") )

    if result==IDM4:
        g_lib.Load_C4D_Preset_File( os.path.join(g_lib.fcsDIR.plugin_c4d_presets_folder, "FCS Studio Pro UV Editor Layout.l4d") )

    if result==IDM5:   
        g_lib.Load_C4D_Preset_File( os.path.join(g_lib.fcsDIR.plugin_c4d_presets_folder, "FCS Scripting_Editor_Layout.l4d") )
        
    return True 