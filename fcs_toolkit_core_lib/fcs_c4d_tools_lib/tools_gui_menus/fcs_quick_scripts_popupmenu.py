import c4d
import os 
import sys
import math 
import glob 
import time
import datetime
from c4d import plugins, gui, bitmaps, documents, storage, utils
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib
import fcs_c4d_common_g_lib as g_lib
#from ..fcs_c4d_dialogs import fcs_common_global_lib as g_lib

config_data = None
fcsID = g_lib.fcsID
jsonEdit = g_lib.jsonEdit
fs_lib = g_lib.fs_lib

def FCS_OuickScripts():

    IDM0 = c4d.FIRST_POPUP_ID

    IDM17 = c4d.FIRST_POPUP_ID+17
    IDM18 = c4d.FIRST_POPUP_ID+18
    IDM19 = c4d.FIRST_POPUP_ID+19
    IDM20 = c4d.FIRST_POPUP_ID+20
    IDM21 = c4d.FIRST_POPUP_ID+21
    IDM22 = c4d.FIRST_POPUP_ID+22   
    IDM25 = c4d.FIRST_POPUP_ID+25

    menu = c4d.BaseContainer()
    menu.SetString(IDM0, "Quick Commands Scripts")        #IDS.ID(fcsID.IDS_TOOLNAME_FQS)
    menu.InsData(0, '') # Append separator
    menu.InsData(1036188, "CMD")
    menu.InsData(1038466, "CMD")
    menu.InsData(1036481, "CMD")
    menu.InsData(1038971, "CMD")
    menu.InsData(1038025, "CMD")

    menu.InsData(1039524, "CMD")
    menu.InsData(1038706, "CMD")
    menu.InsData(1039227, "CMD")
    menu.InsData(1038011, "CMD")
    #menu.SetString(IDM6,'&i1038352&'+StudioName+'Freeze Object')
    menu.InsData(1036394, "CMD")
    menu.InsData(1036583, "CMD")
    menu.InsData(1038536, "CMD")
    #menu.SetString(IDM12,'&i1038477&'+StudioName+'Fill UV Poly')
    menu.InsData(1039535, "CMD")
    menu.InsData(0, '') # Append separator
    submenu = c4d.BaseContainer()
    submenu.SetString(1,'&i5166&Add Character for Scaling')
    submenu.SetString(IDM17,'&i5166&Centimeters')
    submenu.SetString(IDM18,'&i5166&Feet')
    submenu.SetString(IDM19,'&i5166&Inches')
    submenu.SetString(IDM20,'&i5166&Meters')
    submenu.SetString(IDM21,'&i5166&Millimetters')
    menu.SetContainer(IDM22, submenu)
    menu.InsData(0, '') # Append separator 
    menu.SetString(IDM25,'&i5181&Add FCS Sim Track Setup')

    result = gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)

    if result==IDM17:
        P_MergeFile = "P_Character_Centimeters.c4d"
        g_lib.Load_Character2Doc_Scene(P_MergeFile, g_lib.fcsDIR.plugin_c4d_sus_presets_folder)
    if result==IDM18:
        P_MergeFile = "P_Character_Feet.c4d"
        g_lib.Load_Character2Doc_Scene(P_MergeFile, g_lib.fcsDIR.plugin_c4d_sus_presets_folder)
    if result==IDM19:
        P_MergeFile = "P_Character_Inches.c4d"
        g_lib.Load_Character2Doc_Scene(P_MergeFile, g_lib.fcsDIR.plugin_c4d_sus_presets_folder)
    if result==IDM20:
        P_MergeFile = "P_Character_Meters.c4d"
        g_lib.Load_Character2Doc_Scene(P_MergeFile, g_lib.fcsDIR.plugin_c4d_sus_presets_folder)
    if result==IDM21:
        P_MergeFile = "P_Character_Millimeters.c4d"
        g_lib.Load_Character2Doc_Scene(P_MergeFile, g_lib.fcsDIR.plugin_c4d_sus_presets_folder)

    if result==IDM25:
        doc = documents.GetActiveDocument()  
        fileName_Path = os.path.join(g_lib.fcsDIR.plugin_c4d_demo_presets_folder, "P_FCS_Track Setuos.path.c4d")
        file_Load = c4d.documents.MergeDocument(doc, fileName_Path, c4d.SCENEFILTER_OBJECTS | c4d.SCENEFILTER_MATERIALS | c4d.SCENEFILTER_MERGESCENE)
        c4d.documents.BaseDocument(file_Load)
        print("---------------------------------------\nFCS Track Setup as been Added!\n---------------------------------------")
        c4d.EventAdd() 
        c4d.CallCommand(13323, 13323) # Select All
        c4d.CallCommand(13324, 13324) # Deselect All                
        c4d.EventAdd()
    return True