"""
Flight Sim SDK Tools Helpers Functions Methods
"""
# ----------------------------------------------------
#               Imports for Cinema 4D
# ----------------------------------------------------
import c4d
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as WindowDialog        # eg:( class My_Dialog(c4d.gui.GeDialog): ) to ( class My_Dialog(WindowDialog): ) Just keeping code line short.
from c4d.plugins import CommandData, TagData, ObjectData
from random import randint
from os import urandom
# XML Imports is for Saving and Loading Project Data for Plugin UI / Xml good but long functions to make.
from xml.dom import minidom
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree
# FCS Imports Modules .py Files.
import fcs_c4d_common_g_lib as g_lib
#from fcs_c4d_common_g_lib import Global_Enums_IDs as fcsID
#jsonEdit = g_lib.FCS_JsonSystem_Editor()
c4d_prefs_folder = c4d.storage.GeGetC4DPath(c4d.C4D_PATH_PREFS) # To find Cinema 4D Appdata Dir's of the Preferences Folder.
FCS_LOGGING_CONIG = os.path.join(c4d_prefs_folder, 'fcs_plugin_prefs', 'FCS_Tools_Logging_Output.txt')
#fcsLog = g_lib.FCS_LOGGER(MainLogFile=FCS_LOGGING_CONIG)
#fcsLog.INFO("Flight Sim Module is Added and Loaded", False)

# Create Flight Sim GUID 
def Get_FS_GUID():
    """ Generate Flight Sim and Set GUID To the Edit String Box. """
    b = bytearray(urandom(16))
    b[6] = (b[6]&0xf)|0x40 # indicate that was generated at random  
    GUID = '%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x' % tuple(b)
    return GUID   

# Getting and Adding each effect to .X file.
def Adding_Fx_To_Xfile(E, fxfile):
    """ Getting Effect Tag and Adding each effect to .X file """
    doc = c4d.documents.GetActiveDocument()
    effectObjname = E.GetName()
    #fcsLog.DEBUG(effectObjname, False)
    # // Get Object FS Fx Tag //
    UTag = E.GetTag(1040639)
    if UTag:
        # Get XML Data from Fx Tag.
        FS_fxXml = UTag[c4d.FSFx_XML_FIELD]
        # Get X, Y, Z Postion Coor.. Data from Fx Tag.
        X = UTag[c4d.FSFx_GLOBAL_X]
        Y = UTag[c4d.FSFx_GLOBAL_Y] 
        Z = UTag[c4d.FSFx_GLOBAL_Z]

        #fcsLog.DEBUG(FS_fxXml + "\n" + X + "\n" + Y + "\n" + Z, False)
        #fcsLog.DEBUG("============= FSX Eeffect Tag  ================", False)
        
        # // Effect Maker //
        PartData = FS_fxXml
        with open(fxfile,'a') as WritePartFx:
            WritePartFx.write("\n  Frame frm-" + effectObjname + "{\n")
            WritePartFx.write("  PartData {\n")
            WritePartFx.write("  " + "%i;\n" %(len(PartData)+1))
            for i, char in enumerate(PartData):
                if i%10 == 0:
                    WritePartFx.write("%i, " %(ord(char)))
                else:
                    WritePartFx.write("%i, " %(ord(char)))
                if i%10 == 9:
                    WritePartFx.write("\n")
            WritePartFx.write("0;\n" + "   }  // End PartData\n")
            WritePartFx.write("\n  FrameTransformMatrix{\n")
            WritePartFx.write("     1.000000, 0.000000, 0.000000, 0.000000,\n")
            WritePartFx.write("     0.000000, 0.000000, 1.000000, 0.000000,\n")
            WritePartFx.write("     0.000000, 1.000000, 0.000000, 0.000000,\n")
            WritePartFx.write("     "+ X + "," + Z + "," + Y + ", 1.000000;;")
            WritePartFx.write("\n   }")    
            WritePartFx.write("\n   } // End of frame frm-"+ effectObjname + "\n")
    c4d.EventAdd()
    return True


class FlightSimLib(object):
        Templates ="""template GuidToName {
    <7419dfe5-b73a-4d66-98d8-c082591dc9e7>
    STRING Guid;
    STRING Name;
}

template ShadowMapReady {
    <2F4F86A9-FE94-4f75-AA1B-299BBD98907B>
    Boolean SMReady;
}

template PartData {
    <79B183BA-7E70-44d1-914A-23B304CA91E5>
    DWORD nByteCount;
    array BYTE XMLData[ nByteCount ];
}

"""
        Matix ="""Frame frm-MasterScale {
FrameTransformMatrix {
    0.000977, 0.0, 0.0, 0.0,
    0.0, 0.000977, 0.0, 0.0,
    0.0, 0.0, 0.000977, 0.0,
    0.0, 0.0, 0.0, 1.0;;
}  // End frm-MasterScale FrameTransformMatrix

Frame frm-MasterUnitConversion {
FrameTransformMatrix {
    1024.000000, 0.0, 0.0, 0.0,
    0.0, 1024.000000, 0.0, 0.0,
    0.0, 0.0, 1024.000000, 0.0,
    0.0, 0.0, 0.0, 1.0;;
}  // End frm-MasterUnitConversion FrameTransformMatrix

Frame RotateAroundX {
FrameTransformMatrix {
    1.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 1.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 1.0;;
} // End Frame RotateAroundX FrameTransformMatrix """
