"""
FCS Station Engine Managerment System 
"""

import c4d
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
import weakref
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as WindowDialog        # eg:( class My_Dialog(c4d.gui.GeDialog): ) to ( class My_Dialog(WindowDialog): ) Just keeping code line short.
from c4d.plugins import CommandData, TagData, ObjectData
from random import randint
from os import urandom
# Regex Import
import re
# XML Imports is for Saving and Loading Project Data for Plugin UI / Xml good but long functions to make.
from xml.dom import minidom
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree
# Json Import to store or load descriptions or any data / .json is easy and alot flexable and short coding.
import json
# Python Plugin System Load Modules Dynamically With importlib.
import importlib

import fcs_c4d_common_g_lib as g_lib
fcsID = g_lib.fcsID 
jsonEdit = g_lib.jsonEdit

def RUN_FCS_STATION_APP(RunFS_fx, RunC4D_MCX, RunX_File, Data):
    # CHECK OPERATING SYSTEM
    if (c4d.GeGetCurrentOS() == c4d.OPERATINGSYSTEM_WIN):
        # Saving Data to XML File.
        MakeXml_root = minidom.Document()
        AddToXML = MakeXml_root.createElement('FCSStation')
        AddToXML.setAttribute('FCS_STATION_DEBUG_MODE', "True")
        MakeXml_root.appendChild(AddToXML)
        # Get FS FX's
        if (RunFS_fx == True):
            XML_DATA = MakeXml_root.createElement('FX')
            XML_DATA.setAttribute('Run', "Yes")
            AddToXML.appendChild(XML_DATA)
        # C4D to MCX
        if (RunC4D_MCX == True):
            XML_DATA = MakeXml_root.createElement('MCX')
            XML_DATA.setAttribute('ModelName', Data['ModelName'])
            XML_DATA.setAttribute('MCXPath', Data['MCXPath'])
            XML_DATA.setAttribute('ModelPath', Data['ModelPath'])
            AddToXML.appendChild(XML_DATA)
        # Creating X Effect File
        if (RunX_File == True):
            XML_DATA = MakeXml_root.createElement('xGen')
            XML_DATA.setAttribute('XName', Data['XName'])
            XML_DATA.setAttribute('MCXPath', Data['MCXPath'])
            XML_DATA.setAttribute('ExportPath', Data['ExportPath'])
            XML_DATA.setAttribute('ObjectPath', Data['ObjectPath'])
            AddToXML.appendChild(XML_DATA)
            g_lib.fcsLog.MODEL("FCS ToolBox ran MCX with the file " + Data['XName'] + " Successfully", False, os.path.join(Data['ExportPath'],Data['XName']))


        # Saving XML 
        SaveXml = MakeXml_root.toprettyxml(indent="\t")
        with open(g_lib.fcsDIR.fcs_station_xml_file, 'w') as f:
            f.write(SaveXml)
            f.close()

        ######################################
        # Execute FCS Station Engine
        ######################################
        APP_Name = "FCSStation.exe"
        APP_Path = os.path.join(g_lib.fcsDIR.plugin_Station_folder, APP_Name)

        print ("Attempting to execute FCSStation")
        result = c4d.storage.GeExecuteProgram(APP_Path, g_lib.fcsDIR.plugin_Station_folder)
        if result:
            g_lib.fcsLog.INFO("FCSStation executed successfully", False)
        else:
            g_lib.fcsLog.ERROR("FCSStation failed to execute", False)
    else:
        print ("FCSStation is found, but only PC.")
        pass
    return True
def RUN_FCS_STATION_LOGGER_APP():
    """ Run FCS Station Log """

    APP_Name = "FCSStationLog.exe"
    APP_Path = os.path.join(g_lib.fcsDIR.plugin_Station_folder, APP_Name)

    print ("Attempting to execute Log")
    result = c4d.storage.GeExecuteProgram(APP_Path, g_lib.fcsDIR.plugin_Station_folder)
    if result:
        print ("FCSStation Log executed successfully")
    else:
        print ("FCSStation Log failed to execute")    
    return True

    
def ToggleStationSetting(State):
    """ Toggles a setting in the FCSStation config.xml """

    #Parse the xml file and get the data (Creates an object of the xml file to minipulate)
    tree = etree.parse(g_lib.fcsDIR.plugin_FCSStation_Config_file)
   
    #Now Edit the xml attribute by providing the tree elementName, attribute value and the new value in a string
    EditXmlAttribute(tree,"Setting", "ExportXFileToMCX", str(State))

    #Now lets get the root of the xml
    root = tree.getroot()

    #Fix the xml structure from the root level
    indent(root)

    # and finally save the xml to the file
    with open(g_lib.fcsDIR.plugin_FCSStation_Config_file, 'w') as f:
        f.write(etree.tostring(root))
    return True
def EditXmlAttribute(tree, elementName, attributeName, newValue):
    """ A Function that takes a tree of an xml and finds he element and edits the attribute value. """
    #Lets find all elements with the same name
    sh = tree.findall(elementName)
    #print(attributeName)
    #print(elementName)
    #For each element...
    for element in sh:
        #Let get all the attributes
        for attribute in element.attrib:
            #if the attribute name matches the provided attribute name
            if attribute == attributeName:
                #print("===============")               
                #print(attributeName)
                #print("===============") 
                #lets set the value of that attribute to the new value
                element.set(attributeName, newValue)
    return
def indent(elem, level=0, more_sibs=False):
    """ A function that indents an xml root. """
    i = "\n"
    if level:
        i += (level-1) * '  '
    num_kids = len(elem)
    if num_kids:
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
            if level:
                elem.text += '  '
        count = 0
        for kid in elem:
            indent(kid, level+1, count < num_kids - 1)
            count += 1
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
            if more_sibs:
                elem.tail += '  '
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i
            if more_sibs:
                elem.tail += '  '
    return

class LogType:
    """ A class that defines a LogType (To be used as a ENUM) """
    InfoLog = "Info"
    ModelExportLog = "ModelExport"
    ErrorLog = "Error"
    WarningLog ="Warning"
    CriticalLog ="Critical"
    DebugLog = "Debug"
    NoneLog = "None"

def LogMessage (message, logType, isObjectModel = "False", modelLocation = ""):
    """ A function that logs a message to the FCSStation and prints the message to the console. """

    tree = etree.parse(g_lib.fcsDIR.plugin_FCSStation_Log_file)
    root = tree.getroot()
    
    now = datetime.datetime.now()
    newMessage = etree.Element("Log", Message = message, DateTime = str(now), isObject = isObjectModel, ModelLocation = modelLocation, LogType = logType)

    root.append(newMessage)
    indent(root)
    
    with open(g_lib.fcsDIR.plugin_FCSStation_Log_file, 'w') as f:
        f.write(etree.tostring(root))

    return


