
import os
import sys
import subprocess
import webbrowser
import collections
import math
import random
import urllib
import glob
import shutil
import time
import datetime
from random import randint
# Cinema 4D Imports
import c4d
from c4d import plugins, gui, bitmaps, documents, storage, utils
# Regex Import
import re
# Json Import to store or load descriptions or any data / .json is easy and alot flexable and short coding.
import json


# ---------------------------------------------------------------------
#                FCS .JSON SYSTEM EDITOR
# ---------------------------------------------------------------------
class FCS_JsonSystem_Editor():

    """
    FCS Json Editor System
    ``Eg:``
    jsonEdit = g_lib.FCS_JsonSystem_Editor()
    jsonEdit.GetItemProperties(json_file=FILE, main_property="TOOLS", itemData"Tool1")
    """

    def SaveFile(self, json_file, saveData):
        """ Save and Create .Json File. """
        try:
            with open(json_file,'w') as f:
                json.dump(saveData, f, indent=2, sort_keys=True)
        except Exception as e:
            gui.MessageDialog(e)
            return False
        return True

    def LoadFile(self, json_file):
        """ Load and Read .json file. """
        try:
            with open(json_file, 'r') as Jfile:
                return json.load(Jfile)
        except Exception as e:

            return None

        return True

    def PrintAllObjects(self, json_file):
        """ Get all Items .json file. """
        print("======| Main Items |======")
        jsonFileData = self.LoadFile(json_file)

        if jsonFileData == None:
            return False

        for i in jsonFileData:
            print(i)
        return True


    def GetItemProperty(self, json_file, main_property):
        """ Getting the main item property values. """
        jsonFileData = self.LoadFile(json_file)

        if jsonFileData == None:
            return None

        try:
            if jsonFileData[main_property]:
                print(jsonFileData[main_property])
                #gui.MessageDialog(jsonFileData[main_property])
                return jsonFileData[main_property]
        except KeyError:
            return gui.MessageDialog("Key doesn't exist ({0})".format(main_property))
        return True

    def GetItemProperties(self, json_file, main_property, itemData):
        """ Getting the child item property values of a main item property. """
        jsonFileData = self.LoadFile(json_file)

        if jsonFileData == None:
            return None

        for key, value in jsonFileData.iteritems():
            try:
                if key == main_property:
                    #print(value[itemData])
                    #gui.MessageDialog(value[itemData])
                    return value[itemData]
            except KeyError:
                return #print "Key doesn't exist"
        return True

    def GetItem(self, json_file, childPropertyName):
            """ Getting the child item property values of a main item property. """

            try:
                data = None

                jsonFileData = self.LoadFile(json_file)

                if jsonFileData == None:
                    return None

                if childPropertyName in jsonFileData:
                    data = jsonFileData[childPropertyName]

                if data == None:
                    print(type(jsonFileData))

                    if isinstance(jsonFileData, dict):
                        data = self.FindPropertyInDict(childPropertyName, jsonFileData)

                return data

            except KeyError as ke:
                print(ke)
                return False

            except Exception as e:
                print(e)
                return False
            return True

    def FindPropertyInDict(self, propertyName, jsonFileData):
        for key, value in jsonFileData.iteritems():
            if(key == propertyName):
                return jsonFileData

            if isinstance(value, dict):
                result = self.FindPropertyInDict(propertyName, value)
                return result
        return True

    def ArgumentFix(self, json):
        return json.replace("\"", "**",)

    def ConvertToJson(self, c4dData):
        jsonFile = ArgumentFix(json.dumps(c4dData.__dict__))
        return jsonFile

    def GetItemArrayOfProperties(self, main_property, json_file):
        print("======| "+ main_property +" All Items Names |======")
        jsonFileData = self.LoadFile(json_file)
        for stores in jsonFileData[main_property]:
         for item in stores:
           # Print my results
            print(item)
        return True

    def EditItemProperty(self, json_file, main_property, new_value):
        """ Editing a main property value. """
        jsonFileData = self.LoadFile(json_file)
        jsonFileData[main_property] = new_value
        self.SaveFile(json_file, jsonFileData)
        return True

    def EditItemProperties(self, json_file, main_property, child_property, new_value):
        """ Editing a properties child values. """
        jsonFileData = self.LoadFile(json_file)
        data = jsonFileData[main_property]
        data[child_property] = new_value
        self.SaveFile(json_file, jsonFileData)
        return True

    def DeletePropertyItem(self, json_file, property_name):
        """  """
        jsonFileData = self.LoadFile(json_file)
        try:
            if jsonFileData[property_name]:
                del jsonFileData[property_name]
        except KeyError:
            return #print "Key doesn't exist"
        self.SaveFile(json_file, jsonFileData)
        return True

    def DeleteChildPropertyItem(self, json_file, main_property, property_name):
        """  """
        jsonFileData = self.LoadFile(json_file)
        data = jsonFileData[main_property]
        try:
            if data[property_name]:
                del data[property_name]
        except KeyError:
            return #print "Key doesn't exist"
        self.SaveFile(json_file, jsonFileData)
        return True