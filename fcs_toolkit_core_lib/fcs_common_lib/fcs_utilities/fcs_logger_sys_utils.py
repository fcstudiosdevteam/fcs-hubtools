"""
FCS Logger Class System
""" 
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
from random import randint
# Cinema 4D Imports
import c4d
from c4d import plugins, gui, bitmaps, documents, storage, utils
# Regex Import
import re
# XML Imports is for Saving and Loading Project Data for Plugin UI / Xml good but long functions to make.
from xml.dom import minidom
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree
# Json Import to store or load descriptions or any data / .json is easy and alot flexable and short coding.
import json
# Python Plugin System Load Modules Dynamically With importlib.
import importlib
# Date System
DateT = str(datetime.date.today())

import fcs_c4d_common_g_lib as g_lib


# ---------------------------------------------------------------------
#     FCS Logger System  
# ---------------------------------------------------------------------
class FCS_LOGGER(object):
    """ 
    FCS Logging System 

    ``About:``
    Printing messages to Cinema 4D Console and to a file, that shows errors.
    eg: ( CRITICAL, DEBUG, ERROR, INFO, WARNING) info.

    ``Use Code Example:``

    log = FCS_LOGGER(MainLogFile=``FILEPATH``)

        if CHK == True:
            log.ERROR(message=``["HelloWorld! and How are you?"]``, gui_popup=``[True]``)
    """
    # The __init__ is an Constuctor and help get and passes data on from the another class.
    def __init__(self, MainLogFile):
        self.logfile = MainLogFile

    def PrintToCinema4D(self, message, ActiveDebugMode):
        if ActiveDebugMode:
            print(message) # Print to  Cinema 4D
        else:
            pass
        return True

    def PopupMessageBoxDialog(self, message, popup):
        if popup == True:
            gui.MessageDialog(message)
        else:
            pass        
        return True

    def Log_Data_File(self, result):
        DATA = result
        with open(self.logfile, 'a') as f:
            f.write(DATA)
            f.close()         
        return True

    def CRITICAL(self, message, gui_popup, ActiveDebugMode = True):
        """ A serious error, indicating that the program itself may be unable to continue running. """
        mess_data = "CRITICAL!: "+ message
        data_result = DateT + " | "+ mess_data +'\n'
        self.Log_Data_File(data_result)
        self.PrintToCinema4D(data_result, ActiveDebugMode)
        self.PopupMessageBoxDialog(message=mess_data, popup=gui_popup)
        g_lib.fcsEngine.LogMessage(message,g_lib.fcsEngine.LogType.CriticalLog)
        return True

    def DEBUG(self, message, gui_popup, ActiveDebugMode = True):
        """ Detailed information, typically of interest only when diagnosing problems. """
        mess_data = "DEBUG!: "+ message
        data_result = DateT + " | "+ mess_data +'\n'
        self.Log_Data_File(data_result)
        self.PopupMessageBoxDialog(message=mess_data, popup=gui_popup)
        self.PrintToCinema4D(data_result, ActiveDebugMode)
        g_lib.fcsEngine.LogMessage(message,g_lib.fcsEngine.LogType.DebugLog)
        return True

    def ERROR(self, message, gui_popup, ActiveDebugMode = True):
        """ Due to a more serious problem, the software has not been able to perform some function. """
        mess_data = "ERROR!: "+ message
        data_result = DateT + " | "+ mess_data +'\n'
        self.Log_Data_File(data_result)     
        self.PrintToCinema4D(data_result, ActiveDebugMode)
        self.PopupMessageBoxDialog(message=mess_data, popup=gui_popup)
        g_lib.fcsEngine.LogMessage(message,g_lib.fcsEngine.LogType.ErrorLog)
        return True

    def INFO(self, message, gui_popup, ActiveDebugMode = True):
        """ Confirmation that things are working as expected. """
        mess_data = "INFO!: "+ message
        data_result = DateT + " | "+ mess_data +'\n'
        self.Log_Data_File(data_result)      
        #self.PrintToCinema4D(data_result, )
        print(data_result)
        self.PopupMessageBoxDialog(message=mess_data, popup=gui_popup)
        g_lib.fcsEngine.LogMessage(message,g_lib.fcsEngine.LogType.InfoLog)
        return True

    def WARNING(self, message, gui_popup, ActiveDebugMode = True):
        """ An indication that things something unexpected happened, or indicative of some problem in the near future (e.g. 'disk space low'). The software is still working as expected. """
        mess_data = "WARNING!: "+ message
        data_result = DateT + " | "+ mess_data +'\n'
        self.Log_Data_File(data_result)
        self.PrintToCinema4D(data_result, ActiveDebugMode)
        self.PopupMessageBoxDialog(message=mess_data, popup=gui_popup)
        g_lib.fcsEngine.LogMessage(message,g_lib.fcsEngine.LogType.WarningLog)
        return True

    def MODEL(self, message, gui_popup, modelLocation , ActiveDebugMode = True):
        """ An export of a model """
        mess_data = "MODEL EXPORT!: "+ message
        data_result = DateT + " | "+ mess_data +'\n'
        self.Log_Data_File(data_result)
        self.PrintToCinema4D(data_result, ActiveDebugMode)
        self.PopupMessageBoxDialog(message=mess_data, popup=gui_popup)
        g_lib.fcsEngine.LogMessage(message,g_lib.fcsEngine.LogType.ModelExportLog, "True", modelLocation)
        return True
