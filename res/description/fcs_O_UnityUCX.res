CONTAINER fcs_O_UnityUCX  // Like the filename without ".res"
{
	NAME ID_OBJECT_INS_NAME;  // Name of object or tag etc.. for the parameters.

	// [INCLUDE] Base description for the node type plugin, 
	// For example: 
	// Tbase for tags. 
	// Obase for Objects.
	// Xbase for shader.
	// Mpreview for material preview.	
	// ToolBase
	
	INCLUDE Obase;

	GROUP ID_UNITY_UCX_OBJECT_GRP1   // The Group Id.
	{

		// Group [flags] Settings
		COLUMNS 1;	

		LONG ID_COLLISON_TYPE_COMBOBOX
		{
			CYCLE
			{
				ID_COLLISON_TYPE_BOX;
				ID_COLLISON_TYPE_BOX_TIGGER;
				ID_COLLISON_TYPE_MESH;
				ID_COLLISON_TYPE_MESH_TIGGER;
			}
		}

		GROUP ID_COLOR_PICKER_GRP
		{
			COLUMNS 1;
			COLOR ID_COLOR_PICKER_COL1 { }
			COLOR ID_COLOR_PICKER_COL2 { }
			COLOR ID_COLOR_PICKER_COL3 { }
			BUTTON ID_COLOR_PICKER_RESET { }
		}

		GROUP ID_GRP_CONTROLLERS
		{
			COLUMNS 2;

			REAL ID_UNITY_UCX_FRONT_CTRL		{ UNIT METER;  }
			REAL ID_UNITY_UCX_BACK_CTRL			{ UNIT METER;  }
			REAL ID_UNITY_UCX_LEFT_CTRL			{ UNIT METER;  }
			REAL ID_UNITY_UCX_RIGHT_CTRL		{ UNIT METER;  }
			REAL ID_UNITY_UCX_TOP_CTRL			{ UNIT METER;  }
			REAL ID_UNITY_UCX_BOTTOM_CTRL		{ UNIT METER;  }
		}


		GROUP ID_GRP_SYSTEM_PARENT
		{
			STRING ID_SYSTEM_PARENT
			{
				ANIM ON;
			}       		
		}

	}

}