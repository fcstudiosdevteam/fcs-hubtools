#ifndef _fcs_Oplts_connector_H_
#define _fcs_Oplts_connector_H_

enum
{
   ID_CONNECTOR_GRP1 = 1000,
   ID_CONNECTOR_LK1A = 1001,
   ID_CONNECTOR_LK2B = 1002,
   ID_GRP_SYSTEM_PARENT = 1005,
   ID_SYSTEM_PARENT = 1006,
};
#endif
