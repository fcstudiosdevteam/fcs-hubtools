CONTAINER fcs_Oplts_controller  // Like the filename without ".res"
{
	NAME ID_CONNECTOR_NAME;  // Name of object or tag etc.. for the parameters.

	// [INCLUDE] Base description for the node type plugin, 
	// For example: 
	// Tbase for tags. 
	// Obase for Objects.
	// Xbase for shader.
	// Mpreview for material preview.	
	// ToolBase
	
	INCLUDE Obase;

	GROUP ID_CONNECTOR_GRP1   // The Group Id.
	{
		// Group [flags] Settings
		COLUMNS 1;


        LINK ID_CONNECTOR_LK1A
        {
            ANIM ON;
            ACCEPT { Obase; }
            REFUSE { Osky; Oforeground; Tbase; }            

        }

        LINK ID_CONNECTOR_LK2B
        {
            ANIM ON;
            ACCEPT { Obase; }
            REFUSE { Osky; Oforeground; Tbase; }            

        } 

		BOOL ID_CONTROLLER_CAL
		{
			ANIM ON;
		}

	}

	GROUP ID_GRP_SYSTEM_PARENT
	{
        STRING ID_SYSTEM_PARENT
        {
            ANIM ON;
        }       		
	}

}