CONTAINER fcs_Oplts_object_ins  // Like the filename without ".res"
{
	NAME ID_OBJECT_INS_NAME;  // Name of object or tag etc.. for the parameters.

	// [INCLUDE] Base description for the node type plugin, 
	// For example: 
	// Tbase for tags. 
	// Obase for Objects.
	// Xbase for shader.
	// Mpreview for material preview.	
	// ToolBase
	
	INCLUDE Obase;

	GROUP ID_OBJECT_INS_GRP1   // The Group Id.
	{
		// Group [flags] Settings
		COLUMNS 1;


        LINK ID_OBJECT_INS_LINK
        {
            ANIM ON;
            ACCEPT { Obase; }
            REFUSE { Osky; Oforeground; Tbase; }
        }
	}

	GROUP ID_GRP_SYSTEM_PARENT
	{
        STRING ID_SYSTEM_PARENT
        {
            ANIM ON;
        }       		
	}
}