CONTAINER fcs_Oplts_spline_sys  // Like the filename without ".res"
{
	NAME SPLINE_NAME;  // Name of object or tag etc.. for the parameters.

	// [INCLUDE] Base description for the node type plugin, 
	// For example: 
	// Tbase for tags. 
	// Obase for Objects.
	// Xbase for shader.
	// Mpreview for material preview.	
	// ToolBase
	
	INCLUDE Obase;

	GROUP SPLINE_GRP1   // The Group Id.
	{
		// Group [flags] Settings
		COLUMNS 1;


        STRING SPLINE_PARENT
        {
            ANIM ON;
        }

	}
}