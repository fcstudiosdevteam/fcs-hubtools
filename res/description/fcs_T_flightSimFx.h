#ifndef _fcs_T_flightSimFx_H_
#define _fcs_T_flightSimFx_H_

enum
{
   FSFx_XML_GROUP = 1000,
   FSFx_XML = 1001,
   FSFx_XML_FIELD = 1002,
   FSFx_GLOBAL_PROPERTIES_GROUP = 1003,
   FSFx_GLOBAL_X = 1004,
   FSFx_GLOBAL_Y = 1005,
   FSFx_GLOBAL_Z = 1006,
};
#endif
