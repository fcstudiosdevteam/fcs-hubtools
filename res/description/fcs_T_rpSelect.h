#ifndef _fcs_T_rpSelect_H_
#define _fcs_T_rpSelect_H_

enum
{
   FCS_RP_TAG_GRP_SETTINGS = 1000,
   FCS_RP_TAG_ITEMTYPE = 1001,
   FCS_RP_TAG_COLOR_1 = 1002,
};
#endif
