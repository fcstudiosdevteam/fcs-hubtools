#ifndef _fcs_T_rpTemp_H_
#define _fcs_T_rpTemp_H_

enum
{
   FCS_RP_TEMP_TAG_GRP_SETTINGS = 1000,
   FCS_RP_TEMP_TAG_ITEMTYPE = 1001,
   FCS_RP_TEMP_TAG_COLOR_1 = 1002,
};
#endif
