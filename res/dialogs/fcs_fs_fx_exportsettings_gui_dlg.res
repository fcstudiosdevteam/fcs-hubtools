DIALOG fcs_fs_fx_exportsettings_gui_dlg
{

    NAME ID_FS_SDK_DIALOG_TITLE;
    SCALE_H;

    GROUP ID_FS_OVERALL_GRP
    {
        SCALE_H;
        COLUMNS 1;

        GROUP ID_FS_EXPORTER_GRP_PANEL
        {
            COLUMNS 1;
            BORDERSIZE 5,5,4,4;
            FIT_H;
        }
        GROUP ID_FS_QUICKTABHEADER_GRP1
        {
            SCALE_H;
            COLUMNS 1;
        }

        GROUP ID_FS_FRIENDLY_GRP
        {
            FIT_H;
            COLUMNS 2;

            STATICTEXT ID_FS_FRIENDLY_TXT
            {
                CENTER_H;
                SIZE 0,10;
                NAME ID_FS_FRIENDLY_TXT;
            }

            EDITTEXT ID_FS_FRIENDLY_EDIT
            {
                SIZE 250,10;
                SCALE_H;
            }
        }

        GROUP ID_FS_GUID_GRP
        {
            FIT_H;
            COLUMNS 3;

            STATICTEXT ID_FS_GUID_TXT
            {
                CENTER_H;
                SIZE 0,10;
                NAME ID_FS_GUID_TXT;
            }

            EDITTEXT ID_FS_GUID_EDIT
            {
                SIZE 250,10;
                SCALE_H;
            }

            BUTTON ID_FS_GUID_BTN
            {
                NAME ID_FS_GUID_BTN;
                SIZE 0,10;
            }

        }


        GROUP ID_FS_QUICKTABHEADER_GRP2
        {
            SCALE_H;
            COLUMNS 1;
        }

        CHECKBOX ID_FS_EXPORT_TO_MCX_CHK
        {
            NAME ID_FS_EXPORT_TO_MCX_CHK;
        }


        GROUP ID_FS_FILENAME_GRP
        {
            FIT_H;
            COLUMNS 2;

            STATICTEXT ID_FS_FILENAME_TXT
            {
                CENTER_H;
                SIZE 0,10;
                NAME ID_FS_FILENAME_TXT;
            }

            EDITTEXT ID_FS_FILENAME_EDIT
            {
                SIZE 250,10;
                SCALE_H;
            }

        }


        GROUP ID_FS_FILE_PATH_GRP
        {
            FIT_H;
            COLUMNS 3;
            
            STATICTEXT ID_FS_FILE_PATH_TXT
            {
                CENTER_H;
                SIZE 0,10;
                NAME ID_FS_FILE_PATH_TXT;
            }

            EDITTEXT ID_FS_FILE_PATH_EDIT
            {
                SIZE 250,10;
                SCALE_H;
            }

            BUTTON ID_FS_FILE_PATH_BTN
            {
                NAME ID_FS_FILE_PATH_BTN;
                SIZE 0,10;
            }

        }

        BUTTON ID_FS_MODEL_EXPORT_BTN
        {
            FIT_H;
            SIZE 0,10;
            NAME ID_FS_MODEL_EXPORT_BTN;
        }

    }
}